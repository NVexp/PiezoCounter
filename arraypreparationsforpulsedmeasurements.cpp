#include "arraypreparationsforpulsedmeasurements.h"

#include <iostream>
#include <QMessageBox>

#include "m8195adev.h"
#include "m2icard.h"

// Delay time for instruments (including cables, fiberws etc.) in ns
static const int delayLaserEnableRising_ns = 300;
static const int delayLaserEnableFalling_ns = 200;
static const int delayLaserDigital_ns = 20;
static const int delayAWG_ns = 635;
static const int sechCutoff = 10;
static const int waitingTimeAfterPolarization = 600;


arrayPreparationsForPulsedMeasurements::arrayPreparationsForPulsedMeasurements()
{

}

//#############################################################
//###################### cw ODMR: Start #######################
//#############################################################

void arrayPreparationsForPulsedMeasurements::prepareCwODMR(int loopNb, double excitTime, double freqStart, double freqEnd, int stepNb, M2iCard* M2i7005_var, M8195Adev* M8195A_var)
{
    int actChN = next_power_of_2(6);
    std::cout << next_power_of_2(6) << " activated channels" << std::endl;

    int64 mask;

    switch(actChN)
    {
        case 1: mask = (int64)1; break;
        case 2: mask = (int64)0x3; break;
        case 4: mask = (int64)0xF; break;
        case 8: mask = (int64)0xFF; break;
        case 16: mask = (int64)0xFFFF; break;
        default: std::cout << "Error: Channel mask is not allowed !" << std::endl; return;
    }

    int frequency_MHz = (int)(1000./1000);
    //int length_in_samples = lround(10047) + 1;  // ~10.047 µs

    int numbOfSteps = (excitTime+10)*stepNb;
    int length_in_samples;
    if(numbOfSteps%64==0){
        length_in_samples=numbOfSteps;
    }else{
        length_in_samples=64*(numbOfSteps/64+1);
    }

    std::cout << "loop_np: " << loopNb << ", frequency_MHz: " << frequency_MHz << " length_in_samples: " << length_in_samples << std::endl;

    M2i7005_var->M2iSetup_StdReplay(mask, frequency_MHz, false, false, loopNb, length_in_samples);
    //######### End: on_confM2i_clicked ###########

    std::vector< std::vector<int>> risingEdges;
    std::vector< std::vector<int>> fallingEdges;
    createCwODMRSequence(risingEdges, fallingEdges, stepNb, excitTime);
    M2i7005_var->M2iLoadIntoMemoryFromVec(6, risingEdges, fallingEdges);

    createCwODMRFrequencies(loopNb, excitTime, freqStart, freqEnd, stepNb, M8195A_var);

    emit uploadIsDone("ODMR");
}

void arrayPreparationsForPulsedMeasurements::createCwODMRSequence(std::vector<std::vector<int>> &risingEdges,  std::vector<std::vector<int>> &fallingEdges, int stepNb, double excitTime)
{
    std::vector<int> rising1Temp;
    std::vector<int> falling1Temp;
    std::vector<int> rising2Temp;
    std::vector<int> falling2Temp;
    std::vector<int> risingLaserEnabled;
    std::vector<int> fallingLaserEnabled;
    std::vector<int> risingLaserDigital;
    std::vector<int> fallingLaserDigital;

    for(int i = 0; i<stepNb; ++i){
        {
            rising1Temp.push_back(i*excitTime);
            falling1Temp.push_back(i*excitTime+10);

            risingLaserEnabled.push_back(i*excitTime);
            risingLaserDigital.push_back(i*excitTime);
            fallingLaserEnabled.push_back((i+1)*excitTime-5);
            fallingLaserDigital.push_back((i+1)*excitTime-5);
        }
        rising2Temp.push_back(i*excitTime+10);
        falling2Temp.push_back(i*excitTime+excitTime-10);
    }
    risingEdges.push_back({0});
    fallingEdges.push_back({10});
    risingEdges.push_back(rising1Temp);
    fallingEdges.push_back(falling1Temp);
    risingEdges.push_back(rising2Temp);
    fallingEdges.push_back(falling2Temp);
    risingEdges.push_back(risingLaserEnabled);
    fallingEdges.push_back(fallingLaserEnabled);
    risingEdges.push_back(risingLaserDigital);
    fallingEdges.push_back(fallingLaserDigital);
    risingEdges.push_back({0});
    fallingEdges.push_back({10});
}

void arrayPreparationsForPulsedMeasurements::createCwODMRFrequencies(int loopNb, double excitTime, double freqStart, double freqEnd, int stepNb, M8195Adev* M8195A_var)
{
    ViConstString chan = "1";

    auto frequencies = linearFreqArray(freqStart*MHz_M8195A, freqEnd*MHz_M8195A, stepNb);

    std::vector<double> times (frequencies.size(), excitTime*microsec_M8195A);
    std::vector<double> phases (frequencies.size(), 0);
    std::vector<double> amplitudes (frequencies.size(), 1);

    ViStatus status = M8195A_var->loadSineArrayWaveformAndSeqTable(chan, frequencies, maxSampleRate, times, loopNb, phases, amplitudes);
}
//#############################################################
//####################### cw ODMR: End ########################
//#############################################################



//#############################################################
//######################## RABI: Start ########################
//#############################################################

int arrayPreparationsForPulsedMeasurements::prepareRabi(double freq, int MWsteps, double MWstepsize, double timeCounter, double timeLaser, int loops, double rabi0, double amp, M2iCard* M2i7005_var, M8195Adev* M8195A_var)
{
    int actChN = next_power_of_2(6);
    std::cout << next_power_of_2(6) << " activated channels" << std::endl;

    int64 mask;

    switch(actChN)
    {
        case 1: mask = (int64)1; break;
        case 2: mask = (int64)0x3; break;
        case 4: mask = (int64)0xF; break;
        case 8: mask = (int64)0xFF; break;
        case 16: mask = (int64)0xFFFF; break;
        default: std::cout << "Error: Channel mask is not allowed !" << std::endl; return 0;
    }


    std::vector<std::vector<int>> risingEdges;
    std::vector<std::vector<int>> fallingEdges;
    int numbOfSteps = createRabiSequence(risingEdges, fallingEdges, MWsteps, MWstepsize, timeCounter, timeLaser, rabi0);
    int length_in_samples;

    if(numbOfSteps%64==0){
        length_in_samples=numbOfSteps;
    }else{
        length_in_samples=64*(numbOfSteps/64+1);
    }
    M2i7005_var->M2iSetup_StdReplay(mask, 100, false, false, loops, length_in_samples);
    M2i7005_var->M2iLoadIntoMemoryFromVec(6, risingEdges, fallingEdges);

    return length_in_samples;

    // IO-card and AWG needs to be prepared separately
    //createRabiFrequencies(freq, MWsteps, MWstepsize, loops, amp, M8195A_var);
}

int arrayPreparationsForPulsedMeasurements::createRabiSequence(std::vector<std::vector<int>>& risingEdges, std::vector<std::vector<int>>& fallingEdges, int MWsteps, double MWstepsize, double timeCounter, double timeLaser, double rabi0)
{
    std::vector<int> risingAWGTrigger;
    std::vector<int> fallingAWGTrigger;
    std::vector<int> risingAWGEvent;
    std::vector<int> fallingAWGEvent;
    std::vector<int> risingCounter;
    std::vector<int> fallingCounter;
    std::vector<int> risingLaserDigital;
    std::vector<int> fallingLaserDigital;
    std::vector<int> risingLaserEnable;
    std::vector<int> fallingLaserEnable;

    int TimeLaser=timeLaser;
    int Steps = MWsteps;
    int Stepsize = MWstepsize;
    int TimeCounter = timeCounter;
    int WaitBetweenMWAndReadout = rabi0;

    int TimePrevSeg=0;

    //Trigger for AWG; minus shift of 70 ns
    risingAWGTrigger.push_back((int)((TimeLaser-delayAWG_ns+delayLaserEnableRising_ns-20)/10));
    fallingAWGTrigger.push_back((int)((TimeLaser+5-delayAWG_ns+delayLaserEnableRising_ns-20)/10));

    for(int i = 0; i<(Steps); ++i){
        //Signal for Laser Enable (init)
        if(i==0){
            risingLaserEnable.push_back((int)(TimePrevSeg)/10);
        }
        fallingLaserEnable.push_back((int)((TimeLaser+TimePrevSeg)/10));

        //Signal for Laser Digital (init)
        if(i==0){
            risingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+TimePrevSeg)/10));
        }
        fallingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+TimeLaser+TimePrevSeg)/10));


        //Event for AWG; minus shift of 70 ns
        risingAWGEvent.push_back((int)((waitingTimeAfterPolarization+TimeLaser-delayAWG_ns+delayLaserEnableRising_ns-20+TimePrevSeg)/10));
        fallingAWGEvent.push_back((int)((waitingTimeAfterPolarization+TimeLaser+5-delayAWG_ns+delayLaserEnableRising_ns-20+TimePrevSeg)/10));

        //##########  Added 500 ns for all following steps, due to get 1µs between MW and readout !!! ###################

        //Signal for TT; additional shift of 50 ns
        risingCounter.push_back((int)((waitingTimeAfterPolarization+TimeLaser+WaitBetweenMWAndReadout+Stepsize*i+delayLaserEnableRising_ns-delayLaserDigital_ns+50+TimePrevSeg)/10)+1);
        fallingCounter.push_back((int)((waitingTimeAfterPolarization+TimeLaser+WaitBetweenMWAndReadout+Stepsize*i+delayLaserEnableRising_ns-delayLaserDigital_ns+50+TimeCounter+TimePrevSeg)/10)+1);

        //Signal for Laser Enable (readout) additional shift of 20 ns
        risingLaserEnable.push_back((int)((waitingTimeAfterPolarization+TimeLaser+WaitBetweenMWAndReadout+Stepsize*i+TimePrevSeg+20)/10)+1);
        if(i==(Steps-1)){
            fallingLaserEnable.push_back((int)((waitingTimeAfterPolarization+TimeLaser+WaitBetweenMWAndReadout+Stepsize*i+delayLaserEnableRising_ns-delayLaserEnableFalling_ns+20+TimeCounter+TimePrevSeg)/10)+1);
        }

        //Signal for Laser Digital (readout) additional shift of 20 ns
        risingLaserDigital.push_back((int)((waitingTimeAfterPolarization+TimeLaser+WaitBetweenMWAndReadout+Stepsize*i+delayLaserEnableRising_ns-delayLaserDigital_ns+20+TimePrevSeg)/10)+1);
        if(i==(Steps-1)){
            fallingLaserDigital.push_back((int)((waitingTimeAfterPolarization+TimeLaser+WaitBetweenMWAndReadout+Stepsize*i+delayLaserEnableRising_ns-delayLaserDigital_ns+20+TimeCounter+TimePrevSeg)/10)+1);
        }

        TimePrevSeg=*std::max_element(fallingCounter.begin(),fallingCounter.end())*10;
    }


    /*int WaitBetweenMWAndReadout = rabi0;

    int TimePrevSeg=0;

    //Trigger for AWG; minus shift of 70 ns
    risingAWGTrigger.push_back((int)((timeLaser-delayAWG_ns+delayLaserEnableRising_ns-20)/10));
    fallingAWGTrigger.push_back((int)((timeLaser+5-delayAWG_ns+delayLaserEnableRising_ns-20)/10));

    for(int i = 0; i<(MWsteps); ++i){

        //Signal for Laser Enable (init)
        if(i==0){
            risingLaserEnable.push_back((int)(TimePrevSeg)/10);
        }
        fallingLaserEnable.push_back((int)((timeLaser+TimePrevSeg)/10));

        //Signal for Laser Digital (init)
        if(i==0){
            risingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+TimePrevSeg)/10));
        }
        fallingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+timeLaser+TimePrevSeg)/10));


        //Event for AWG; minus shift of 70 ns
        risingAWGEvent.push_back((int)((waitingTimeAfterPolarization+timeLaser-delayAWG_ns+delayLaserEnableRising_ns-20+TimePrevSeg)/10));
        fallingAWGEvent.push_back((int)((waitingTimeAfterPolarization+timeLaser+5-delayAWG_ns+delayLaserEnableRising_ns-20+TimePrevSeg)/10));

        //##########  Added 500 ns for all following steps, due to get 1µs between MW and readout !!! ###################

        //Signal for TT; additional shift of 50 ns
        risingCounter.push_back((int)((waitingTimeAfterPolarization+timeLaser+WaitBetweenMWAndReadout+MWstepsize*i+delayLaserEnableRising_ns-delayLaserDigital_ns+50+TimePrevSeg)/10)+1);
        fallingCounter.push_back((int)((waitingTimeAfterPolarization+timeLaser+WaitBetweenMWAndReadout+MWstepsize*i+delayLaserEnableRising_ns-delayLaserDigital_ns+50+timeCounter+TimePrevSeg)/10)+1);

        //Signal for Laser Enable (readout) additional shift of 20 ns
        risingLaserEnable.push_back((int)((waitingTimeAfterPolarization+timeLaser+WaitBetweenMWAndReadout+MWstepsize*i+TimePrevSeg+20)/10)+1);
        if(i==(MWsteps-1)){
            fallingLaserEnable.push_back((int)((waitingTimeAfterPolarization+timeLaser+WaitBetweenMWAndReadout+MWstepsize*i+delayLaserEnableRising_ns-delayLaserEnableFalling_ns+20+timeCounter+TimePrevSeg)/10)+1);
        }

        //Signal for Laser Digital (readout) additional shift of 20 ns
        risingLaserDigital.push_back((int)((waitingTimeAfterPolarization+timeLaser+WaitBetweenMWAndReadout+MWstepsize*i+delayLaserEnableRising_ns-delayLaserDigital_ns+20+TimePrevSeg)/10)+1);
        if(i==(MWsteps-1)){
            fallingLaserDigital.push_back((int)((waitingTimeAfterPolarization+timeLaser+WaitBetweenMWAndReadout+MWstepsize*i+delayLaserEnableRising_ns-delayLaserDigital_ns+20+timeCounter+TimePrevSeg)/10)+1);
        }

        TimePrevSeg=*std::max_element(fallingCounter.begin(),fallingCounter.end())*10;
    }*/

    risingEdges.push_back({0});
    fallingEdges.push_back({100});
    risingEdges.push_back(risingAWGEvent);
    fallingEdges.push_back(fallingAWGEvent);
    risingEdges.push_back(risingCounter);
    fallingEdges.push_back(fallingCounter);
    risingEdges.push_back(risingLaserEnable);
    fallingEdges.push_back(fallingLaserEnable);
    risingEdges.push_back(risingLaserDigital);
    fallingEdges.push_back(fallingLaserDigital);
    risingEdges.push_back(risingAWGTrigger);
    fallingEdges.push_back(fallingAWGTrigger);

    return TimePrevSeg/10;
}

void arrayPreparationsForPulsedMeasurements::createRabiFrequencies(double freq, int MWstep, int MWstepsize, int loops, double amp, M8195Adev* M8195A_var)
{
    ViConstString chan = "1";

    // Create frequency-array
    auto frequencies = linearFreqArray(freq*MHz_M8195A, freq*MHz_M8195A, MWstep-1);

    // Create times-array
    std::vector<double> times (frequencies.size(), 1*microsec_M8195A);

    double FreqToZerro = 0;
    for(int i=0; i<times.size(); ++i){
        times[i]=(MWstepsize/1000.0*i)*microsec_M8195A;
    }
    std::vector<double> phases (frequencies.size(), 0);
    std::vector<double> amplitudes (frequencies.size(), amp);

    std::cout << "Amplitude: " << amplitudes[0] << std::endl;

    ViStatus status = M8195A_var->loadSineArrayWaveformAndSeqTable(chan, frequencies, maxSampleRate, times, loops, phases, amplitudes);
}

//#############################################################
//######################### RABI: End #########################
//#############################################################



//#############################################################
//#################### pulsed ODMR: Start #####################
//#############################################################

int arrayPreparationsForPulsedMeasurements::preparePulsedODMR(double freqStart, double freqEnd, int nbSteps, double piPulse, int timeLaser, int timeCounter, int loops, M2iCard* M2i7005_var, M8195Adev* M8195A_var)
{
    int actChN = next_power_of_2(6);
    std::cout << next_power_of_2(6) << " activated channels" << std::endl;

    int64 mask;

    switch(actChN)
    {
        case 1: mask = (int64)1; break;
        case 2: mask = (int64)0x3; break;
        case 4: mask = (int64)0xF; break;
        case 8: mask = (int64)0xFF; break;
        case 16: mask = (int64)0xFFFF; break;
        default: std::cout << "Error: Channel mask is not allowed !" << std::endl; return 0;
    }

    //int length_in_samples = lround(10047) + 1;  // ~10.047 µs

    double pulsedODMRTime = (timeLaser+piPulse+timeCounter+200)+500;

    /*
    int numbOfSteps = (int)((delayLaserEnableRising_ns+(100+pulsedODMRTime)*nbSteps)/10)+4;
    int length_in_samples;
    if(numbOfSteps%64==0){
        length_in_samples=numbOfSteps;
    }else{
        length_in_samples=64*(numbOfSteps/64+1);
    }

    M2i7005_var->M2iSetup_StdReplay(mask, 100, false, false, loops, length_in_samples);

    std::vector< std::vector<int>> risingEdges;
    std::vector< std::vector<int>> fallingEdges;
    createPulsedODMRSequence(risingEdges, fallingEdges, timeLaser, nbSteps, timeCounter, pulsedODMRTime, piPulse);

    M2i7005_var->M2iLoadIntoMemoryFromVec(6, risingEdges, fallingEdges);
    createPulsedODMRFrequencies(freqStart, freqEnd, nbSteps, piPulse, loops, M8195A_var);

    return length_in_samples;
    */

    std::vector< std::vector<int>> risingEdges;
    std::vector< std::vector<int>> fallingEdges;
    int numbOfSteps = createPulsedODMRSequence(risingEdges, fallingEdges, timeLaser, nbSteps, timeCounter, pulsedODMRTime, piPulse);
    int length_in_samples;

    if(numbOfSteps%64==0){
        length_in_samples=numbOfSteps;
    }else{
        length_in_samples=64*(numbOfSteps/64+1);
    }
    M2i7005_var->M2iSetup_StdReplay(mask, 100, false, false, loops, length_in_samples);
    M2i7005_var->M2iLoadIntoMemoryFromVec(6, risingEdges, fallingEdges);

    return length_in_samples;

}

int arrayPreparationsForPulsedMeasurements::createPulsedODMRSequence(std::vector<std::vector<int>>& risingEdges, std::vector<std::vector<int>>& fallingEdges, int timeLaser, int nbSteps, int timeCounter, int pulsedODMRTime, double piPulse)
{
    std::vector<int> risingAWGTrigger;
    std::vector<int> fallingAWGTrigger;
    std::vector<int> risingAWGEvent;
    std::vector<int> fallingAWGEvent;
    std::vector<int> risingCounter;
    std::vector<int> fallingCounter;
    std::vector<int> risingLaserDigital;
    std::vector<int> fallingLaserDigital;
    std::vector<int> risingLaserEnable;
    std::vector<int> fallingLaserEnable;


    int timePrevSeg=0;

    //Trigger for AWG; minus shift of 70 ns
    risingAWGTrigger.push_back((int)((timeLaser-delayAWG_ns+delayLaserEnableRising_ns-70)/10));
    fallingAWGTrigger.push_back((int)((timeLaser+10-delayAWG_ns+delayLaserEnableRising_ns-70)/10));

    for(int i = 0; i<(nbSteps); ++i){

        //Signal for Laser Enable (init)
        if(i==0){
            risingLaserEnable.push_back((int)(timePrevSeg)/10);
        }
        fallingLaserEnable.push_back((int)((timeLaser+timePrevSeg)/10));

        //Signal for Laser Digital (init)
        if(i==0){
            risingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+timePrevSeg)/10));
        }
        fallingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+timeLaser+timePrevSeg)/10));



        //Event for AWG; minus shift of 70 ns
        risingAWGEvent.push_back((int)((timeLaser-delayAWG_ns+delayLaserEnableRising_ns-70+timePrevSeg)/10));
        fallingAWGEvent.push_back((int)((timeLaser+10-delayAWG_ns+delayLaserEnableRising_ns-70+timePrevSeg)/10));

        //##########  Added 500 ns for all following steps, due to get 1µs between MW and readout !!! ###################

        //Signal for TT; additional shift of 30 ns
        risingCounter.push_back((int)((timeLaser+piPulse+delayLaserEnableRising_ns-delayLaserDigital_ns+500+30+timePrevSeg)/10)+1);
        fallingCounter.push_back((int)((timeLaser+piPulse+delayLaserEnableRising_ns-delayLaserDigital_ns+500+timeCounter+30+timePrevSeg)/10)+1);

        //Signal for Laser Enable (readout) additional shift of 20 ns
        risingLaserEnable.push_back((int)((timeLaser+piPulse+timePrevSeg+20)/10)+1);
        if(i==(nbSteps-1)){
            fallingLaserEnable.push_back((int)((timeLaser+piPulse+delayLaserEnableRising_ns-delayLaserEnableFalling_ns+500+timeCounter+timePrevSeg)/10)+1);
        }

        //Signal for Laser Digital (readout)
        risingLaserDigital.push_back((int)((timeLaser+piPulse+delayLaserEnableRising_ns-delayLaserDigital_ns+500+timePrevSeg)/10)+1);
        if(i==(nbSteps-1)){
            fallingLaserDigital.push_back((int)((timeLaser+piPulse+delayLaserEnableRising_ns-delayLaserDigital_ns+500+timeCounter+timePrevSeg)/10)+1);
        }

        timePrevSeg=*std::max_element(fallingCounter.begin(),fallingCounter.end())*10;
    }

    risingEdges.push_back({1});
    fallingEdges.push_back({2});
    risingEdges.push_back(risingAWGEvent);
    fallingEdges.push_back(fallingAWGEvent);
    risingEdges.push_back(risingCounter);
    fallingEdges.push_back(fallingCounter);
    risingEdges.push_back(risingLaserEnable);
    fallingEdges.push_back(fallingLaserEnable);
    risingEdges.push_back(risingLaserDigital);
    fallingEdges.push_back(fallingLaserDigital);
    risingEdges.push_back(risingAWGTrigger);
    fallingEdges.push_back(fallingAWGTrigger);

    return timePrevSeg/10;

}

void arrayPreparationsForPulsedMeasurements::createPulsedODMRFrequencies(double freqStart, double freqEnd, int nbSteps, double piPulse, int loops, M8195Adev* M8195A_var) {
    ViConstString chan = "1";

    // Create frequency-array
    auto frequencies = linearFreqArray(freqStart*MHz_M8195A, freqEnd*MHz_M8195A, nbSteps-1);

    // Create times-array
    std::vector<double> times (frequencies.size(), piPulse/1000.0*microsec_M8195A);

    std::vector<double> phases (frequencies.size(), 0);
    std::vector<double> amplitudes (frequencies.size(), 1);

    ViStatus status = M8195A_var->loadSineArrayWaveformAndSeqTable(chan, frequencies, maxSampleRate, times, loops, phases, amplitudes);

}

//#############################################################
//##################### pulsed ODMR: End ######################
//#############################################################



//#############################################################
//######################## T1: Start ##########################
//#############################################################

int arrayPreparationsForPulsedMeasurements::prepareT1(double Freq, int nbSteps, double stepSize, double piPulse, double timeCounter, double timeLaser, int loops, M2iCard* M2i7005_var) {
    int actChN = next_power_of_2(6);
    std::cout << next_power_of_2(6) << " activated channels" << std::endl;

    int64 mask;

    switch(actChN)
    {
        case 1: mask = (int64)1; break;
        case 2: mask = (int64)0x3; break;
        case 4: mask = (int64)0xF; break;
        case 8: mask = (int64)0xFF; break;
        case 16: mask = (int64)0xFFFF; break;
        default: std::cout << "Error: Channel mask is not allowed !" << std::endl; return 0;
    }

    //int length_in_samples = lround(10047) + 1;  // ~10.047 µs

    std::vector< std::vector<int>> risingEdges;
    std::vector< std::vector<int>> fallingEdges;
    int numbOfSteps = createT1Sequence(risingEdges, fallingEdges, timeLaser, nbSteps, stepSize, timeCounter, piPulse);
    int length_in_samples;
    if(numbOfSteps%64==0){
        length_in_samples=numbOfSteps;
    }else{
        length_in_samples=64*(numbOfSteps/64+1);
    }

    M2i7005_var->M2iSetup_StdReplay(mask, 100, false, false, loops, length_in_samples);
    M2i7005_var->M2iLoadIntoMemoryFromVec(6, risingEdges, fallingEdges);

    return length_in_samples;
}

int arrayPreparationsForPulsedMeasurements::createT1Sequence(std::vector< std::vector<int>>& risingEdges,  std::vector< std::vector<int>>& fallingEdges, int timeLaser, int nbSteps, int stepSize, int timeCounter, double piPulse) {
    std::vector<int> risingAWGTrigger;
    std::vector<int> fallingAWGTrigger;
    std::vector<int> risingAWGEvent;
    std::vector<int> fallingAWGEvent;
    std::vector<int> risingCounter;
    std::vector<int> fallingCounter;
    std::vector<int> risingLaserDigital;
    std::vector<int> fallingLaserDigital;
    std::vector<int> risingLaserEnable;
    std::vector<int> fallingLaserEnable;

    int TimeLaser = timeLaser;
    int Steps = nbSteps;
    int Stepsize = stepSize;
    int TimeCounter = timeCounter;
    int MWTime = piPulse;

    int TimePrevSeg=0;
    double TimePrevSegTemp=0;

    //Trigger for AWG; minus shift of 70 ns
    risingAWGTrigger.push_back((int)((TimeLaser-delayAWG_ns+delayLaserEnableRising_ns-70)/10));
    fallingAWGTrigger.push_back((int)((TimeLaser+100-delayAWG_ns+delayLaserEnableRising_ns-70)/10));

    for(int i = 0; i<(Steps*2); ++i){

        //Signal for Laser Enable (init)
        if(i==0){
            risingLaserEnable.push_back((int)(TimePrevSeg)/10);
        }
        fallingLaserEnable.push_back((int)((TimeLaser+TimePrevSeg)/10));

        //Signal for Laser Digital (init)
        if(i==0){
            risingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+TimePrevSeg)/10));
        }
        fallingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+TimeLaser+TimePrevSeg)/10));

        if(!(i%2)){
            //Event for AWG; minus shift of 70 ns
            risingAWGEvent.push_back((int)((TimeLaser-delayAWG_ns+delayLaserEnableRising_ns-70+TimePrevSeg)/10));
            fallingAWGEvent.push_back((int)((TimeLaser+100-delayAWG_ns+delayLaserEnableRising_ns-70+TimePrevSeg)/10));
        }

        //Signal for TT; additional shift of 60 ns
        risingCounter.push_back((int)((TimeLaser+MWTime+Stepsize*((int)(i/2))+delayLaserEnableRising_ns-delayLaserDigital_ns+60+TimePrevSeg)/10));
        fallingCounter.push_back((int)((TimeLaser+MWTime+Stepsize*(int)(i/2)+delayLaserEnableRising_ns-delayLaserDigital_ns+TimeCounter+60+TimePrevSeg)/10));

        //Signal for Laser Enable (readout) additional shift of 20 ns
        risingLaserEnable.push_back((int)((TimeLaser+MWTime+Stepsize*(int)(i/2)+TimePrevSeg+20)/10));
        if(i==(Steps*2-1)){
            fallingLaserEnable.push_back((int)((TimeLaser+MWTime+Stepsize*(int)(i/2)+delayLaserEnableRising_ns-delayLaserEnableFalling_ns+TimeCounter+TimePrevSeg+100)/10));
        }

        //Signal for Laser Digital (readout)
        risingLaserDigital.push_back((int)((TimeLaser+MWTime+Stepsize*(int)(i/2)+delayLaserEnableRising_ns-delayLaserDigital_ns+TimePrevSeg)/10));
        if(i==(Steps*2-1)){
            fallingLaserDigital.push_back((int)((TimeLaser+MWTime+Stepsize*(int)(i/2)+delayLaserEnableRising_ns-delayLaserDigital_ns+TimeCounter+TimePrevSeg+100)/10));
        }

        TimePrevSeg=*std::max_element(fallingCounter.begin(),fallingCounter.end())*10;
    }

    risingEdges.push_back({0});
    fallingEdges.push_back({100});
    risingEdges.push_back(risingAWGEvent);
    fallingEdges.push_back(fallingAWGEvent);
    risingEdges.push_back(risingCounter);
    fallingEdges.push_back(fallingCounter);
    risingEdges.push_back(risingLaserEnable);
    fallingEdges.push_back(fallingLaserEnable);
    risingEdges.push_back(risingLaserDigital);
    fallingEdges.push_back(fallingLaserDigital);
    risingEdges.push_back(risingAWGTrigger);
    fallingEdges.push_back(fallingAWGTrigger);

    return TimePrevSeg/10;
}

void arrayPreparationsForPulsedMeasurements::createT1Frequencies(double Freq, int nbSteps, double piPulse, int loops, M8195Adev* M8195A_var){

    ViConstString chan = "1";

    // Create frequency-array
    auto frequencies = linearFreqArray(Freq*MHz_M8195A, Freq*MHz_M8195A, nbSteps-1);

    // Create times-array
    std::vector<double> times (frequencies.size(), piPulse/1000.0*microsec_M8195A);

    std::vector<double> phases (frequencies.size(), 0);
    std::vector<double> amplitudes (frequencies.size(), 1);

    ViStatus status = M8195A_var->loadSineArrayWaveformAndSeqTable(chan, frequencies, maxSampleRate, times, loops, phases, amplitudes);

}

//#############################################################
//######################### T1: End ###########################
//#############################################################
