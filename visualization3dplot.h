#ifndef VISUALIZATION3DPLOT_H
#define VISUALIZATION3DPLOT_H

#include <QWidget>

namespace Ui {
class visualization3DPlot;
}

class visualization3DPlot : public QWidget
{
    Q_OBJECT

public:
    explicit visualization3DPlot(QWidget *parent = nullptr);
    ~visualization3DPlot();

private:
    Ui::visualization3DPlot *ui;
};

#endif // VISUALIZATION3DPLOT_H
