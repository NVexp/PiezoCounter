#ifndef PULSEDMeasurementSANDDATA_H
#define PULSEDMeasurementSANDDATA_H

#include <QMainWindow>

#include "include/TimeTagger.h"
#include "include/Iterators.h"

#include <iostream>
#include <filesystem>
#include <direct.h>




typedef struct dataFromFileAWG
{
    std::string fileNameLoaded;
    std::string PathLoaded;

} dataFromFileAWG;

typedef struct cwODMRParams
{
    int loopNb;
    double excitTime;
    double FreqStart;
    double FreqEnd;
    int stepNb;

} cwODMRParams;

typedef struct FitParameters
{
    double w0;
    double w0_err;
    double FWHM;
    double FWHM_err;
    double Amp;
    double Amp_err;

} FitParameters;

typedef struct RabiParams
{
    double Freq;
    int Steps;
    double Stepsize;
    double TimeLaser;
    double TimeCounter;
    int LoopNb;
    int MinRabiTime;
    double Rabi0Time;
    double Amp;

} RabiParams;

typedef struct pulsedODMRParams
{
    double FreqStart;
    double FreqEnd;
    int stepNb;
    double pi_Pulse;
    int TimeLaser;
    int TimeCounter;
    int LoopNb;
    double PulsedODMRTime;

} pulsedODMRParams;

typedef struct T1Params
{
    double Freq;
    int Steps;
    double Stepsize;
    double TimeLaser;
    double TimeCounter;
    int LoopNb;
    double PiTime;
    double Segment0Time;

} T1Params;

typedef struct RamseyParams
{
    double Freq;
    int Steps;
    double Stepsize;
    double PiOver2time;
    double TimeLaser;
    double TimeCounter;
    int LoopNb;
    double MinRamseyTime;
} RamseyParams;

typedef struct SpinEchoParams
{
    double Freq;
    int Steps;
    double Stepsize;
    double PiOver2time;
    double PiTime;
    double TimeLaser;
    double TimeCounter;
    int LoopNb;
    double MinSpinEchoTime;
} SpinEchoParams;

typedef struct TestReadout
{
    double Freq;
    double PiTime;
    double TimeLaser;
    double TimeCounter;
    int LoopNb;
} TestReadout;

typedef struct CompPulsDetunParams
{
    double Freq;
    int Steps;
    double Stepsize;
    double TimeLaser;
    double TimeCounter;
    int LoopNb;
    double PiTime;
    int PhaseNb;
    double segmentTime;

} CompPulsDetunParams;

typedef struct SechParams
{
    double Freq;
    int Steps;
    double Stepsize;
    double TimeLaser;
    double TimeCounter;
    int LoopNb;
    double segment0Time;

} SechDetunParams;

typedef struct Position
{
    int xPos;
    int yPos;
    int zPos;

} Position;

typedef struct compusitePulses
{
    std::vector<double> phases;
    std::vector<double> startTimes;

} compusitePulses;


class PulsedMeasurementsAndData : public QObject
{
    Q_OBJECT
public:
    PulsedMeasurementsAndData();
    ~PulsedMeasurementsAndData();

    cwODMRParams currentODMRParams;
    std::filesystem::path globalPath;
    // type of read data
    std::string typeOfMeasurement;
    CountBetweenMarkers* ODMRCountsWithMarker;
    CountBetweenMarkers* RabiCountsWithMarker;
    CountBetweenMarkers* PulsedODMRCountsWithMarker;
    CountBetweenMarkers* T1CountsWithMarker;
    CountBetweenMarkers* RamseyCountsWithMarker;
    CountBetweenMarkers* SpinEchoCountsWithMarker;
    CountBetweenMarkers* TestReadoutCountsWithMarker;
    CountBetweenMarkers* CompPulsDetunCountsWithMarker;
    CountBetweenMarkers* CompPulsPI2CountsWithMarker;
    CountBetweenMarkers* SechCountsWithMarker;
    std::vector<double> photonCountsDataProcessed;
    std::vector<double> photonCountsPIDataProcessed;
    std::vector<double> CountBinsTimeLengthProcessed;
    FitParameters* Dip1FitParameters;
    FitParameters* Dip2FitParameters;
    RabiParams currentRabiParams;
    pulsedODMRParams currentPulsedODMRParams;
    T1Params currentT1Params;
    RamseyParams currentRamseyParams;
    SpinEchoParams currentSpinEchoParams;
    TestReadout currentTestReadoutParams;
    CompPulsDetunParams currentCompPulsDetunParams;
    SechParams currentSechParams;

    compusitePulses currentCompusitePulses;

    dataFromFileAWG currentDataFromFile;

    Position CurrentPosition;

    TimeTagger* TTPulsed; // for connection to TimeTagger20

    int withRefocus;
    int NbOfRefocus;
    std::vector<double> withRefocusXValues;
    std::vector<double> withRefocusYValues;
    std::vector<double> withRefocusY2Values;

    void prepareCounterForODMR();
    void fetchAndProcessDataForODMR();

    void prepareCounterForRabi();
    void fetchAndProcessDataForRabi();

    void prepareCounterForPulsedODMR();
    void fetchAndProcessDataForPulsedODMR();

    void prepareCounterForT1();
    void fetchAndProcessDataForT1();

    void prepareCounterForCompPulsDetun();
    void fetchAndProcessDataForCompPulsDetun();

    void prepareCounterForRamsey();
    void fetchAndProcessDataForRamsey();

    void prepareCounterForSpinEcho();
    void fetchAndProcessDataForSpinEcho();

    void prepareCounterForTestReadout();
    void fetchAndProcessDataForTestReadout();

    void prepareCounterForCompPulsPI2();
    void fetchAndProcessDataForCompPulsPI2();

    void prepareCounterForSech();
    void fetchAndProcessDataForSech();

    void setGlobalPath(std::filesystem::path path);
    void writeDataToFile(std::string pathName);
    void readDataFromDatFile(QString fileName);

signals:
    void ODMRValuesClear();
    void ODMRValues(double valx, double valy);
    void PlotODMR();
    void T1Value(double valy);
    void PlotT1();
    void compositePulsesVectorFill(QVector<double> compPulse);

};

#endif // PULSEDMeasurementSANDDATA_H
