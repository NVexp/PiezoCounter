#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>
#include <algorithm>
#include <fstream>
#include <chrono>
#include <ctime>
#include <iomanip>
#include <QMessageBox>
#include <QTimer>
#include <utility>
#include <filesystem>
#include <direct.h>
#include <thread>
#include <QFileDialog>
#include <sstream>

#include "scanandcount.h"
#include "loadfilecreatmosaic.h"
#include "m8195adev.h"
#include "pulsedmeasurementsanddata.h"
#include "arrayPreparationsForPulsedMeasurements.h"
#include "magnetstage/magnetcontrolclass.h"
#include "magnetstage/visualization3dplot.h"
//#include "alglib/fittingroutines.h"
#include "Sora/cnn.h"
#include "Sora/control.h"
#include "Sora/sora.h"
//#include "Control/controlmainwindow.h"

#define NO_DEVICE_PHYS_CONNECT false

#define M8195A_PHYS_CONNECT (true&&(!NO_DEVICE_PHYS_CONNECT))
#define TT20_PHYS_CONNECT (true&&(!NO_DEVICE_PHYS_CONNECT))
#define PI_E725_PHYS_CONNECT (true&&(!NO_DEVICE_PHYS_CONNECT))
#define M2iCARD_PHYS_CONNECT (true&&(!NO_DEVICE_PHYS_CONNECT))
#define MAG_MOTORS_PHYS_CONNECT (true&&(!NO_DEVICE_PHYS_CONNECT))
#define LJ_T7_PHYS_CONNECT (false&&(!NO_DEVICE_PHYS_CONNECT))


std::string getNow()
{
    auto now = std::chrono::system_clock::now();
    auto nowT = std::chrono::system_clock::to_time_t(now);
    //use a C-string here because it was impossible to find how to use a
    //C++-style of time formatting and strftime works and is pretty convenient
    char strNow[20];
    strftime(strNow, 20, "%F %T", localtime(&nowT));
    std::string stdStrNow{ strNow };//make std::string out of C-string

    return stdStrNow;
}



// Delay time for instruments (including cables, fiberws etc.) in ns
static const int delayLaserEnableRising_ns = 300;
static const int delayLaserEnableFalling_ns = 200;
static const int delayLaserDigital_ns = 20;
static const int delayAWG_ns = 635;
static const int sechCutoff = 10;
static const int waitingTimeAfterPolarization = 600;



// set the color gradient to custom gradient:
QCPColorGradient rainBowPlusWhiteGradient;
void setGradient_rainbowPlusWhite()
{
    rainBowPlusWhiteGradient.clearColorStops();
    rainBowPlusWhiteGradient.setColorStopAt(0, QColor(0, 0, 0));
    rainBowPlusWhiteGradient.setColorStopAt(0.25, QColor(0, 0, 255));
    rainBowPlusWhiteGradient.setColorStopAt(0.40, QColor(50, 180, 50));
    rainBowPlusWhiteGradient.setColorStopAt(0.50, QColor(255, 255, 0));
    rainBowPlusWhiteGradient.setColorStopAt(0.75, QColor(255, 0, 0));
    rainBowPlusWhiteGradient.setColorStopAt(1, QColor(255, 255, 255));
}



MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)

{
    ui->setupUi(this);


    if (M8195A_PHYS_CONNECT){M8195A_var = new M8195Adev;}

    if (M2iCARD_PHYS_CONNECT){M2i7005_var = new M2iCard;}

    if(MAG_MOTORS_PHYS_CONNECT){magnet = new magnetcontrolclass(parent);}


    ScanAndCount_var = new ScanAndCount(ui->QCPimageWROI, PI_E725_PHYS_CONNECT, TT20_PHYS_CONNECT);


    PulsedMeasurementsAndData_var = new PulsedMeasurementsAndData;
    ArrayPrepForPulsedMeasurements_var = new arrayPreparationsForPulsedMeasurements();
    LoadFileCreatMosaic_var = new LoadFileCreatMosaic(ui->QCPimageWROI, ui->image3DPlot);
    colorScale3Dplot = new QCPColorScale(ui->image3DPlot);
    colorScale2Dplot = new QCPColorScale(ui->QCPimageWROI);
    //fittingroutines_var = new fittingroutines(ui->ODMRPlot);
    cnnwoBias_var = new CNN("C:/Users/DiamondExp/Documents/Qt projects/PiezoScanPlusCounter/Sora/CNN/","cnnwoBias");

    connect(ScanAndCount_var, SIGNAL(scanProgress_pxCount(int)), ui->scanProgress, SLOT(setValue(int)));//for progress bar display
    connect(ScanAndCount_var, SIGNAL(readyFor2DPlot()), this, SLOT(plotAfterDataAcq()));//for plotting data after acquisition
    connect(ScanAndCount_var, SIGNAL(ScanPlot2D(int,int)), this, SLOT(plot2DData(int, int)));//for ploting the 2D data

    // ###### For Ploting ODMR Scan ######
    connect(PulsedMeasurementsAndData_var, SIGNAL(ODMRValuesClear()), this, SLOT(clearODMRValues()));
    connect(PulsedMeasurementsAndData_var, SIGNAL(ODMRValues(double, double)), this, SLOT(writeODMRValues(double, double)));
    connect(PulsedMeasurementsAndData_var, SIGNAL(PlotODMR()), this, SLOT(plotODMRData()));
    connect(PulsedMeasurementsAndData_var, SIGNAL(T1Value(double)), this, SLOT(writeT1Value(double)));
    connect(PulsedMeasurementsAndData_var, SIGNAL(PlotT1()), this, SLOT(plotT1Data()));

//    connect(fittingroutines_var, SIGNAL(graphDataClear()), this, SLOT(clearGraphData()));
//    connect(fittingroutines_var, SIGNAL(graphData(double, double)), this, SLOT(writeGraphData(double, double)));
//    connect(fittingroutines_var, SIGNAL(plotGraph()), this, SLOT(PlotGraphData()));

    connect(ui->QCPimageWROI, SIGNAL(posMovedTo(double, double)), this, SLOT(moveToPosition(double, double)));

    // create graphs for the ...
    // ... ODMR Plot (1. measure data, 2. normed measure data, 3. fit Data, 4. fourier data)
    for(int i=0; i<4; i++){
        ui->ODMRPlot->addGraph();
    }
    // ... comp pulses
    ui->CompPuls->addGraph();
    ui->CompPuls->addGraph();

    // drop down menu (right click) for the ...
    // ... ODMR Plot
    ui->ODMRPlot->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->ODMRPlot, SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(ODMRContextMenuRequest(QPoint)));

    //define own color gradient for color map
    setGradient_rainbowPlusWhite();


    EnableDisableButtonsMagnet(false);
    //ui->SingleLorentzianFit->setEnabled(false);
    //ui->DoubleLorentzianFit->setEnabled(false);


    //create directory for current day (if it does not exist) and sets the member variable 'globalPath' to the full name of this directory
    //for writing data and images into this directory
    std::time_t rawtime;
    std::tm* timeinfo;
    char yyyymmdd [80];
    std::time(&rawtime);
    timeinfo = std::localtime(&rawtime);
    std::strftime(yyyymmdd,80,"%Y%m%d",timeinfo);
    std::string subfolder = "E:\\data\\";
    std::string yyyymmdd_string = yyyymmdd;
    std::string tempPath = subfolder + yyyymmdd_string;
    globalPath = tempPath;
    doesPathExist_ifNotCreate();
    ScanAndCount_var->setGlobalPath(globalPath);
    PulsedMeasurementsAndData_var->setGlobalPath(globalPath);

    ui->ShowPath->setText(QString::fromStdString(tempPath));

    ui->pxSizeX->setMaximum((int)ScanAndCount_var->getIStepXmax());
    ui->pxSizeY->setMaximum((int)ScanAndCount_var->getIStepYmax());

    //ui->picsList->setViewMode(QListView::IconMode);
    //ui->picsList->setIconSize(QSize(200,200));
    //ui->picsList->setResizeMode(QListView::Adjust);

    ui->horizontalLine->addGraph();
    ui->horizontalLine->graph(0)->setPen(QPen(Qt::blue));

    ui->verticalLine->addGraph(ui->verticalLine->yAxis, ui->verticalLine->xAxis);//here we set that xAxis should be f(t) if we call yAxis variable t; by default, it is the other way around i.e. the standard case
    ui->verticalLine->graph(0)->setPen(QPen(Qt::green));

    //setup the color scale for the Zscan plot
    ui->image3DPlot->plotLayout()->addElement(0, 1, colorScale3Dplot); //add color scale to plot
    colorScale3Dplot->setType(QCPAxis::atRight);
    ui->image3DPlot->colorMap->setColorScale(colorScale3Dplot); // associate the color map with the color scale

    //setup the color scale for the 2D plot with ROIs
    ui->QCPimageWROI->plotLayout()->addElement(0, 1, colorScale2Dplot); //add color scale to plot
    colorScale2Dplot->setType(QCPAxis::atRight);
    ui->QCPimageWROI->colorMap->setColorScale(colorScale2Dplot); // associate the color map with the color scale

    //set the gradient for the plots
    //WARNING: HAS TO BE DONE AFTER setColorScale !!!!!
    ui->QCPimageWROI->colorMap->setGradient(rainBowPlusWhiteGradient);
    ui->QCPimageWROI->colorMap->setInterpolate(false);
    ui->image3DPlot->colorMap->setGradient(rainBowPlusWhiteGradient);
    ui->image3DPlot->colorMap->setInterpolate(false);

    //LoadFileCreatMosaic_var->readDataFromDatFile("E:\\data\\20200515\\normal-scan_20200515123720.dat");

    ui->WTRpixelTime->setValue(lround(initValueWaveTableRate*servoCycleTimeInMillisec));
    //connect(ui->picsList, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(loadPicture(QListWidgetItem*)));


    // -------------------------------
    // ---- Sequence editor stuff ----
    // ------------ Begin ------------
    seqEditors_in_GUI.push_back(ui->chan0);
    seqEditors_in_GUI.push_back(ui->chan1);
    seqEditors_in_GUI.push_back(ui->chan2);
    seqEditors_in_GUI.push_back(ui->chan3);
    seqEditors_in_GUI.push_back(ui->chan4);
    seqEditors_in_GUI.push_back(ui->chan5);
    seqEditors_in_GUI.push_back(ui->chan6);
    seqEditors_in_GUI.push_back(ui->chan7);

    constStates_in_GUI.push_back(ui->constState_chan0);
    constStates_in_GUI.push_back(ui->constState_chan1);
    constStates_in_GUI.push_back(ui->constState_chan2);
    constStates_in_GUI.push_back(ui->constState_chan3);
    constStates_in_GUI.push_back(ui->constState_chan4);
    constStates_in_GUI.push_back(ui->constState_chan5);
    constStates_in_GUI.push_back(ui->constState_chan6);
    constStates_in_GUI.push_back(ui->constState_chan7);

    //connect all sequence editor to the others in order to synchronize their range
    for (int k = 0; k<8; ++k)
    {
        for (int l = 0; l <8; ++l)
        {
            if (l != k )
                connect(seqEditors_in_GUI[k]->axisRect()->axis(QCPAxis::AxisType::atBottom), SIGNAL(rangeChanged(const QCPRange&)), seqEditors_in_GUI[l], SLOT(setNewRange(const QCPRange&)));
        }
    }
    connect(ArrayPrepForPulsedMeasurements_var, SIGNAL(uploadIsDone(QString)), this, SLOT(frequencyUploadDone(QString)));
    connect(this, SIGNAL(frequencyMeasurementFinished(QString)), this, SLOT(frequencyMeasurementDone(QString)));
    // -------------------------------
    // ---- Sequence editor stuff ----
    // ------------- End -------------

    // -------------------------------
    // ---- Magnetstage functions ----
    // ------------ Begin ------------

    //create timer which is connected to the movement function. the timer is active as long the position isn't reached
    updateMagnetStageUI = new QTimer(this);
    connect(updateMagnetStageUI, SIGNAL(timeout()), SLOT(updateMagnetTab()));

    // ------------------------------
    // ---- Magnetstage functions ---
    // ------------ End -------------

    // ------------------------------
    // ----- Sora (AI) functions ----
    // ------------ Begin -----------

    control_var = new Control();
    sora_var = new Sora();

    // set AIUse (no comments in the console, usning diffrent path to save data)
    connect(sora_var, SIGNAL(AIuse(bool)), ScanAndCount_var, SLOT(setAIuse(bool)));

    // fill WavTableLine
    connect(sora_var, SIGNAL(setWavGenTableLine(int, int, int, QString)), this, SLOT(WavGenTableLineFill(int, int, int, QString)));// ###

    // CONFOCAL SCAN
    // set scan parameters
    connect(sora_var, SIGNAL(set2DScanParameters(int, int, int, int, int, int, QString)), this, SLOT(TwoDScanSetup(int, int, int, int, int, int, QString)));
    // set laser settings
    connect(sora_var, SIGNAL(checkLaser()), this, SLOT(checkForLaser()));
    connect(sora_var, SIGNAL(setPermWord()), this, SLOT(on_setPermWord_clicked()));
    // start 2D scan
    connect(sora_var, SIGNAL(start2DScan()), this, SLOT(TwoDScanStart()));
    // emit signal if the scan is done
    connect(ScanAndCount_var, SIGNAL(finished_2DScan(bool)), sora_var, SLOT(state2DScan(bool)));

    // get current positions
    connect(sora_var, SIGNAL(getPositions()), ScanAndCount_var, SLOT(readAllPositions_Closed()));
    // get current counts
    connect(sora_var, SIGNAL(getCounts()), ScanAndCount_var, SLOT(getImageVec()));
    // go to position
    connect(sora_var, SIGNAL(newPosition(int, int, int)), this, SLOT(goToPosition(int, int, int)));
    // set triggermode
    connect(sora_var, SIGNAL(setAWGTriggerMode(const QString)), this, SLOT(on_triggerMode_currentIndexChanged(const QString)));

    // finish states






    // Start Control
    connect(control_var, SIGNAL(startScanNVOnly(int,int,int)), ScanAndCount_var, SLOT(setAIParameters(int,int,int)));
    // Measurment is finished
    connect(ScanAndCount_var, SIGNAL(StateAIMeasurement(bool)),control_var, SLOT(stateNVMeasurment(bool)));
    // set Wavetable
    connect(control_var, SIGNAL(setWavetabel(int, int, int, int, int, int)), ScanAndCount_var, SLOT(setupWaveTableScan_ZigZag(int,int,int,int, int, int)));

    // set Magnetstage
    connect(control_var, SIGNAL(setDistanceValue(double)), ui->distanceDiamondMagnet, SLOT(setValue(double)));//###
    connect(control_var, SIGNAL(setSmallAngleValue(double)), ui->angleSmallMotor, SLOT(setValue(double)));//###
    connect(control_var, SIGNAL(setBigAngleValue(double)), ui->angleBigMotor, SLOT(setValue(double)));//###
    connect(control_var, SIGNAL(startMagnetstage()), this, SLOT(on_MoveButtonMagnet_clicked()));//###
    connect(control_var, SIGNAL(setMagnetStage(double, double, double)), this, SLOT(moveMagnetStage(double, double, double)));
    connect(magnet, SIGNAL(reachPosition(bool)), control_var, SLOT(stateMagnetStage(bool)));
    // Magnetstage is set
    connect(this, SIGNAL(stateAIMagnet(bool)), control_var, SLOT(stateMagnetStage(bool)));

    // upload ODMR parameter
    connect(control_var, SIGNAL(uploadCwODMRParameter(int,double,double,double,int)), this, SLOT(loadWaveformAndSeqTable(int,double,double,double,int)));
    connect(this, SIGNAL(StateAIODMRUpload(bool)), control_var, SLOT(stateODMRUpload(bool)));

    // start cm ODMR-Scan
    connect(control_var, SIGNAL(startCwODMR(int)), this, SLOT(startAIcwODMR(int)));
    connect(this, SIGNAL(StateAIcwODMR(bool)), control_var, SLOT(stateCwODMR(bool)));


    // ------------------------------
    // ----- Sora (AI) functions ----
    // ------------ End -------------

    // start sequence
    // connect magnetstage
    on_ConnectMagnetstage_clicked();

    // set trigger State to Continuos
    M8195A_var->setTriggermode("Triggered");
}



MainWindow::~MainWindow()
{
    if(magnet->getStateMotors()){
        magnet->disconnect2motors();
    }
    delete magnet;
    delete colorScale2Dplot;
    delete colorScale3Dplot;
    delete LoadFileCreatMosaic_var;
    delete ScanAndCount_var;
    delete sora_var;
    delete ui;
}



// Function to controle if measurement is prepared/done to start/read out
bool MainWindow::checkClickedButton(int index, std::string MeasurementType){

    //Calculate index and value of max element in MeasurementVectorInt (0: nothing definded or done, 1: definded, 2: measurement done)
    int maxElementIndex = std::max_element(MeasurementVectorInt.begin(),MeasurementVectorInt.end()) - MeasurementVectorInt.begin();
    int maxElement = *std::max_element(MeasurementVectorInt.begin(), MeasurementVectorInt.end());

    //Check if no measurement was defined
    if((maxElement==0)&&(MeasurementType!="prep")){
        QMessageBox msgBox;
        msgBox.setText("No measurement is definde. Please define a "+MeasurementVectorString[index]+"-measurement!");
        msgBox.exec();
        return true;
    }

    if(MeasurementType=="prep"){
        for(int i=0; i<MeasurementVectorInt.size(); i++){
            if(i==index){
                MeasurementVectorInt[i]=1;
            }else{
                MeasurementVectorInt[i]=0;
            }
        }
        return false;
    }else if(MeasurementType=="start"){
        if(MeasurementVectorInt[index]==1){
            MeasurementVectorInt[index]=2;
            return false;
        }else if(MeasurementVectorInt[index]==2){
            QMessageBox msgBox;
            msgBox.setText(MeasurementVectorString[index]+"-measurement already done, use read button!");
            msgBox.setInformativeText("Do you want start a new measurement?");
            msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
            msgBox.setDefaultButton(QMessageBox::No);
            int ret = msgBox.exec();
            switch (ret) {
              case QMessageBox::Yes:
                  // Yes was clicked
                  return false;
              case QMessageBox::No:
                  // No was clicked
                  return true;
              default:
                  // should never be reached
                  break;
            }
        }else if(MeasurementVectorInt[index]==0){
            if(maxElement==1){
                QMessageBox msgBox;
                msgBox.setText("A "+MeasurementVectorString[maxElementIndex]+"-measurement is definde! You can't start a "+MeasurementVectorString[index]+"-measurement");
                msgBox.exec();
                return true;
            }else{
                QMessageBox msgBox;
                msgBox.setText("A "+MeasurementVectorString[maxElementIndex]+"-measurement is already done! You can't start a "+MeasurementVectorString[index]+"-measurement");
                msgBox.exec();
                return true;
            }
        }
    }else if(MeasurementType=="read"){
        if(MeasurementVectorInt[index]==2){
            MeasurementVectorInt[index]=1;
            return false;
        }else if(MeasurementVectorInt[index]==1){
            QMessageBox msgBox;
            msgBox.setText(MeasurementVectorString[index]+"-measurement is only defined, please start first! You can't read a "+MeasurementVectorString[index]+"-measurement");
            msgBox.exec();
            return true;
        }else if(MeasurementVectorInt[index]==0){
            if(maxElement==1){
                QMessageBox msgBox;
                msgBox.setText("A "+MeasurementVectorString[maxElementIndex]+"-measurement is definde! You can't read a "+MeasurementVectorString[index]+"-measurement");
                msgBox.exec();
                return true;
            }else{
                QMessageBox msgBox;
                msgBox.setText("A "+MeasurementVectorString[maxElementIndex]+"-measurement is already done! You can't read a "+MeasurementVectorString[index]+"-measurement");
                msgBox.exec();
                return true;
            }
        }
    }
    QMessageBox msgBox;
    msgBox.setText("If you see this message, there is something wrong in the source code!");
    msgBox.exec();
    return true;
}


//replace all same character 'charOut' in a string 'str' after an index 'start' by a certain string 'strIn'
void replace_all_after(std::string::size_type start, std::string& str, const char& charOut, const std::string& strIn)
{
    std::string::size_type n = 0;
    //check if the character to be replaced does not appear in the replacement string
    n = strIn.find(charOut, 0);
    if (n != std::string::npos)
    {
        std::cout << "Aborted: character found in replacement string -> function would not terminate !" <<std::endl;
        return;
    }

    //find and replace ; should always terminate despite the while(1)
    n = start;
    while (1)
    {
        std::cout << n << '\n';
        n = str.find(charOut, n);
        if (n == std::string::npos)
            break;
        else
            str.replace(n, 1, strIn);
    }
}


//
std::vector<std::vector<int> > find_all_nonzero(int**& V, size_t nbOfArrays, size_t sizeOfArrays)
{
    std::vector<std::vector<int> >result{ nbOfArrays };
    std::cout << "nb of vectors: " << result.size() << std::endl;

    for (size_t k = 0; k<nbOfArrays; ++k)
    {
        int* begin = &(V[k][0]);
        int* end = &(V[k][sizeOfArrays]);
        int* found = nullptr;
        int* pointer = &V[k][0]-1;
        int counter = 0;
        while (pointer != end)
        {
            found = std::find_if(++pointer, end, [](int g) {return (g != 0); });
            pointer = found;

            if (pointer != end)
            {
                result[k].push_back(pointer-begin);
                ++counter;
            }
        }
    }
    return result;
}


// fills a line of the table showing what has been defined in the Wave Table
void MainWindow::WavGenTableLineFill(int lineNbInQTable, int pxNumber, int pxSize, QString st)
{
    //st = AxisToQString(A);
    QTableWidgetItem* cell1 = new QTableWidgetItem(st);
    ui->defdWaveTables->setItem(lineNbInQTable, 0, cell1);

    st = QString::number(pxSize);
    QTableWidgetItem* cell2 = new QTableWidgetItem(st);
    ui->defdWaveTables->setItem(lineNbInQTable, 1, cell2);

    st = QString::number(pxNumber);
    QTableWidgetItem* cell3 = new QTableWidgetItem(st);
    ui->defdWaveTables->setItem(lineNbInQTable, 2, cell3);

}


/*
std::vector<double> MainWindow::getRecordedData(int length)
{
    int RecTableIds[] = {1,2};
    double* dDataPtr = nullptr;
    if(!PI_qDRR(ID, RecTableIds, 2, 1, length, &dDataPtr, bufferDataRec, 400))
    {
        iError = PI_GetError(ID);
        PI_TranslateError(iError, szErrorMessage, 1024);
        std::cout << "Failure getting recorded data: ERROR" << iError << ": " << szErrorMessage << std::endl;
    }

    //get the present time into a std::string
    auto now = std::chrono::system_clock::now();
    auto nowT = std::chrono::system_clock::to_time_t(now);
    std::cout << "Now is\n" << std::put_time(std::localtime(&nowT), "%F %T") << '\n';
    //use a C-string here because it was impossible to find how to use a
    //C++-style of time formatting and strftime works and is pretty convenient
    char strNow[20];
    std::strftime(strNow, 20, "%F %T", std::localtime(&nowT));
    std::string stdStrNow{ strNow };//make std::string out of C-string

    //assemble full name with date and time in a path string and extension
    std::string path{ "E:\\data2.0\\" };
    std::string filename{ "DataRead " };
    std::string ext{ ".dat" };
    filename += stdStrNow;
    filename += ext;
    path += filename;
    //and get rid of the colon ':' (invalid character for Windows filenames) and replace space ' ' by underscore '_' (not necessary)
    //hours, minutes, seconds separator is a tilda '~'
    replace_all_after(0, path, ' ', "_");
    replace_all_after(3, path, ':', "~");

    std::ofstream filestm;
    filestm.open(path);

    for (int i =0; i<length; i++)
    {
        filestm << *(dDataPtr+2*i) << '\t' << *(dDataPtr+2*i+1) << '\n';
    }

    filestm.close();

    std::vector<double> ret{1.,2.,3.14};
    return ret;
}*/





void MainWindow::on_readValues_clicked()
{
    if(closedLoopCheck())
    {
        std::vector<double> positions {ScanAndCount_var->readAllPositions_Closed()};
        ui->readXVal->display(positions[0]);
        ui->setValX->setValue(positions[0]);
        ui->readYVal->display(positions[1]);
        ui->setValY->setValue(positions[1]);
        ui->readZVal->display(positions[2]);
        ui->setValZ->setValue(positions[2]);
    }
}

void MainWindow::on_STOPButton_clicked()
{

    ScanAndCount_var->on_STOPButton_clicked_scanandcount();

    //display positions after stopping
    on_readValues_clicked();
}

void MainWindow::on_loopX_stateChanged(int state)
{

    ScanAndCount_var->on_loopAXIS_stateChanged(state, Axis::X);
}

void MainWindow::on_loopY_stateChanged(int state)
{

    ScanAndCount_var->on_loopAXIS_stateChanged(state, Axis::Y);
}

void MainWindow::on_loopZ_stateChanged(int state)
{

    ScanAndCount_var->on_loopAXIS_stateChanged(state, Axis::Z);
}

void MainWindow::goToPosition(int xPos, int yPos, int zPos)
{
    ScanAndCount_var->moveAxisTo_VelLimit(Axis::X, xPos);
    ScanAndCount_var->moveAxisTo_VelLimit(Axis::Y, yPos);
    ScanAndCount_var->moveAxisTo_VelLimit(Axis::Z, zPos);
}

void MainWindow::on_setValX_editingFinished()
{
    if(closedLoopCheck())
    {
        int newPos = ui->setValX->value();
        ScanAndCount_var->moveAxisTo_VelLimit(Axis::X, newPos);
    }
}

void MainWindow::on_stepX_valueChanged(int newStep)
{
    ui->setValX->setSingleStep(newStep);
}

void MainWindow::on_setValY_editingFinished()
{
    if(closedLoopCheck())
    {
        int newPos = ui->setValY->value();
        ScanAndCount_var->moveAxisTo_VelLimit(Axis::Y, newPos);
    }
}

void MainWindow::on_stepY_valueChanged(int newStep)
{
    ui->setValY->setSingleStep(newStep);
}

void MainWindow::on_setValZ_editingFinished()
{
    if(closedLoopCheck())
    {
        int newPos = ui->setValZ->value();
        ScanAndCount_var->moveAxisTo_VelLimit(Axis::Z, newPos);
    }
}

void MainWindow::on_stepZ_valueChanged(int newStep)
{
    ui->setValZ->setSingleStep(newStep);
}

bool MainWindow::closedLoopCheck()
{
    if(ui->loopX->isChecked() && ui->loopY->isChecked() && ui->loopZ->isChecked())
    {
        return true;
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("One (or more) control loop(s) are open!");
        msgBox.exec();
        return false;
    }
}

void MainWindow::on_TwoDScanSetup_clicked()
{

    int L1 = ui->pxNbX->value();
    int L2 = ui->pxNbY->value();
    int steps1 = ui->pxSizeX->value();
    int steps2 = ui->pxSizeY->value();
    QString ScanAxis = ui->ScanAxis->currentText();

    int IDAxis1 = 1;
    int IDAxis2 = 2;

    if(ScanAxis.toStdString() == "XZ"){
        IDAxis1 = 1;
        IDAxis2 = 3;
    }else if(ScanAxis.toStdString() == "YZ"){
        IDAxis1 = 2;
        IDAxis2 = 3;
    }

    //std::cout << "Scan-Axis: " << ScanAxis.toStdString() << " IDAxis1: " << IDAxis1 << " IDAxis2: " << IDAxis2 << std::endl;


    TwoDScanSetup(L1, L2, steps1, steps2, IDAxis1, IDAxis2, ScanAxis);
}

void MainWindow::TwoDScanSetup(int L1, int L2, int steps1, int steps2, int IDAxis1, int IDAxis2, QString ScanAxis)
{
    ScanAndCount_var->on_TwoDScanSetup_clicked_scanandcount(L1, L2, steps1, steps2, IDAxis1, IDAxis2, ScanAxis);

    QString X_str = "X";
    QString Y_str = "Y";
    QString Z_str = "Z";

    if(ScanAxis.toStdString()=="XY"){
        WavGenTableLineFill(0, L1, steps1, X_str);
        WavGenTableLineFill(1, L2, steps2, Y_str);
        WavGenTableLineFill(2, 0, 0, Z_str);
    }else if(ScanAxis.toStdString()=="XZ"){
        WavGenTableLineFill(0, L1, steps1, X_str);
        WavGenTableLineFill(1, 0, 0, Y_str);
        WavGenTableLineFill(2, L2, steps2, Z_str);
    }else if(ScanAxis.toStdString()=="YZ"){
        WavGenTableLineFill(0, 0, 0, X_str);
        WavGenTableLineFill(1, L1, steps1, Y_str);
        WavGenTableLineFill(2, L2, steps2, Z_str);
    }
}



void MainWindow::on_TwoDScanStart_clicked()
{
    if(closedLoopCheck())
    {
        checkForLaser();
        on_setPermWord_clicked();
        TwoDScanStart();
    }
}

void MainWindow::TwoDScanStart()
{
    std::thread th(&ScanAndCount::on_TwoDScanStart_clicked_scanandcount, ScanAndCount_var);
    th.detach();
}


//Check if selected folder exist, if not create it
void MainWindow::doesPathExist_ifNotCreate()
{
    if (!std::filesystem::exists(globalPath))
    {
        std::cout << "Path " << globalPath << " does not exist!" << std::endl;
        if(std::filesystem::create_directory(globalPath)) {
                std::cout << "Path created" << std::endl;
        }else{
            std::cout << "unable to create Path" << std::endl;
        }
    }
}


void MainWindow::on_ThreeDScanStart_clicked()
{

    int Lz = ui->pxNbZ->value();
    int Zstep = ui->pxSizeZ->value();
    if(closedLoopCheck())
    {
        checkForLaser();
        on_setPermWord_clicked();
        ScanAndCount_var->set3DScanParameters(Lz, Zstep);
        std::thread th(&ScanAndCount::on_ThreeDScanStart_clicked_scanandcount, ScanAndCount_var);
        th.detach();
    }
}

void MainWindow::on_MosaicScanStart_clicked()
{
    return;
    int LeftPicturesMosaic = ui->PicturesMosaicLeft->value();
    int RightPicturesMosaic = ui->PicturesMosaicRight->value();
    int UpPicturesMosaic = ui->PicturesMosaicUp->value();
    int DownPicturesMosaic = ui->PicturesMosaicDown->value();

    std::vector<double> positions {ScanAndCount_var->readAllPositions_Closed()};
    if((positions[0]-ScanAndCount_var->currentScanParameters.Npx_X*ScanAndCount_var->currentScanParameters.Xstep*(LeftPicturesMosaic+0.5))<10000 ||
            (positions[0]-ScanAndCount_var->currentScanParameters.Npx_X*ScanAndCount_var->currentScanParameters.Xstep*(RightPicturesMosaic+0.5))>290000 ||
            (positions[1]-ScanAndCount_var->currentScanParameters.Npx_Y*ScanAndCount_var->currentScanParameters.Ystep*(DownPicturesMosaic+0.5))<10000 ||
            (positions[1]-ScanAndCount_var->currentScanParameters.Npx_Y*ScanAndCount_var->currentScanParameters.Ystep*(UpPicturesMosaic+0.5))>290000){
        std::cout << "Scan range bigger than range of piezo!" << std::endl;
        return;
    }
    if(closedLoopCheck())
    {
        ScanAndCount_var->setMosaicParameters(LeftPicturesMosaic, RightPicturesMosaic, UpPicturesMosaic, DownPicturesMosaic);
        std::thread th(&ScanAndCount::StartMosaicScan, ScanAndCount_var);
        th.detach();
    }
}

void MainWindow::on_SetPath_clicked()
{
    QString directoryToOpen = QFileDialog::getExistingDirectory(this, "Open directory" , QString::fromStdString(globalPath.string()));
    if(!directoryToOpen.isEmpty()&& !directoryToOpen.isNull()){
        globalPath = directoryToOpen.toStdString();
        ScanAndCount_var->setGlobalPath(globalPath);
        PulsedMeasurementsAndData_var->setGlobalPath(globalPath);
        ui->ShowPath->setText(directoryToOpen);
    }
}

void MainWindow::on_Bleaching_clicked()
{
    int xSteps = ui->pxSizeX_bleaching->value();
    int ySteps = ui->pxSizeY_bleaching->value();
    int time = ui->time_bleaching->value();
    ScanAndCount_var->bleaching(xSteps, ySteps, time*60*1000);
}



//###############################################################################################################################################
//##                                                                                                                                           ##
//##                                                Scan Visualization Code                                                                    ##
//##                                                                                                                                           ##
//###############################################################################################################################################

//slot function through which the ScanAndCount instance commands the MainWindow to plot ( connected to signal readyTo2DPlot() )

void MainWindow::plotAfterDataAcq()
{
    //rescale the data dimension (color) such that all data points lie in the span visualized by the color gradient
    //colorMap data has already been defined
    ui->QCPimageWROI->colorMap->rescaleDataRange();
    //rescale the key (x) and value (y) axes so the whole color map is visible
    ui->QCPimageWROI->rescaleAxes();

    ui->QCPimageWROI->replot();


    //fill vectors containing parameter/abscissa for plotting along crosshair lines
    //these vectors stay constant for a given aquired image
    fillKeyVec();
    fillValueVec();
    //set the ranges for the parameter/abscissa axis (x resp. y for plot along horizontal resp. vertical);
    ui->horizontalLine->xAxis->setRange(ui->QCPimageWROI->colorMap->data()->keyRange());
    ui->verticalLine->yAxis->setRange(ui->QCPimageWROI->colorMap->data()->valueRange());

}


//--------------------------
// functions for crosshair
//--------------------------

void MainWindow::enableCrosshair()
{
    ui->QCPimageWROI->enableCrosshair();
    connect(ui->QCPimageWROI->lineH, SIGNAL(lineMoved()), this, SLOT(replotAlongHLine()));
    connect(ui->QCPimageWROI->lineV, SIGNAL(lineMoved()), this, SLOT(replotAlongVLine()));
}

void MainWindow::disableCrosshair()
{
    ui->QCPimageWROI->disableCrosshair();
}

void MainWindow::fillKeyVec()
{
    int sizeKeys = ui->QCPimageWROI->colorMap->data()->keySize();
    keyVec.clear();
    keyVec.resize(sizeKeys); //QVector resize() function sets added values to 0 (and no way to set it to another value)
    double key;
    for (int i = 0; i<sizeKeys;++i)
    {
        ui->QCPimageWROI->colorMap->data()->cellToCoord(i, 1, &key, 0);
        keyVec[i] = key;
    }
}

void MainWindow::fillValueVec()
{
    int sizeValues = ui->QCPimageWROI->colorMap->data()->valueSize();
    valueVec.clear();//not really needed but to be sure
    valueVec.resize(sizeValues); //QVector resize() function sets added values to 0 (and no way to set it to another value)
    double value;
    for (int j = 0; j<sizeValues;++j)
    {
        ui->QCPimageWROI->colorMap->data()->cellToCoord(1, j, 0, &value);
        valueVec[j] = value;
    }
}

QVector<double> MainWindow::getVData()
{
    QVector<double> yVec;
    // need to get the cell index (from 0 to keySize-1 ) corresponding to the coordinate position of the vertical line
    // first get the graph coordinates (i.e. a double) from point1 of the vertical line, then get the cell number of that point
    //double key, value;
    int cellPos_VLine;
    QPointF Pt1 {ui->QCPimageWROI->lineV->point1->coords()};
    //ui->QCPimageWROI->colorMap->pixelsToCoords(ui->QCPimageWROI->lineV->point1->coords(), key, value);
    ui->QCPimageWROI->colorMap->data()->coordToCell(Pt1.x(), Pt1.y(), &cellPos_VLine, 0);
    //fill the vector with all the cell values along the vertical line
    for (int i = 0; i < ui->QCPimageWROI->colorMap->data()->valueSize(); ++i)
    {
        yVec.push_back(ui->QCPimageWROI->colorMap->data()->cell(cellPos_VLine, i));
    }
    return yVec;
}

QVector<double> MainWindow::getHData()
{
    QVector<double> xVec;
    // need to get the index (from 0 to valueSize -1 ) corresponding to the pixel position of the horizontal line
    // first get the graph coordinates (i.e. a double) from point1 of the horizontal line, then get the cell number of that point
    //double key, value;
    int cellPos_HLine;
    QPointF Pt1{ui->QCPimageWROI->lineH->point1->coords()};
    //ui->QCPimageWROI->colorMap->pixelsToCoords(ui->QCPimageWROI->lineH->point1->coords(), key, value);
    ui->QCPimageWROI->colorMap->data()->coordToCell(Pt1.x(), Pt1.y(), 0, &cellPos_HLine);
    //fill the vector with all the cell values along the horizontal line
    for (int i = 0; i < ui->QCPimageWROI->colorMap->data()->keySize(); ++i)
    {
        xVec.push_back(ui->QCPimageWROI->colorMap->data()->cell(i, cellPos_HLine));
    }
    return xVec;
}

void MainWindow::replotAlongHLine()
{
    QVector<double> HData = getHData();
    double Hmax = *std::max_element(HData.constBegin(), HData.constEnd());
    double Hmin = *std::min_element(HData.constBegin(), HData.constEnd());
    ui->horizontalLine->yAxis->setRange(Hmin, Hmax);
    ui->horizontalLine->graph(0)->setData(keyVec, HData);
    ui->horizontalLine->replot();
}

void MainWindow::replotAlongVLine()
{
    QVector<double> VData = getVData();
    double Vmax = *std::max_element(VData.constBegin(), VData.constEnd());
    double Vmin = *std::min_element(VData.constBegin(), VData.constEnd());
    ui->verticalLine->xAxis->setRange(Vmin, Vmax);
    ui->verticalLine->graph(0)->setData(valueVec, VData);
    ui->verticalLine->replot();
}

void MainWindow::on_crossHairBox_stateChanged(int newState)
{
    if(newState)
    {
        enableCrosshair();
    }
    else if (ui->QCPimageWROI->lineH != nullptr && ui->QCPimageWROI->lineV != nullptr)
    {
        disableCrosshair();
    }
}


//-----------------------------------------
// functions for Regions of Interest (ROI)
//-----------------------------------------

void MainWindow::moveToPosition(double xPos, double yPos)
{
    int newPositionX =  xPos*1000.;
    int newPositionY =  yPos*1000.;

    //actually move the stage to the center of the ROI
    ScanAndCount_var->moveAxisTo_VelLimit(Axis::X, newPositionX);
    ScanAndCount_var->moveAxisTo_VelLimit(Axis::Y, newPositionY);
}

void MainWindow::on_setROILegend_clicked(bool checked)
{
    ui->QCPimageWROI->enableROILegend(checked);
    ui->QCPimageWROI->replot();
}

void MainWindow::on_addROI_clicked()
{
    int n = ui->QCPimageWROI->ROInumber();

    double xstep = ui->QCPimageWROI->singleCellStepX();
    double ystep = ui->QCPimageWROI->singleCellStepY();
    int lx = ui->QCPimageWROI->colorMap->data()->keySize();
    int ly = ui->QCPimageWROI->colorMap->data()->valueSize();
    double Xcenter = ui->QCPimageWROI->colorMap->data()->keyRange().lower + (lx/5)*xstep + (n%4)*(lx/5)*xstep;
    double Ycenter = ui->QCPimageWROI->colorMap->data()->valueRange().lower + (ly/5)*ystep + (n/4)*(lx/5)*ystep;

    QPointF center{Xcenter, Ycenter};
    // use pixel to set ROI's
    // use micrometer to set ROI's
    ui->QCPimageWROI->addROI(center, 0.3, 0.3);

    ui->QCPimageWROI->replot();
}

void MainWindow::on_removeROI_clicked()
{
    ui->QCPimageWROI->removeROI();
    ui->QCPimageWROI->replot();
}

void MainWindow::on_goToRoiButton_clicked()
{
    int i = ui->ROInumber->value();

    if (i<1 || i>(ui->QCPimageWROI->ROInumber()))
    {
        std::cout << "ROI index invalid !!!" << std::endl;
        return;
    }
    else if (ui->QCPimageWROI->ROInumber() == 0)
    {
        std::cout << "No ROI to go to !!!" << std::endl;
        return;
    }

    int newPositionX =  ui->QCPimageWROI->getROIposition(i).x()*1000.;
    int newPositionY =  ui->QCPimageWROI->getROIposition(i).y()*1000.;
    std::cout << "new position: " << newPositionX << "/" << newPositionY << std::endl;

    //actually move the stage to the center of the ROI
    ScanAndCount_var->moveAxisTo_VelLimit(Axis::X, newPositionX);
    ScanAndCount_var->moveAxisTo_VelLimit(Axis::Y, newPositionY);

    //put a "You are here" label where the stage is
    ui->QCPimageWROI->setYouAreHereLabel(i);
}

void MainWindow::on_resetAxes_clicked()
{
    ui->QCPimageWROI->xAxis->setRange(ui->QCPimageWROI->colorMap->data()->keyRange());
    ui->QCPimageWROI->yAxis->setRange(ui->QCPimageWROI->colorMap->data()->valueRange());

    //keep position of "you are here !" sign with respect to ROI
    if (ui->QCPimageWROI->YouAreHere->visible())
    {
        QPointF here = ui->QCPimageWROI->stagePosition->position->pixelPosition() - QCPRectROI::label_offset;
        ui->QCPimageWROI->YouAreHere->position->setCoords(here);
    }
    //keep position of size label with respect to the ROI
    for (auto ROI : ui->QCPimageWROI->ROIStore)
    {
        QPointF sizepos = ROI->bottom->pixelPosition() - QCPRectROI::label_offset;
        ROI->sizeLabel->position->setCoords(sizepos);
    }

    ui->QCPimageWROI->replot();
}

void MainWindow::on_setMaxPlot_clicked()
{
    double maxValPLot = ui->maxValPlot->value();
    QCPRange r(0, maxValPLot);
    ui->QCPimageWROI->colorMap->setDataRange(r);
    ui->QCPimageWROI->replot();
}

void MainWindow::on_setMaxPlotAuto_clicked()
{
    ui->QCPimageWROI->colorMap->rescaleDataRange();
    ui->QCPimageWROI->replot();
}

void MainWindow::on_PDFsave_clicked()
{
    QString Path = QString::fromStdString(ScanAndCount_var->currentImagePath);
    QString Name = QString::fromStdString(ScanAndCount_var->currentImageDate_yyyymmddHHMMSS);
    ui->QCPimageWROI->savePdf(Path+"\\"+Name+".pdf");
}

void MainWindow::on_PNGsave_clicked()
{
    QString Path = QString::fromStdString(ScanAndCount_var->currentImagePath);
    QString Name = QString::fromStdString(ScanAndCount_var->currentImageDate_yyyymmddHHMMSS);
    ui->QCPimageWROI->savePng(Path+"\\"+Name+".png");
}

void MainWindow::on_LoadDATFile_clicked()
{
    //LoadFileCreatMosaic_var->loadDataFromZScan("E:/data/20200515/Z-Scan1");
    QString fileToOpen = QFileDialog::getOpenFileName(this, "Open file" , QString::fromStdString(globalPath.string()));
    if(!fileToOpen.isEmpty()&& !fileToOpen.isNull()){
        LoadFileCreatMosaic_var->loadDataFromSingleScan(fileToOpen.toStdString());
        plotAfterDataAcq();
        ScanAndCount_var->currentImageDate_yyyymmddHHMMSS = LoadFileCreatMosaic_var->getFileNameOfLoaded2D();
        ScanAndCount_var->currentImagePath = LoadFileCreatMosaic_var->getPathOfLoaded2D();
    }
}

void MainWindow::on_WaveTableRateButton_clicked()
{
    int pxTimeInMillisec = ui->WTRpixelTime->value();
    if(pxTimeInMillisec<minTimePerPtInMillisec)
    {
        std::cout << "Time too short!\nPiezo table has a significant chance of not stabilising to positions." << std::endl;
        ui->WTRpixelTime->setValue(lround(minTimePerPtInMillisec));
    }
    else
    {
        double WTR_inServoCycles = ((double)pxTimeInMillisec)/servoCycleTimeInMillisec;
        std::cout << "Setting Wave Table Rate to "  << lround(WTR_inServoCycles) << " cycles." << std::endl;
        ScanAndCount_var->setWaveTableRate(lround(WTR_inServoCycles));
    }
}

void MainWindow::on_LoadDatFile3DScan_clicked()
{
    QString directoryToOpen = QFileDialog::getExistingDirectory(this, "Open directory" , QString::fromStdString(globalPath.string()));
    if(!directoryToOpen.isEmpty()&& !directoryToOpen.isNull()){
        LoadFileCreatMosaic_var->loadDataFromZScan(directoryToOpen.toStdString());
    }
}

void MainWindow::on_setMaxPlot_3D_clicked()
{
    double maxValPLot = ui->maxValPlot_3D->value();
    QCPRange r(0, maxValPLot);
    ui->image3DPlot->colorMap->setDataRange(r);
    ui->image3DPlot->replot();
}

void MainWindow::on_setMaxPlot_3D_auto_clicked()
{
    std::cout << "Range: lower=" << ui->image3DPlot->colorMap->dataRange().lower << ", upper=" << ui->image3DPlot->colorMap->dataRange().upper;

    ui->image3DPlot->colorMap->rescaleDataRange();
    ui->image3DPlot->replot();
}

void MainWindow::on_PNGsave3D_clicked()
{
    QString Path = QString::fromStdString(LoadFileCreatMosaic_var->getPathOfLoaded3D());
    QString Date = QString::fromStdString(LoadFileCreatMosaic_var->getFileNameOfLoaded3D());
    ui->image3DPlot->savePng(Path+"\\"+Date+".png");
}

void MainWindow::on_PDFsave3D_clicked()
{
    QString Path = QString::fromStdString(LoadFileCreatMosaic_var->getPathOfLoaded3D());
    QString Date = QString::fromStdString(LoadFileCreatMosaic_var->getFileNameOfLoaded3D());
    ui->image3DPlot->savePdf(Path+"\\"+Date+".pdf");
}

void MainWindow::plot2DData(int Lx, int NumbersOfPixels){
    int pxIndex = 0;

    if(NumbersOfPixels<Lx){
        return;
    }else{
        //IMPORTANT : this is where the data is passed to the 2D plotting widget
        for(int Yint = 0; Yint<ceil(NumbersOfPixels/Lx); ++Yint)
        {
            for(int Xint = (Yint%2==0? 0: Lx-1); Xint>=0 && Xint<Lx;  Yint%2==0? ++Xint: --Xint)//zigzag scanning
            {
                ui->QCPimageWROI->colorMap->data()->setCell(Xint, Yint, ScanAndCount_var->imageVec[pxIndex]);
                ++pxIndex;
            }
        }
        ui->QCPimageWROI->replot();
        return;
    }
}


// -------------------------------------------
// -------------- Frequency Tab --------------
// -------------------------------------------

void MainWindow::on_STOPbutton_clicked()
{
    M8195A_var->stopGen();
}

void MainWindow::on_RUNbutton_clicked()
{
    PostionForPulsedMeasurments();
    M8195A_var->run();
}

void MainWindow::on_triggerMode_currentIndexChanged(const QString &mode)
{
    M8195A_var->setTriggermode(mode);
}

void MainWindow::startFrequencySequence(QString typeOfMeasurement)
{
    if(typeOfMeasurement == "cwODMR"){
        PulsedMeasurementsAndData_var->prepareCounterForODMR();
    }else if(typeOfMeasurement == "Rabi"){
        PulsedMeasurementsAndData_var->prepareCounterForRabi();
    }else if(typeOfMeasurement == "pulsedODMR"){
        PulsedMeasurementsAndData_var->prepareCounterForPulsedODMR();
    }
    // start to run
    PostionForPulsedMeasurments();
    M8195A_var->run();
    // start output
    M2i7005_var->M2iOutputData();
    // stop
    M8195A_var->stopGen();

    emit frequencyMeasurementFinished(typeOfMeasurement);
}

void MainWindow::frequencyUploadDone(QString typeOfMeasurement){
    QMessageBox cwODMR_MB;
    cwODMR_MB.setWindowTitle("Sora");
    cwODMR_MB.setText("I uploaded all frequencies for the " + typeOfMeasurement + " measurement. You can measure now.");
    cwODMR_MB.exec();
    ui->measureType->setText("cw ODMR upload done");
}

void MainWindow::frequencyMeasurementDone(QString typeOfMeasurement)
{
    QMessageBox cwODMR_MB;
    cwODMR_MB.setWindowTitle("Sora");
    cwODMR_MB.setText("I finished the " + typeOfMeasurement + " measurement. You can look at it now.");
    cwODMR_MB.exec();
    ui->measureType->setText("cw ODMR measurement done");
}


//#####################################################################
//############################## cw ODMR ##############################
//#####################################################################

void MainWindow::on_loadWaveformAndSeqTable_clicked()
{
    int loops = ui->loopNbWaveForm->value();
    double excite = ui->excTime->value();
    double Fstart = ui->scanStartFreq->value();
    double Fend = ui->scanStopFreq->value();
    int stepNb = ui->scanStepNb->value();


    loadWaveformAndSeqTable(loops, excite, Fstart, Fend, stepNb);
}

// prepare
void MainWindow::loadWaveformAndSeqTable(int loopNb, double excitTime, double freqStart, double freqEnd, int stepNb)
{
    if(checkClickedButton(0, "prep")){
        return;
    }

    ui->measureType->setText("cw ODMR upload in process");

    PulsedMeasurementsAndData_var->currentODMRParams.loopNb=loopNb;
    PulsedMeasurementsAndData_var->currentODMRParams.stepNb=stepNb+1;
    PulsedMeasurementsAndData_var->currentODMRParams.FreqEnd=freqEnd;
    PulsedMeasurementsAndData_var->currentODMRParams.FreqStart=freqStart;
    PulsedMeasurementsAndData_var->currentODMRParams.excitTime=excitTime;

    // upload cw ODMR
    std::thread uploadCwODMRThread(&arrayPreparationsForPulsedMeasurements::prepareCwODMR, ArrayPrepForPulsedMeasurements_var, loopNb, excitTime, freqStart, freqEnd, stepNb, M2i7005_var, M8195A_var);
    uploadCwODMRThread.detach();

    //ArrayPrepForPulsedMeasurements_var->prepareCwODMR(loopNb, excitTime, freqStart, freqEnd, stepNb, M2i7005_var, M8195A_var);
/*
    ViConstString chan = "1";

    auto frequencies = linearFreqArray(freqStart*MHz_M8195A, freqEnd*MHz_M8195A, stepNb);

    std::vector<double> times (frequencies.size(), excitTime*microsec_M8195A);
    std::vector<double> phases (frequencies.size(), 0);
    std::vector<double> amplitudes (frequencies.size(), 1);

    ViStatus status = M8195A_var->loadSineArrayWaveformAndSeqTable(chan, frequencies, maxSampleRate, times, loopNb, phases, amplitudes);
*/

    //emit StateAIODMRUpload(false);
}

void MainWindow::on_StartODMR_clicked()
{

    if(checkClickedButton(0, "start")){
        return;
    }

    ui->measureType->setText("cw ODMR measurement in process");
    std::thread startCwODMRThread(&MainWindow::startFrequencySequence, this, "cwODMR");
    startCwODMRThread.detach();
    //startFrequencySequence("cwODMR");

    //prepareWavegenForODMR();
    /*PulsedMeasurementsAndData_var->prepareCounterForODMR();
    //on_RUNbutton_clicked();
    PostionForPulsedMeasurments();
    M8195A_var->run();
    M2i7005_var->M2iOutputData();
    //on_outputM2i_clicked();
    on_STOPbutton_clicked();*/
}

/*
//###
void MainWindow::prepareWavegenForODMR()
{

    int actChN = next_power_of_2(6);
    std::cout << next_power_of_2(6) << " activated channels" << std::endl;

    int64 mask;

    switch(actChN)
    {
        case 1: mask = (int64)1; break;
        case 2: mask = (int64)0x3; break;
        case 4: mask = (int64)0xF; break;
        case 8: mask = (int64)0xFF; break;
        case 16: mask = (int64)0xFFFF; break;
        default: std::cout << "Error: Channel mask is not allowed !" << std::endl; return;
    }

    int loop_nb = ui->loopNbWaveForm->value();
    int frequency_MHz = (int)(1000./1000);
    //int length_in_samples = lround(10047) + 1;  // ~10.047 µs

    int numbOfSteps = (PulsedMeasurementsAndData_var->currentODMRParams.excitTime+10)*PulsedMeasurementsAndData_var->currentODMRParams.stepNb;
    int length_in_samples;
    if(numbOfSteps%64==0){
        length_in_samples=numbOfSteps;
    }else{
        length_in_samples=64*(numbOfSteps/64+1);
    }

    std::cout << "loop_np: " << loop_nb << ", frequency_MHz: " << frequency_MHz << " length_in_samples: " << length_in_samples << std::endl;

    M2i7005_var->M2iSetup_StdReplay(mask, frequency_MHz, false, false, loop_nb, length_in_samples);
    //######### End: on_confM2i_clicked ###########

    std::vector< std::vector<int>> risingEdges;
    std::vector< std::vector<int>> fallingEdges;
    CreateCW_ODMRSequence(risingEdges, fallingEdges);
    M2i7005_var->M2iLoadIntoMemoryFromVec(6, risingEdges, fallingEdges);
}*/

/*
//###
void MainWindow::CreateCW_ODMRSequence(std::vector< std::vector<int>>& risingEdges,  std::vector< std::vector<int>>& fallingEdges)
{
    std::vector<int> rising1Temp;
    std::vector<int> falling1Temp;
    std::vector<int> rising2Temp;
    std::vector<int> falling2Temp;
    std::vector<int> risingLaserEnabled;
    std::vector<int> fallingLaserEnabled;
    std::vector<int> risingLaserDigital;
    std::vector<int> fallingLaserDigital;

    for(int i = 0; i<PulsedMeasurementsAndData_var->currentODMRParams.stepNb; ++i){
        //if(i>0)
        {
            rising1Temp.push_back(i*PulsedMeasurementsAndData_var->currentODMRParams.excitTime);
            falling1Temp.push_back(i*PulsedMeasurementsAndData_var->currentODMRParams.excitTime+10);

            risingLaserEnabled.push_back(i*PulsedMeasurementsAndData_var->currentODMRParams.excitTime);
            risingLaserDigital.push_back(i*PulsedMeasurementsAndData_var->currentODMRParams.excitTime);
            fallingLaserEnabled.push_back((i+1)*PulsedMeasurementsAndData_var->currentODMRParams.excitTime-5);
            fallingLaserDigital.push_back((i+1)*PulsedMeasurementsAndData_var->currentODMRParams.excitTime-5);
        }
        rising2Temp.push_back(i*PulsedMeasurementsAndData_var->currentODMRParams.excitTime+10);
        falling2Temp.push_back(i*PulsedMeasurementsAndData_var->currentODMRParams.excitTime+PulsedMeasurementsAndData_var->currentODMRParams.excitTime-10);
    }
    risingEdges.push_back({0});
    fallingEdges.push_back({10});
    risingEdges.push_back(rising1Temp);
    fallingEdges.push_back(falling1Temp);
    risingEdges.push_back(rising2Temp);
    fallingEdges.push_back(falling2Temp);
    risingEdges.push_back(risingLaserEnabled);
    fallingEdges.push_back(fallingLaserEnabled);
    risingEdges.push_back(risingLaserDigital);
    fallingEdges.push_back(fallingLaserDigital);
    risingEdges.push_back({0});
    fallingEdges.push_back({10});

}*/

void MainWindow::on_ReadDataODMR_clicked()
{
    PulsedMeasurementsAndData_var->typeOfMeasurement = "ODMR";
    if(checkClickedButton(0, "read")){
        return;
    }

    curDataWidget = ui->ODMRPlot;
    PulsedMeasurementsAndData_var->fetchAndProcessDataForODMR();

    /*
    QMessageBox cwODMRread_MB;
    cwODMRread_MB.setWindowTitle("Sora");
    int nbPeaks = sora_var->evalFit_var->nbOfPeaks(ODMRXValues, normedODMRYValues);
    if(nbPeaks == 1){
        cwODMRread_MB.setText("I found a Peak. Do you want me to fit a loretzian?");
        cwODMRread_MB.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        int ret = cwODMRread_MB.exec();
        if(ret == QMessageBox::Yes){
            on_SingleLorentzianFit_clicked();
        }
    }else if(nbPeaks == 2){
        cwODMRread_MB.setText("I found two Peaks. Do you want me to fit a double loretzian?");
        cwODMRread_MB.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        int ret = cwODMRread_MB.exec();
        if(ret == QMessageBox::Yes){
            on_DoubleLorentzianFit_clicked();
        }
    }*/
}

//#####################################################################
//################################ Rabi ###############################
//#####################################################################

// We use 10 ns timesteps for the DI/O-card
void MainWindow::on_PrepareRabi_clicked()
{
    double freq = ui->FreqForRabi->value();
    int MWsteps = ui->MWStepsForRabi->value()+1;
    double MWstepsize = ui->MWStepsSizeForRabi->value();
    double timeCounter = ui->TimeCounterForRabi->value();
    double timeLaser = ui->LaserForRabi->value();
    int loops = ui->LoopsForRabi->value();
    double rabi0 = ui->Rabi0->value();
    double amp = ui->AmpForRabi->value();
    int withRefocus = 0;

    PrepareRabi(freq, MWsteps, MWstepsize, timeCounter, timeLaser, loops, rabi0, amp, withRefocus);
    return;

    //int numbOfSteps = (int)((delayLaserEnableRising_ns+(300+PulsedMeasurementsAndData_var->currentRabiParams.MinRabiTime)*(MWstepsize+1)+(((MWstepsize+1)*((MWstepsize)+2)/2)*MWstepsize))/10)+50;
    //ui->RabiApproxTime->display((double)(numbOfSteps)*(double)(ui->LoopsForRabi->value())*1e-8/60);

   //############# AWG needs to be prepared !!!!!!!!!!! #############
/**/
    int actChN = next_power_of_2(6);
    std::cout << next_power_of_2(6) << " activated channels" << std::endl;

    int64 mask;

    switch(actChN)
    {
        case 1: mask = (int64)1; break;
        case 2: mask = (int64)0x3; break;
        case 4: mask = (int64)0xF; break;
        case 8: mask = (int64)0xFF; break;
        case 16: mask = (int64)0xFFFF; break;
        default: std::cout << "Error: Channel mask is not allowed !" << std::endl; return;
    }
/**/
    //int length_in_samples = lround(10047) + 1;  // ~10.047 µs

    /*
    int RabiSteps=ui->MWStepsForRabi->value();
    RabiTime0=ui->Rabi0->value();

    PulsedMeasurementsAndData_var->currentRabiParams.MinRabiTime=(waitingTimeAfterPolarization+PulsedMeasurementsAndData_var->currentRabiParams.TimeLaser+RabiTime0+PulsedMeasurementsAndData_var->currentRabiParams.TimeCounter+500);

    int numbOfSteps = (int)((delayLaserEnableRising_ns+(300+PulsedMeasurementsAndData_var->currentRabiParams.MinRabiTime)*(RabiSteps+1)+(((RabiSteps+1)*((RabiSteps)+2)/2)*PulsedMeasurementsAndData_var->currentRabiParams.Stepsize))/10)+50;
    int length_in_samples;
    if(numbOfSteps%64==0){
        length_in_samples=numbOfSteps;
    }else{
        length_in_samples=64*(numbOfSteps/64+1);
    }

    std::cout << "loop_np: " << PulsedMeasurementsAndData_var->currentRabiParams.LoopNb << ", frequency_MHz: " << 100 << " length_in_samples: " << length_in_samples << std::endl;
    std::cout << "MinRabiTime: " << PulsedMeasurementsAndData_var->currentRabiParams.MinRabiTime << ", numbOfSteps: " << numbOfSteps << std::endl;

    M2i7005_var->M2iSetup_StdReplay(mask, 100, false, false, PulsedMeasurementsAndData_var->currentRabiParams.LoopNb, length_in_samples);
    */
/**/
    std::vector< std::vector<int>> risingEdges;
    std::vector< std::vector<int>> fallingEdges;
    int numbOfSteps = CreateRabiSequence(risingEdges, fallingEdges);
    int length_in_samples;

    if(numbOfSteps%64==0){
        length_in_samples=numbOfSteps;
    }else{
        length_in_samples=64*(numbOfSteps/64+1);
    }
    M2i7005_var->M2iSetup_StdReplay(mask, 100, false, false, PulsedMeasurementsAndData_var->currentRabiParams.LoopNb, length_in_samples);
    M2i7005_var->M2iLoadIntoMemoryFromVec(6, risingEdges, fallingEdges);

    CreateRabiFrequencies(PulsedMeasurementsAndData_var->currentRabiParams.Freq, PulsedMeasurementsAndData_var->currentRabiParams.Steps, PulsedMeasurementsAndData_var->currentRabiParams.Stepsize, PulsedMeasurementsAndData_var->currentRabiParams.LoopNb);

    ui->RabiApproxTime->display((double)(numbOfSteps)*(double)(ui->LoopsForRabi->value())*1e-8/60);
/**/

//    ui->StartRabi->setEnabled(true);
}

void MainWindow::PrepareRabi(double freq, int MWsteps, double MWstepsize, double timeCounter, double timeLaser, int loops, double rabi0, double amp, int withRefocus)
{
    if(checkClickedButton(1, "prep")){
        return;
    }

    PulsedMeasurementsAndData_var->currentRabiParams.Freq = freq;
    PulsedMeasurementsAndData_var->currentRabiParams.Steps = MWsteps;
    PulsedMeasurementsAndData_var->currentRabiParams.Stepsize = MWstepsize;
    PulsedMeasurementsAndData_var->currentRabiParams.TimeCounter = timeCounter;
    PulsedMeasurementsAndData_var->currentRabiParams.TimeLaser = timeLaser;
    PulsedMeasurementsAndData_var->currentRabiParams.LoopNb = loops;
    PulsedMeasurementsAndData_var->currentRabiParams.Rabi0Time = rabi0;
    PulsedMeasurementsAndData_var->currentRabiParams.Amp = amp;
    PulsedMeasurementsAndData_var->withRefocus = withRefocus;

    // upload Rabi sequence
    int length_in_samples = ArrayPrepForPulsedMeasurements_var->prepareRabi(freq, MWsteps, MWstepsize, timeCounter, timeLaser, loops, rabi0, amp, M2i7005_var, M8195A_var);
    ArrayPrepForPulsedMeasurements_var->createRabiFrequencies(freq, MWsteps, MWstepsize, loops, amp, M8195A_var);

    ui->RabiApproxTime->display((double)(length_in_samples)*(double)(loops)*1e-8/60);
    //std::thread uploadRabiThread(&arrayPreparationsForPulsedMeasurements::prepareRabi, ArrayPrepForPulsedMeasurements_var, freq, MWsteps, MWstepsize, timeCounter, timeLaser, loops, rabi0, amp, M2i7005_var, M8195A_var);
    //uploadRabiThread.detach();

}

// T1 messung!!
int MainWindow::CreateRabiSequence(std::vector< std::vector<int>>& risingEdges,  std::vector< std::vector<int>>& fallingEdges)
{
    std::vector<int> risingAWGTrigger;
    std::vector<int> fallingAWGTrigger;
    std::vector<int> risingAWGEvent;
    std::vector<int> fallingAWGEvent;
    std::vector<int> risingCounter;
    std::vector<int> fallingCounter;
    std::vector<int> risingLaserDigital;
    std::vector<int> fallingLaserDigital;
    std::vector<int> risingLaserEnable;
    std::vector<int> fallingLaserEnable;

    int TimeLaser=PulsedMeasurementsAndData_var->currentRabiParams.TimeLaser;
    int Steps = PulsedMeasurementsAndData_var->currentRabiParams.Steps;
    int Stepsize = PulsedMeasurementsAndData_var->currentRabiParams.Stepsize;
    int TimeCounter = PulsedMeasurementsAndData_var->currentRabiParams.TimeCounter;
    int WaitBetweenMWAndReadout = PulsedMeasurementsAndData_var->currentRabiParams.Rabi0Time;

    int TimePrevSeg=0;

    //Trigger for AWG; minus shift of 70 ns
    risingAWGTrigger.push_back((int)((TimeLaser-delayAWG_ns+delayLaserEnableRising_ns-20)/10));
    fallingAWGTrigger.push_back((int)((TimeLaser+5-delayAWG_ns+delayLaserEnableRising_ns-20)/10));

    for(int i = 0; i<(Steps); ++i){

        //Signal for Laser Enable (init)
        if(i==0){
            risingLaserEnable.push_back((int)(TimePrevSeg)/10);
        }
        fallingLaserEnable.push_back((int)((TimeLaser+TimePrevSeg)/10));

        //Signal for Laser Digital (init)
        if(i==0){
            risingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+TimePrevSeg)/10));
        }
        fallingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+TimeLaser+TimePrevSeg)/10));


        //Event for AWG; minus shift of 70 ns
        risingAWGEvent.push_back((int)((waitingTimeAfterPolarization+TimeLaser-delayAWG_ns+delayLaserEnableRising_ns-20+TimePrevSeg)/10));
        fallingAWGEvent.push_back((int)((waitingTimeAfterPolarization+TimeLaser+5-delayAWG_ns+delayLaserEnableRising_ns-20+TimePrevSeg)/10));

        //##########  Added 500 ns for all following steps, due to get 1µs between MW and readout !!! ###################

        //Signal for TT; additional shift of 50 ns
        risingCounter.push_back((int)((waitingTimeAfterPolarization+TimeLaser+WaitBetweenMWAndReadout+Stepsize*i+delayLaserEnableRising_ns-delayLaserDigital_ns+50+TimePrevSeg)/10)+1);
        fallingCounter.push_back((int)((waitingTimeAfterPolarization+TimeLaser+WaitBetweenMWAndReadout+Stepsize*i+delayLaserEnableRising_ns-delayLaserDigital_ns+50+TimeCounter+TimePrevSeg)/10)+1);

        //Signal for Laser Enable (readout) additional shift of 20 ns
        risingLaserEnable.push_back((int)((waitingTimeAfterPolarization+TimeLaser+WaitBetweenMWAndReadout+Stepsize*i+TimePrevSeg+20)/10)+1);
        if(i==(Steps-1)){
            fallingLaserEnable.push_back((int)((waitingTimeAfterPolarization+TimeLaser+WaitBetweenMWAndReadout+Stepsize*i+delayLaserEnableRising_ns-delayLaserEnableFalling_ns+20+TimeCounter+TimePrevSeg)/10)+1);
        }

        //Signal for Laser Digital (readout) additional shift of 20 ns
        risingLaserDigital.push_back((int)((waitingTimeAfterPolarization+TimeLaser+WaitBetweenMWAndReadout+Stepsize*i+delayLaserEnableRising_ns-delayLaserDigital_ns+20+TimePrevSeg)/10)+1);
        if(i==(Steps-1)){
            fallingLaserDigital.push_back((int)((waitingTimeAfterPolarization+TimeLaser+WaitBetweenMWAndReadout+Stepsize*i+delayLaserEnableRising_ns-delayLaserDigital_ns+20+TimeCounter+TimePrevSeg)/10)+1);
        }

        TimePrevSeg=*std::max_element(fallingCounter.begin(),fallingCounter.end())*10;
    }

    risingEdges.push_back({0});
    fallingEdges.push_back({100});
    risingEdges.push_back(risingAWGEvent);
    fallingEdges.push_back(fallingAWGEvent);
    risingEdges.push_back(risingCounter);
    fallingEdges.push_back(fallingCounter);
    risingEdges.push_back(risingLaserEnable);
    fallingEdges.push_back(fallingLaserEnable);
    risingEdges.push_back(risingLaserDigital);
    fallingEdges.push_back(fallingLaserDigital);
    risingEdges.push_back(risingAWGTrigger);
    fallingEdges.push_back(fallingAWGTrigger);

    return TimePrevSeg/10;
}


void MainWindow::CreateRabiFrequencies(double Freq, int stepNb, int Stepsize, int loops){

    ViConstString chan = "1";

    // Create frequency-array
    auto frequencies = linearFreqArray(Freq*MHz_M8195A, Freq*MHz_M8195A, stepNb-1);

    // Create times-array
    std::vector<double> times (frequencies.size(), 1*microsec_M8195A);

    double FreqToZerro = 0;
    for(int i=0; i<times.size(); ++i){
        times[i]=(Stepsize/1000.0*i)*microsec_M8195A;
    }
    std::vector<double> phases (frequencies.size(), 0);
    std::vector<double> amplitudes (frequencies.size(), ui->AmpForRabi->value());

    std::cout << "Amplitude: " << amplitudes[0] << std::endl;

    ViStatus status = M8195A_var->loadSineArrayWaveformAndSeqTable(chan, frequencies, maxSampleRate, times, loops, phases, amplitudes);
//    ViStatus status = M8195A_var->loadSineArrayWaveformAndSeqTable(chan, frequencies, maxSampleRate, times, loops);

}

void MainWindow::on_StartRabi_clicked()
{
//    ui->loadWaveformAndSeqTable->setEnabled(false);
//    ui->PrepareT1->setEnabled(false);
//    ui->PrepareRabi->setEnabled(false);
//    ui->PrepareT1wPI->setEnabled(false);
//    ui->pulsedODMRPrepare->setEnabled(false);
    //ui->compPulsPrep->setEnabled(false);

    if(checkClickedButton(1, "start")){
        return;
    }

    std::thread startRabiThread(&MainWindow::startFrequencySequence, this, "Rabi");
    startRabiThread.detach();

    //PulsedMeasurementsAndData_var->prepareCounterForRabi();
    //on_RUNbutton_clicked();
    //on_outputM2i_clicked();

//    ui->ReadDataRabi->setEnabled(true);
}

void MainWindow::on_ReadDataRabi_clicked()
{
//    ui->loadWaveformAndSeqTable->setEnabled(true);
//    ui->PrepareT1->setEnabled(true);
//    ui->PrepareRabi->setEnabled(true);
//    ui->PrepareT1wPI->setEnabled(true);
//    ui->pulsedODMRPrepare->setEnabled(true);
//    ui->SingleLorentzianFit->setEnabled(true);
//    ui->DoubleLorentzianFit->setEnabled(true);
    //ui->compPulsPrep->setEnabled(true);

    PulsedMeasurementsAndData_var->typeOfMeasurement = "Rabi";

    if(checkClickedButton(1, "read")){
        return;
    }

    curDataWidget = ui->ODMRPlot;
    PulsedMeasurementsAndData_var->fetchAndProcessDataForRabi();

    /*
    QMessageBox rabiRead_MB;
    rabiRead_MB.setWindowTitle("Sora");
    bool oszillation = sora_var->evalFit_var->checkOscillation(ODMRXValues, normedODMRYValues);
    if(oszillation){
        rabiRead_MB.setText("I found an oszillation. Do you want me to fit a damped Sine?");
        rabiRead_MB.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        int ret = rabiRead_MB.exec();
        if(ret == QMessageBox::Yes){
            on_DampedSinusFit_clicked();
        }
   }*/
    //on_STOPbutton_clicked();

//    ui->ReadDataRabi->setEnabled(false);
}

//#####################################################################
//############################ pulsed ODMR ############################
//#####################################################################

void MainWindow::on_pulsedODMRPrepare_clicked()
{

    if(checkClickedButton(2, "prep")){
        return;
    }

    double freqStart = ui->pulsedODMRStartFreq->value();
    double freqEnd = ui->pulsedODMRStopFreq->value();
    int nbSteps = ui->pulsedODMRNumbSteps->value()+1;
    double piPulse = ui->pulsedODMRPI_Pulse->value();
    double timeLaser = ui->pulsedODMRLaserPulse->value();
    double timeCounter = ui->pulsedODMRCounterTime->value();
    int loops = ui->pulsedODMRLoops->value();
    int withRefocus = 0;

    pulsedODMRPrepare(freqStart, freqEnd, nbSteps, piPulse, timeLaser, timeCounter, loops, withRefocus);

    return;


   //############# AWG needs to be prepared !!!!!!!!!!! #############

    PulsedMeasurementsAndData_var->currentPulsedODMRParams.FreqStart=ui->pulsedODMRStartFreq->value();
    PulsedMeasurementsAndData_var->currentPulsedODMRParams.FreqEnd=ui->pulsedODMRStopFreq->value();
    PulsedMeasurementsAndData_var->currentPulsedODMRParams.stepNb=ui->pulsedODMRNumbSteps->value()+1;
    PulsedMeasurementsAndData_var->currentPulsedODMRParams.pi_Pulse=ui->pulsedODMRPI_Pulse->value();
    PulsedMeasurementsAndData_var->currentPulsedODMRParams.TimeLaser=ui->pulsedODMRLaserPulse->value();
    PulsedMeasurementsAndData_var->currentPulsedODMRParams.TimeCounter=ui->pulsedODMRCounterTime->value();
    PulsedMeasurementsAndData_var->currentPulsedODMRParams.LoopNb=ui->pulsedODMRLoops->value();



    int actChN = next_power_of_2(6);
    std::cout << next_power_of_2(6) << " activated channels" << std::endl;

    int64 mask;

    switch(actChN)
    {
        case 1: mask = (int64)1; break;
        case 2: mask = (int64)0x3; break;
        case 4: mask = (int64)0xF; break;
        case 8: mask = (int64)0xFF; break;
        case 16: mask = (int64)0xFFFF; break;
        default: std::cout << "Error: Channel mask is not allowed !" << std::endl; return;
    }

    //int length_in_samples = lround(10047) + 1;  // ~10.047 µs

    PulsedMeasurementsAndData_var->currentPulsedODMRParams.PulsedODMRTime=(PulsedMeasurementsAndData_var->currentPulsedODMRParams.TimeLaser+PulsedMeasurementsAndData_var->currentPulsedODMRParams.pi_Pulse+PulsedMeasurementsAndData_var->currentPulsedODMRParams.TimeCounter+200)+500;

/*
    int numbOfSteps = (int)((delayLaserEnableRising_ns+(100+PulsedMeasurementsAndData_var->currentPulsedODMRParams.PulsedODMRTime)*PulsedMeasurementsAndData_var->currentPulsedODMRParams.stepNb)/10)+4;
    int length_in_samples;
    if(numbOfSteps%64==0){
        length_in_samples=numbOfSteps;
    }else{
        length_in_samples=64*(numbOfSteps/64+1);
    }

    std::cout << "loop_np: " << PulsedMeasurementsAndData_var->currentPulsedODMRParams.LoopNb << ", frequency_MHz: " << 100 << " length_in_samples: " << length_in_samples << std::endl;
    std::cout << "Time per Step: " << PulsedMeasurementsAndData_var->currentPulsedODMRParams.PulsedODMRTime << ", numbOfSteps: " << numbOfSteps << std::endl;

    M2i7005_var->M2iSetup_StdReplay(mask, 100, false, false, PulsedMeasurementsAndData_var->currentPulsedODMRParams.LoopNb, length_in_samples);

    std::vector< std::vector<int>> risingEdges;
    std::vector< std::vector<int>> fallingEdges;
    numbOfSteps = CreatePulsedODMRSequence(risingEdges, fallingEdges);

    M2i7005_var->M2iLoadIntoMemoryFromVec(6, risingEdges, fallingEdges);
    CreatePulsedODMRFrequencies();

    ui->RabiApproxTime->display((double)(numbOfSteps)*(double)(ui->pulsedODMRLoops->value())*1e-8/60);
*/


    std::vector< std::vector<int>> risingEdges;
    std::vector< std::vector<int>> fallingEdges;
    int numbOfSteps = CreatePulsedODMRSequence(risingEdges, fallingEdges);
    int length_in_samples;

    if(numbOfSteps%64==0){
        length_in_samples=numbOfSteps;
    }else{
        length_in_samples=64*(numbOfSteps/64+1);
    }
    M2i7005_var->M2iSetup_StdReplay(mask, 100, false, false, PulsedMeasurementsAndData_var->currentPulsedODMRParams.LoopNb, length_in_samples);
    M2i7005_var->M2iLoadIntoMemoryFromVec(6, risingEdges, fallingEdges);

    CreatePulsedODMRFrequencies();

    ui->RabiApproxTime->display((double)(length_in_samples)*(double)(ui->pulsedODMRLoops->value())*1e-8/60);

//    ui->pulsedODMRStart->setEnabled(true);

}

void MainWindow::pulsedODMRPrepare(double freqStart, double freqEnd, int nbSteps, double piPulse, int timeLaser, int timeCounter, int loops, int withRefocus)
{
    PulsedMeasurementsAndData_var->currentPulsedODMRParams.FreqStart = freqStart;
    PulsedMeasurementsAndData_var->currentPulsedODMRParams.FreqEnd = freqEnd;
    PulsedMeasurementsAndData_var->currentPulsedODMRParams.stepNb = nbSteps;
    PulsedMeasurementsAndData_var->currentPulsedODMRParams.pi_Pulse = piPulse;
    PulsedMeasurementsAndData_var->currentPulsedODMRParams.TimeLaser = timeLaser;
    PulsedMeasurementsAndData_var->currentPulsedODMRParams.TimeCounter = timeCounter;
    PulsedMeasurementsAndData_var->currentPulsedODMRParams.LoopNb = loops;
    PulsedMeasurementsAndData_var->currentPulsedODMRParams.PulsedODMRTime=(timeLaser+piPulse+timeCounter+200)+500;
    PulsedMeasurementsAndData_var->withRefocus = withRefocus;

    // upload pulsed ODMR sequence
    double length_in_samples = ArrayPrepForPulsedMeasurements_var->preparePulsedODMR(freqStart, freqEnd, nbSteps, piPulse, timeLaser, timeCounter, loops, M2i7005_var, M8195A_var);
    ArrayPrepForPulsedMeasurements_var->createPulsedODMRFrequencies(freqStart, freqEnd, nbSteps, piPulse, loops, M8195A_var);

    ui->RabiApproxTime->display((double)(length_in_samples)*(double)(loops)*1e-8/60);;
}


int MainWindow::CreatePulsedODMRSequence(std::vector< std::vector<int>>& risingEdges,  std::vector< std::vector<int>>& fallingEdges)
{
    std::vector<int> risingAWGTrigger;
    std::vector<int> fallingAWGTrigger;
    std::vector<int> risingAWGEvent;
    std::vector<int> fallingAWGEvent;
    std::vector<int> risingCounter;
    std::vector<int> fallingCounter;
    std::vector<int> risingLaserDigital;
    std::vector<int> fallingLaserDigital;
    std::vector<int> risingLaserEnable;
    std::vector<int> fallingLaserEnable;

    int TimeLaser=PulsedMeasurementsAndData_var->currentPulsedODMRParams.TimeLaser;
    int Steps = PulsedMeasurementsAndData_var->currentPulsedODMRParams.stepNb;
    int TimeCounter = PulsedMeasurementsAndData_var->currentPulsedODMRParams.TimeCounter;
    double pi_Pulse = PulsedMeasurementsAndData_var->currentPulsedODMRParams.pi_Pulse;

    int TimePrevSeg=0;

    //Trigger for AWG; minus shift of 70 ns
    risingAWGTrigger.push_back((int)((TimeLaser-delayAWG_ns+delayLaserEnableRising_ns-70)/10));
    fallingAWGTrigger.push_back((int)((TimeLaser+10-delayAWG_ns+delayLaserEnableRising_ns-70)/10));

    for(int i = 0; i<(Steps); ++i){

        //Signal for Laser Enable (init)
        if(i==0){
            risingLaserEnable.push_back((int)(TimePrevSeg)/10);
        }
        fallingLaserEnable.push_back((int)((TimeLaser+TimePrevSeg)/10));

        //Signal for Laser Digital (init)
        if(i==0){
            risingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+TimePrevSeg)/10));
        }
        fallingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+TimeLaser+TimePrevSeg)/10));



        //Event for AWG; minus shift of 70 ns
        risingAWGEvent.push_back((int)((TimeLaser-delayAWG_ns+delayLaserEnableRising_ns-70+TimePrevSeg)/10));
        fallingAWGEvent.push_back((int)((TimeLaser+10-delayAWG_ns+delayLaserEnableRising_ns-70+TimePrevSeg)/10));

        //##########  Added 500 ns for all following steps, due to get 1µs between MW and readout !!! ###################

        //Signal for TT; additional shift of 30 ns
        risingCounter.push_back((int)((TimeLaser+pi_Pulse+delayLaserEnableRising_ns-delayLaserDigital_ns+500+30+TimePrevSeg)/10)+1);
        fallingCounter.push_back((int)((TimeLaser+pi_Pulse+delayLaserEnableRising_ns-delayLaserDigital_ns+500+TimeCounter+30+TimePrevSeg)/10)+1);

        //Signal for Laser Enable (readout) additional shift of 20 ns
        risingLaserEnable.push_back((int)((TimeLaser+pi_Pulse+TimePrevSeg+20)/10)+1);
        if(i==(Steps-1)){
            fallingLaserEnable.push_back((int)((TimeLaser+pi_Pulse+delayLaserEnableRising_ns-delayLaserEnableFalling_ns+500+TimeCounter+TimePrevSeg)/10)+1);
        }

        //Signal for Laser Digital (readout)
        risingLaserDigital.push_back((int)((TimeLaser+pi_Pulse+delayLaserEnableRising_ns-delayLaserDigital_ns+500+TimePrevSeg)/10)+1);
        if(i==(Steps-1)){
            fallingLaserDigital.push_back((int)((TimeLaser+pi_Pulse+delayLaserEnableRising_ns-delayLaserDigital_ns+500+TimeCounter+TimePrevSeg)/10)+1);
        }

        TimePrevSeg=*std::max_element(fallingCounter.begin(),fallingCounter.end())*10;
    }

    risingEdges.push_back({1});
    fallingEdges.push_back({2});
    risingEdges.push_back(risingAWGEvent);
    fallingEdges.push_back(fallingAWGEvent);
    risingEdges.push_back(risingCounter);
    fallingEdges.push_back(fallingCounter);
    risingEdges.push_back(risingLaserEnable);
    fallingEdges.push_back(fallingLaserEnable);
    risingEdges.push_back(risingLaserDigital);
    fallingEdges.push_back(fallingLaserDigital);
    risingEdges.push_back(risingAWGTrigger);
    fallingEdges.push_back(fallingAWGTrigger);

    return TimePrevSeg/10;
}

void MainWindow::CreatePulsedODMRFrequencies(){

    ViConstString chan = "1";

    // Create frequency-array
    auto frequencies = linearFreqArray(PulsedMeasurementsAndData_var->currentPulsedODMRParams.FreqStart*MHz_M8195A, PulsedMeasurementsAndData_var->currentPulsedODMRParams.FreqEnd*MHz_M8195A, ui->pulsedODMRNumbSteps->value());

    // Create times-array
    std::vector<double> times (frequencies.size(), PulsedMeasurementsAndData_var->currentPulsedODMRParams.pi_Pulse/1000.0*microsec_M8195A);

    std::vector<double> phases (frequencies.size(), 0);
    std::vector<double> amplitudes (frequencies.size(), 1);

    ViStatus status = M8195A_var->loadSineArrayWaveformAndSeqTable(chan, frequencies, maxSampleRate, times, PulsedMeasurementsAndData_var->currentPulsedODMRParams.LoopNb, phases, amplitudes);
//    ViStatus status = M8195A_var->loadSineArrayWaveformAndSeqTable(chan, frequencies, maxSampleRate, times, PulsedMeasurementsAndData_var->currentPulsedODMRParams.LoopNb);

}

void MainWindow::on_pulsedODMRStart_clicked()
{
//    ui->loadWaveformAndSeqTable->setEnabled(false);
//    ui->PrepareT1->setEnabled(false);
//    ui->PrepareRabi->setEnabled(false);
//    ui->PrepareT1wPI->setEnabled(false);
//    ui->pulsedODMRPrepare->setEnabled(false);
    //ui->compPulsPrep->setEnabled(false);

    if(checkClickedButton(2, "start")){
        return;
    }

    std::thread startPulsedODMRThread(&MainWindow::startFrequencySequence, this, "pulsedODMR");
    startPulsedODMRThread.detach();

    /*PulsedMeasurementsAndData_var->prepareCounterForPulsedODMR();
    std::cout << "Counter prepared!" << std::endl;
    on_RUNbutton_clicked();
    on_outputM2i_clicked();*/

//    ui->pulsedODMRReadData->setEnabled(true);
}

void MainWindow::on_pulsedODMRReadData_clicked()
{
//    ui->loadWaveformAndSeqTable->setEnabled(true);
//    ui->PrepareT1->setEnabled(true);
//    ui->PrepareRabi->setEnabled(true);
//    ui->PrepareT1wPI->setEnabled(true);
//    ui->pulsedODMRPrepare->setEnabled(true);
//    ui->SingleLorentzianFit->setEnabled(true);
//    ui->DoubleLorentzianFit->setEnabled(true);
    //ui->compPulsPrep->setEnabled(true);
    PulsedMeasurementsAndData_var->typeOfMeasurement = "ODMR";

    if(checkClickedButton(2, "read")){
        return;
    }

    //on_STOPbutton_clicked();
    curDataWidget = ui->ODMRPlot;
    PulsedMeasurementsAndData_var->fetchAndProcessDataForPulsedODMR();

//    ui->pulsedODMRReadData->setEnabled(false);
}





void MainWindow::clearODMRValues(){
    ODMRXValues.clear();
    ODMRYValues.clear();
    ODMRFFTXValues.clear();
    ODMRFFTYValues.clear();
    normedODMRYValues.clear();
    fitODMRYValues.clear();
    T1YValues.clear();
}

void MainWindow::writeODMRValues(double xValue, double yValue){
    ODMRXValues.append(xValue);
    ODMRYValues.append(yValue);
}

void MainWindow::writeT1Value(double yValue){
    T1YValues.append(yValue);
}

void MainWindow::plotODMRData(){

    if(curDataWidget == ui->ODMRPlot){
        // clear all graphs after loading a new one
        for(int i=0; i<4; i++){
            curDataWidget->graph(i)->data()->clear();
        }

        // set the boarders (for fitting)
        ui->lowerLimitFitWavegen->setValue(ODMRXValues[0]);
        ui->upperLimitFitWavegen->setValue(ODMRXValues[ODMRXValues.size()-1]);

        // Allow user to drag axis ranges with mouse, zoom with mouse wheel and select graphs by clicking:
        curDataWidget->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);

        // set new data and colors
        curDataWidget->graph(0)->setPen(QPen(Qt::blue));
        curDataWidget->graph(0)->setData(ODMRXValues, ODMRYValues);
        curDataWidget->graph(0)->setVisible(true);

        // let the ranges scale themselves so graph 0 fits perfectly in the visible area:
        curDataWidget->rescaleAxes();

        // evaluate data and save them
        if(PulsedMeasurementsAndData_var->typeOfMeasurement == "Rabi"){
            // normed data
            double rabiYmax = *std::max_element(ODMRYValues.begin(), ODMRYValues.end());
            double rabiYmin = *std::min_element(ODMRYValues.begin(), ODMRYValues.end());
            double amp = (rabiYmax-rabiYmin)/2.;
            double off = rabiYmax-amp;
            normedODMRYValues.resize(ODMRXValues.size());
            for(int i=0; i<ODMRYValues.size(); i++){
                normedODMRYValues[i]=(ODMRYValues[i]-amp)/off;
            }
            curDataWidget->graph(1)->setPen(QPen(Qt::blue));
            curDataWidget->graph(1)->setData(ODMRXValues, normedODMRYValues);
            curDataWidget->graph(1)->setVisible(false);

            // fast fourier trafo data
            QVector<QVector<double>> fft = control_var->evalFit_var->FFT(ODMRXValues, normedODMRYValues);
            ODMRFFTXValues = fft[0];
            ODMRFFTYValues = fft[1];
            curDataWidget->graph(3)->setPen(QPen(Qt::blue));
            curDataWidget->graph(3)->setData(fft[0], fft[1]);
            curDataWidget->graph(3)->setVisible(false);

            // set axis
            curDataWidget->xAxis->setLabel("time in ns");
            curDataWidget->yAxis->setLabel("counts");
        }else if(PulsedMeasurementsAndData_var->typeOfMeasurement == "ODMR"){
            // normed data
            double cwODMRYmax = *std::max_element(ODMRYValues.begin(),ODMRYValues.end());
            normedODMRYValues.resize(ODMRXValues.size());
            for(int i=0; i<(ODMRYValues.size()); i++){
                normedODMRYValues[i]=ODMRYValues[i]/cwODMRYmax;
            }
            curDataWidget->graph(1)->setPen(QPen(Qt::blue));
            curDataWidget->graph(1)->setData(ODMRXValues, normedODMRYValues);
            curDataWidget->graph(1)->setVisible(false);

            // set axis
            curDataWidget->xAxis->setLabel("frequency in MHz");
            curDataWidget->yAxis->setLabel("counts");
        }else if(PulsedMeasurementsAndData_var->typeOfMeasurement == "Ramsey"){
            // fast fourier trafo data
            QVector<QVector<double>> fft = control_var->evalFit_var->FFT(ODMRXValues, ODMRYValues);
            ODMRFFTXValues = fft[0];
            ODMRFFTYValues = fft[1];
            curDataWidget->graph(3)->setPen(QPen(Qt::blue));
            curDataWidget->graph(3)->setData(fft[0], fft[1]);
            curDataWidget->graph(3)->setVisible(false);

            // set axis
            curDataWidget->xAxis->setLabel("time in ns");
            curDataWidget->yAxis->setLabel("counts");
        }else if(PulsedMeasurementsAndData_var->typeOfMeasurement == "SpinEcho"){
            // fast fourier trafo data
            QVector<QVector<double>> fft = control_var->evalFit_var->FFT(ODMRXValues, ODMRYValues);
            ODMRFFTXValues = fft[0];
            ODMRFFTYValues = fft[1];
            curDataWidget->graph(3)->setPen(QPen(Qt::blue));
            curDataWidget->graph(3)->setData(fft[0], fft[1]);
            curDataWidget->graph(3)->setVisible(false);

            // set axis
            curDataWidget->xAxis->setLabel("time in ns");
            curDataWidget->yAxis->setLabel("counts");
        }





    // composite pulses
    }else{
        curDataWidget->graph(1)->data()->clear();
        curDataWidget->graph(0)->setPen(QPen(Qt::blue)); // line color blue for first graph
        //ui->ODMRPlot->graph(0)->setData(ODMRXValues, ODMRYValues);
        curDataWidget->graph(0)->setData(ODMRXValues, ODMRYValues);
        // let the ranges scale themselves so graph 0 fits perfectly in the visible area:
        curDataWidget->rescaleAxes();
        // Allow user to drag axis ranges with mouse, zoom with mouse wheel and select graphs by clicking:
        curDataWidget->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
    }

    curDataWidget->replot();


    /*
    curDataWidget->graph(1)->data()->clear();
    ui->lowerLimitFitWavegen->setValue(ODMRXValues[0]);
    ui->upperLimitFitWavegen->setValue(ODMRXValues[ODMRXValues.size()-1]);
    curDataWidget->graph(0)->setPen(QPen(Qt::blue)); // line color blue for first graph
    //ui->ODMRPlot->graph(0)->setData(ODMRXValues, ODMRYValues);
    curDataWidget->graph(0)->setData(ODMRXValues, ODMRYValues);
    // let the ranges scale themselves so graph 0 fits perfectly in the visible area:
    curDataWidget->rescaleAxes();
    // Allow user to drag axis ranges with mouse, zoom with mouse wheel and select graphs by clicking:
    curDataWidget->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
    curDataWidget->replot();*/
}

void MainWindow::plotT1Data(){
    curDataWidget->graph(1)->setPen(QPen(Qt::blue)); // line color blue for first graph
    //ui->ODMRPlot->graph(0)->setData(ODMRXValues, T1YValues);
    curDataWidget->graph(1)->setData(ODMRXValues, T1YValues);
    std::cout << "Plot graph(1) " << std::endl;
    // let the ranges scale themselves so graph 0 fits perfectly in the visible area:
    curDataWidget->rescaleAxes();
    // Allow user to drag axis ranges with mouse, zoom with mouse wheel and select graphs by clicking:
    curDataWidget->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
    curDataWidget->replot();
}

void MainWindow::clearGraphData(){
    GraphXValues.clear();
    GraphYValues.clear();
}

void MainWindow::writeGraphData(double xValue, double yValue){
    GraphXValues.append(xValue);
    GraphYValues.append(yValue);
}

void MainWindow::PlotGraphData(){
    curDataWidget->graph(1)->setPen(QPen(Qt::red)); // line color blue for first graph
    //ui->ODMRPlot->graph(0)->setData(ODMRXValues, T1YValues);
    curDataWidget->graph(1)->setData(GraphXValues, GraphYValues);
    std::cout << "Plot graph(1) " << std::endl;
    // let the ranges scale themselves so graph 0 fits perfectly in the visible area:
    curDataWidget->rescaleAxes();
    // Allow user to drag axis ranges with mouse, zoom with mouse wheel and select graphs by clicking:
    curDataWidget->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
    curDataWidget->replot();
}

void MainWindow::PostionForPulsedMeasurments(){
    std::vector<double> positions {ScanAndCount_var->readAllPositions_Closed()};
    PulsedMeasurementsAndData_var->CurrentPosition.xPos=positions[0];
    PulsedMeasurementsAndData_var->CurrentPosition.yPos=positions[1];
    PulsedMeasurementsAndData_var->CurrentPosition.zPos=positions[2];
}
// drop down menu
void MainWindow::ODMRContextMenuRequest(QPoint pos)
{
    QMenu *ODMRMenu = new QMenu(this);
    ODMRMenu->setAttribute(Qt::WA_DeleteOnClose);

    if(ODMRXValues.size()>0){
        ODMRMenu->addAction("show measure values", this, SLOT(dropDownMeasureValues_ODMR()));
        if(normedODMRYValues.size()>0){
            ODMRMenu->addAction("show normed measure values", this, SLOT(dropDownNormedMeasureValues_ODMR()));
        }
        if(fitODMRYValues.size()>0){
            ODMRMenu->addAction("show fit", this, SLOT(dropDownFitValues_ODMR()));
        }
        if(ODMRFFTXValues.size()>0){
            ODMRMenu->addAction("FFT", this, SLOT(dropDownFFTMeasureValues_ODMR()));
        }
    }
    ODMRMenu->popup(ui->ODMRPlot->mapToGlobal(pos));
}

void MainWindow::dropDownMeasureValues_ODMR()
{
    ui->ODMRPlot->graph(0)->setVisible(true);
    ui->ODMRPlot->graph(1)->setVisible(false);
    ui->ODMRPlot->graph(2)->setVisible(false);
    ui->ODMRPlot->graph(3)->setVisible(false);

    // set new range and rename the axis
    double xAxisMin = *std::min_element(ODMRXValues.begin(), ODMRXValues.end());
    double xAxisMax = *std::max_element(ODMRXValues.begin(), ODMRXValues.end());
    ui->ODMRPlot->xAxis->setRange(xAxisMin, xAxisMax);
    if(ODMRFFTXValues.isEmpty()){
        curDataWidget->xAxis->setLabel("frequency in MHz");
    }else{
        curDataWidget->xAxis->setLabel("time in ns");
    }

    double yAxisMin = *std::min_element(ODMRYValues.begin(), ODMRYValues.end());
    double yAxisMax = *std::max_element(ODMRYValues.begin(), ODMRYValues.end());
    ui->ODMRPlot->yAxis->setRange(yAxisMin, yAxisMax);
    curDataWidget->yAxis->setLabel("counts");

    ui->ODMRPlot->replot();
}

void MainWindow::dropDownNormedMeasureValues_ODMR()
{
    ui->ODMRPlot->graph(0)->setVisible(false);
    ui->ODMRPlot->graph(1)->setVisible(true);
    ui->ODMRPlot->graph(2)->setVisible(false);
    ui->ODMRPlot->graph(3)->setVisible(false);

    // set new range and rename the axis
    double xAxisMin = *std::min_element(ODMRXValues.begin(), ODMRXValues.end());
    double xAxisMax = *std::max_element(ODMRXValues.begin(), ODMRXValues.end());
    ui->ODMRPlot->xAxis->setRange(xAxisMin, xAxisMax);
    if(ODMRFFTXValues.isEmpty()){
        curDataWidget->xAxis->setLabel("frequency in MHz");
    }else{
        curDataWidget->xAxis->setLabel("time in ns");
    }

    double yAxisMin = *std::min_element(normedODMRYValues.begin(), normedODMRYValues.end());
    double yAxisMax = *std::max_element(normedODMRYValues.begin(), normedODMRYValues.end());
    ui->ODMRPlot->yAxis->setRange(yAxisMin, yAxisMax);
    curDataWidget->yAxis->setLabel("normed counts");

    ui->ODMRPlot->replot();
}

void MainWindow::dropDownFitValues_ODMR()
{
    ui->ODMRPlot->graph(0)->setVisible(false);
    ui->ODMRPlot->graph(1)->setVisible(true);
    ui->ODMRPlot->graph(2)->setVisible(true);
    ui->ODMRPlot->graph(3)->setVisible(false);

    // set new range and rename the axis
    double xAxisMin = *std::min_element(ODMRXValues.begin(), ODMRXValues.end());
    double xAxisMax = *std::max_element(ODMRXValues.begin(), ODMRXValues.end());
    ui->ODMRPlot->xAxis->setRange(xAxisMin, xAxisMax);
    if(ODMRFFTXValues.isEmpty()){
        curDataWidget->xAxis->setLabel("frequency in MHz");
    }else{
        curDataWidget->xAxis->setLabel("time in ns");
    }

    double yAxisMin = *std::min_element(normedODMRYValues.begin(), normedODMRYValues.end());
    double yAxisMax = *std::max_element(normedODMRYValues.begin(), normedODMRYValues.end());
    ui->ODMRPlot->yAxis->setRange(yAxisMin, yAxisMax);
    curDataWidget->yAxis->setLabel("normed counts");

    ui->ODMRPlot->replot();
}

void MainWindow::dropDownFFTMeasureValues_ODMR()
{
    ui->ODMRPlot->graph(0)->setVisible(false);
    ui->ODMRPlot->graph(1)->setVisible(false);
    ui->ODMRPlot->graph(2)->setVisible(false);
    ui->ODMRPlot->graph(3)->setVisible(true);

    // set new range and rename the axis
    double xAxisMax = *std::max_element(ODMRFFTXValues.begin(), ODMRFFTXValues.end());
    ui->ODMRPlot->xAxis->setRange(0, xAxisMax);
    curDataWidget->xAxis->setLabel("frequency in MHz");

    double yAxisMin = *std::min_element(ODMRFFTYValues.begin(), ODMRFFTYValues.end());
    double yAxisMax = *std::max_element(ODMRFFTYValues.begin(), ODMRFFTYValues.end());
    ui->ODMRPlot->yAxis->setRange(yAxisMin, yAxisMax);
    curDataWidget->yAxis->setLabel("counts");

    ui->ODMRPlot->replot();
}

void MainWindow::on_SingleLorentzianFit_clicked()
{
    if(!ODMRXValues.isEmpty()){
        double guessDip1w0 = ui->guessDip1w0->value();
        double guessDip1Amp = ui->guessDip1Amp->value();
        double guessDip1FWHM = ui->guessDip1FWHM->value();

        // set guess parameters if the user set some
        if(guessDip1Amp > 0){
            QVector<double> guessParams;
            guessParams = {guessDip1Amp, guessDip1FWHM, guessDip1w0, 1};
            control_var->evalFit_var->setUserFitParameter(guessParams);
        }

        QVector<double> fitParams = control_var->evalFit_var->startFit(ODMRXValues, normedODMRYValues, 1000, "LM_lorentzian");

        fitODMRYValues.resize(ODMRXValues.size());
        for(int i=0; i<ODMRXValues.size(); i++){
            fitODMRYValues[i] = -(fitParams[0]*pow(fitParams[1],2))/(pow((ODMRXValues[i]-fitParams[2]),2)+pow(fitParams[1],2))+fitParams[3];
        }

        ui->fitDip1w0->setText(QString::number(fitParams[2]));
        ui->fitDip1FWHM->setText(QString::number(fitParams[1]));
        ui->fitDip1Amp->setText(QString::number(fitParams[0]));
        ui->fitDip2w0->setText(QString::number(0));
        ui->fitDip2FWHM->setText(QString::number(0));
        ui->fitDip2Amp->setText(QString::number(0));

        ui->ODMRPlot->graph(2)->setData(ODMRXValues, fitODMRYValues);
        ui->ODMRPlot->graph(2)->setPen(QPen(Qt::red));

        dropDownFitValues_ODMR();
    }


    /*if((ui->lowerLimitFitWavegen->value()>=ui->upperLimitFitWavegen->value()) || (ui->lowerLimitFitWavegen->value()<ODMRXValues[0]) || (ui->upperLimitFitWavegen->value()>ODMRXValues[ODMRXValues.size()-1])){
         QMessageBox::critical(this,"ERROR BY FITTING!", "boarders are either out of range or the lower one is bigger than the upper one.");
    }else{
        //fittingroutines_var->setCurFilename(QString::fromStdString(PulsedMeasurementsAndData_var->currentDataFromFile.PathLoaded+"/"+PulsedMeasurementsAndData_var->currentDataFromFile.fileNameLoaded));
        //fittingroutines_var->singleLorentzian(guessDip1w0, guessDip1FWHM, guessDip1Amp, ODMRXValues, ODMRYValues, ui->lowerLimitFitWavegen->value(), ui->upperLimitFitWavegen->value());

        std::cout << "After fitting Lorentzian" << std::endl;

        //ui->fitDip1w0->display(fittingroutines_var->Lorentzian1.w0);
        //ui->fitDip1Amp->display((fittingroutines_var->Lorentzian1.amp/fittingroutines_var->Lorentzian1.offset)*100);
        //ui->fitDip1FWHM->display(fittingroutines_var->Lorentzian1.FWHM);

        std::cout << "After setting Lorentzian values in GUI" << std::endl;

        ui->fitDip2w0->display(0);
        ui->fitDip2Amp->display(0);
        ui->fitDip2FWHM->display(0);
    }*/
}

void MainWindow::on_DoubleLorentzianFit_clicked()
{
    if(!ODMRXValues.isEmpty()){

        double guessDip1w0 = ui->guessDip1w0->value();
        double guessDip1Amp = ui->guessDip1Amp->value();
        double guessDip1FWHM = ui->guessDip1FWHM->value();

        double guessDip2w0 = ui->guessDip2w0->value();
        double guessDip2Amp = ui->guessDip2Amp->value();
        double guessDip2FWHM = ui->guessDip2FWHM->value();

        // set guess parameters if the user set some
        if(guessDip2Amp > 0){
            QVector<double> guessParams;
            guessParams = {guessDip1Amp, guessDip1FWHM, guessDip1w0, 1, guessDip2Amp, guessDip2FWHM, guessDip2w0};
            control_var->evalFit_var->setUserFitParameter(guessParams);
        }

        QVector<double> fitParams = control_var->evalFit_var->startFit(ODMRXValues, normedODMRYValues, 1000, "LM_doubleLorentzian");

        fitODMRYValues.resize(ODMRXValues.size());
        for(int i=0; i<ODMRXValues.size(); i++){
            fitODMRYValues[i] = -(fitParams[0]*pow(fitParams[1],2)/(pow((ODMRXValues[i]-fitParams[2]),2)+pow(fitParams[1],2))+fitParams[4]*pow(fitParams[5],2)/(pow((ODMRXValues[i]-fitParams[6]),2)+pow(fitParams[5],2)))+fitParams[3];
        }

        ui->fitDip1w0->setText(QString::number(fitParams[2]));
        ui->fitDip1FWHM->setText(QString::number(fitParams[1]));
        ui->fitDip1Amp->setText(QString::number(fitParams[0]));
        ui->fitDip2w0->setText(QString::number(fitParams[4]));
        ui->fitDip2FWHM->setText(QString::number(fitParams[5]));
        ui->fitDip2Amp->setText(QString::number(fitParams[6]));

        ui->ODMRPlot->graph(2)->setData(ODMRXValues, fitODMRYValues);
        ui->ODMRPlot->graph(2)->setPen(QPen(Qt::red));

        dropDownFitValues_ODMR();
    }

/*
    if((ui->lowerLimitFitWavegen->value()>=ui->upperLimitFitWavegen->value()) || (ui->lowerLimitFitWavegen->value()<ODMRXValues[0]) || (ui->upperLimitFitWavegen->value()>ODMRXValues[ODMRXValues.size()-1])){
         QMessageBox::critical(this,"ERROR BY FITTING!", "boarders are either out of range or the lower one is bigger than the upper one.");
    }else{
        //fittingroutines_var->setCurFilename(QString::fromStdString(PulsedMeasurementsAndData_var->currentDataFromFile.PathLoaded+"/"+PulsedMeasurementsAndData_var->currentDataFromFile.fileNameLoaded));
        //fittingroutines_var->doubleLorentzian(guessDip1w0, guessDip1FWHM, guessDip1Amp, guessDip2w0, guessDip2FWHM, guessDip2Amp, ODMRXValues, ODMRYValues, ui->lowerLimitFitWavegen->value(), ui->upperLimitFitWavegen->value());

        //ui->fitDip1w0->display(fittingroutines_var->Lorentzian1.w0);
        //ui->fitDip1Amp->display((fittingroutines_var->Lorentzian1.amp/fittingroutines_var->Lorentzian1.offset)*100);
        //ui->fitDip1FWHM->display(fittingroutines_var->Lorentzian1.FWHM);

        //ui->fitDip2w0->display(fittingroutines_var->Lorentzian2.w0);
        //ui->fitDip2Amp->display((fittingroutines_var->Lorentzian2.amp/fittingroutines_var->Lorentzian2.offset)*100);
        //ui->fitDip2FWHM->display(fittingroutines_var->Lorentzian2.FWHM);
    }*/
}

void MainWindow::on_DampedSinusFit_clicked()
{
    if(!ODMRXValues.isEmpty()){
        double guessDampSinAmp = ui->guessDampSinAmp->value();
        double guessDampSinOmega = ui->guessDampSinOmega->value()/500*M_PI;
        double guessDampSinPhi = ui->guessDampSinPhi->value();
        double guessDampSinTau = ui->guessDampSinTau->value();
        double guessDampSinOff = ui->guessDampSinOff->value();

        // set guess parameters if the user set some
        if(guessDampSinAmp > 0){
            QVector<double> guessParams;
            guessParams = {guessDampSinAmp, guessDampSinOff, guessDampSinOmega, guessDampSinPhi, 1/(guessDampSinTau*1000)};
            control_var->evalFit_var->setUserFitParameter(guessParams);
        }

        QVector<double> fitParams;
        if(normedODMRYValues.size()>1){
            fitParams = control_var->evalFit_var->startFit(ODMRXValues, normedODMRYValues, 1000, "LM_dampedSine");
        }else{
            fitParams = control_var->evalFit_var->startFit(ODMRXValues, ODMRYValues, 1000, "LM_dampedSine");
        }

        ui->fitDampSinAmp->setText(QString::number(fitParams[0]));
        ui->fitDampSinOff->setText(QString::number(fitParams[1]));
        ui->fitDampSinOmega->setText(QString::number(fitParams[2]));
        ui->fitDampSinPhi->setText(QString::number(fitParams[3]));
        ui->fitDampSinTau->setText(QString::number(1/(fitParams[4]*1000)));

        fitODMRYValues.resize(ODMRXValues.size());
        for(int i=0; i<ODMRXValues.size(); i++){
            fitODMRYValues[i]=fitParams[0]*qExp(-fitParams[4]*ODMRXValues[i])*qSin((fitParams[2]*2*M_PI)/1000.*ODMRXValues[i]+fitParams[3])+fitParams[1];
        }
        ui->ODMRPlot->graph(2)->setData(ODMRXValues, fitODMRYValues);
        ui->ODMRPlot->graph(2)->setPen(QPen(Qt::red));

        dropDownFitValues_ODMR();


        /*if((ui->lowerLimitFitWavegen->value()>=ui->upperLimitFitWavegen->value()) || (ui->lowerLimitFitWavegen->value()<ODMRXValues[0]) || (ui->upperLimitFitWavegen->value()>ODMRXValues[ODMRXValues.size()-1])){
             QMessageBox::critical(this,"ERROR BY FITTING!", "boarders are either out of range or the lower one is bigger than the upper one.");
        }else{
            //fittingroutines_var->setCurFilename(QString::fromStdString(PulsedMeasurementsAndData_var->currentDataFromFile.PathLoaded+"/"+PulsedMeasurementsAndData_var->currentDataFromFile.fileNameLoaded));
            //fittingroutines_var->dampedSin(guessDampSinAmp, guessDampSinOmega, guessDampSinPhi, guessDampSinTau, ODMRXValues, ODMRYValues, ui->lowerLimitFitWavegen->value(), ui->upperLimitFitWavegen->value());
            //ui->fitDampSinAmp->display(fittingroutines_var->dampedSinus.amp);
            //ui->fitDampSinAmp->display(fittingroutines_var->dampedSinus.offset/fittingroutines_var->dampedSinus.amp);
            //ui->fitDampSinOmega->display(fittingroutines_var->dampedSinus.omegaMHz*500/3.14159265358979323846);
            //ui->fitDampSinPhi->display(fittingroutines_var->dampedSinus.phi);
            //ui->fitDampSinTau->display(fittingroutines_var->dampedSinus.tau/1000);
            //ui->fitDampSinOff->display(fittingroutines_var->dampedSinus.offset);
        }*/
    }
}

void MainWindow::on_saveFitParametersODMRPlot_clicked()
{
    //fittingroutines_var->saveFitParametersinDatFile();
}


/*
void MainWindow::on_PrepareT1_clicked()
{
    PulsedMeasurementsAndData_var->currentRabiParams.Freq=ui->FreqForRabi->value();
    PulsedMeasurementsAndData_var->currentRabiParams.Steps=ui->MWStepsForRabi->value()+1;
    PulsedMeasurementsAndData_var->currentRabiParams.Stepsize=ui->MWStepsSizeForRabi->value();
    PulsedMeasurementsAndData_var->currentRabiParams.TimeCounter=ui->TimeCounterForRabi->value();
    PulsedMeasurementsAndData_var->currentRabiParams.TimeLaser=ui->LaserForRabi->value();
    PulsedMeasurementsAndData_var->currentRabiParams.LoopNb=ui->LoopsForRabi->value();
    PulsedMeasurementsAndData_var->currentRabiParams.Rabi0Time=ui->Rabi0->value();

    RabiTime0=ui->Rabi0->value();


   //############# AWG needs to be prepared !!!!!!!!!!! #############

    int actChN = next_power_of_2(6);
    std::cout << next_power_of_2(6) << " activated channels" << std::endl;

    int64 mask;

    switch(actChN)
    {
        case 1: mask = (int64)1; break;
        case 2: mask = (int64)0x3; break;
        case 4: mask = (int64)0xF; break;
        case 8: mask = (int64)0xFF; break;
        case 16: mask = (int64)0xFFFF; break;
        default: std::cout << "Error: Channel mask is not allowed !" << std::endl; return;
    }

    //int length_in_samples = lround(10047) + 1;  // ~10.047 µs
    int RabiSteps=ui->MWStepsForRabi->value();

    PulsedMeasurementsAndData_var->currentRabiParams.MinRabiTime=(PulsedMeasurementsAndData_var->currentRabiParams.TimeLaser+RabiTime0+PulsedMeasurementsAndData_var->currentRabiParams.TimeCounter+500);

    int numbOfSteps = (int)((delayLaserEnableRising_ns+(300+PulsedMeasurementsAndData_var->currentRabiParams.MinRabiTime)*(RabiSteps+1)+(((RabiSteps+1)*((RabiSteps)+2)/2)*PulsedMeasurementsAndData_var->currentRabiParams.Stepsize))/10)+50;
    int length_in_samples;
    if(numbOfSteps%64==0){
        length_in_samples=numbOfSteps;
    }else{
        length_in_samples=64*(numbOfSteps/64+1);
    }

    std::cout << "loop_np: " << PulsedMeasurementsAndData_var->currentRabiParams.LoopNb << ", frequency_MHz: " << 100 << " length_in_samples: " << length_in_samples << std::endl;
    std::cout << "MinRabiTime: " << PulsedMeasurementsAndData_var->currentRabiParams.MinRabiTime << ", numbOfSteps: " << numbOfSteps << std::endl;

    M2i7005_var->M2iSetup_StdReplay(mask, 100, false, false, PulsedMeasurementsAndData_var->currentRabiParams.LoopNb, length_in_samples);

    std::vector< std::vector<int>> risingEdges;
    std::vector< std::vector<int>> fallingEdges;
    CreateRabiSequence(risingEdges, fallingEdges);

    M2i7005_var->M2iLoadIntoMemoryFromVec(6, risingEdges, fallingEdges);

    ui->RabiApproxTime->display((double)(numbOfSteps)*(double)(ui->LoopsForRabi->value())*1e-8/60);

//    ui->StartT1->setEnabled(true);
}


void MainWindow::on_StartT1_clicked()
{
    PulsedMeasurementsAndData_var->prepareCounterForRabi();
    on_outputM2i_clicked();

//    ui->ReadDataT1->setEnabled(true);
}

void MainWindow::on_ReadDataT1_clicked()
{
    curDataWidget = ui->ODMRPlot;
    PulsedMeasurementsAndData_var->fetchAndProcessDataForT1();

//    ui->ReadDataT1->setEnabled(false);
}
*/


void MainWindow::on_PrepareT1wPI_clicked()
{
    double Freq = ui->T1Frequency->value();
    int nbSteps = ui->T1Steps->value()+1;
    int stepSize = ui->T1StepSize->value()*1000;
    int timeCounter = ui->T1Counter->value();
    int timeLaser = ui->T1Laser->value();
    int loops = ui->T1Loops->value();
    double piPulse = ui->T1PI_Pulse->value();


    /*
    T1Prepare(Freq, nbSteps, stepSize, piPulse, timeLaser, timeCounter, loops);
    return;
    */

    PulsedMeasurementsAndData_var->currentT1Params.Freq=Freq;
    PulsedMeasurementsAndData_var->currentT1Params.Steps=nbSteps;
    PulsedMeasurementsAndData_var->currentT1Params.Stepsize=stepSize;
    PulsedMeasurementsAndData_var->currentT1Params.TimeCounter=timeCounter;
    PulsedMeasurementsAndData_var->currentT1Params.TimeLaser=timeLaser;
    PulsedMeasurementsAndData_var->currentT1Params.LoopNb=loops;
    PulsedMeasurementsAndData_var->currentT1Params.PiTime=piPulse;
    PulsedMeasurementsAndData_var->withRefocus = ui->compPulsRefocusCheckbox->isChecked();

    if((PulsedMeasurementsAndData_var->withRefocus)&&!(loops%10000)){
        PulsedMeasurementsAndData_var->currentCompPulsDetunParams.LoopNb=10000;
        PulsedMeasurementsAndData_var->NbOfRefocus = loops/10000;
        loops=10000;
        std::cout << "with Refocus and right loops" << std::endl;
    }else{
        PulsedMeasurementsAndData_var->currentCompPulsDetunParams.LoopNb=loops;
        PulsedMeasurementsAndData_var->NbOfRefocus = 1;
        std::cout << "Without refocus and/or wrong loops" << std::endl;
    }

   //############# AWG needs to be prepared !!!!!!!!!!! #############

    int actChN = next_power_of_2(6);
    std::cout << next_power_of_2(6) << " activated channels" << std::endl;

    int64 mask;

    switch(actChN)
    {
        case 1: mask = (int64)1; break;
        case 2: mask = (int64)0x3; break;
        case 4: mask = (int64)0xF; break;
        case 8: mask = (int64)0xFF; break;
        case 16: mask = (int64)0xFFFF; break;
        default: std::cout << "Error: Channel mask is not allowed !" << std::endl; return;
    }

    //int length_in_samples = lround(10047) + 1;  // ~10.047 µs
    //int Steps=ui->MWStepsForRabi->value();

    //PulsedMeasurementsAndData_var->currentT1Params.Segment0Time=(PulsedMeasurementsAndData_var->currentT1Params.TimeLaser+PulsedMeasurementsAndData_var->currentT1Params.PiTime+PulsedMeasurementsAndData_var->currentT1Params.TimeCounter+500);

    /*
    int numbOfSteps = (int)((delayLaserEnableRising_ns+(100+PulsedMeasurementsAndData_var->currentT1Params.Segment0Time)*(Steps+1)*2+(((Steps)*(Steps+1))*PulsedMeasurementsAndData_var->currentT1Params.Stepsize))/10)+50;
    int length_in_samples;
    if(numbOfSteps%64==0){
        length_in_samples=numbOfSteps;
    }else{
        length_in_samples=64*(numbOfSteps/64+1);
    }

    std::cout << "loop_np: " << PulsedMeasurementsAndData_var->currentT1Params.LoopNb << ", frequency_MHz: " << 100 << " length_in_samples: " << length_in_samples << std::endl;
    std::cout << "Segment0Time: " << PulsedMeasurementsAndData_var->currentT1Params.Segment0Time << ", numbOfSteps: " << numbOfSteps << std::endl;

    M2i7005_var->M2iSetup_StdReplay(mask, 100, false, false, PulsedMeasurementsAndData_var->currentT1Params.LoopNb, length_in_samples);
    */

    std::vector< std::vector<int>> risingEdges;
    std::vector< std::vector<int>> fallingEdges;
    int numbOfSteps = CreateT1Sequence(risingEdges, fallingEdges);
    int length_in_samples;
    if(numbOfSteps%64==0){
        length_in_samples=numbOfSteps;
    }else{
        length_in_samples=64*(numbOfSteps/64+1);
    }

    M2i7005_var->M2iSetup_StdReplay(mask, 100, false, false, PulsedMeasurementsAndData_var->currentT1Params.LoopNb, length_in_samples);
    M2i7005_var->M2iLoadIntoMemoryFromVec(6, risingEdges, fallingEdges);

    CreateT1wPIFrequencies(PulsedMeasurementsAndData_var->currentT1Params.Freq, PulsedMeasurementsAndData_var->currentT1Params.Steps-1, PulsedMeasurementsAndData_var->currentT1Params.Stepsize, PulsedMeasurementsAndData_var->currentT1Params.LoopNb*2);


    ui->RabiApproxTime->display((double)(length_in_samples)*(double)(ui->LoopsForRabi->value())*1e-8/60);

//    ui->StartT1wPI->setEnabled(true);
}

int MainWindow::CreateT1Sequence(std::vector< std::vector<int>>& risingEdges,  std::vector< std::vector<int>>& fallingEdges)
{
    std::vector<int> risingAWGTrigger;
    std::vector<int> fallingAWGTrigger;
    std::vector<int> risingAWGEvent;
    std::vector<int> fallingAWGEvent;
    std::vector<int> risingCounter;
    std::vector<int> fallingCounter;
    std::vector<int> risingLaserDigital;
    std::vector<int> fallingLaserDigital;
    std::vector<int> risingLaserEnable;
    std::vector<int> fallingLaserEnable;

    int TimeLaser = PulsedMeasurementsAndData_var->currentT1Params.TimeLaser;
    int Steps = PulsedMeasurementsAndData_var->currentT1Params.Steps;
    int Stepsize = PulsedMeasurementsAndData_var->currentT1Params.Stepsize;
    int TimeCounter = PulsedMeasurementsAndData_var->currentT1Params.TimeCounter;
    //int Segment0Time = PulsedMeasurementsAndData_var->currentT1Params.Segment0Time;
    int MWTime = PulsedMeasurementsAndData_var->currentT1Params.PiTime;

    int TimePrevSeg=0;
    double TimePrevSegTemp=0;

    //Trigger for AWG; minus shift of 70 ns
    risingAWGTrigger.push_back((int)((TimeLaser-delayAWG_ns+delayLaserEnableRising_ns-70)/10));
    fallingAWGTrigger.push_back((int)((TimeLaser+100-delayAWG_ns+delayLaserEnableRising_ns-70)/10));

    for(int i = 0; i<(Steps*2); ++i){

        //Signal for Laser Enable (init)
        if(i==0){
            risingLaserEnable.push_back((int)(TimePrevSeg)/10);
        }
        fallingLaserEnable.push_back((int)((TimeLaser+TimePrevSeg)/10));

        //Signal for Laser Digital (init)
        if(i==0){
            risingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+TimePrevSeg)/10));
        }
        fallingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+TimeLaser+TimePrevSeg)/10));

        if(!(i%2)){
            //Event for AWG; minus shift of 70 ns
            risingAWGEvent.push_back((int)((TimeLaser-delayAWG_ns+delayLaserEnableRising_ns-70+TimePrevSeg)/10));
            fallingAWGEvent.push_back((int)((TimeLaser+100-delayAWG_ns+delayLaserEnableRising_ns-70+TimePrevSeg)/10));
        }

        //Signal for TT; additional shift of 60 ns
        risingCounter.push_back((int)((TimeLaser+MWTime+Stepsize*((int)(i/2))+delayLaserEnableRising_ns-delayLaserDigital_ns+60+TimePrevSeg)/10));
        fallingCounter.push_back((int)((TimeLaser+MWTime+Stepsize*(int)(i/2)+delayLaserEnableRising_ns-delayLaserDigital_ns+TimeCounter+60+TimePrevSeg)/10));

        //Signal for Laser Enable (readout) additional shift of 20 ns
        risingLaserEnable.push_back((int)((TimeLaser+MWTime+Stepsize*(int)(i/2)+TimePrevSeg+20)/10));
        if(i==(Steps*2-1)){
            fallingLaserEnable.push_back((int)((TimeLaser+MWTime+Stepsize*(int)(i/2)+delayLaserEnableRising_ns-delayLaserEnableFalling_ns+TimeCounter+TimePrevSeg+100)/10));
        }

        //Signal for Laser Digital (readout)
        risingLaserDigital.push_back((int)((TimeLaser+MWTime+Stepsize*(int)(i/2)+delayLaserEnableRising_ns-delayLaserDigital_ns+TimePrevSeg)/10));
        if(i==(Steps*2-1)){
            fallingLaserDigital.push_back((int)((TimeLaser+MWTime+Stepsize*(int)(i/2)+delayLaserEnableRising_ns-delayLaserDigital_ns+TimeCounter+TimePrevSeg+100)/10));
        }

        TimePrevSeg=*std::max_element(fallingCounter.begin(),fallingCounter.end())*10;
        /*
        TimePrevSegTemp = TimePrevSeg;

        if(i<2){
            TimePrevSeg=Segment0Time*(i+1)+delayLaserEnableRising_ns;
        }else if(i%2){  // for i odd
            TimePrevSeg=Segment0Time*(i+1)+Stepsize*((i*i-1)/4)+delayLaserEnableRising_ns;
        }else{          // for i even
            TimePrevSeg=Segment0Time*(i+1)+Stepsize*(i*i/4)+delayLaserEnableRising_ns;
        }
        */

        //std::cout << "TimePrevSeg=" << TimePrevSeg << " , Time Counter=" << (TimeLaser+MWTime+Stepsize*((int)(i/2))+delayLaserEnableRising_ns-delayLaserDigital_ns+60+TimePrevSegTemp) << std::endl;
    }

    risingEdges.push_back({0});
    fallingEdges.push_back({100});
    risingEdges.push_back(risingAWGEvent);
    fallingEdges.push_back(fallingAWGEvent);
    risingEdges.push_back(risingCounter);
    fallingEdges.push_back(fallingCounter);
    risingEdges.push_back(risingLaserEnable);
    fallingEdges.push_back(fallingLaserEnable);
    risingEdges.push_back(risingLaserDigital);
    fallingEdges.push_back(fallingLaserDigital);
    risingEdges.push_back(risingAWGTrigger);
    fallingEdges.push_back(fallingAWGTrigger);

    return TimePrevSeg/10;
}

void MainWindow::CreateT1wPIFrequencies(double Freq, int stepNb, int Stepsize, int loops){

    ViConstString chan = "1";

    // Create frequency-array
    auto frequencies = linearFreqArray(Freq*MHz_M8195A, Freq*MHz_M8195A, stepNb);

    // Create times-array
    std::vector<double> times (frequencies.size(), PulsedMeasurementsAndData_var->currentT1Params.PiTime/1000.0*microsec_M8195A);

    std::vector<double> phases (frequencies.size(), 0);
    std::vector<double> amplitudes (frequencies.size(), 1);

    ViStatus status = M8195A_var->loadSineArrayWaveformAndSeqTable(chan, frequencies, maxSampleRate, times, PulsedMeasurementsAndData_var->currentT1Params.LoopNb, phases, amplitudes);
//    ViStatus status = M8195A_var->loadSineArrayWaveformAndSeqTable(chan, frequencies, maxSampleRate, times, PulsedMeasurementsAndData_var->currentT1Params.LoopNb);

}

void MainWindow::T1Prepare(double Freq, int nbSteps, int stepSize, double piPulse, int timeLaser, int timeCounter, int loops){

    PulsedMeasurementsAndData_var->currentT1Params.Freq=Freq;
    PulsedMeasurementsAndData_var->currentT1Params.Steps=nbSteps;
    PulsedMeasurementsAndData_var->currentT1Params.Stepsize=stepSize;
    PulsedMeasurementsAndData_var->currentT1Params.TimeCounter=timeCounter;
    PulsedMeasurementsAndData_var->currentT1Params.TimeLaser=timeLaser;
    PulsedMeasurementsAndData_var->currentT1Params.PiTime=piPulse;
    PulsedMeasurementsAndData_var->withRefocus = ui->compPulsRefocusCheckbox->isChecked();


    if((PulsedMeasurementsAndData_var->withRefocus)&&!(loops%10000)){
        PulsedMeasurementsAndData_var->currentCompPulsDetunParams.LoopNb=10000;
        PulsedMeasurementsAndData_var->NbOfRefocus = loops/10000;
        loops=10000;
        std::cout << "with Refocus and right loops" << std::endl;
    }else{
        PulsedMeasurementsAndData_var->currentCompPulsDetunParams.LoopNb=loops;
        PulsedMeasurementsAndData_var->NbOfRefocus = 1;
        std::cout << "Without refocus and/or wrong loops" << std::endl;
    }
    std::cout << "Loops: " << loops << " NbOfRefocus: " << PulsedMeasurementsAndData_var->NbOfRefocus << std::endl;

    // upload pulsed ODMR sequence
    int length_in_samples = ArrayPrepForPulsedMeasurements_var->prepareT1(Freq, nbSteps, stepSize, piPulse, timeCounter, timeLaser, loops, M2i7005_var);
    ArrayPrepForPulsedMeasurements_var->createT1Frequencies(Freq, nbSteps, piPulse, loops, M8195A_var);

    ui->RabiApproxTime->display((double)(length_in_samples)*PulsedMeasurementsAndData_var->NbOfRefocus*(double)(loops)*1e-8/60);
}

void MainWindow::on_StartT1wPI_clicked()
{
//    ui->loadWaveformAndSeqTable->setEnabled(false);
//    ui->PrepareT1->setEnabled(false);
//    ui->PrepareRabi->setEnabled(false);
//    ui->PrepareT1wPI->setEnabled(false);
//    ui->pulsedODMRPrepare->setEnabled(false);
    //ui->compPulsPrep->setEnabled(false);



/*
    std::cout << "Without refocus!" << std::endl;
    on_RUNbutton_clicked();
    PulsedMeasurementsAndData_var->prepareCounterForT1();
    std::cout << "preparation of counter finished" << std::endl;
    on_outputM2i_clicked();
    on_STOPbutton_clicked();
    return;*/



    std::cout << "withRefocus: " << PulsedMeasurementsAndData_var->withRefocus << std::endl;
    if(PulsedMeasurementsAndData_var->withRefocus){
        std::cout << "With refocus!" << std::endl;
        pulsedMeasurementWithRefocus("T1-Scan");
    }else{
        std::cout << "Without refocus!" << std::endl;
        on_RUNbutton_clicked();
        PulsedMeasurementsAndData_var->prepareCounterForT1();
        std::cout << "preparation of counter finished" << std::endl;
        on_outputM2i_clicked();
        on_STOPbutton_clicked();
    }

//    ui->ReadDataT1wPI->setEnabled(true);
}

void MainWindow::on_ReadDataT1wPI_clicked()
{
//    ui->loadWaveformAndSeqTable->setEnabled(true);
//    ui->PrepareT1->setEnabled(true);
//    ui->PrepareRabi->setEnabled(true);
//    ui->PrepareT1wPI->setEnabled(true);
//    ui->pulsedODMRPrepare->setEnabled(true);
//    ui->SingleLorentzianFit->setEnabled(true);
//    ui->DoubleLorentzianFit->setEnabled(true);
    //ui->compPulsPrep->setEnabled(true);

    on_STOPbutton_clicked();
    curDataWidget = ui->ODMRPlot;
    PulsedMeasurementsAndData_var->fetchAndProcessDataForT1();

//    ui->ReadDataT1wPI->setEnabled(false);
}


void MainWindow::on_ramseyButtonPrepare_clicked()
{

    if(checkClickedButton(3, "prep")){
        return;
    }

    PulsedMeasurementsAndData_var->currentRamseyParams.Freq=ui->ramseyFreq->value();
    PulsedMeasurementsAndData_var->currentRamseyParams.Steps=ui->ramseyNbSteps->value()+1;
    PulsedMeasurementsAndData_var->currentRamseyParams.Stepsize=ui->ramseyStepsize->value();
    PulsedMeasurementsAndData_var->currentRamseyParams.PiOver2time=ui->ramseyPiOver2Time->value();
    PulsedMeasurementsAndData_var->currentRamseyParams.TimeLaser=ui->ramseyLaserTime->value();
    PulsedMeasurementsAndData_var->currentRamseyParams.TimeCounter=ui->ramseyTimerTime->value();
    PulsedMeasurementsAndData_var->currentRamseyParams.LoopNb=ui->ramseyLoops->value();
    PulsedMeasurementsAndData_var->withRefocus = 0;

   //############# AWG needs to be prepared !!!!!!!!!!! #############

    int actChN = next_power_of_2(6);
    std::cout << next_power_of_2(6) << " activated channels" << std::endl;

    int64 mask;

    switch(actChN)
    {
        case 1: mask = (int64)1; break;
        case 2: mask = (int64)0x3; break;
        case 4: mask = (int64)0xF; break;
        case 8: mask = (int64)0xFF; break;
        case 16: mask = (int64)0xFFFF; break;
        default: std::cout << "Error: Channel mask is not allowed !" << std::endl; return;
    }

    //int length_in_samples = lround(10047) + 1;  // ~10.047 µs
    PulsedMeasurementsAndData_var->currentRamseyParams.MinRamseyTime=(PulsedMeasurementsAndData_var->currentRamseyParams.TimeLaser+PulsedMeasurementsAndData_var->currentRamseyParams.PiOver2time*2+PulsedMeasurementsAndData_var->currentRamseyParams.TimeCounter+200)+520;

    int numbOfSteps;
    int length_in_samples;

    std::vector< std::vector<int>> risingEdges;
    std::vector< std::vector<int>> fallingEdges;
    if(ui->checkBox->isChecked()){
        numbOfSteps=CreateRamseySequenceSmall(risingEdges, fallingEdges);
        CreateRamseyFrequenciesSmall(PulsedMeasurementsAndData_var->currentRamseyParams.Freq, PulsedMeasurementsAndData_var->currentRamseyParams.Steps, PulsedMeasurementsAndData_var->currentRamseyParams.LoopNb, PulsedMeasurementsAndData_var->currentRamseyParams.PiOver2time, PulsedMeasurementsAndData_var->currentRamseyParams.Stepsize);
    }else{
        numbOfSteps=CreateRamseySequence(risingEdges, fallingEdges);
        CreateRamseyFrequencies(PulsedMeasurementsAndData_var->currentRamseyParams.Freq, PulsedMeasurementsAndData_var->currentRamseyParams.Steps, PulsedMeasurementsAndData_var->currentRamseyParams.LoopNb, PulsedMeasurementsAndData_var->currentRamseyParams.PiOver2time);
    }
    if(numbOfSteps%64==0){
        length_in_samples=numbOfSteps;
    }else{
        length_in_samples=64*(numbOfSteps/64+1);
    }
    M2i7005_var->M2iSetup_StdReplay(mask, 100, false, false, PulsedMeasurementsAndData_var->currentRamseyParams.LoopNb, length_in_samples);
    M2i7005_var->M2iLoadIntoMemoryFromVec(6, risingEdges, fallingEdges);

    ui->RabiApproxTime->display((double)(length_in_samples)*(double)(ui->ramseyLoops->value())*1e-8/60);
}

int MainWindow::CreateRamseySequence(std::vector< std::vector<int>>& risingEdges,  std::vector< std::vector<int>>& fallingEdges)
{
    std::vector<int> risingAWGTrigger;
    std::vector<int> fallingAWGTrigger;
    std::vector<int> risingAWGEvent;
    std::vector<int> fallingAWGEvent;
    std::vector<int> risingCounter;
    std::vector<int> fallingCounter;
    std::vector<int> risingLaserDigital;
    std::vector<int> fallingLaserDigital;
    std::vector<int> risingLaserEnable;
    std::vector<int> fallingLaserEnable;

    int TimeLaser = PulsedMeasurementsAndData_var->currentRamseyParams.TimeLaser;
    int Steps = PulsedMeasurementsAndData_var->currentRamseyParams.Steps;
    double Stepsize = PulsedMeasurementsAndData_var->currentRamseyParams.Stepsize;
    int TimeCounter = PulsedMeasurementsAndData_var->currentRamseyParams.TimeCounter;
    int PiOver2time = PulsedMeasurementsAndData_var->currentRamseyParams.PiOver2time;

    int TimePrevSeg=0;

    //Trigger for AWG; minus shift of 20 ns
    risingAWGTrigger.push_back((int)((TimeLaser-delayAWG_ns+delayLaserEnableRising_ns-20)/10));
    fallingAWGTrigger.push_back((int)((TimeLaser+5-delayAWG_ns+delayLaserEnableRising_ns-20)/10));

    for(int i = 0; i<(Steps); ++i){

        //Signal for Laser Enable (init)
        if(i==0){
            risingLaserEnable.push_back((int)(TimePrevSeg)/10);
        }
        fallingLaserEnable.push_back((int)((TimeLaser+TimePrevSeg)/10));

        //Signal for Laser Digital (init)
        if(i==0){
            risingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+TimePrevSeg)/10));
        }
        fallingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+TimeLaser+TimePrevSeg)/10));

        //Event for AWG; minus shift of 20 ns
        risingAWGEvent.push_back((int)((TimeLaser-delayAWG_ns+delayLaserEnableRising_ns-20+TimePrevSeg)/10));
        fallingAWGEvent.push_back((int)((TimeLaser+5-delayAWG_ns+delayLaserEnableRising_ns-20+TimePrevSeg)/10));

        //Event for AWG; minus shift of 20 ns
        risingAWGEvent.push_back((int)((TimeLaser+PiOver2time+Stepsize*i-delayAWG_ns+delayLaserEnableRising_ns-20+TimePrevSeg)/10));
        fallingAWGEvent.push_back((int)((TimeLaser+5+PiOver2time+Stepsize*i-delayAWG_ns+delayLaserEnableRising_ns-20+TimePrevSeg)/10));

        //Signal for TT; additional shift of 50 ns
        risingCounter.push_back((int)((TimeLaser+2*PiOver2time+Stepsize*i+delayLaserEnableRising_ns-delayLaserDigital_ns+50+TimePrevSeg)/10));
        fallingCounter.push_back((int)((TimeLaser+2*PiOver2time+Stepsize*i+delayLaserEnableRising_ns-delayLaserDigital_ns+50+TimeCounter+TimePrevSeg)/10));

        //Signal for Laser Enable (readout) additional shift of 20 ns
        risingLaserEnable.push_back((int)((TimeLaser+2*PiOver2time+Stepsize*i-delayLaserDigital_ns+20+TimePrevSeg)/10));
        if(i==(Steps-1)){
            fallingLaserEnable.push_back((int)((TimeLaser+2*PiOver2time+Stepsize*i+delayLaserEnableRising_ns-delayLaserEnableFalling_ns+20+TimeCounter+TimePrevSeg+100)/10));
        }

        //Signal for Laser Digital (readout) additional shift of 20 ns
        risingLaserDigital.push_back((int)((TimeLaser+2*PiOver2time+Stepsize*i+delayLaserEnableRising_ns-delayLaserDigital_ns+20+TimePrevSeg)/10));
        if(i==(Steps-1)){
            fallingLaserDigital.push_back((int)((TimeLaser+2*PiOver2time+Stepsize*i+delayLaserEnableRising_ns-delayLaserDigital_ns+20+TimeCounter+TimePrevSeg+100)/10));
        }

        TimePrevSeg=*std::max_element(fallingCounter.begin(),fallingCounter.end())*10;

        //std::cout << "TimePrevSeg=" << TimePrevSeg << " , Time Counter=" << (TimeLaser+MWTime+Stepsize*((int)(i/2))+delayLaserEnableRising_ns-delayLaserDigital_ns+60+TimePrevSegTemp) << std::endl;
    }

    risingEdges.push_back({0});
    fallingEdges.push_back({100});
    risingEdges.push_back(risingAWGEvent);
    fallingEdges.push_back(fallingAWGEvent);
    risingEdges.push_back(risingCounter);
    fallingEdges.push_back(fallingCounter);
    risingEdges.push_back(risingLaserEnable);
    fallingEdges.push_back(fallingLaserEnable);
    risingEdges.push_back(risingLaserDigital);
    fallingEdges.push_back(fallingLaserDigital);
    risingEdges.push_back(risingAWGTrigger);
    fallingEdges.push_back(fallingAWGTrigger);

    return TimePrevSeg/10;
}

int MainWindow::CreateRamseySequenceSmall(std::vector< std::vector<int>>& risingEdges,  std::vector< std::vector<int>>& fallingEdges)
{
    std::vector<int> risingAWGTrigger;
    std::vector<int> fallingAWGTrigger;
    std::vector<int> risingAWGEvent;
    std::vector<int> fallingAWGEvent;
    std::vector<int> risingCounter;
    std::vector<int> fallingCounter;
    std::vector<int> risingLaserDigital;
    std::vector<int> fallingLaserDigital;
    std::vector<int> risingLaserEnable;
    std::vector<int> fallingLaserEnable;

    int TimeLaser = PulsedMeasurementsAndData_var->currentRamseyParams.TimeLaser;
    int Steps = PulsedMeasurementsAndData_var->currentRamseyParams.Steps;
    int Stepsize = PulsedMeasurementsAndData_var->currentRamseyParams.Stepsize;
    int TimeCounter = PulsedMeasurementsAndData_var->currentRamseyParams.TimeCounter;
    int PiOver2time = PulsedMeasurementsAndData_var->currentRamseyParams.PiOver2time;

    int TimePrevSeg=0;

    //Trigger for AWG; minus shift of 20 ns
    risingAWGTrigger.push_back((int)((TimeLaser-delayAWG_ns+delayLaserEnableRising_ns-20)/10));
    fallingAWGTrigger.push_back((int)((TimeLaser+5-delayAWG_ns+delayLaserEnableRising_ns-20)/10));

    for(int i = 0; i<(Steps); ++i){

        //Signal for Laser Enable (init)
        if(i==0){
            risingLaserEnable.push_back((int)(TimePrevSeg)/10);
        }
        fallingLaserEnable.push_back((int)((TimeLaser+TimePrevSeg)/10));

        //Signal for Laser Digital (init)
        if(i==0){
            risingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+TimePrevSeg)/10));
        }
        fallingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+TimeLaser+TimePrevSeg)/10));

        //Event for AWG; minus shift of 20 ns
        risingAWGEvent.push_back((int)((TimeLaser-delayAWG_ns+delayLaserEnableRising_ns-20+TimePrevSeg)/10));
        fallingAWGEvent.push_back((int)((TimeLaser+5-delayAWG_ns+delayLaserEnableRising_ns-20+TimePrevSeg)/10));

        //Signal for TT; additional shift of 50 ns
        risingCounter.push_back((int)((TimeLaser+2*PiOver2time+Stepsize*i+delayLaserEnableRising_ns-delayLaserDigital_ns+50+TimePrevSeg)/10));
        fallingCounter.push_back((int)((TimeLaser+2*PiOver2time+Stepsize*i+delayLaserEnableRising_ns-delayLaserDigital_ns+50+TimeCounter+TimePrevSeg)/10));

        //Signal for Laser Enable (readout) additional shift of 20 ns
        risingLaserEnable.push_back((int)((TimeLaser+2*PiOver2time+Stepsize*i-delayLaserDigital_ns+20+TimePrevSeg)/10));
        if(i==(Steps-1)){
            fallingLaserEnable.push_back((int)((TimeLaser+2*PiOver2time+Stepsize*i+delayLaserEnableRising_ns-delayLaserEnableFalling_ns+20+TimeCounter+TimePrevSeg+100)/10));
        }

        //Signal for Laser Digital (readout) additional shift of 20 ns
        risingLaserDigital.push_back((int)((TimeLaser+2*PiOver2time+Stepsize*i+delayLaserEnableRising_ns-delayLaserDigital_ns+20+TimePrevSeg)/10));
        if(i==(Steps-1)){
            fallingLaserDigital.push_back((int)((TimeLaser+2*PiOver2time+Stepsize*i+delayLaserEnableRising_ns-delayLaserDigital_ns+20+TimeCounter+TimePrevSeg+100)/10));
        }

        TimePrevSeg=*std::max_element(fallingCounter.begin(),fallingCounter.end())*10;
    }

    risingEdges.push_back({0});
    fallingEdges.push_back({100});
    risingEdges.push_back(risingAWGEvent);
    fallingEdges.push_back(fallingAWGEvent);
    risingEdges.push_back(risingCounter);
    fallingEdges.push_back(fallingCounter);
    risingEdges.push_back(risingLaserEnable);
    fallingEdges.push_back(fallingLaserEnable);
    risingEdges.push_back(risingLaserDigital);
    fallingEdges.push_back(fallingLaserDigital);
    risingEdges.push_back(risingAWGTrigger);
    fallingEdges.push_back(fallingAWGTrigger);

    return TimePrevSeg/10;
}

void MainWindow::CreateRamseyFrequencies(double Freq, int stepNb, int loops, double PiOver2time){

    ViConstString chan = "1";

    // Create frequency-array
    //auto frequencies = linearFreqArray(Freq*MHz_M8195A, Freq*MHz_M8195A, stepNb*2-1);
    std::vector<double> frequencies (stepNb*2, Freq*MHz_M8195A);

    // Create times-array
    std::vector<double> times (frequencies.size(), PiOver2time/1000.0*microsec_M8195A);

    std::vector<double> phases (frequencies.size(), 0);
    std::vector<double> amplitudes (frequencies.size(), 1);

    ViStatus status = M8195A_var->loadSineArrayWaveformAndSeqTable(chan, frequencies, maxSampleRate, times, loops, phases, amplitudes);
//    ViStatus status = M8195A_var->loadSineArrayWaveformAndSeqTable(chan, frequencies, maxSampleRate, times, loops);
}

void MainWindow::CreateRamseyFrequenciesSmall(double Freq, int stepNb, int loops, double PiOver2time, double stepSize){

    ViConstString chan = "1";

    // Create frequency-array
    //auto frequencies = linearFreqArray(Freq*MHz_M8195A, Freq*MHz_M8195A, stepNb-1);
    std::vector<double> frequencies (stepNb, Freq*MHz_M8195A);

    // Create times-array
    std::vector<double> times_piOver2 (frequencies.size(), PiOver2time/1000.0*microsec_M8195A);

    ViStatus status = M8195A_var->loadRamseySpinEchoWaveformAndSeqTable(chan, frequencies, maxSampleRate, PiOver2time/1000.0*microsec_M8195A, 0, loops, stepSize/1000.0*microsec_M8195A, true);
//    ViStatus status = M8195A_var->loadSineArrayWaveformAndSeqTable(chan, frequencies, maxSampleRate, times, loops);
}

void MainWindow::on_ramseyButtonStart_clicked()
{
    if(checkClickedButton(3, "start")){
        return;
    }

    PulsedMeasurementsAndData_var->prepareCounterForRamsey();
    on_RUNbutton_clicked();
    on_outputM2i_clicked();
    on_STOPbutton_clicked();
}

void MainWindow::on_ramseyButtonRead_clicked()
{
    if(checkClickedButton(3, "read")){
        return;
    }

    curDataWidget = ui->ODMRPlot;
    PulsedMeasurementsAndData_var->fetchAndProcessDataForRamsey();
}

void MainWindow::on_spinEchoButtonPrepare_clicked()
{
    if(checkClickedButton(4, "prep")){
        return;
    }
    PulsedMeasurementsAndData_var->currentSpinEchoParams.Freq=ui->spinEchoFreq->value();
    PulsedMeasurementsAndData_var->currentSpinEchoParams.Steps=ui->spinEchoNbSteps->value()+1;
    PulsedMeasurementsAndData_var->currentSpinEchoParams.Stepsize=ui->spinEchoStepsize->value();
    PulsedMeasurementsAndData_var->currentSpinEchoParams.PiOver2time=ui->spinEchoPiOver2Time->value();
    PulsedMeasurementsAndData_var->currentSpinEchoParams.TimeLaser=ui->spinEchoLaserTime->value();
    PulsedMeasurementsAndData_var->currentSpinEchoParams.TimeCounter=ui->spinEchoTimerTime->value();
    PulsedMeasurementsAndData_var->currentSpinEchoParams.LoopNb=ui->spinEchoLoops->value();
    PulsedMeasurementsAndData_var->currentSpinEchoParams.PiTime=ui->spinEchoPiTime->value();
    PulsedMeasurementsAndData_var->withRefocus = 0;

   //############# AWG needs to be prepared !!!!!!!!!!! #############

    int actChN = next_power_of_2(6);
    std::cout << next_power_of_2(6) << " activated channels" << std::endl;

    int64 mask;

    switch(actChN)
    {
        case 1: mask = (int64)1; break;
        case 2: mask = (int64)0x3; break;
        case 4: mask = (int64)0xF; break;
        case 8: mask = (int64)0xFF; break;
        case 16: mask = (int64)0xFFFF; break;
        default: std::cout << "Error: Channel mask is not allowed !" << std::endl; return;
    }

    //int length_in_samples = lround(10047) + 1;  // ~10.047 µs
    PulsedMeasurementsAndData_var->currentSpinEchoParams.MinSpinEchoTime=(PulsedMeasurementsAndData_var->currentSpinEchoParams.TimeLaser+PulsedMeasurementsAndData_var->currentSpinEchoParams.PiOver2time*2+PulsedMeasurementsAndData_var->currentSpinEchoParams.PiTime+PulsedMeasurementsAndData_var->currentSpinEchoParams.TimeCounter+200);

    int numbOfSteps;
    int length_in_samples;

    std::vector< std::vector<int>> risingEdges;
    std::vector< std::vector<int>> fallingEdges;

    if(ui->checkBox->isChecked()){
        numbOfSteps=CreateSpinEchoSequenceSmall(risingEdges, fallingEdges);
        CreateSpinEchoFrequenciesSmall(PulsedMeasurementsAndData_var->currentSpinEchoParams.Freq, PulsedMeasurementsAndData_var->currentSpinEchoParams.Steps, PulsedMeasurementsAndData_var->currentSpinEchoParams.LoopNb, PulsedMeasurementsAndData_var->currentSpinEchoParams.PiOver2time, PulsedMeasurementsAndData_var->currentSpinEchoParams.PiTime, PulsedMeasurementsAndData_var->currentSpinEchoParams.Stepsize/2);
    }else{
        numbOfSteps=CreateSpinEchoSequence(risingEdges, fallingEdges);
        CreateSpinEchoFrequencies(PulsedMeasurementsAndData_var->currentSpinEchoParams.Freq, PulsedMeasurementsAndData_var->currentSpinEchoParams.Steps, PulsedMeasurementsAndData_var->currentSpinEchoParams.LoopNb, PulsedMeasurementsAndData_var->currentSpinEchoParams.PiOver2time, PulsedMeasurementsAndData_var->currentSpinEchoParams.PiTime);
    }

    if(numbOfSteps%64==0){
        length_in_samples=numbOfSteps;
    }else{
        length_in_samples=64*(numbOfSteps/64+1);
    }
    M2i7005_var->M2iSetup_StdReplay(mask, 100, false, false, PulsedMeasurementsAndData_var->currentSpinEchoParams.LoopNb, length_in_samples);
    M2i7005_var->M2iLoadIntoMemoryFromVec(6, risingEdges, fallingEdges);

    ui->RabiApproxTime->display((double)(numbOfSteps)*(double)(ui->spinEchoLoops->value())*1e-8/60);
}

int MainWindow::CreateSpinEchoSequence(std::vector< std::vector<int>>& risingEdges,  std::vector< std::vector<int>>& fallingEdges)
{
    std::vector<int> risingAWGTrigger;
    std::vector<int> fallingAWGTrigger;
    std::vector<int> risingAWGEvent;
    std::vector<int> fallingAWGEvent;
    std::vector<int> risingCounter;
    std::vector<int> fallingCounter;
    std::vector<int> risingLaserDigital;
    std::vector<int> fallingLaserDigital;
    std::vector<int> risingLaserEnable;
    std::vector<int> fallingLaserEnable;

    int TimeLaser = PulsedMeasurementsAndData_var->currentSpinEchoParams.TimeLaser;
    int Steps = PulsedMeasurementsAndData_var->currentSpinEchoParams.Steps;
    double Stepsize = PulsedMeasurementsAndData_var->currentSpinEchoParams.Stepsize/2;
    int TimeCounter = PulsedMeasurementsAndData_var->currentSpinEchoParams.TimeCounter;
    int MinSpinEchoTime = PulsedMeasurementsAndData_var->currentSpinEchoParams.MinSpinEchoTime;
    int PiOver2time = PulsedMeasurementsAndData_var->currentSpinEchoParams.PiOver2time;
    int PiTime = PulsedMeasurementsAndData_var->currentSpinEchoParams.PiTime;

    int TimePrevSeg=0;

    //Trigger for AWG; minus shift of 20 ns
    risingAWGTrigger.push_back((int)((TimeLaser-delayAWG_ns+delayLaserEnableRising_ns-20)/10));
    fallingAWGTrigger.push_back((int)((TimeLaser+5-delayAWG_ns+delayLaserEnableRising_ns-250)/10));

    for(int i = 0; i<(Steps); ++i){

        //Signal for Laser Enable (init)
        if(i==0){
            risingLaserEnable.push_back((int)(TimePrevSeg)/10);
        }
        fallingLaserEnable.push_back((int)((TimeLaser+TimePrevSeg)/10));

        //Signal for Laser Digital (init)
        if(i==0){
            risingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+TimePrevSeg)/10));
        }
        fallingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+TimeLaser+TimePrevSeg)/10));

        //Event for AWG; minus shift of 20 ns
        risingAWGEvent.push_back((int)((TimeLaser-delayAWG_ns+delayLaserEnableRising_ns-20+TimePrevSeg)/10));
        fallingAWGEvent.push_back((int)((TimeLaser+5-delayAWG_ns+delayLaserEnableRising_ns-20+TimePrevSeg)/10));

        //Event for AWG; minus shift of 20 ns
        risingAWGEvent.push_back((int)((TimeLaser+PiOver2time+Stepsize*i-delayAWG_ns+delayLaserEnableRising_ns-20+TimePrevSeg)/10));
        fallingAWGEvent.push_back((int)((TimeLaser+5+PiOver2time+Stepsize*i-delayAWG_ns+delayLaserEnableRising_ns-20+TimePrevSeg)/10));

        //Event for AWG; minus shift of 20 ns
        risingAWGEvent.push_back((int)((TimeLaser+PiOver2time+PiTime+2*Stepsize*i-delayAWG_ns+delayLaserEnableRising_ns-20+TimePrevSeg)/10));
        fallingAWGEvent.push_back((int)((TimeLaser+5+PiOver2time+PiTime+2*Stepsize*i-delayAWG_ns+delayLaserEnableRising_ns-20+TimePrevSeg)/10));

        //Signal for TT; additional shift of 60 ns
        risingCounter.push_back((int)((TimeLaser+2*PiOver2time+PiTime+2*Stepsize*i+delayLaserEnableRising_ns-delayLaserDigital_ns+60+TimePrevSeg)/10));
        fallingCounter.push_back((int)((TimeLaser+2*PiOver2time+PiTime+2*Stepsize*i+delayLaserEnableRising_ns-delayLaserDigital_ns+TimeCounter+60+TimePrevSeg)/10));

        //Signal for Laser Enable (readout) additional shift of 20 ns
        risingLaserEnable.push_back((int)((TimeLaser+2*PiOver2time+PiTime+2*Stepsize*i+TimePrevSeg+20)/10));
        if(i==(Steps*2-1)){
            fallingLaserEnable.push_back((int)((TimeLaser+2*PiOver2time+PiTime+2*Stepsize*i+delayLaserEnableRising_ns-delayLaserEnableFalling_ns+TimeCounter+TimePrevSeg+100)/10));
        }

        //Signal for Laser Digital (readout)
        risingLaserDigital.push_back((int)((TimeLaser+2*PiOver2time+PiTime+2*Stepsize*i+delayLaserEnableRising_ns-delayLaserDigital_ns+TimePrevSeg)/10));
        if(i==(Steps*2-1)){
            fallingLaserDigital.push_back((int)((TimeLaser+2*PiOver2time+PiTime+2*Stepsize*i+delayLaserEnableRising_ns-delayLaserDigital_ns+TimeCounter+TimePrevSeg+100)/10));
        }

        TimePrevSeg=*std::max_element(fallingCounter.begin(),fallingCounter.end())*10;

        //std::cout << "TimePrevSeg=" << TimePrevSeg << std::endl;
    }

    risingEdges.push_back({0});
    fallingEdges.push_back({100});
    risingEdges.push_back(risingAWGEvent);
    fallingEdges.push_back(fallingAWGEvent);
    risingEdges.push_back(risingCounter);
    fallingEdges.push_back(fallingCounter);
    risingEdges.push_back(risingLaserEnable);
    fallingEdges.push_back(fallingLaserEnable);
    risingEdges.push_back(risingLaserDigital);
    fallingEdges.push_back(fallingLaserDigital);
    risingEdges.push_back(risingAWGTrigger);
    fallingEdges.push_back(fallingAWGTrigger);

    return TimePrevSeg/10;
}

int MainWindow::CreateSpinEchoSequenceSmall(std::vector< std::vector<int>>& risingEdges,  std::vector< std::vector<int>>& fallingEdges)
{
    std::vector<int> risingAWGTrigger;
    std::vector<int> fallingAWGTrigger;
    std::vector<int> risingAWGEvent;
    std::vector<int> fallingAWGEvent;
    std::vector<int> risingCounter;
    std::vector<int> fallingCounter;
    std::vector<int> risingLaserDigital;
    std::vector<int> fallingLaserDigital;
    std::vector<int> risingLaserEnable;
    std::vector<int> fallingLaserEnable;

    int TimeLaser = PulsedMeasurementsAndData_var->currentSpinEchoParams.TimeLaser;
    int Steps = PulsedMeasurementsAndData_var->currentSpinEchoParams.Steps;
    double Stepsize = PulsedMeasurementsAndData_var->currentSpinEchoParams.Stepsize/2;
    int TimeCounter = PulsedMeasurementsAndData_var->currentSpinEchoParams.TimeCounter;
    int MinSpinEchoTime = PulsedMeasurementsAndData_var->currentSpinEchoParams.MinSpinEchoTime;
    int PiOver2time = PulsedMeasurementsAndData_var->currentSpinEchoParams.PiOver2time;
    int PiTime = PulsedMeasurementsAndData_var->currentSpinEchoParams.PiTime;

    int TimePrevSeg=0;

    //Trigger for AWG; minus shift of 70 ns
    risingAWGTrigger.push_back((int)((TimeLaser-delayAWG_ns+delayLaserEnableRising_ns-20)/10));
    fallingAWGTrigger.push_back((int)((TimeLaser+5-delayAWG_ns+delayLaserEnableRising_ns-20)/10));

    for(int i = 0; i<(Steps); ++i){

        //Signal for Laser Enable (init)
        if(i==0){
            risingLaserEnable.push_back((int)(TimePrevSeg)/10);
        }
        fallingLaserEnable.push_back((int)((TimeLaser+TimePrevSeg)/10));

        //Signal for Laser Digital (init)
        if(i==0){
            risingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+TimePrevSeg)/10));
        }
        fallingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+TimeLaser+TimePrevSeg)/10));

        //Event for AWG; additional shift of 40 ns
        risingAWGEvent.push_back((int)((TimeLaser-delayAWG_ns+delayLaserEnableRising_ns-20+TimePrevSeg)/10));
        fallingAWGEvent.push_back((int)((TimeLaser+5-delayAWG_ns+delayLaserEnableRising_ns-20+TimePrevSeg)/10));

        //Signal for TT; additional shift of 60 ns
        risingCounter.push_back((int)((TimeLaser+2*PiOver2time+PiTime+2*Stepsize*i+delayLaserEnableRising_ns-delayLaserDigital_ns+60+TimePrevSeg)/10));
        fallingCounter.push_back((int)((TimeLaser+2*PiOver2time+PiTime+2*Stepsize*i+delayLaserEnableRising_ns-delayLaserDigital_ns+TimeCounter+60+TimePrevSeg)/10));

        //Signal for Laser Enable (readout) additional shift of 20 ns
        risingLaserEnable.push_back((int)((TimeLaser+2*PiOver2time+PiTime+2*Stepsize*i+TimePrevSeg+20)/10));
        if(i==(Steps-1)){
            fallingLaserEnable.push_back((int)((TimeLaser+2*PiOver2time+PiTime+2*Stepsize*i+delayLaserEnableRising_ns-delayLaserEnableFalling_ns+TimeCounter+TimePrevSeg+100)/10));
        }

        //Signal for Laser Digital (readout)
        risingLaserDigital.push_back((int)((TimeLaser+2*PiOver2time+PiTime+2*Stepsize*i+delayLaserEnableRising_ns-delayLaserDigital_ns+TimePrevSeg)/10));
        if(i==(Steps-1)){
            fallingLaserDigital.push_back((int)((TimeLaser+2*PiOver2time+PiTime+2*Stepsize*i+delayLaserEnableRising_ns-delayLaserDigital_ns+TimeCounter+TimePrevSeg+100)/10));
        }

        TimePrevSeg=*std::max_element(fallingCounter.begin(),fallingCounter.end())*10;
    }

    risingEdges.push_back({0});
    fallingEdges.push_back({100});
    risingEdges.push_back(risingAWGEvent);
    fallingEdges.push_back(fallingAWGEvent);
    risingEdges.push_back(risingCounter);
    fallingEdges.push_back(fallingCounter);
    risingEdges.push_back(risingLaserEnable);
    fallingEdges.push_back(fallingLaserEnable);
    risingEdges.push_back(risingLaserDigital);
    fallingEdges.push_back(fallingLaserDigital);
    risingEdges.push_back(risingAWGTrigger);
    fallingEdges.push_back(fallingAWGTrigger);

    return TimePrevSeg/10;
}

void MainWindow::CreateSpinEchoFrequencies(double Freq, int stepNb, int loops, double PiOver2time, double PiTime){

    ViConstString chan = "1";

    // Create frequency-array
    auto frequencies = linearFreqArray(Freq*MHz_M8195A, Freq*MHz_M8195A, stepNb*3-1);

    // Create times-array
    std::vector<double> times (frequencies.size(), PiOver2time/1000.0*microsec_M8195A);
    int i=1;
    while(i<times.size()){
        times[i]=PiTime/1000.0*microsec_M8195A;
        i+=3;
    }

    std::vector<double> phases (frequencies.size(), 0);
    std::vector<double> amplitudes (frequencies.size(), 1);

    ViStatus status = M8195A_var->loadSineArrayWaveformAndSeqTable(chan, frequencies, maxSampleRate, times, loops, phases, amplitudes);
//    ViStatus status = M8195A_var->loadSineArrayWaveformAndSeqTable(chan, frequencies, maxSampleRate, times, loops);
}

void MainWindow::CreateSpinEchoFrequenciesSmall(double Freq, int stepNb, int loops, double PiOver2time, double PiTime, double stepSize){

    ViConstString chan = "1";

    // Create frequency-array
    auto frequencies = linearFreqArray(Freq*MHz_M8195A, Freq*MHz_M8195A, stepNb-1);

    // Create times-array
    std::vector<double> times_piOver2 (frequencies.size(), PiOver2time/1000.0*microsec_M8195A);
    std::vector<double> times_pi (frequencies.size(), PiTime/1000.0*microsec_M8195A);

    ViStatus status = M8195A_var->loadRamseySpinEchoWaveformAndSeqTable(chan, frequencies, maxSampleRate, PiOver2time/1000.0*microsec_M8195A, PiTime/1000.0*microsec_M8195A, loops, stepSize/1000.0*microsec_M8195A, false);
//    ViStatus status = M8195A_var->loadSineArrayWaveformAndSeqTable(chan, frequencies, maxSampleRate, times, loops);
}

void MainWindow::on_spinEchoButtonStart_clicked()
{
    if(checkClickedButton(4, "start")){
        return;
    }

    PulsedMeasurementsAndData_var->prepareCounterForSpinEcho();
    on_RUNbutton_clicked();
    on_outputM2i_clicked();
    on_STOPbutton_clicked();
}

void MainWindow::on_spinEchoButtonRead_clicked()
{
    if(checkClickedButton(4, "read")){
        return;
    }

    curDataWidget = ui->ODMRPlot;
    PulsedMeasurementsAndData_var->fetchAndProcessDataForSpinEcho();
}


void MainWindow::on_pushButton_3_clicked()
{
    PulsedMeasurementsAndData_var->currentTestReadoutParams.Freq = ui->TestFreq->value();
    PulsedMeasurementsAndData_var->currentTestReadoutParams.LoopNb = ui->TestLoops->value();
    PulsedMeasurementsAndData_var->currentTestReadoutParams.PiTime = ui->TestPiTime->value();
    PulsedMeasurementsAndData_var->currentTestReadoutParams.TimeLaser = 1500;
    PulsedMeasurementsAndData_var->currentTestReadoutParams.TimeCounter = 300;

    //############# AWG needs to be prepared !!!!!!!!!!! #############

     int actChN = next_power_of_2(6);
     std::cout << next_power_of_2(6) << " activated channels" << std::endl;

     int64 mask;

     switch(actChN)
     {
         case 1: mask = (int64)1; break;
         case 2: mask = (int64)0x3; break;
         case 4: mask = (int64)0xF; break;
         case 8: mask = (int64)0xFF; break;
         case 16: mask = (int64)0xFFFF; break;
         default: std::cout << "Error: Channel mask is not allowed !" << std::endl; return;
     }

     //int length_in_samples = lround(10047) + 1;  // ~10.047 µs

     int numbOfSteps = (int)((4*PulsedMeasurementsAndData_var->currentTestReadoutParams.PiTime+2000+3*1500+500)/10)+4;
     int length_in_samples;
     if(numbOfSteps%64==0){
         length_in_samples=numbOfSteps;
     }else{
         length_in_samples=64*(numbOfSteps/64+1);
     }

     std::cout << "loop_np: " << PulsedMeasurementsAndData_var->currentTestReadoutParams.LoopNb << ", frequency_MHz: " << 100 << " length_in_samples: " << length_in_samples << std::endl;
     std::cout << "TimePerSeg: " << (numbOfSteps-4)*10 << ", numbOfSteps: " << numbOfSteps << std::endl;

     M2i7005_var->M2iSetup_StdReplay(mask, 100, false, false, PulsedMeasurementsAndData_var->currentTestReadoutParams.LoopNb, length_in_samples);

     std::vector< std::vector<int>> risingEdges;
     std::vector< std::vector<int>> fallingEdges;
     CreateTestReadoutSequence(risingEdges, fallingEdges);

     M2i7005_var->M2iLoadIntoMemoryFromVec(6, risingEdges, fallingEdges);
     CreateTestReadoutFrequencies(PulsedMeasurementsAndData_var->currentTestReadoutParams.Freq, PulsedMeasurementsAndData_var->currentTestReadoutParams.LoopNb, PulsedMeasurementsAndData_var->currentTestReadoutParams.PiTime);

     ui->RabiApproxTime->display((double)(numbOfSteps)*(double)(ui->TestLoops->value())*1e-8/60);

}

bool MainWindow::CreateTestReadoutSequence(std::vector< std::vector<int>>& risingEdges,  std::vector< std::vector<int>>& fallingEdges)
{
    std::vector<int> risingAWGTrigger;
    std::vector<int> fallingAWGTrigger;
    std::vector<int> risingAWGEvent;
    std::vector<int> fallingAWGEvent;
    std::vector<int> risingCounter;
    std::vector<int> fallingCounter;
    std::vector<int> risingLaserDigital;
    std::vector<int> fallingLaserDigital;
    std::vector<int> risingLaserEnable;
    std::vector<int> fallingLaserEnable;

    int TimeLaser = PulsedMeasurementsAndData_var->currentTestReadoutParams.TimeLaser;
    int TimeCounter = PulsedMeasurementsAndData_var->currentTestReadoutParams.TimeCounter;
    int PiTime = PulsedMeasurementsAndData_var->currentTestReadoutParams.PiTime;


    //-----Bright readout-----

    //Signal for Laser Enable (init)
    risingLaserEnable.push_back((int)(0)/10);
    fallingLaserEnable.push_back((int)((TimeLaser)/10));

    //Signal for Laser Digital (init)
    risingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns)/10));
    fallingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+TimeLaser)/10));

    //Signal for Laser Enable (readout brightstate and init)
    risingLaserEnable.push_back((int)((TimeLaser+1.5*PiTime)/10));
    fallingLaserEnable.push_back((int)((2*TimeLaser+1.5*PiTime)/10));

    //Signal for Laser Digital (readout brightstate and init)
    risingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+TimeLaser+1.5*PiTime)/10));
    fallingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+2*TimeLaser+1.5*PiTime)/10));

    //Signal for TT; additional shift of 20 ns
    risingCounter.push_back((int)((TimeLaser+1.5*PiTime+delayLaserEnableRising_ns-delayLaserDigital_ns+20)/10));
    fallingCounter.push_back((int)((TimeLaser+1.5*PiTime+delayLaserEnableRising_ns-delayLaserDigital_ns+TimeCounter+20)/10));

    //-----Dark readout-----

    //Trigger for AWG; additional shift of 20 ns
    risingAWGTrigger.push_back((int)((2*TimeLaser+1.5*PiTime-delayAWG_ns+delayLaserEnableRising_ns+100)/10));
    fallingAWGTrigger.push_back((int)((2*TimeLaser+1.5*PiTime+5-delayAWG_ns+delayLaserEnableRising_ns+100)/10));

    //Event for AWG; additional shift of 20 ns
    risingAWGEvent.push_back((int)((2*TimeLaser+1.5*PiTime-delayAWG_ns+delayLaserEnableRising_ns+100)/10));
    fallingAWGEvent.push_back((int)((2*TimeLaser+1.5*PiTime+5-delayAWG_ns+delayLaserEnableRising_ns+100)/10));

    //Signal for Laser Enable (readout brightstate and init); additional shift of 20 ns
    risingLaserEnable.push_back((int)((2*TimeLaser+3*PiTime+100)/10));
    fallingLaserEnable.push_back((int)((3*TimeLaser+3*PiTime+100)/10));

    //Signal for Laser Digital (readout brightstate and init); additional shift of 20 ns
    risingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+2*TimeLaser+3*PiTime+120)/10));
    fallingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+3*TimeLaser+3*PiTime+120)/10));

    //Signal for TT; additional shift of 40 ns
    risingCounter.push_back((int)((2*TimeLaser+3*PiTime+delayLaserEnableRising_ns-delayLaserDigital_ns+140)/10));
    fallingCounter.push_back((int)((2*TimeLaser+3*PiTime+delayLaserEnableRising_ns-delayLaserDigital_ns+TimeCounter+140)/10));

    //-----Manipulations (for 2 µs)-----

    //Event for AWG; minus shift of 70 ns
    risingAWGEvent.push_back((int)((3*TimeLaser+3*PiTime-delayAWG_ns+delayLaserEnableRising_ns+170)/10));
    fallingAWGEvent.push_back((int)((3*TimeLaser+3*PiTime+5-delayAWG_ns+delayLaserEnableRising_ns+170)/10));

    risingEdges.push_back({0});
    fallingEdges.push_back({100});
    risingEdges.push_back(risingAWGEvent);
    fallingEdges.push_back(fallingAWGEvent);
    risingEdges.push_back(risingCounter);
    fallingEdges.push_back(fallingCounter);
    risingEdges.push_back(risingLaserEnable);
    fallingEdges.push_back(fallingLaserEnable);
    risingEdges.push_back(risingLaserDigital);
    fallingEdges.push_back(fallingLaserDigital);
    risingEdges.push_back(risingAWGTrigger);
    fallingEdges.push_back(fallingAWGTrigger);

    return true;
}

void MainWindow::CreateTestReadoutFrequencies(double Freq, int loops, double PiTime){

    ViConstString chan = "1";

    // Create frequency-array
    auto frequencies = linearFreqArray(Freq*MHz_M8195A, Freq*MHz_M8195A, 1);

    // Create times-array
    std::vector<double> times (frequencies.size(), PiTime/1000.0*microsec_M8195A);
    times[1] = 2000/1000.0*microsec_M8195A;

    std::vector<double> phases (frequencies.size(), 0);
    std::vector<double> amplitudes (frequencies.size(), 1);

    ViStatus status = M8195A_var->loadSineArrayWaveformAndSeqTable(chan, frequencies, maxSampleRate, times, loops, phases, amplitudes);
//    ViStatus status = M8195A_var->loadSineArrayWaveformAndSeqTable(chan, frequencies, maxSampleRate, times, loops);

}


void MainWindow::on_pushButton_2_clicked()
{
    PulsedMeasurementsAndData_var->prepareCounterForTestReadout();
    on_RUNbutton_clicked();
    on_outputM2i_clicked();
    on_STOPbutton_clicked();
}

void MainWindow::on_pushButton_5_clicked()
{
    curDataWidget = ui->ODMRPlot;
    PulsedMeasurementsAndData_var->fetchAndProcessDataForTestReadout();
}



void MainWindow::on_LoadWagenData_clicked()
{
    //LoadFileCreatMosaic_var->loadDataFromZScan("E:/data/20200515/Z-Scan1");
    curDataWidget = ui->ODMRPlot;

    QString fileToOpen = QFileDialog::getOpenFileName(this, "Open file" , QString::fromStdString(globalPath.string()));
    if(!fileToOpen.isEmpty()&& !fileToOpen.isNull()){
        PulsedMeasurementsAndData_var->readDataFromDatFile(fileToOpen);
//        ui->SingleLorentzianFit->setEnabled(true);
//        ui->DoubleLorentzianFit->setEnabled(true);
    }
}

void MainWindow::on_WavegenPDFsave_clicked()
{
    QString Path = QString::fromStdString(PulsedMeasurementsAndData_var->currentDataFromFile.PathLoaded);
    QString Name = QString::fromStdString(PulsedMeasurementsAndData_var->currentDataFromFile.fileNameLoaded);
    ui->ODMRPlot->savePdf(Path+"\\"+Name+".pdf");
}

void MainWindow::on_WavegenPNGsave_clicked()
{
    QString Path = QString::fromStdString(PulsedMeasurementsAndData_var->currentDataFromFile.PathLoaded);
    QString Name = QString::fromStdString(PulsedMeasurementsAndData_var->currentDataFromFile.fileNameLoaded);
    ui->ODMRPlot->savePng(Path+"\\"+Name+".png");
}


// ----------------------------------------------------
// ------------- Compusite Tab and Functions -----------
// ----------------------------------------------------



void MainWindow::on_pushButton_clicked()
{
    double Freq = 3000;
    int stepNb = 1;
    int loops = 1;

    //CreateCompTest(Freq, stepNb, loops);

    on_RUNbutton_clicked();
}

void MainWindow::CreateCompTest(double Freq, int stepNb, int loops){

    ViConstString chan = "1";

    // Create frequency-array
    auto frequencies = linearFreqArray(Freq*MHz_M8195A, Freq*MHz_M8195A, stepNb);

    // Create times-array
    std::vector<double> times (frequencies.size(), 0.05*microsec_M8195A);

    std::vector<double> phases (frequencies.size(), ui->doubleSpinBox->value());
    std::vector<double> amplitudes (frequencies.size(), ui->doubleSpinBox_2->value());

    ViStatus status = M8195A_var->loadSineArrayWaveformAndSeqTable(chan, frequencies, maxSampleRate, times, PulsedMeasurementsAndData_var->currentPulsedODMRParams.LoopNb, phases, amplitudes);
//    ViStatus status = M8195A_var->loadSineArrayWaveformAndSeqTable(chan, frequencies, maxSampleRate, times, PulsedMeasurementsAndData_var->currentPulsedODMRParams.LoopNb);

}


void MainWindow::compositePulsesReadAndFill(QString fileName){
    QFile file(fileName);
    QFileInfo getFilenName_var(fileName);

    PulsedMeasurementsAndData_var->currentCompusitePulses.phases.clear();
    PulsedMeasurementsAndData_var->currentCompusitePulses.startTimes.clear();

    if(file.open(QIODevice::ReadOnly)){
        QTextStream DataFromFile(&file);
        QString line;
        while(!DataFromFile.atEnd()){
            line = DataFromFile.readLine();
            QStringList spliteLine = line.split("\t");
            PulsedMeasurementsAndData_var->currentCompusitePulses.phases.push_back(spliteLine[0].toDouble());
            PulsedMeasurementsAndData_var->currentCompusitePulses.startTimes.push_back(spliteLine[1].toDouble());
        }
        file.close();
    }

    ui->compositePulsesTable->setRowCount(2);
    ui->compositePulsesTable->setColumnCount(PulsedMeasurementsAndData_var->currentCompusitePulses.phases.size()+1);

    ui->compositePulsesTable->setItem(0,0,new QTableWidgetItem("Phases [pi]: "));
    ui->compositePulsesTable->setItem(1,0,new QTableWidgetItem("Starttimes [ns]: "));

    for(int i=1; i < (int)PulsedMeasurementsAndData_var->currentCompusitePulses.phases.size()+1; i++){
        ui->compositePulsesTable->setItem(0,i,new QTableWidgetItem(QString::number(PulsedMeasurementsAndData_var->currentCompusitePulses.phases[i-1])));
        ui->compositePulsesTable->setItem(1,i,new QTableWidgetItem(QString::number(PulsedMeasurementsAndData_var->currentCompusitePulses.startTimes[i-1])));
    }


}

void MainWindow::on_LoadCompPulses_clicked()
{
    QString fileToOpen = QFileDialog::getOpenFileName(this, "Open file" , QString::fromStdString(globalPath.string()));
    if(!fileToOpen.isEmpty()&& !fileToOpen.isNull()){
        compositePulsesReadAndFill(fileToOpen);
        //compPulsesForTable.insert(compPulsesForTable.begin(),PulsedMeasurementsAndData_var->readDataForCompositePulses(fileToOpen));
    }
}


void MainWindow::on_LoadCompPulsData_clicked()
{
    curDataWidget = ui->CompPuls;
//    ui->CompPuls->clearGraphs();

    QString fileToOpen = QFileDialog::getOpenFileName(this, "Open file" , QString::fromStdString(globalPath.string()));
    if(!fileToOpen.isEmpty()&& !fileToOpen.isNull()){
        PulsedMeasurementsAndData_var->readDataFromDatFile(fileToOpen);
    }
}

void MainWindow::on_CompPulsPNGsave_clicked()
{
    QString Path = QString::fromStdString(PulsedMeasurementsAndData_var->currentDataFromFile.PathLoaded);
    QString Name = QString::fromStdString(PulsedMeasurementsAndData_var->currentDataFromFile.fileNameLoaded);
    ui->CompPuls->savePng(Path+"\\"+Name+".png");
    std::cout << "PNG saved" << std::endl;
}

void MainWindow::on_CompPulsPDFsave_clicked() {
    QString Path = QString::fromStdString(PulsedMeasurementsAndData_var->currentDataFromFile.PathLoaded);
    QString Name = QString::fromStdString(PulsedMeasurementsAndData_var->currentDataFromFile.fileNameLoaded);
    ui->CompPuls->savePng(Path+"\\"+Name+".pdf");
    std::cout << "PDF saved" << std::endl;
}

void MainWindow::on_compPulsPrep_clicked() {
    if(checkClickedButton(5, "prep")){
        return;
    }

    PulsedMeasurementsAndData_var->currentCompPulsDetunParams.Freq=ui->compPulsFreq->value();
    PulsedMeasurementsAndData_var->currentCompPulsDetunParams.Steps=ui->compPulsSteps->value();
    PulsedMeasurementsAndData_var->currentCompPulsDetunParams.Stepsize=ui->compPulsStepSize->value();
    PulsedMeasurementsAndData_var->currentCompPulsDetunParams.TimeCounter=300;
    PulsedMeasurementsAndData_var->currentCompPulsDetunParams.TimeLaser=1500;
    //PulsedMeasurementsAndData_var->currentCompPulsDetunParams.PiTime=ui->compPulsPI->value();
    PulsedMeasurementsAndData_var->currentCompPulsDetunParams.PiTime=ui->compPulsPI->value()/3.14159265358979323846;
    PulsedMeasurementsAndData_var->currentCompPulsDetunParams.PhaseNb=PulsedMeasurementsAndData_var->currentCompusitePulses.phases.size();
    PulsedMeasurementsAndData_var->withRefocus = ui->compPulsRefocusCheckbox->isChecked();

    //testing if it has to be prepared a refocus measurement and if the loops are a divider of 100000
    if((PulsedMeasurementsAndData_var->withRefocus)&&!(ui->compPulsLoops->value()%100000)){
        PulsedMeasurementsAndData_var->currentCompPulsDetunParams.LoopNb=100000;
        PulsedMeasurementsAndData_var->NbOfRefocus = ui->compPulsLoops->value()/100000;
        std::cout << "with Refocus and right loops" << std::endl;
    }else{
        PulsedMeasurementsAndData_var->currentCompPulsDetunParams.LoopNb=ui->compPulsLoops->value();
        PulsedMeasurementsAndData_var->NbOfRefocus = 1;
        std::cout << "Without refocus and/or wrong loops" << std::endl;
    }
    std::cout << "Loops: " << PulsedMeasurementsAndData_var->currentCompPulsDetunParams.LoopNb << ", Numbers of refocus: " << PulsedMeasurementsAndData_var->NbOfRefocus << std::endl;


   //############# AWG needs to be prepared !!!!!!!!!!! #############

    int actChN = next_power_of_2(6);
    std::cout << next_power_of_2(6) << " activated channels" << std::endl;

    int64 mask;

    switch(actChN)
    {
        case 1: mask = (int64)1; break;
        case 2: mask = (int64)0x3; break;
        case 4: mask = (int64)0xF; break;
        case 8: mask = (int64)0xFF; break;
        case 16: mask = (int64)0xFFFF; break;
        default: std::cout << "Error: Channel mask is not allowed !" << std::endl; return;
    }

    //int length_in_samples = lround(10047) + 1;  // ~10.047 µs


    PulsedMeasurementsAndData_var->currentCompPulsDetunParams.segmentTime = 1000+(PulsedMeasurementsAndData_var->currentCompPulsDetunParams.PiTime*sechCutoff+1)*PulsedMeasurementsAndData_var->currentCompPulsDetunParams.PhaseNb+300+50;

    /*
    int numbOfSteps = (int)((delayLaserEnableRising_ns+(PulsedMeasurementsAndData_var->currentCompPulsDetunParams.segmentTime)*(ui->compPulsSteps->value()))/10)+50;
    int length_in_samples;
    if(numbOfSteps%64==0){
        length_in_samples=numbOfSteps;
    }else{
        length_in_samples=64*(numbOfSteps/64+1);
    }

    std::cout << "loop_np: " << PulsedMeasurementsAndData_var->currentCompPulsDetunParams.LoopNb << ", frequency_MHz: " << 100 << " length_in_samples: " << length_in_samples << std::endl;
    std::cout << "segmentTime: " << PulsedMeasurementsAndData_var->currentCompPulsDetunParams.segmentTime << ", numbOfSteps: " << numbOfSteps << std::endl;

    M2i7005_var->M2iSetup_StdReplay(mask, 100, false, false, PulsedMeasurementsAndData_var->currentCompPulsDetunParams.LoopNb, length_in_samples);
    */

    std::vector< std::vector<int>> risingEdges;
    std::vector< std::vector<int>> fallingEdges;
    int numbOfSteps = CreateCompusitePulseSequence(risingEdges, fallingEdges);
    int length_in_samples;

    if(numbOfSteps%64==0){
        length_in_samples=numbOfSteps;
    }else{
        length_in_samples=64*(numbOfSteps/64+1);
    }
    M2i7005_var->M2iSetup_StdReplay(mask, 100, false, false, PulsedMeasurementsAndData_var->currentCompPulsDetunParams.LoopNb, length_in_samples);
    M2i7005_var->M2iLoadIntoMemoryFromVec(6, risingEdges, fallingEdges);
    CreateCompPulsDetunFrequencies(PulsedMeasurementsAndData_var->currentCompPulsDetunParams.Freq, PulsedMeasurementsAndData_var->currentCompPulsDetunParams.Steps, PulsedMeasurementsAndData_var->currentCompPulsDetunParams.Stepsize, PulsedMeasurementsAndData_var->currentCompPulsDetunParams.LoopNb);


    ui->RabiApproxTime->display((double)(length_in_samples)*(double)(ui->compPulsLoops->value())*1e-8/60);

}


int MainWindow::CreateCompusitePulseSequence(std::vector< std::vector<int>>& risingEdges,  std::vector< std::vector<int>>& fallingEdges)
{
    std::vector<int> risingAWGTrigger;
    std::vector<int> fallingAWGTrigger;
    std::vector<int> risingAWGEvent;
    std::vector<int> fallingAWGEvent;
    std::vector<int> risingCounter;
    std::vector<int> fallingCounter;
    std::vector<int> risingLaserDigital;
    std::vector<int> fallingLaserDigital;
    std::vector<int> risingLaserEnable;
    std::vector<int> fallingLaserEnable;

    // ------------- TEST VALUES !!! -------------
    int TimeLaser = 1000;
    int NbOfPulses = PulsedMeasurementsAndData_var->currentCompPulsDetunParams.PhaseNb;
    int TimeCounter = 300;
    double pi_Pulse = PulsedMeasurementsAndData_var->currentCompPulsDetunParams.PiTime;
    double MWTimes = (pi_Pulse*sechCutoff+1)*NbOfPulses;
    double SegmentTime = PulsedMeasurementsAndData_var->currentCompPulsDetunParams.segmentTime;
    int Steps = PulsedMeasurementsAndData_var->currentCompPulsDetunParams.Steps;

    int TimePrevSeg=0;

    //Trigger for AWG; minus shift of 70 ns
    risingAWGTrigger.push_back((int)((TimeLaser-delayAWG_ns+delayLaserEnableRising_ns-15+TimePrevSeg)/10));
    fallingAWGTrigger.push_back((int)((TimeLaser+10-delayAWG_ns+delayLaserEnableRising_ns-15+TimePrevSeg)/10));

    for(int i = 0; i<(Steps); ++i){

        //Signal for Laser Enable (init)
        if(i==0){
            risingLaserEnable.push_back((int)(TimePrevSeg)/10);
        }
        fallingLaserEnable.push_back((int)((TimeLaser+TimePrevSeg)/10));

        //Signal for Laser Digital (init)
        if(i==0){
            risingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+TimePrevSeg)/10));
        }
        fallingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+TimeLaser+TimePrevSeg)/10));



        //Event for AWG;
        risingAWGEvent.push_back((int)((TimeLaser-delayAWG_ns+delayLaserEnableRising_ns-15+TimePrevSeg)/10));
        fallingAWGEvent.push_back((int)((TimeLaser+10-delayAWG_ns+delayLaserEnableRising_ns-15+TimePrevSeg)/10));

        //##########  Added 500 ns for all following steps, due to get 1µs between MW and readout !!! ###################

        //Signal for TT; additional shift of 30 ns
        risingCounter.push_back((int)((TimeLaser+MWTimes+delayLaserEnableRising_ns-delayLaserDigital_ns+30+TimePrevSeg+50)/10)+1);
        fallingCounter.push_back((int)((TimeLaser+MWTimes+delayLaserEnableRising_ns-delayLaserDigital_ns+TimeCounter+30+TimePrevSeg+50)/10)+1);

        //Signal for Laser Enable (readout) additional shift of 20 ns
        risingLaserEnable.push_back((int)((TimeLaser+MWTimes+TimePrevSeg+20+50)/10)+1);
        if(i==(Steps-1)){
            fallingLaserEnable.push_back((int)((TimeLaser+MWTimes+delayLaserEnableRising_ns-delayLaserEnableFalling_ns+TimeCounter+TimePrevSeg+50)/10)+1);
        }

        //Signal for Laser Digital (readout)
        risingLaserDigital.push_back((int)((TimeLaser+MWTimes+delayLaserEnableRising_ns-delayLaserDigital_ns+TimePrevSeg+50)/10)+1);
        if(i==(Steps-1)){
            fallingLaserDigital.push_back((int)((TimeLaser+MWTimes+delayLaserEnableRising_ns-delayLaserDigital_ns+TimeCounter+TimePrevSeg+50)/10)+1);
        }

        TimePrevSeg=*std::max_element(fallingCounter.begin(),fallingCounter.end())*10;
    }

    risingEdges.push_back({1});
    fallingEdges.push_back({2});
    risingEdges.push_back(risingAWGEvent);
    fallingEdges.push_back(fallingAWGEvent);
    risingEdges.push_back(risingCounter);
    fallingEdges.push_back(fallingCounter);
    risingEdges.push_back(risingLaserEnable);
    fallingEdges.push_back(fallingLaserEnable);
    risingEdges.push_back(risingLaserDigital);
    fallingEdges.push_back(fallingLaserDigital);
    risingEdges.push_back(risingAWGTrigger);
    fallingEdges.push_back(fallingAWGTrigger);

    return TimePrevSeg/10;
}

void MainWindow::CreateCompPulsDetunFrequencies(double Freq, int stepNb, double Stepsize, int loops){

    ViConstString chan = "1";

    // Create frequency-array
    auto frequencies = linearFreqArray(Freq*MHz_M8195A, Freq*MHz_M8195A, stepNb-1);
    std::cout << "StepNb: " << stepNb << std::endl;
    std::cout << "frequencies.size(): " << frequencies.size() << std::endl;

    // Create times-array
    std::vector<double> times (frequencies.size(), PulsedMeasurementsAndData_var->currentCompPulsDetunParams.PiTime/1000.0*microsec_M8195A);

    std::vector<double> startTimes (frequencies.size(), 0);

    std::vector<double> amplitudes (frequencies.size(), 1);

    //std::cout << "f_0: " << Freq << ", Stepsize: " << Stepsize << ", stepNb: " << stepNb << " length(times): " << times.size() << std::endl;

    for(int i=0; i<frequencies.size(); ++i){
        //amplitudes.push_back(1-(double)(i)*Stepsize);
        frequencies[i]=(Freq+Stepsize*(i-stepNb/2))*MHz_M8195A;
        //std::cout << "Frequency[" << i << "]: " << frequencies[i] << std::endl;
    }

    ViStatus status = M8195A_var->loadCompPulsesWaveformAndSeqTable(chan, frequencies, maxSampleRate, times, loops, PulsedMeasurementsAndData_var->currentCompusitePulses.phases, startTimes, amplitudes, true);
    //ViStatus status = M8195A_var->loadSineArrayWaveformAndSeqTable(chan, frequencies, maxSampleRate, times, loops, PulsedMeasurementsAndData_var->currentCompusitePulses.phases, amplitudes);

}

void MainWindow::on_compPulsStart_clicked()
{
    if(checkClickedButton(5, "start")){
        return;
    }

    if(PulsedMeasurementsAndData_var->withRefocus){
        pulsedMeasurementWithRefocus("CompPulsDetun-Scan");
    }else{
        on_RUNbutton_clicked();
        PulsedMeasurementsAndData_var->prepareCounterForCompPulsDetun();
        on_outputM2i_clicked();
        on_STOPbutton_clicked();
    }
}

void MainWindow::on_compPulsRead_clicked()
{

    if(checkClickedButton(5, "read")){
        return;
    }

    curDataWidget = ui->CompPuls;
    PulsedMeasurementsAndData_var->fetchAndProcessDataForCompPulsDetun();

}


void MainWindow::on_pi2SequencePrepare_clicked()
{
    if(checkClickedButton(6, "prep")){
        return;
    }

    PulsedMeasurementsAndData_var->currentCompPulsDetunParams.Freq=ui->compPulsFreq->value();
    PulsedMeasurementsAndData_var->currentCompPulsDetunParams.Steps=ui->compPulsSteps->value()+1;
    PulsedMeasurementsAndData_var->currentCompPulsDetunParams.Stepsize=ui->compPulsStepSize->value();
    PulsedMeasurementsAndData_var->currentCompPulsDetunParams.TimeCounter=300;
    PulsedMeasurementsAndData_var->currentCompPulsDetunParams.TimeLaser=1500;
    PulsedMeasurementsAndData_var->currentCompPulsDetunParams.LoopNb=ui->compPulsLoops->value();
    PulsedMeasurementsAndData_var->currentCompPulsDetunParams.PhaseNb=PulsedMeasurementsAndData_var->currentCompusitePulses.phases.size();
    PulsedMeasurementsAndData_var->currentCompPulsDetunParams.PiTime=ui->compPulsPI->value();
   //############# AWG needs to be prepared !!!!!!!!!!! #############

    int actChN = next_power_of_2(6);
    std::cout << next_power_of_2(6) << " activated channels" << std::endl;

    int64 mask;

    switch(actChN)
    {
        case 1: mask = (int64)1; break;
        case 2: mask = (int64)0x3; break;
        case 4: mask = (int64)0xF; break;
        case 8: mask = (int64)0xFF; break;
        case 16: mask = (int64)0xFFFF; break;
        default: std::cout << "Error: Channel mask is not allowed !" << std::endl; return;
    }

    //int length_in_samples = lround(10047) + 1;  // ~10.047 µs

    PulsedMeasurementsAndData_var->currentCompPulsDetunParams.segmentTime = PulsedMeasurementsAndData_var->currentCompPulsDetunParams.TimeLaser+(PulsedMeasurementsAndData_var->currentCompPulsDetunParams.PiTime+1)*PulsedMeasurementsAndData_var->currentCompPulsDetunParams.PhaseNb+PulsedMeasurementsAndData_var->currentCompPulsDetunParams.TimeCounter+500;

    int numbOfSteps = (int)((delayLaserEnableRising_ns+(PulsedMeasurementsAndData_var->currentCompPulsDetunParams.segmentTime)*(ui->compPulsSteps->value()+1)+((((ui->compPulsSteps->value()+2)/2)*ui->compPulsPI->value())))/10)+50;
    int length_in_samples;
    if(numbOfSteps%64==0){
        length_in_samples=numbOfSteps;
    }else{
        length_in_samples=64*(numbOfSteps/64+1);
    }

    std::cout << "loop_np: " << ui->compPulsLoops->value() << ", frequency_MHz: " << 100 << " length_in_samples: " << length_in_samples << std::endl;
    std::cout << "segmentTime: " << PulsedMeasurementsAndData_var->currentCompPulsDetunParams.segmentTime << ", numbOfSteps: " << numbOfSteps << std::endl;

    M2i7005_var->M2iSetup_StdReplay(mask, 100, false, false, ui->compPulsLoops->value(), length_in_samples);

    std::vector< std::vector<int>> risingEdges;
    std::vector< std::vector<int>> fallingEdges;
    CreateCompusitePulsePi2Sequence(risingEdges, fallingEdges);

    M2i7005_var->M2iLoadIntoMemoryFromVec(6, risingEdges, fallingEdges);
    CreateCompPulsPi2SequenceFrequencies(PulsedMeasurementsAndData_var->currentCompPulsDetunParams.Freq, PulsedMeasurementsAndData_var->currentCompPulsDetunParams.Steps-1, PulsedMeasurementsAndData_var->currentCompPulsDetunParams.Stepsize, PulsedMeasurementsAndData_var->currentCompPulsDetunParams.LoopNb);


    ui->RabiApproxTime->display((double)(length_in_samples)*(double)(ui->compPulsLoops->value())*1e-8/60);
}

void MainWindow::CreateCompPulsPi2SequenceFrequencies(double Freq, int stepNb, double Stepsize, int loops){

    ViConstString chan = "1";

    // Create frequency-array
    auto frequencies = linearFreqArray(Freq*MHz_M8195A, Freq*MHz_M8195A, stepNb);

    // Create times-array
    std::vector<double> times ;

    times.push_back(PulsedMeasurementsAndData_var->currentCompPulsDetunParams.PiTime/1000.0*microsec_M8195A*(double)(1)/(stepNb));
    for(int i=1; i<stepNb+1; ++i){
        times.push_back(PulsedMeasurementsAndData_var->currentCompPulsDetunParams.PiTime/1000.0*microsec_M8195A*(double)(i)/(stepNb));
    }

    std::vector<double> startTimes (frequencies.size(), 0);

    std::vector<double> amplitudes (frequencies.size(), 1);
    //set first amplitude to 0 due to a pulse area of 0
    amplitudes[0]=0;

    for(int i=0; i<stepNb+1; ++i){
        std::cout << "Time: " << times[i] << ", amplitudes: " << amplitudes[i] << std::endl;
    }

    ViStatus status = M8195A_var->loadCompPulsesWaveformAndSeqTable(chan, frequencies, maxSampleRate, times, loops, PulsedMeasurementsAndData_var->currentCompusitePulses.phases, startTimes, amplitudes, false);

}

bool MainWindow::CreateCompusitePulsePi2Sequence(std::vector< std::vector<int>>& risingEdges,  std::vector< std::vector<int>>& fallingEdges)
{
    std::vector<int> risingAWGTrigger;
    std::vector<int> fallingAWGTrigger;
    std::vector<int> risingAWGEvent;
    std::vector<int> fallingAWGEvent;
    std::vector<int> risingCounter;
    std::vector<int> fallingCounter;
    std::vector<int> risingLaserDigital;
    std::vector<int> fallingLaserDigital;
    std::vector<int> risingLaserEnable;
    std::vector<int> fallingLaserEnable;

    // ------------- TEST VALUES !!! -------------
    int TimeLaser = PulsedMeasurementsAndData_var->currentCompPulsDetunParams.TimeLaser;
    int NbOfPulses = PulsedMeasurementsAndData_var->currentCompPulsDetunParams.PhaseNb;
    int TimeCounter = 300;
    double pi_Pulse = PulsedMeasurementsAndData_var->currentCompPulsDetunParams.PiTime;
    double MWTimes = (pi_Pulse+1)*NbOfPulses;
    double SegmentTime = PulsedMeasurementsAndData_var->currentCompPulsDetunParams.segmentTime;
    int Steps = PulsedMeasurementsAndData_var->currentCompPulsDetunParams.Steps;

    int TimePrevSeg=0;

    //Trigger for AWG; minus shift of 70 ns
    risingAWGTrigger.push_back((int)((TimeLaser-delayAWG_ns+delayLaserEnableRising_ns+5+TimePrevSeg)/10));
    fallingAWGTrigger.push_back((int)((TimeLaser+10-delayAWG_ns+delayLaserEnableRising_ns+5+TimePrevSeg)/10));

    for(int i = 0; i<(Steps); ++i){

        //Signal for Laser Enable (init)
        if(i==0){
            risingLaserEnable.push_back((int)(TimePrevSeg)/10);
        }
        fallingLaserEnable.push_back((int)((TimeLaser+TimePrevSeg)/10));

        //Signal for Laser Digital (init)
        if(i==0){
            risingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+TimePrevSeg)/10));
        }
        fallingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+TimeLaser+TimePrevSeg)/10));


        //Event for AWG;
        risingAWGEvent.push_back((int)((TimeLaser-delayAWG_ns+delayLaserEnableRising_ns+5+TimePrevSeg)/10));
        fallingAWGEvent.push_back((int)((TimeLaser+10-delayAWG_ns+delayLaserEnableRising_ns+5+TimePrevSeg)/10));

        //##########  Added 70 ns for all following steps, due to get time between MW and readout !!! ###################

        //Signal for TT; additional shift of 30 ns
        risingCounter.push_back((int)((TimeLaser+MWTimes+delayLaserEnableRising_ns-delayLaserDigital_ns+50+TimePrevSeg+50)/10)+1);
        fallingCounter.push_back((int)((TimeLaser+MWTimes+delayLaserEnableRising_ns-delayLaserDigital_ns+TimeCounter+50+TimePrevSeg+50)/10)+1);

        //Signal for Laser Enable (readout) additional shift of 20 ns
        risingLaserEnable.push_back((int)((TimeLaser+MWTimes+TimePrevSeg+20+50)/10)+1);
        if(i==(Steps-1)){
            fallingLaserEnable.push_back((int)((TimeLaser+MWTimes+delayLaserEnableRising_ns-delayLaserEnableFalling_ns+20+TimeCounter+TimePrevSeg+50)/10)+1);
        }

        //Signal for Laser Digital (readout)
        risingLaserDigital.push_back((int)((TimeLaser+MWTimes+delayLaserEnableRising_ns-delayLaserDigital_ns+20+TimePrevSeg+50)/10)+1);
        if(i==(Steps-1)){
            fallingLaserDigital.push_back((int)((TimeLaser+MWTimes+delayLaserEnableRising_ns-delayLaserDigital_ns+20+TimeCounter+TimePrevSeg+50)/10)+1);
        }

        TimePrevSeg=(SegmentTime*(i+1)+delayLaserEnableRising_ns+40);
    }

    risingEdges.push_back({1});
    fallingEdges.push_back({2});
    risingEdges.push_back(risingAWGEvent);
    fallingEdges.push_back(fallingAWGEvent);
    risingEdges.push_back(risingCounter);
    fallingEdges.push_back(fallingCounter);
    risingEdges.push_back(risingLaserEnable);
    fallingEdges.push_back(fallingLaserEnable);
    risingEdges.push_back(risingLaserDigital);
    fallingEdges.push_back(fallingLaserDigital);
    risingEdges.push_back(risingAWGTrigger);
    fallingEdges.push_back(fallingAWGTrigger);

    return true;
}

void MainWindow::on_pi2SequenceStart_clicked()
{
    if(checkClickedButton(6, "start")){
        return;
    }

    on_RUNbutton_clicked();
    PulsedMeasurementsAndData_var->prepareCounterForCompPulsPI2();
    on_outputM2i_clicked();
    on_STOPbutton_clicked();
}

void MainWindow::on_pi2SequenceRead_clicked()
{
    if(checkClickedButton(6, "read")){
        return;
    }

    curDataWidget = ui->CompPuls;
    PulsedMeasurementsAndData_var->fetchAndProcessDataForCompPulsPI2();
    std::cout << "End of PI/2 sequence read" << std::endl;
}


void MainWindow::on_sechPrep_clicked()
{
    if(checkClickedButton(7, "prep")){
        return;
    }

    PulsedMeasurementsAndData_var->currentSechParams.Freq=ui->compPulsFreq->value();
    PulsedMeasurementsAndData_var->currentSechParams.Steps=ui->compPulsSteps->value()+1;
    PulsedMeasurementsAndData_var->currentSechParams.Stepsize=ui->compPulsStepSize->value();
    PulsedMeasurementsAndData_var->currentSechParams.TimeCounter=300;
    PulsedMeasurementsAndData_var->currentSechParams.TimeLaser=1500;
    PulsedMeasurementsAndData_var->currentSechParams.LoopNb=ui->compPulsLoops->value();
    PulsedMeasurementsAndData_var->currentCompusitePulses.phases=std::vector<double> {0};

   //############# AWG needs to be prepared !!!!!!!!!!! #############

    int actChN = next_power_of_2(6);
    std::cout << next_power_of_2(6) << " activated channels" << std::endl;

    int64 mask;

    switch(actChN)
    {
        case 1: mask = (int64)1; break;
        case 2: mask = (int64)0x3; break;
        case 4: mask = (int64)0xF; break;
        case 8: mask = (int64)0xFF; break;
        case 16: mask = (int64)0xFFFF; break;
        default: std::cout << "Error: Channel mask is not allowed !" << std::endl; return;
    }

    //int length_in_samples = lround(10047) + 1;  // ~10.047 µs
    int SechSteps=ui->compPulsSteps->value();

    PulsedMeasurementsAndData_var->currentSechParams.segment0Time=(PulsedMeasurementsAndData_var->currentSechParams.TimeLaser+SechSteps+PulsedMeasurementsAndData_var->currentSechParams.TimeCounter+500);

    int numbOfSteps = (int)((delayLaserEnableRising_ns+(300+PulsedMeasurementsAndData_var->currentSechParams.segment0Time)*(SechSteps+1)+(((SechSteps+2)*((SechSteps+1)+2)/2)*sechCutoff*PulsedMeasurementsAndData_var->currentSechParams.Stepsize))/10)+50;
    int length_in_samples;
    if(numbOfSteps%64==0){
        length_in_samples=numbOfSteps;
    }else{
        length_in_samples=64*(numbOfSteps/64+1);
    }

    std::cout << "loop_np: " << PulsedMeasurementsAndData_var->currentSechParams.LoopNb << ", frequency_MHz: " << 100 << " length_in_samples: " << length_in_samples << std::endl;
    std::cout << "segment0Time: " << PulsedMeasurementsAndData_var->currentSechParams.segment0Time << ", numbOfSteps: " << numbOfSteps << std::endl;

    M2i7005_var->M2iSetup_StdReplay(mask, 100, false, false, PulsedMeasurementsAndData_var->currentSechParams.LoopNb, length_in_samples);

    std::vector< std::vector<int>> risingEdges;
    std::vector< std::vector<int>> fallingEdges;
    CreateSechSequence(risingEdges, fallingEdges);

    M2i7005_var->M2iLoadIntoMemoryFromVec(6, risingEdges, fallingEdges);
    CreateSechFrequencies(PulsedMeasurementsAndData_var->currentSechParams.Freq, PulsedMeasurementsAndData_var->currentSechParams.Steps-1, PulsedMeasurementsAndData_var->currentSechParams.Stepsize, PulsedMeasurementsAndData_var->currentSechParams.LoopNb);

    ui->RabiApproxTime->display((double)(numbOfSteps)*(double)(ui->compPulsLoops->value())*1e-8/60);
}

void MainWindow::CreateSechFrequencies(double Freq, int stepNb, double Stepsize, int loops){

    ViConstString chan = "1";

    // Create frequency-array
    auto frequencies = linearFreqArray(Freq*MHz_M8195A, Freq*MHz_M8195A, stepNb);

    // Create times-array
    std::vector<double> times (frequencies.size(), 1*microsec_M8195A);

    double FreqToZerro = 0;
    for(int i=0; i<times.size(); ++i){
        times[i]=(10/1000.0+Stepsize/1000.0*i)*microsec_M8195A;
        if(i<10){std::cout << "Time: " << times[i] << std::endl;}
    }
    std::vector<double> phases (frequencies.size(), 0);
    std::vector<double> startTimes (frequencies.size(), 0);
    std::vector<double> amplitudes (frequencies.size(), 1);

    std::cout << "Amplitude: " << amplitudes[0] << std::endl;

    //ViStatus status = M8195A_var->loadSineArrayWaveformAndSeqTable(chan, frequencies, maxSampleRate, times, loops, phases, amplitudes);
    ViStatus status = M8195A_var->loadCompPulsesWaveformAndSeqTable(chan, frequencies, maxSampleRate, times, loops, PulsedMeasurementsAndData_var->currentCompusitePulses.phases, startTimes, amplitudes, true);
//    ViStatus status = M8195A_var->loadSineArrayWaveformAndSeqTable(chan, frequencies, maxSampleRate, times, loops);

}

bool MainWindow::CreateSechSequence(std::vector< std::vector<int>>& risingEdges,  std::vector< std::vector<int>>& fallingEdges)
{
    std::vector<int> risingAWGTrigger;
    std::vector<int> fallingAWGTrigger;
    std::vector<int> risingAWGEvent;
    std::vector<int> fallingAWGEvent;
    std::vector<int> risingCounter;
    std::vector<int> fallingCounter;
    std::vector<int> risingLaserDigital;
    std::vector<int> fallingLaserDigital;
    std::vector<int> risingLaserEnable;
    std::vector<int> fallingLaserEnable;

    int TimeLaser=PulsedMeasurementsAndData_var->currentSechParams.TimeLaser;
    int Steps = PulsedMeasurementsAndData_var->currentSechParams.Steps;
    int Stepsize = PulsedMeasurementsAndData_var->currentSechParams.Stepsize*sechCutoff;
    int TimeCounter = PulsedMeasurementsAndData_var->currentSechParams.TimeCounter;
    int MinRabiTime = PulsedMeasurementsAndData_var->currentSechParams.segment0Time;

    int TimePrevSeg=0;

    //Trigger for AWG; minus shift of 70 ns
    risingAWGTrigger.push_back((int)((TimeLaser-delayAWG_ns+delayLaserEnableRising_ns-70)/10));
    fallingAWGTrigger.push_back((int)((TimeLaser+100-delayAWG_ns+delayLaserEnableRising_ns-70)/10));

    for(int i = 0; i<(Steps); ++i){

        //Signal for Laser Enable (init)
        if(i==0){
            risingLaserEnable.push_back((int)(TimePrevSeg)/10);
        }
        fallingLaserEnable.push_back((int)((TimeLaser+TimePrevSeg)/10));

        //Signal for Laser Digital (init)
        if(i==0){
            risingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+TimePrevSeg)/10));
        }
        fallingLaserDigital.push_back((int)((delayLaserEnableRising_ns-delayLaserDigital_ns+TimeLaser+TimePrevSeg)/10));


        //Event for AWG; minus shift of 70 ns
        risingAWGEvent.push_back((int)((TimeLaser-delayAWG_ns+delayLaserEnableRising_ns-70+TimePrevSeg)/10));
        fallingAWGEvent.push_back((int)((TimeLaser+100-delayAWG_ns+delayLaserEnableRising_ns-70+TimePrevSeg)/10));

        //##########  Added 500 ns for all following steps, due to get 1µs between MW and readout !!! ###################

        // due to 100 MHz clock, the smallest steps are 10 µs. If the stepsize is smaller then 10 µs the following times needs to be 10 µs (1 step) larger du to get no problems wiht conversion to int
        if(Stepsize<10){
            //Signal for TT; additional shift of 30 ns
            risingCounter.push_back((int)((TimeLaser+10+Stepsize*(i+1)+delayLaserEnableRising_ns-delayLaserDigital_ns+30+TimePrevSeg)/10)+1);
            fallingCounter.push_back((int)((TimeLaser+10+Stepsize*(i+1)+delayLaserEnableRising_ns-delayLaserDigital_ns+TimeCounter+30+TimePrevSeg)/10)+1);

            //Signal for Laser Enable (readout) additional shift of 20 ns
            risingLaserEnable.push_back((int)((TimeLaser+10+Stepsize*(i+1)+TimePrevSeg+20)/10)+1);
            if(i==(Steps-1)){
                fallingLaserEnable.push_back((int)((TimeLaser+10+Stepsize*(i+1)+delayLaserEnableRising_ns-delayLaserEnableFalling_ns+TimeCounter+TimePrevSeg)/10)+1);
            }

            //Signal for Laser Digital (readout)
            risingLaserDigital.push_back((int)((TimeLaser+10+Stepsize*i+delayLaserEnableRising_ns-delayLaserDigital_ns+TimePrevSeg)/10)+1);
            if(i==(Steps-1)){
                fallingLaserDigital.push_back((int)((TimeLaser+10+Stepsize*(i+1)+delayLaserEnableRising_ns-delayLaserDigital_ns+TimeCounter+TimePrevSeg)/10)+1);
            }
        }else{
            //Signal for TT; additional shift of 60 ns
            risingCounter.push_back((int)((TimeLaser+10+Stepsize*(i+1)+delayLaserEnableRising_ns-delayLaserDigital_ns+60+TimePrevSeg)/10));
            fallingCounter.push_back((int)((TimeLaser+10+Stepsize*(i+1)+delayLaserEnableRising_ns-delayLaserDigital_ns+TimeCounter+60+TimePrevSeg)/10));

            //Signal for Laser Enable (readout) additional shift of 20 ns
            risingLaserEnable.push_back((int)((TimeLaser+10+Stepsize*(i+1)+TimePrevSeg+20)/10));
            if(i==(Steps-1)){
                fallingLaserEnable.push_back((int)((TimeLaser+10+Stepsize*(i+1)+delayLaserEnableRising_ns-delayLaserEnableFalling_ns+TimeCounter+TimePrevSeg+100)/10));
            }

            //Signal for Laser Digital (readout)
            risingLaserDigital.push_back((int)((TimeLaser+10+Stepsize*(i+1)+delayLaserEnableRising_ns-delayLaserDigital_ns+TimePrevSeg)/10));
            if(i==(Steps-1)){
                fallingLaserDigital.push_back((int)((TimeLaser+10+Stepsize*(i+1)+delayLaserEnableRising_ns-delayLaserDigital_ns+TimeCounter+TimePrevSeg+100)/10));
            }
        }

        TimePrevSeg=(MinRabiTime*(i+1)+Stepsize*(i+2)*((i+1)+1)/2)+delayLaserEnableRising_ns-500*i;
    }

    risingEdges.push_back({0});
    fallingEdges.push_back({100});
    risingEdges.push_back(risingAWGEvent);
    fallingEdges.push_back(fallingAWGEvent);
    risingEdges.push_back(risingCounter);
    fallingEdges.push_back(fallingCounter);
    risingEdges.push_back(risingLaserEnable);
    fallingEdges.push_back(fallingLaserEnable);
    risingEdges.push_back(risingLaserDigital);
    fallingEdges.push_back(fallingLaserDigital);
    risingEdges.push_back(risingAWGTrigger);
    fallingEdges.push_back(fallingAWGTrigger);

    return true;
}

void MainWindow::on_sechStart_clicked()
{

    if(checkClickedButton(7, "start")){
        return;
    }

    on_RUNbutton_clicked();
    PulsedMeasurementsAndData_var->prepareCounterForSech();
    on_outputM2i_clicked();
    on_STOPbutton_clicked();
}

void MainWindow::on_sechRead_clicked()
{

    if(checkClickedButton(7, "read")){
        return;
    }

    curDataWidget = ui->CompPuls;
    PulsedMeasurementsAndData_var->fetchAndProcessDataForSech();
}

void MainWindow::pulsedMeasurementWithRefocus(std::string pathName){
    globalPathSave = PulsedMeasurementsAndData_var->globalPath;

    PulsedMeasurementsAndData_var->globalPath /= pathName;
    if (!std::filesystem::exists(PulsedMeasurementsAndData_var->globalPath))
    {
        std::cout << "Path " << PulsedMeasurementsAndData_var->globalPath << " does not exist!" << std::endl;
        if(std::filesystem::create_directory(PulsedMeasurementsAndData_var->globalPath)) {
                std::cout << "Path created" << std::endl;
        }else{
            std::cout << "unable to create Path" << std::endl;
        }
    }else{
        std::cout << "Path exists" << std::endl;
    }

    // begin: create new folder for data with refocus
    bool folderExists = false;
    int numberFolder = 1;
    PulsedMeasurementsAndData_var->globalPath /= pathName+std::to_string(numberFolder);
    do{
        if (!std::filesystem::exists(PulsedMeasurementsAndData_var->globalPath))
        {
            std::cout << "Path " << PulsedMeasurementsAndData_var->globalPath << " does not exist!" << std::endl;
            if(std::filesystem::create_directory(PulsedMeasurementsAndData_var->globalPath)) {
                    std::cout << "Path created" << std::endl;
                    folderExists = true;
            }else{
                std::cout << "unable to create Path" << std::endl;
            }
        }else{
            ++numberFolder;
            PulsedMeasurementsAndData_var->globalPath = std::filesystem::path(PulsedMeasurementsAndData_var->globalPath).replace_filename(pathName+std::to_string(numberFolder));
            std::cout << PulsedMeasurementsAndData_var->globalPath << ", " << std::filesystem::path(PulsedMeasurementsAndData_var->globalPath).remove_filename() << ", " << numberFolder << std::endl;
            if(numberFolder>100){
                std::cout << "BREAK!" << std::endl;
                break;
            }
        }
    }while(!folderExists);
    // end: create new folder for data with refocus

    PulsedMeasurementsAndData_var->withRefocusXValues.clear();
    PulsedMeasurementsAndData_var->withRefocusYValues.clear();
    PulsedMeasurementsAndData_var->withRefocusY2Values.clear();

    for(int i=0; i<PulsedMeasurementsAndData_var->NbOfRefocus; i++){
        if(pathName=="CompPulsDetun-Scan"){

            if(i){
                int actChN = next_power_of_2(6);
                std::cout << next_power_of_2(6) << " activated channels" << std::endl;

                int64 mask;

                switch(actChN)
                {
                    case 1: mask = (int64)1; break;
                    case 2: mask = (int64)0x3; break;
                    case 4: mask = (int64)0xF; break;
                    case 8: mask = (int64)0xFF; break;
                    case 16: mask = (int64)0xFFFF; break;
                    default: std::cout << "Error: Channel mask is not allowed !" << std::endl; return;
                }
                std::vector< std::vector<int>> risingEdges;
                std::vector< std::vector<int>> fallingEdges;
                int numbOfSteps = CreateCompusitePulseSequence(risingEdges, fallingEdges);
                int length_in_samples;

                if(numbOfSteps%64==0){
                    length_in_samples=numbOfSteps;
                }else{
                    length_in_samples=64*(numbOfSteps/64+1);
                }
                M2i7005_var->M2iSetup_StdReplay(mask, 100, false, false, PulsedMeasurementsAndData_var->currentCompPulsDetunParams.LoopNb, length_in_samples);
                M2i7005_var->M2iLoadIntoMemoryFromVec(6, risingEdges, fallingEdges);
            }
            on_RUNbutton_clicked();
            PulsedMeasurementsAndData_var->prepareCounterForCompPulsDetun();
            on_outputM2i_clicked();
            on_STOPbutton_clicked();
            curDataWidget = ui->CompPuls;
            PulsedMeasurementsAndData_var->fetchAndProcessDataForCompPulsDetun();

            refocus();
        }else if(pathName=="T1-Scan"){
            std::cout << "Scan nb: " << i << std::endl;
//            int length_in_samples = ArrayPrepForPulsedMeasurements_var->prepareT1(PulsedMeasurementsAndData_var->currentT1Params.Freq, PulsedMeasurementsAndData_var->currentT1Params.Steps, PulsedMeasurementsAndData_var->currentT1Params.Stepsize, PulsedMeasurementsAndData_var->currentT1Params.PiTime, PulsedMeasurementsAndData_var->currentT1Params.TimeCounter, PulsedMeasurementsAndData_var->currentT1Params.TimeLaser, PulsedMeasurementsAndData_var->currentT1Params.LoopNb, M2i7005_var, M8195A_var);
            if(i){
//                int length_in_samples = ArrayPrepForPulsedMeasurements_var->prepareT1(Freq, nbSteps, stepSize, piPulse, timeCounter, timeLaser, loops, M2i7005_var, M8195A_var);
                int length_in_samples = ArrayPrepForPulsedMeasurements_var->prepareT1(PulsedMeasurementsAndData_var->currentT1Params.Freq, PulsedMeasurementsAndData_var->currentT1Params.Steps, PulsedMeasurementsAndData_var->currentT1Params.Stepsize, PulsedMeasurementsAndData_var->currentT1Params.PiTime, PulsedMeasurementsAndData_var->currentT1Params.TimeCounter, PulsedMeasurementsAndData_var->currentT1Params.TimeLaser, PulsedMeasurementsAndData_var->currentT1Params.LoopNb, M2i7005_var);
//                ArrayPrepForPulsedMeasurements_var->createT1Frequencies(Freq, nbSteps, piPulse, loops, M8195A_var);
                //ArrayPrepForPulsedMeasurements_var->createT1Frequencies(PulsedMeasurementsAndData_var->currentT1Params.Freq, PulsedMeasurementsAndData_var->currentT1Params.Steps, PulsedMeasurementsAndData_var->currentT1Params.PiTime, PulsedMeasurementsAndData_var->currentT1Params.LoopNb, M8195A_var);
            }
            on_RUNbutton_clicked();
            PulsedMeasurementsAndData_var->prepareCounterForT1();
            on_outputM2i_clicked();
            on_STOPbutton_clicked();
            curDataWidget = ui->CompPuls;
            PulsedMeasurementsAndData_var->fetchAndProcessDataForCompPulsDetun();
        }else{
            std::cout << "No measurement starte !!!" << std::endl;
        }
    }
    PulsedMeasurementsAndData_var->writeDataToFile(pathName+"_refocus-combined_");

    PulsedMeasurementsAndData_var->globalPath = globalPathSave;
    PulsedMeasurementsAndData_var->withRefocus = 0;
}






// ----------------------------------------------------
// ----------   Functions for sequence editor   -------
// ----------------------------------------------------


bool MainWindow::writeSeqToFile(int nbActChan, int length, double tStep_in_ns, const std::vector< std::vector<edgeProp> >& risingE_Vectors, const std::vector< std::vector<edgeProp> >& fallingE_Vectors, const std::string& path, const std::string& name)
{
    if (risingE_Vectors.size() != nbActChan || fallingE_Vectors.size() != nbActChan)
    {
        std::cout << "Error: Wrong number of vectors of rising or falling edges; this should match the number of actiated channels!" << std::endl;
        return false;
    }

    //set current working directory
    std::filesystem::path p(path);
    std::filesystem::current_path(p);

    //create output stream isntance to file
    std::ofstream fileWritten;

    std::string nowStr = getNow(); //current data and time

    //fileWritten.open(nowStr + "_Sequence_" + name + ".dat", std::ios::out | std::ios::trunc);
    std::string filename = nowStr + " Sequence " + name + ".dat";
    std::replace(filename.begin(),filename.end(), ':', ';'); //IMPORTANT: Windows file system does not accept colon ':', which we replace by ';'
    fileWritten.open(filename, std::ios::out | std::ios::trunc);

    if(fileWritten.is_open())
    {
        fileWritten << "Number of activated channels:\n" << nbActChan << '\n';
        fileWritten << "Time step (ns):\n" << tStep_in_ns << '\n' << "Clock frequency (MHz): " << 1000./tStep_in_ns << '\n';

        fileWritten << "Sequence length:\n" << length << '\n' << '\n' << '\n';

        fileWritten << "RISING EDGES:\n";
        //write how many rising edges each channel has in this sequence
        fileWritten << "Number of rising edges per channel:\n";
        for (int c=0; c<nbActChan; ++c)
        {
            fileWritten << risingE_Vectors[c].size() << '\t';
        }
        fileWritten << '\n' << '\n';
        //write the time points where the edges happen
        fileWritten << "Timestamps:\n";
        for(int c = 0; c<nbActChan; ++c)
        {
            fileWritten << "Channel #" << c << '\n';
            for(int e = 0; e<risingE_Vectors[c].size(); ++e)
            {
                fileWritten << risingE_Vectors[c][e].timestamp << '\t';
            }
            fileWritten << '\n' << '\n';
        }

        fileWritten << '\n';

        fileWritten << "FALLING EDGES:\n";
        //write how many rising edges each channel has in this sequence
        fileWritten << "Number of falling edges per channel:\n";
        for (int c=0; c<nbActChan; ++c)
        {
            fileWritten << fallingE_Vectors[c].size() << '\t';
        }
        fileWritten << '\n' << '\n';
        //write the time points where the edges happen
        fileWritten << "Timestamps:\n";
        for(int c = 0; c<nbActChan; ++c)
        {
            fileWritten << "Channel #" << c << '\n';
            for(int e = 0; e<fallingE_Vectors[c].size(); ++e)
            {
                fileWritten << fallingE_Vectors[c][e].timestamp << '\t';
            }
            fileWritten << '\n' << '\n';
        }

    }
    else
    {
        std::cout << "File failed to open!" << std::endl;
        return false;
    }
    fileWritten.close();

    return true;
}

bool MainWindow::loadSeqFromFile(std::filesystem::path filePath, std::string filename, std::vector< std::vector<edgeProp> >& risingE_outVec, std::vector< std::vector<edgeProp> >& fallingE_outVec)
{
    std::filesystem::current_path(filePath);

    std::ifstream inputF;
    inputF.open(filename);

    std::string line;

    std::getline(inputF, line);
    std::getline(inputF, line); //--> this should be the number of activated channels
    int P = atoi(line.c_str());
    risingE_outVec.resize(P);
    fallingE_outVec.resize(P);

    std::getline(inputF, line);
    std::getline(inputF, line); //--> this should be the time step in ns
    double dt = atof(line.c_str());

    std::getline(inputF, line);

    std::getline(inputF, line);
    std::getline(inputF, line); //--> this should be the length in samples i.e. number of time points
    int L = atoi(line.c_str());

    std::string st1 = "Number of rising edges per channel";
    for (; line.substr(0, st1.size()) != st1 && !inputF.eof(); std::getline(inputF, line))
    {
    }
    //now get the next line with the numbers of edges and parse it into ints
    std::getline(inputF, line); //--> list of the number of edges on each channel

    //now take the new line as a tab-separated stream and parse it as such
    std::istringstream iss_nb;
    iss_nb.str(line);
    std::string edgeNb;
    edgeProp e {0, nullptr};
    int k = 0;

    std::getline(iss_nb, edgeNb, '\t');//there is at least one channel obviously
    do
    {
        //std::cout << atoi(edgeNb.c_str()) << ", k=" << k << std::endl;
        std::vector<edgeProp> v (atoi(edgeNb.c_str()), e);
        if(k<risingE_outVec.size())
        {
            risingE_outVec[k] = v;
        }
        ++k;
        std::getline(iss_nb, edgeNb, '\t');
    }while(!iss_nb.eof());

    std::cout << "rise outVec->" << risingE_outVec.size() << std::endl;
    for(int i = 0; i<risingE_outVec.size(); ++i)
    {
        std::cout << risingE_outVec[i].size() << std::endl;
    }

    //now we are done initializing the vectors of edges

    //skip 2 lines
    std::getline(inputF, line);
    std::getline(inputF, line);

    std::string timestamp;
    std::istringstream iss_t;

    //successively parse the lines containing the timestamps for the rising edges on each channel
    std::cout << "Rising edges first:" << std::endl;
    for(int chInd = 0; chInd<P; ++chInd)
    {
        std::getline(inputF, line); // --> line "Channel #"
        std::getline(inputF, line); // --> line with edges on chInd

        iss_t.clear();
        iss_t.str(line);
        k = 0;
        if(risingE_outVec[chInd].size()>0)//there is at least 1 edge on this channel
        {
            std::getline(iss_t, timestamp, '\t');
            do
            {
                std::cout << atof(timestamp.c_str()) << ", k=" << k << std::endl;
                if(k<risingE_outVec[chInd].size())
                    risingE_outVec[chInd][k].timestamp = atof(timestamp.c_str());
                ++k;
                std::getline(iss_t, timestamp, '\t');
            }while(!iss_t.eof());
        }
        std::getline(inputF, line);// --> empty line
    }

    //---------------------------------------------------------------------------------------
    //THEN REPEAT THE SAME CYCLES FOR THE FALLING EDGES

    std::string st2 = "Number of falling edges per channel";
    for (; line.substr(0, st2.size()) != st2 && !inputF.eof(); std::getline(inputF, line))
    {
    }

    std::cout << "Falling edges from now:" << std::endl;
    //now get the next line with the numbers of edges and parse it into ints
    std::getline(inputF, line); //--> list of the number of edges on each channel

    //now take the new line as a tab-separated stream and parse it as such
    iss_nb.clear();
    iss_nb.str(line);
    k = 0;

    std::getline(iss_nb, edgeNb, '\t');//there is at least one channel obviously
    do
    {
        //std::cout << atoi(edgeNb.c_str()) << ", k=" << k << std::endl;
        std::vector<edgeProp> v (atoi(edgeNb.c_str()), e);
        if(k<fallingE_outVec.size())
        {
            fallingE_outVec[k] = v;
        }
        ++k;
        std::getline(iss_nb, edgeNb, '\t');
    }while(!iss_nb.eof());

    //skip 2 lines
    std::getline(inputF, line);
    std::getline(inputF, line);

    //successively parse the lines containing the timestamps for the falling edges on each channel
    for(int chInd = 0; chInd<P; ++chInd)
    {
        std::getline(inputF, line); // --> line "Channel #"
        std::cout << "is it Chan#? " << line << std::endl;
        std::getline(inputF, line); // --> line with edges on chInd

        iss_t.clear();
        iss_t.str(line);
        k = 0;
        if(fallingE_outVec[chInd].size()>0)//there is at least 1 edge on this channel
        {
            std::getline(iss_t, timestamp, '\t');
            do
            {
                std::cout << atof(timestamp.c_str()) << ", k=" << k << std::endl;
                if(k<fallingE_outVec[chInd].size())
                    fallingE_outVec[chInd][k].timestamp = atof(timestamp.c_str());
                ++k;
                std::getline(iss_t, timestamp, '\t');
            }while(!iss_t.eof());
        }
        std::getline(inputF, line);// --> empty line
    }

    //set the right parameters for plotting the sequence data and reset all plot data to 0
    ui->seqLength->setValue(L);
    ui->seqTimeStep->setValue(dt);
    for (int i=0; i<8; ++i)
    {
        seqEditors_in_GUI[i]->resetData(dt, (L-1)*dt);
    }


    //now set the plot data with the right edges in each channel using
    //the same routine as the parsing and plotting from QCPSequenceData
    //and replot each channel
    for (int k = 0; k<P; ++k)
    {
        if(risingE_outVec[k].size()!=fallingE_outVec[k].size())
        {
            std::cout << "Error: number of rising and falling edges not equal!\n" << "Add an adequate number of raising and falling edges!\n\n" << std::endl;
            return false;
        }

        QCPGraphData* found_plot_rise = seqEditors_in_GUI[k]->seqData->data()->begin();
        QCPGraphData* found_plot_fall = seqEditors_in_GUI[k]->seqData->data()->begin()-1;
        for (int i=0; i<risingE_outVec[k].size(); ++i)
        {
                //get the i-th rising edge
                double risingEdge = risingE_outVec[k][i].timestamp;
                //get the corresponding fallingEdge
                double fallingEdge = fallingE_outVec[k][i].timestamp;
                if (fallingEdge <= risingEdge)//check that the falling edge is correct
                {
                    std::cout << "Error: corresponding falling edge comes before rising edge!\n" << std::endl;
                    return false;
                }
                //find the index of the rising edge in the plot data(first falling edge "is index -1")
                double Tedge = risingEdge;
                auto testEdge = [&Tedge](QCPGraphData data){return std::abs(data.key-Tedge)<0.0001;};
                found_plot_rise = std::find_if(seqEditors_in_GUI[k]->seqData->data()->begin(), seqEditors_in_GUI[k]->seqData->data()->end(), testEdge);
                //find the index of the falling edge in the plot data
                Tedge = fallingEdge;
                found_plot_fall = std::find_if(seqEditors_in_GUI[k]->seqData->data()->begin(), seqEditors_in_GUI[k]->seqData->data()->end(), testEdge);
                std::for_each(found_plot_rise+1, found_plot_fall+1, [](QCPGraphData& data){(&data)->value = 1;});
                //add a point at the rising edge and at the falling edge
                QVector<double> new_keys = {risingEdge, fallingEdge};
                QVector<double> new_values = {1., 0.};
                seqEditors_in_GUI[k]->seqData->addData(new_keys, new_values, true);
                std::cout << "After add_data:\n" << "found_plot_rise: " << found_plot_rise << '\n' << "found_plot_fall: " << found_plot_fall << std::endl;
                //replot
                seqEditors_in_GUI[k]->replot();

        }
    }

    return true;
}





void MainWindow::on_initM2i_clicked()
{
    M2i7005_var->M2iInit();

    QMessageBox IOCardConnect;
    IOCardConnect.setText("IO Card is conneted");
    IOCardConnect.exec();
}

void MainWindow::on_confM2i_clicked()
{
    int actChN = next_power_of_2(ui->chanNumber->value());
    std::cout << next_power_of_2(ui->chanNumber->value()) << " activated channels" << std::endl;

    int64 mask;

    switch(actChN)
    {
        case 1: mask = (int64)1; break;
        case 2: mask = (int64)0x3; break;
        case 4: mask = (int64)0xF; break;
        case 8: mask = (int64)0xFF; break;
        case 16: mask = (int64)0xFFFF; break;
        default: std::cout << "Error: Channel mask is not allowed !" << std::endl; return;
    }

    int loop_nb = ui->loopNb->value();
    int frequency_MHz = ui->DIOfreqVal->value();
    int length_in_samples = lround(ui->chan0->Tmax/ui->chan0->step) + 1;

    M2i7005_var->M2iSetup_StdReplay(mask, frequency_MHz, false, false, loop_nb, length_in_samples);
}

void MainWindow::on_loadData_clicked()
{

    //M2iLoadIntoMemoryFromGui(ui->chanNumber->value());
}

void MainWindow::on_outputM2i_clicked()
{
    M2i7005_var->M2iOutputData();
}

void MainWindow::on_closeM2i_clicked()
{
//    ui->initM2i->setEnabled(true);
//    ui->closeM2i->setEnabled(false);
    M2i7005_var->M2iClose();
}

void MainWindow::on_setPermWord_clicked()
{
    int W = 0;
    for (int c=0; c<8; ++c)
    {
        W += (1<<c)*constStates_in_GUI[c]->isChecked();
    }
    //std::cout << integ_to_binStr((uint16_t) W) << std::endl;

    M2i7005_var->M2iSetConstLvl(8, (uint16_t) W);
}

void MainWindow::checkForLaser(){
    if((ui->constState_chan3->isChecked()) && (ui->constState_chan4->isChecked())){
        return;
    }else{
        ui->constState_chan3->setCheckState(Qt::CheckState(2));
        ui->constState_chan4->setCheckState(Qt::CheckState(2));
        return;
    }
}

void MainWindow::on_parseButton0_clicked()
{
    ui->chan0->seqData->parseAndPlot();
}

void MainWindow::on_parseButton1_clicked()
{
    ui->chan1->seqData->parseAndPlot();
}

void MainWindow::on_parseButton2_clicked()
{
    ui->chan2->seqData->parseAndPlot();
}

void MainWindow::on_parseButton3_clicked()
{
    ui->chan3->seqData->parseAndPlot();
}

void MainWindow::on_parseButton4_clicked()
{
    ui->chan4->seqData->parseAndPlot();
}

void MainWindow::on_parseButton5_clicked()
{
    ui->chan5->seqData->parseAndPlot();
}

void MainWindow::on_parseButton6_clicked()
{
    ui->chan6->seqData->parseAndPlot();
}

void MainWindow::on_parseButton7_clicked()
{
    ui->chan7->seqData->parseAndPlot();
}


void MainWindow::on_writeFileTestButton_clicked()
{
    std::vector< std::vector<edgeProp> > rise;
    std::vector< std::vector<edgeProp> > fall;

    rise.push_back(seqEditors_in_GUI[0]->seqData->risingEdges);
    rise.push_back(seqEditors_in_GUI[1]->seqData->risingEdges);

    fall.push_back(seqEditors_in_GUI[0]->seqData->fallingEdges);
    fall.push_back(seqEditors_in_GUI[1]->seqData->fallingEdges);

    double dt = seqEditors_in_GUI[0]->step;
    int length = lround(seqEditors_in_GUI[0]->Tmax/dt)+1;

    writeSeqToFile(2, length, dt, rise, fall, "E:\\data\\tests_sequences\\", "testFile");
}

void MainWindow::on_loadFileTestButton_clicked()
{
    std::filesystem::path path("E:\\data\\tests_sequences\\");
    std::string name = "2020-07-21 20;03;56 Sequence testFile.dat";

    //now load edges from file into vector ris & fal
    std::vector< std::vector<edgeProp> > ris;
    std::vector< std::vector<edgeProp> > fal;
    loadSeqFromFile(path, name, ris, fal);

    //put the loaded vectors into te rising and falling edges member vectors of the QCPSequenceData
    for (int r = 0; r<8; ++r)
    {
        if(r<ris.size())
            seqEditors_in_GUI[r]->seqData->risingEdges = ris[r];
        else
            seqEditors_in_GUI[r]->seqData->risingEdges.resize(0);
    }
    for (int f =0; f<8; ++f)
    {
        if(f<fal.size())
            seqEditors_in_GUI[f]->seqData->fallingEdges = fal[f];
        else
            seqEditors_in_GUI[f]->seqData->fallingEdges.resize(0);
    }

}

void MainWindow::on_changeSeqParams_clicked()
{
    double Tstep = ui->seqTimeStep->value();
    int seqL = ui->seqLength->value();
    int seqL_rounded;
    if (64*(seqL/64) == seqL)
        seqL_rounded = seqL;
    else
        seqL_rounded = (seqL/64 + 1)*64;
    std::cout << "new length: " << seqL_rounded << std::endl;

    int max_time = (seqL_rounded-1) * Tstep;
    std::cout << "new max T: " << max_time << std::endl;


    //set the plot data to 0 everywhere
    //reset the rising and falling edge vectors to length 0
    for (int i=0; i<8; ++i)
    {
        seqEditors_in_GUI[i]->resetData(Tstep, max_time);
    }
}

void MainWindow::on_getCountsPerSec_clicked()
{
    ui->showCountsPerSec->display(ScanAndCount_var->getCurrentCountsFromTT()/1000);
}



// ----------------------------------------------------
// ---------- Magnetstage Tab and Functions -----------
// ----------------------------------------------------

//distance between full driven motor an diamond
double minDistance_mm = 5.0;
bool instantanMovementMagnetstage = false;

void MainWindow::EnableDisableButtonsMagnet(bool state){
    if (state){
        ui->ConnectMagnetstage->setEnabled(false);
    }else{
        ui->ConnectMagnetstage->setEnabled(true);
    }
    ui->DisconnectMagnetstage->setEnabled(state);
    ui->settingsMagnetstage->setEnabled(state);
    ui->visualization2DPlot->setEnabled(state);
    ui->simultanMovementMagnetstage->setEnabled(state);
    ui->instantanMovementMagnetstage->setEnabled(state);
    ui->MoveButtonMagnet->setEnabled(state);
    ui->StopButtonMagnet->setEnabled(state);
}

void MainWindow::on_MoveButtonMagnet_clicked() // during to the fact another magnet system is used the boarders changed... to solve this problem the boarder has to be redetermiend
{
    double distance_mm = ui->distanceDiamondMagnet->value();
    double angleSmall_deg = ui->angleSmallMotor->value();
    double angleBig_deg = ui->angleBigMotor->value();

    //if(magnet->checkMagnetstageLimits(ui->distanceDiamondMagnet->value(), ui->angleSmallMotor->value(), ui->angleBigMotor->value())){
        //if(magnetInstantanMovement == false && magnet.musicOnOff == true){
            //ElevatorMusic->play();
        //}
        moveMagnetStage(distance_mm, angleSmall_deg, angleBig_deg);
    //}else{
      //  QMessageBox::critical(this,"ERROR BY MOVEMENT!", "The selected position is beyond the limits and can therefore not be approached. Please choose another position.");
    //}
}

void MainWindow::moveMagnetStage(double distance_mm, double angleSmall_deg, double angleBig_deg)
{
    // change the movment state of the magnet stage to true
    magnet->moveState = true;

    // move magnet stage
    std::thread magStageTh(&magnetcontrolclass::moveMagnetStage, magnet, distance_mm, angleSmall_deg, angleBig_deg);
    magStageTh.detach();

    // starts QTimer to update the magnet stage tab
    updateMagnetStageUI->start(15);
}

//update the magnet stage tab during the Movement of the magnet stage
void MainWindow::updateMagnetTab()
{
    // - show the current positions of the motors make them visible at the magnet stage tab
    ui->curFieldStrength->setNum(889.*(pow(19.05,3)/pow((19.05+magnet->getPositionTransStage()+minDistance_mm),3))-1.6);
    ui->curDistanceDiamondMagnet->setNum(magnet->getPositionTransStage());
    ui->curAngleSmallMotor->setNum(magnet->getPositionRotStageSmall());
    ui->curAngleBigMotor->setNum(magnet->getPositionRotStageBig());

    // - show the current position on the 3D Plot
    ui->visualization3DMagnet->Magnetic3DPlot(magnet->getPositionTransStage(),magnet->getPositionRotStageSmall(),magnet->getPositionRotStageBig());

    //updates the 2D Plot if the belonging window is open
    if(magnet->visualization2dim->isVisible()){
        magnet->update2DVisualization();
    }

    if(!magnet->moveState){
        // stop the QTimer --> stop to update the magnet stage tab every 15 ms
        updateMagnetStageUI->stop();
        // emit signal for Sora
        emit stateAIMagnet(true);
    }
}

void MainWindow::on_StopButtonMagnet_clicked()
{
    magnet->stopMotors();
    updateMagnetStageUI->stop();
}


void MainWindow::on_ConnectMagnetstage_clicked()
{
    if(ui->ConnectMagnetstage->isEnabled()){
        magnet->connect2motors();

        if(magnet->getStateMotors()){
            ui->visualization3DMagnet->Magnetic3DPlot(magnet->getPositionTransStage(),magnet->getPositionRotStageSmall(),magnet->getPositionRotStageBig());

            ui->magFieldStrength->setValue(889.*(pow(19.05,3)/pow((19.05+magnet->getPositionTransStage()+minDistance_mm),3))-1.6);
            ui->curFieldStrength->setNum(889.*(pow(19.05,3)/pow((19.05+magnet->getPositionTransStage()+minDistance_mm),3))-1.6);
            ui->distanceDiamondMagnet->setValue(magnet->getPositionTransStage());
            ui->curDistanceDiamondMagnet->setNum(magnet->getPositionTransStage());
            ui->angleSmallMotor->setValue(magnet->getPositionRotStageSmall());
            ui->curAngleSmallMotor->setNum(magnet->getPositionRotStageSmall());
            ui->angleBigMotor->setValue(magnet->getPositionRotStageBig());
            ui->curAngleBigMotor->setNum(magnet->getPositionRotStageBig());

            EnableDisableButtonsMagnet(true);
            QMessageBox magConnect;
            magConnect.setText("Magnet stage is connected");
            magConnect.exec();
        }else{
            QMessageBox::critical(this,"ERROR BY CONNECTION!", "Controller aren't connected. Please connect before continuing.");
        }
    }
}

void MainWindow::on_DisconnectMagnetstage_clicked()
{
    if (ui->DisconnectMagnetstage->isEnabled()){
        magnet->disconnect2motors();
        EnableDisableButtonsMagnet(false);
    }
}


// SpinBox- functions
void MainWindow::on_angleSmallMotor_editingFinished()
{
    if(instantanMovementMagnetstage){
        moveMagnetStage(ui->distanceDiamondMagnet->value(), ui->angleSmallMotor->value(), ui->angleBigMotor->value());
    }
}

void MainWindow::on_angleBigMotor_editingFinished()
{
    if(instantanMovementMagnetstage){
        moveMagnetStage(ui->distanceDiamondMagnet->value(), ui->angleSmallMotor->value(), ui->angleBigMotor->value());
    }
}

void MainWindow::on_magFieldStrength_editingFinished()
{
    double dis2magField = pow(((889.*pow(19.05,3.))/(ui->magFieldStrength->value()+1.6)),(1./3.))-19.05-minDistance_mm;
    ui->distanceDiamondMagnet->setValue(dis2magField);
    if(instantanMovementMagnetstage){
        moveMagnetStage(ui->distanceDiamondMagnet->value(), ui->angleSmallMotor->value(), ui->angleBigMotor->value());
    }
}

void MainWindow::on_distanceDiamondMagnet_editingFinished()
{
    double magField_mT = 889.*(pow(19.05,3)/pow((19.05+ui->distanceDiamondMagnet->value()+minDistance_mm),3))-1.6;
    ui->magFieldStrength->setValue(magField_mT);
    if(instantanMovementMagnetstage){
        moveMagnetStage(ui->distanceDiamondMagnet->value(), ui->angleSmallMotor->value(), ui->angleBigMotor->value());
    }
}

// open other windows
void MainWindow::on_settingsMagnetstage_clicked()
{
    magnet->openSettings();
}

void MainWindow::on_visualization2DPlot_clicked()
{
    magnet->open2DVisualization();
}

// SpinBox for movement options
void MainWindow::on_simultanMovementMagnetstage_clicked(bool simultanMovement)
{
    magnet->setSimultanMovement(simultanMovement);
}

void MainWindow::on_instantanMovementMagnetstage_clicked(bool instantanMovement)
{
    instantanMovementMagnetstage = instantanMovement;
}


void MainWindow::on_ResetMagnetstage_clicked()//###??
{
    magnet->disconnect2motors();
}

void MainWindow::on_bordersMagnetestage_clicked()
{
    magnet->openBorders();
}

// ----------------------------------------------------
// ---------------- Sora CNN functions ----------------
// ----------------------------------------------------

// refocus NV center
void MainWindow::refocus()
{
    sora_var->refocus();
    std::cout << "finish" << std::endl;
}

// opens 'Sora' in another window
void MainWindow::on_openSoraButton_clicked()
{
    sora_var->show();

    // set the sora data to the current data
    if(!QVector<double>::fromStdVector(ScanAndCount_var->imageVec).isEmpty()){
        sora_var->setData(QVector<double>::fromStdVector(ScanAndCount_var->imageVec), ScanAndCount_var->currentScanParameters.Npx_X, ScanAndCount_var->currentScanParameters.Npx_Y, ScanAndCount_var->currentScanParameters.Xstep, ScanAndCount_var->currentScanParameters.center.X, ScanAndCount_var->currentScanParameters.center.Y);
    }else if(!QVector<double>::fromStdVector(LoadFileCreatMosaic_var->currentDataFromFile.kcountsPerSec).isEmpty()){
        sora_var->setData(QVector<double>::fromStdVector(LoadFileCreatMosaic_var->currentDataFromFile.kcountsPerSec), LoadFileCreatMosaic_var->currentDataFromFile.Npx_X, LoadFileCreatMosaic_var->currentDataFromFile.Npx_Y, LoadFileCreatMosaic_var->currentDataFromFile.Xstep, LoadFileCreatMosaic_var->currentDataFromFile.XPos, LoadFileCreatMosaic_var->currentDataFromFile.YPos);
    }

    // set path
    sora_var->curPath = QString::fromStdString(globalPath.string());
}

// AI_moveMagnetStage
void MainWindow::AI_moveMagnetStage(double smallMotor, double bigMotor, double distanceMotor)
{
    //if(magnet->checkMagnetstageLimits(ui->distanceDiamondMagnet->value(), ui->angleSmallMotor->value(), ui->angleBigMotor->value())){
        ui->distanceDiamondMagnet->setValue(distanceMotor);
        ui->angleSmallMotor->setValue(smallMotor);
        ui->angleBigMotor->setValue(bigMotor);

        //magnet->setMoveValues(distanceMotor, smallMotor, bigMotor);
        //std::cout << "distance: " << distanceMotor << ", small Motor: " << smallMotor << ", big motor: " << bigMotor << std::endl;
        //updateMagneticUIMovement->start(15);
    //}
}





// predict and mark NV ROI's in the main colormap
QVector<double> MainWindow::predictNVCenter(QVector<double> inputData, int nb_pixelX, int nb_pixelY, int resolution, int posX, int posY){
    QVector<QVector<double>> positions;
    QVector<double> inputDataWOSnake;

    // undo the snake pattern
    for(int yIndex=0; yIndex<nb_pixelY; ++yIndex){
        for(int xIndex = (yIndex%2==0? 0: nb_pixelY-1); xIndex>=0 && xIndex<nb_pixelY; yIndex%2==0? ++xIndex: --xIndex){
            inputDataWOSnake.append(inputData[(yIndex*nb_pixelX)+xIndex]);
        }
    }

    // predicted the positions using the CNN
    positions = cnnwoBias_var->predicted(inputDataWOSnake, nb_pixelX, nb_pixelY, resolution);

    // create the ROI's
    for(int i=0; i<positions.size(); i++){
        positions[i][0] += posX-((nb_pixelX/2.)*resolution);
        positions[i][1] += posY-((nb_pixelY/2.)*resolution);
        //#ui->QCPimageWROI->addROI({positions[i][0]/1000., positions[i][1]/1000.}, positions[i][2]/1000., positions[i][3]/1000.);
        // show ROI Legend
        //#ui->QCPimageWROI->enableROILegend(true);
    }

    ui->QCPimageWROI->replot();

    return {positions[0][0], positions[0][1]};
}

// predici NV-center unsing CNN
void MainWindow::on_predictNVCenter_clicked()
{
    refocus();
    /*if(!QVector<double>::fromStdVector(LoadFileCreatMosaic_var->currentDataFromFile.kcountsPerSec).isEmpty()){
        predictNVCenter(QVector<double>::fromStdVector(LoadFileCreatMosaic_var->currentDataFromFile.kcountsPerSec), LoadFileCreatMosaic_var->currentDataFromFile.Npx_X, LoadFileCreatMosaic_var->currentDataFromFile.Npx_Y, LoadFileCreatMosaic_var->currentDataFromFile.Xstep, LoadFileCreatMosaic_var->currentDataFromFile.XPos, LoadFileCreatMosaic_var->currentDataFromFile.YPos);
    }*/
}


//#########
void MainWindow::setAIODMRParameters(int loopNb, double excitTime, double freqStart, double freqEnd, int stepNb){

    // start uploading the ODMR sequence in a thread
    std::thread AIODMRUpload(&MainWindow::uploadAIODMRParameters, this, loopNb, excitTime, freqStart, freqEnd, stepNb);
    AIODMRUpload.detach();
}
//#########
void MainWindow::uploadAIODMRParameters(int loopNb, double excitTime, double freqStart, double freqEnd, int stepNb){
    if(checkClickedButton(0, "prep")){
        return;
    }
    ViConstString chan = "1";

    auto frequencies = linearFreqArray(freqStart*MHz_M8195A, freqEnd*MHz_M8195A, stepNb);

    std::vector<double> times (frequencies.size(), excitTime*microsec_M8195A);
    std::vector<double> phases (frequencies.size(), 0);
    std::vector<double> amplitudes (frequencies.size(), 1);

    ViStatus status = M8195A_var->loadSineArrayWaveformAndSeqTable(chan, frequencies, maxSampleRate, times, loopNb, phases, amplitudes);

    emit StateAIODMRUpload(false);
}




void MainWindow::startAIcwODMR(int loop_nb){
    std::thread AIcwODMR(&MainWindow::startCwODMR, this, loop_nb);
    AIcwODMR.detach();
}


void MainWindow::startCwODMR(int loop_nb){
    // ####  prepare the AWG for cw ODMR START  ####
    int actChN = next_power_of_2(6);
    std::cout << next_power_of_2(6) << " activated channels" << std::endl;

    int64 mask;

    switch(actChN)
    {
        case 1: mask = (int64)1; break;
        case 2: mask = (int64)0x3; break;
        case 4: mask = (int64)0xF; break;
        case 8: mask = (int64)0xFF; break;
        case 16: mask = (int64)0xFFFF; break;
        default: std::cout << "Error: Channel mask is not allowed !" << std::endl; return;
    }

    int frequency_MHz = (int)(1000./1000);

    int numbOfSteps = (PulsedMeasurementsAndData_var->currentODMRParams.excitTime+10)*PulsedMeasurementsAndData_var->currentODMRParams.stepNb;
    int length_in_samples;
    if(numbOfSteps%64==0){
        length_in_samples=numbOfSteps;
    }else{
        length_in_samples=64*(numbOfSteps/64+1);
    }

    std::cout << "loop_np: " << loop_nb << ", frequency_MHz: " << frequency_MHz << " length_in_samples: " << length_in_samples << std::endl;

    M2i7005_var->M2iSetup_StdReplay(mask, frequency_MHz, false, false, loop_nb, length_in_samples);

    std::vector< std::vector<int>> risingEdges;
    std::vector< std::vector<int>> fallingEdges;

    // create the cw ODMR Sequence using the risingEdges and fallingEdges
    std::vector<int> rising1Temp;
    std::vector<int> falling1Temp;
    std::vector<int> rising2Temp;
    std::vector<int> falling2Temp;
    std::vector<int> risingLaserEnabled;
    std::vector<int> fallingLaserEnabled;
    std::vector<int> risingLaserDigital;
    std::vector<int> fallingLaserDigital;

    for(int i = 0; i<PulsedMeasurementsAndData_var->currentODMRParams.stepNb; ++i){
        {
            rising1Temp.push_back(i*PulsedMeasurementsAndData_var->currentODMRParams.excitTime);
            falling1Temp.push_back(i*PulsedMeasurementsAndData_var->currentODMRParams.excitTime+10);

            risingLaserEnabled.push_back(i*PulsedMeasurementsAndData_var->currentODMRParams.excitTime);
            risingLaserDigital.push_back(i*PulsedMeasurementsAndData_var->currentODMRParams.excitTime);
            fallingLaserEnabled.push_back((i+1)*PulsedMeasurementsAndData_var->currentODMRParams.excitTime-5);
            fallingLaserDigital.push_back((i+1)*PulsedMeasurementsAndData_var->currentODMRParams.excitTime-5);
        }
        rising2Temp.push_back(i*PulsedMeasurementsAndData_var->currentODMRParams.excitTime+10);
        falling2Temp.push_back(i*PulsedMeasurementsAndData_var->currentODMRParams.excitTime+PulsedMeasurementsAndData_var->currentODMRParams.excitTime-10);
    }
    risingEdges.push_back({0});
    fallingEdges.push_back({10});
    risingEdges.push_back(rising1Temp);
    fallingEdges.push_back(falling1Temp);
    risingEdges.push_back(rising2Temp);
    fallingEdges.push_back(falling2Temp);
    risingEdges.push_back(risingLaserEnabled);
    fallingEdges.push_back(fallingLaserEnabled);
    risingEdges.push_back(risingLaserDigital);
    fallingEdges.push_back(fallingLaserDigital);
    risingEdges.push_back({0});
    fallingEdges.push_back({10});

    M2i7005_var->M2iLoadIntoMemoryFromVec(6, risingEdges, fallingEdges);

    // ####  prepare the AWG for cw ODMR END  ####

    PulsedMeasurementsAndData_var->prepareCounterForODMR();

    PostionForPulsedMeasurments();
    M8195A_var->run();

    M2i7005_var->M2iOutputData();
    M8195A_var->stopGen();

    PulsedMeasurementsAndData_var->fetchAndProcessDataForODMR();

    emit StateAIcwODMR(false);
}
