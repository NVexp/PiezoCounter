#ifndef M8195ADEV_H
#define M8195ADEV_H

#include "KtM8195.h"
#include <assert.h>
#include <algorithm>
#include <iostream>
#include <vector>
#include <math.h>
#include <QString>


// only one command code possible apparently, which is 'idle'
//const ViInt32 idle_command = 1 << 31;
const ViInt32 idle_command = 0x80000000;
//beginning/end of sequence or scenario
//const ViInt32 end_sequence = 1 << 30;
const ViInt32 end_sequence = 0x40000000;
//const ViInt32 end_scenario = 1 << 29;
const ViInt32 end_scenario = 0x20000000;
//const ViInt32 init_sequence = 1 << 28;
const ViInt32 init_sequence = 0x10000000;
// for marker channels
//const ViInt32 marker_enable = 1 << 24;
const ViInt32 marker_enable = 0x1000000;
//setting sequence advancement mode
const ViInt32 sequence_adv_auto = 0;
//const ViInt32 sequence_adv_cond = 1 << 20;
const ViInt32 sequence_adv_cond = 0x100000;
//const ViInt32 sequence_adv_repeat = 2 << 20;
const ViInt32 sequence_adv_repeat = 0x200000;
//const ViInt32 sequence_adv_single = 3 << 20;
const ViInt32 sequence_adv_single = 0x300000;
//setting segment advancement mode
const ViInt32 segment_adv_auto = 0;
//const ViInt32 segment_adv_cond = 1 << 16;
const ViInt32 segment_adv_cond = 0x10000;
//const ViInt32 segment_adv_repeat = 2 << 16;
const ViInt32 segment_adv_repeat = 0x20000;
//const ViInt32 segment_adv_single = 3 << 16;
const ViInt32 segment_adv_single = 0x30000;

// base units are Hz and s for the M8195A
static const double MHz_M8195A = 1.e6;
static const double GHz_M8195A = 1000.*MHz_M8195A;
static const double kHz_M8195A = 0.001*MHz_M8195A;
static const double microsec_M8195A = 1.e-6;
static const double nanosec_M8195A = 0.001*microsec_M8195A;
static const double millisec_M8195A = 1000.*microsec_M8195A;

static const ViReal64 maxSampleRate = 65.*GHz_M8195A;
static const ViInt32 maxGranularity = 256;
static const ViInt32 maxMinimumSegmentLength = 1280;    //for every sample clock divider, there is a different
                                                        //minimum segment length hence maxMinimum...
static const double dPI =  3.14159265358979323846;

typedef struct int_pair{
    long first;
    long second;
} int_pair;

typedef struct int_triplet{
    int first;
    int second;
    int third;
} int_triplet;


class M8195Adev
{
public:
    ViStatus status;
    ViSession session;
    ViChar str[128];
    ViChar ErrorMessage[256];
    ViInt32 ErrorCode;
    //int granularity;

    M8195Adev();
    ~M8195Adev();

    ViStatus loadSineArrayWaveformAndSeqTable(ViConstString channel, const std::vector<double>& freqs, double sample_freq, const std::vector<double>& excitation_times, ViInt64 scenLoopCount, const std::vector<double>& phases, const std::vector<double>& amplitudes);
//    ViStatus loadSineArrayWaveformAndSeqTable(ViConstString channel, const std::vector<double>& freqs, double sample_freq, const std::vector<double>& excitation_times, ViInt64 scenLoopCount);
    ViStatus loadCompPulsesWaveformAndSeqTable(ViConstString channel, const std::vector<double>& freqs, double sample_freq, const std::vector<double>& excitation_times, ViInt64 scenLoopCount, const std::vector<double>& phases, const std::vector<double>& startTimes, const std::vector<double> amplitudes, const bool DetuningMeasurment);
    ViStatus loadRamseySpinEchoWaveformAndSeqTable(ViConstString channel, const std::vector<double>& freqs, double sample_freq, const double piOver2_times, const double pi_times, ViInt64 scenLoopCount, const double stepSize, const bool Ramsey);
    ViStatus loadRamseySpinEchoWaveformAndSeqTableSeparated(ViConstString channel, const std::vector<double>& freqs, double sample_freq, const double piOver2_times, const double pi_times, ViInt64 scenLoopCount, const double stepSize, const bool Ramsey);
    ViStatus stopGen();
    ViStatus run();
    ViStatus setTriggermode(const QString& mode);

};


// greatest common divider
// in std library since C++17
static int gcd(int a, int b)
{
    if (a*b == 0)
    {
        std::cout << "Wrong Input : computing GCD of argument 0" << std::endl;
        return 0;
    }
    //rest of euclidian division
    int r;

    if (a < b) { std::swap(a, b); }

    //simple Euclidean algorithm
    while (1) {//algorithm is guaranteed to halt if a,b > 0
        r = a%b;
        if (r == 0)
        {
            return b;
        }
        else
        {
            a = b; b = r;
        }
    }
}

//least common multiplier
//compupted using the formula GCD(x,y)*LCM(x,y) = |x*y|
static int lcm(int a, int b)
{
    return (a*b != 0) ? (a*b / gcd(a, b)) : 0;
}

// given a frequency 'freq', calculates a minimum amount of samples at sample clock frequency 'sampling_freq' such that
// the samples describe a sine wave which starts AND ENDS at 0, within the specified precision 'prec' ; in other words, such that the segment made of these samples
// contains an integer number of periods of the sine wave at the specified frequency 'freq', in order to avoid phase slips/jumps in the generated waveform.
// this is a problem of so-called "Diophantine approximation" ; see e.g. https://en.wikipedia.org/wiki/Diophantine_approximation
// an efficient solution might be to use the continuous fraction representation, not sure

// here the search is done by brute force because the precision required for 8-bit resolution of the M8195A is not high. this means all numbers of periods starting at 1 upwards are checked to see whether
// after this number of periods, the sine argument gets close enough to a possible sample time value (which is p/Fsample, p integer).
//
// primary goal is to find p such that |sin(2*Pi*Fsig*tm)| = |sin(2*Pi*Fsig*Pm/Fsam)|<= g (Fsig = "sine signal frequency", Fsam = "sampling frequency"), and Pm large enough to span at least one approximate period (so Pm/Fs > 3Pi/2 for example)
// equivalent goal is to find the smallest integer K such that there exists an integer P that satisfies |K*a - P| <= a*d
// here a is the ratio Fsam/Fsig and d is Arcsin(g)/(2*Pi) where g is the precision, for instance 1/2^n
// that integer P can only be the integer part of K*a (floor function) or its successor (ceiling function), assuming d is smaller than 1, obviously

// returning both the smallest number of samples P and the associated number of periods K to generate a perfect sine wave within a certain precision
// return vector is {result[0] = min number of periods, result[1] = associated nb of samples}
static int_pair minimumPeriodNumberForSineLoops(double signal_freq, double sample_freq, double epsilon)
{
    //k is the period number/index !
    long k = 1;
    double kf = (double)k;
    double ratio = sample_freq / signal_freq;

    double lower = floor(kf * ratio);//integer part of P*a
    double upper = lower + 1.;
    double error = asin(epsilon)/(2.*dPI);//should be out of the function because always has the same value in practice
    //checking for any integer number of period P if there is a integer number of sample in the vicinity of P
    while ((fabs(kf*ratio - lower) >= error*ratio) && (fabs(kf*ratio - upper) >= error*ratio))
    {
        ++k;
        kf = (double)k;
        lower = floor(kf * ratio);
        upper = lower + 1.;
    }

    //the right number of period has been found
    //select the right output out of the two possibilites
    if (fabs(kf*ratio - lower) < fabs(kf*ratio - upper))
    {
        int_pair result{ k, std::lround(lower)+1 };
        return result;
    }
    else
    {
        int_pair result{ k, std::lround(upper)+1 };
        return result;
    }
    // the integer result.second is the NUMBER OF SAMPLES needed, when starting at t=0,
    // INCLUDING THE FIRST 0 that have be generated
    // until the sin(2*pi*f*t) gets back to 0 within the given epsilon
    // the INDEX of the sample is just result.second-1, obviously
}




static int_pair minimumPeriodNumberForSineLoopsWGran(double signal_freq, double sample_freq, double epsilon, int gran)
{
    //k is the period number/index !
    long k = 1;
    double kf = (double)k;
    double ratio = sample_freq / signal_freq;

    double lower = floor(kf * ratio);//integer part of P*a
    double upper = lower + 1.;
    double error = epsilon/(gran*2.*dPI);//should be out of the function because always has the same value in practice
    //checking for any integer number of period P if there is a integer number of sample in the vicinity of P
    while ((fabs(kf*ratio - lower) >= error*ratio) && (fabs(kf*ratio - upper) >= error*ratio))
    {
        ++k;
        kf = (double)k;
        lower = floor(kf * ratio);
        upper = lower + 1.;
    }

    //the right number of period has been found
    //select the right output out of the two possibilites
    if (fabs(kf*ratio - lower) < fabs(kf*ratio - upper))
    {
        int_pair result{ gran*k, gran*std::lround(lower)+1 };
        return result;
    }
    else
    {
        int_pair result{ gran*k, gran*std::lround(upper)+1 };
        return result;
    }
    // the integer result.second is the NUMBER OF SAMPLES needed, when starting at t=0,
    // INCLUDING THE FIRST 0 that have be generated
    // until the sin(2*pi*f*t) gets back to 0 within the given epsilon/granularity
    // the INDEX of the sample is just result.second-1, obviously
}



// main segment is the segment containing the minimum amount of periods calculated in MinimumPeriodNumber
//
// fmod() is found in cmath
// returns a triplet of {loop number, number of samples in loopable segment, sample number of rest segment} for a given signal_freq and excitation time
static int_triplet sequenceParamsForSinePulse(int granularity, double signal_freq, double sample_freq, double excitation_time, double prec)
{
    int_pair mins = minimumPeriodNumberForSineLoops(signal_freq, sample_freq, prec);

    int min_nb_samples = mins.second;
    std::cout << "Min number of samples for f=" << signal_freq << " : " << min_nb_samples << '\n';
    //apply granularity criterion: the minimum segment length is the least common multiple of the
    //segment granularity and the absolute minimum number of samples needed for it to be loopable
    int min_seg_with_granularity = lcm(granularity, min_nb_samples - 1);

    // #### BEGIN of finding the length of the rest segment ####
    //length of main segment i.e length of each loop (in time unit)
    double main_seg_time = (double)(min_seg_with_granularity) / sample_freq;
    //length of rest segment i.e excitation time modulo main segment time
    double rest_t = fmod(excitation_time, main_seg_time);
    //loop number is integer quotient of excitation time divided by main segment time
    //which is also (excitation time - rest time) divided by main segment time
    long loop_nb = std::lround((excitation_time - rest_t) / main_seg_time);
    //rounded number of periods in the rest segment
    long rest_periods_nb = std::lround(rest_t * signal_freq);
    //rest time after rounding number of periods
    double rounded_rest_t = (double)rest_periods_nb / signal_freq;
    //final number of samples generated in the rest segment
    long nb_samples_in_rest = std::lround(rounded_rest_t * sample_freq);
    // #### END of finding the length of the rest segment ####
    // THE LENGTH OF THE REST SEGMENT HERE IS NOT A MULTIPLE OF THE GRANULARITY
    //we need this in order to know where to cut the main segment to obtain the rest segment

    int_triplet result{ loop_nb, min_nb_samples - 1, nb_samples_in_rest };

    return result;
    // now we know we just to have to generate a rest sine waveform spanning 'result.third' samples and append it after
    // an amount 'result.first' loops of the main segment, which is 'result.second' samples long
}


static int_triplet sequenceParamsForSinePulseWGran(int granularity, double signal_freq, double sample_freq, double excitation_time, double prec)
{
    int_pair mins = minimumPeriodNumberForSineLoopsWGran(signal_freq, sample_freq, prec, granularity);

    int min_seg_with_granularity = mins.second;
    //std::cout << "Min number of samples with granularity for f=" << signal_freq << " : " << min_seg_with_granularity << '\n';

    // #### BEGIN of finding the length of the rest segment ####
    //length of main segment i.e length of each loop (in time unit)
    double main_seg_time = (double)(min_seg_with_granularity) / sample_freq;
    //length of rest segment i.e excitation time modulo main segment time
    double rest_t = fmod(excitation_time, main_seg_time);
    //loop number is integer quotient of excitation time divided by main segment time
    //which is also (excitation time - rest time) divided by main segment time
    long loop_nb = std::lround((excitation_time - rest_t) / main_seg_time);
    //rounded number of periods in the rest segment
    long rest_periods_nb = std::lround(rest_t * signal_freq);
    //rest time after rounding number of periods
    double rounded_rest_t = (double)rest_periods_nb / signal_freq;
    //final number of samples generated in the rest segment
    long nb_samples_in_rest = std::lround(rounded_rest_t * sample_freq);
    // #### END of finding the length of the rest segment ####
    // THE LENGTH OF THE REST SEGMENT HERE IS NOT A MULTIPLE OF THE GRANULARITY
    //we need this in order to know where to cut the main segment to obtain the rest segment

    int_triplet result{ loop_nb, min_seg_with_granularity - 1, nb_samples_in_rest };

    return result;
    // now we know we just to have to generate a rest sine waveform spanning 'result.third' samples and append it after
    // an amount 'result.first' loops of the main segment, which is 'result.second' samples long
}


static std::vector<double> linearFreqArray(double Fstart, double Fend, size_t steps)
{
    assert(("Error: start frequency is larger than stop frequency", Fstart < Fend));
    std::vector<double> freqs;
    freqs.reserve(steps+1);
    double dSteps = (double)steps;
    for (size_t i = 0; i<steps+1; ++i)
    {
        freqs.push_back(Fstart + (Fend-Fstart)*((double)i)/dSteps);
    }

    return freqs;
}

static std::vector<double> logFreqArray(double Fstart, double Fend, size_t steps)
{
    assert(("Error: start frequency is larger than stop frequency", Fstart < Fend));
    std::vector<double> freqs;
    freqs.reserve(steps+1);
    double stepFactor = std::pow(Fend/Fstart, 1./((double)steps));
    for (size_t i = 0; i<steps+1; ++i)
    {
        freqs.push_back(Fstart * stepFactor);
    }

    return freqs;
}




#endif // M8195ADEV_H
