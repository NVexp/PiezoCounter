/*
This file is part of Time Tagger software defined digital data acquisition.

Copyright (C) 2011-2019 Swabian Instruments
All Rights Reserved

Unauthorized copying of this file is strictly prohibited.
*/

#ifndef TIMETAGGER_H_
#define TIMETAGGER_H_

#ifdef LIBTIMETAGGER_EXPORTS
#define TT_API __declspec(dllexport) 
#pragma warning (disable : 4251)
#else
#if defined(__linux) || defined(SWIG) || defined(NOEXPORT)
#define TT_API
#else
#define TT_API __declspec(dllimport) 
#pragma warning (disable : 4251)
#endif
#endif

#include <string>
#include <vector>
#include <stdint.h>
#include <limits>
#include <functional>
#include <map>
#include <set>

class _Iter;

/*! \mainpage TimeTagger
 *
 * \brief backend for TimeTagger, an OpalKelly based single photon counting library
 *
 * \author Markus Wick <markus@swabianinstruments.com>
 * \author Helmut Fedder <helmut@swabianinstruments.com>
 * \author Michael Schlagmüller <michael@swabianinstruments.com>
 *
 * TimeTagger provides an easy to use and cost effective hardware solution for
 * time-resolved single photon counting applications.
 *
 * This document describes the C++ native interface to the TimeTagger device.
 */

class TimeTagger;
class _Iterator;
class _TimeTagger;
class _Worker;


#define timestamp_t long long
#define channel_t int

#define TIMETAGGER_VERSION_MAJOR 2
#define TIMETAGGER_VERSION_MIDDLE 4
#define TIMETAGGER_VERSION_MINOR 4

/*
 *\brief Get the version of the TimeTagger cxx backend
 */

TT_API std::vector<int> getVersion();

/**
 * \brief Constant for unused channel.
 * Magic channel_t value to indicate an unused channel. So the iterators either
 * have to disable this channel, or to choose a default one.
 *
 * This value changed in version 2.1. The old value -1 aliases with falling events.
 * The old value will still be accepted for now if the old numbering scheme is active.
 */
#define CHANNEL_UNUSED -134217728
#define CHANNEL_UNUSED_OLD -1

 /**
* \brief Constant for unused channel.
* @deprecated use CHANNEL_UNUSED
 */
#define CHANNEL_INVALID CHANNEL_UNUSED

/**
 * Allowed values for setTimeTaggerChannelNumberScheme().
 *
 * _ZERO will typically allocate the channel numbers 0 to 7 for the 8 input channels.
 * 8 to 15 will be allocated for the coresponding falling events.
 *
 * _ONE will typically allocate the channel numbers 1 to 8 for the 8 input channels.
 * -1 to -8 will be allocated for the coresponding falling events.
 *
 * _AUTO will choose the scheme based on the hardware revision and so based on the printed label.
 */
#define TT_CHANNEL_NUMBER_SCHEME_AUTO 0
#define TT_CHANNEL_NUMBER_SCHEME_ZERO 1
#define TT_CHANNEL_NUMBER_SCHEME_ONE  2

#ifndef TIMETAGGER_NO_WRAPPER
  /**
  * This are the default wrapper functions without any overloadings.
  */
  #define GET_DATA_1D(function_name, type, argout, attribute) attribute void function_name(std::function<type*(int)> argout)
  #define GET_DATA_2D(function_name, type, argout, attribute) attribute void function_name(std::function<type*(int, int)> argout)
  #define GET_DATA_3D(function_name, type, argout, attribute) attribute void function_name(std::function<type*(int, int, int)> argout)
#endif

/**
 * \brief default constructor factory.
 *
 * \param serial serial number of FPGA board to use. if empty, the first board found is used.
 */
TT_API TimeTagger* createTimeTagger(std::string serial="");

/**
* \brief set path and filename of the bitfile to be loeaded into the FPGA
*
* For debugging/development purposes the firmware loaded into the FPGA can be set manually with this function. To load the default bitfile set bitFileName = ""
* \param bitFileName custom bitfile to use for the FPGA.
*/
TT_API void setCustomBitFileName(std::string bitFileName);

/**
 * \brief free a copy of a timetagger reference.
 * \param tagger the timetagger reference to free
 */
TT_API bool freeTimeTagger(TimeTagger* tagger);

TT_API bool hasInitializedTimeTaggers();

/**
 * \brief fetches a list of all available TimeTagger serials.
 *
 * This function may return serials blocked by other processes or already disconnected some milliseconds later.
 */
TT_API std::vector<std::string> scanTimeTagger();

TT_API std::string getTimeTaggerModel(std::string serial);

/**
 * \brief Configure the numbering scheme for new TimeTagger objects.
 * \param scheme new numbering scheme, must be TT_CHANNEL_NUMBER_SCHEME_AUTO, TT_CHANNEL_NUMBER_SCHEME_ZERO or TT_CHANNEL_NUMBER_SCHEME_ONE
 *
 * This function sets the numbering scheme for newly created TimeTagger objects.
 * The default value is _AUTO.
 *
 * Note: TimeTagger objects are cached internally, so the scheme should be set before the first call of createTimeTagger().
 *
 * _ZERO will typically allocate the channel numbers 0 to 7 for the 8 input channels.
 * 8 to 15 will be allocated for the coresponding falling events.
 *
 * _ONE will typically allocate the channel numbers 1 to 8 for the 8 input channels.
 * -1 to -8 will be allocated for the coresponding falling events.
 *
 * _AUTO will choose the scheme based on the hardware revision and so based on the printed label.
 */
TT_API void setTimeTaggerChannelNumberScheme(int scheme);

/**
 * \brief Fetch the currently configured global numbering scheme.
 *
 * Please see setTimeTaggerChannelNumberScheme() for details.
 * Please use TimeTagger::getChannelNumberScheme() to query the actual used numbering scheme,
 * this function here will just return the scheme a newly created TimeTagger object will use.
 */
TT_API int getTimeTaggerChannelNumberScheme();

enum ErrorLevel {ERROR_LOG, WARNING_LOG, INFO_LOG};
typedef void (*logger_callback)(ErrorLevel level, std::string msg);

/**
 * \brief Sets the notifier callback which is called for each log message
 * \returns The old callback
 *
 * If this function is called with nullptr, the default callback will be used.
 */
TT_API logger_callback setLogger(logger_callback callback);

/**
 * \brief Raise a new log message. Please use the XXXLog macro instead.
 */
TT_API void _Log(ErrorLevel level, const char *file, int line, const char *fmt, ...)
#ifdef __GNUC__
    __attribute__((format(printf, 4, 5)))
#endif
;
#define __Log(level, ...) _Log(level, __FILE__, __LINE__, __VA_ARGS__);
#define ErrorLog(...) __Log(ERROR_LOG, __VA_ARGS__);
#define WarningLog(...) __Log(WARNING_LOG, __VA_ARGS__);
#define InfoLog(...) __Log(INFO_LOG, __VA_ARGS__);

/**
 * \brief backend for the TimeTagger.
 *
 * The TimeTagger class connects to the hardware, and handles the communication over the usb.
 * There may be only one instance of the backend per physical device.
 *
 *
 */
class TT_API TimeTagger {
  friend class _Iterator;
protected:
  /**
   * \brief abstract interface class
   */
  TimeTagger() {}

  /**
   * destructor
   */
  virtual ~TimeTagger() {};

  // Non Copyable
  TimeTagger(const TimeTagger&) = delete;
  TimeTagger& operator=(const TimeTagger&) = delete;
public:

  /**
  * \brief reset the TimeTagger object to default settings and detach all iterators
  */
  virtual void reset() = 0;

  /**
  * \brief set the divider for the frequency of the test signal
  * 
  * The base clock of the test signal oscillator for the Time Tagger Ultra is running at 100.8 MHz sampled down by an factor of 2 to have a similar base clock as the Time Tagger 20 (~50 MHz).
  * The default divider is 63 -> ~800 kEvents/s
  * 
  * \param divider frequency divisor of the oscillator
  */
  virtual void setTestSignalDivider(int divider) = 0;

  /**
  * \brief set the trigger voltage threshold of a channel
  *
  * \param channel   the channel to set
  * \param voltage    voltage level.. [0..1]
  */
  virtual void setTriggerLevel(channel_t channel, double voltage) = 0;

  /**
   * \brief get the trigger voltage threshold of a channel
   *
   * \param channel the channel
   */
  virtual double getTriggerLevel(channel_t channel) = 0;

  /**
   * \brief set time delay on a channel
   *
   * When set, every event on the channel is delayed by the given delay in picoseconds.
   * Setting larger time delays consumes, dependend on input signal, a significant
   * amount of memory.
   * Implementers are adviced to keep the delay below 1 micro second.
   *
   * \param channel  the channel to set
   * \param delay    the delay in picoseconds
   */
  virtual void setInputDelay(channel_t channel, timestamp_t delay) = 0;

  /**
   * \brief get time delay of a channel
   *
   * see setInputDelay
   *
   * \param channel   the channel
   */
  virtual timestamp_t getInputDelay(channel_t channel) = 0;

  /**
   * \brief configures the input multiplexer
   *
   * Every phyiscal input channel has an input multiplexer with 4 modes:
   * 0: normal input mode
   * 1: use the input from channel -1 (left)
   * 2: use the input from channel +1 (right)
   * 3: use the reference oscillator
   *
   * Mode 1 and 2 cascades, so many inputs can be configured to get the same input events.
   *
   * \param channel the phyiscal channel of the input multiplexer
   * \param mux_mode the configuration mode of the input multiplexer
   */
  virtual void setInputMux(channel_t channel, int mux_mode) = 0;

  /**
   * \brief fetches the configuration of the input multiplexer
   *
   * \param channel the phyiscal channel of the input multiplexer
   * \return the configuration mode of the input multiplexer
   */
  virtual int getInputMux(channel_t channel) = 0;

  /**
   * \brief configures the conditional filter
   *
   * After each event on the trigger channels, one event per filtered channel
   * will pass afterwards. This is handled in a very early stage in the pipeline,
   * so all event limitations but the deadtime are supressed. But the accuracy
   * of the order of those events is low.
   *
   * Refer the Manual for a description of this function.
   *
   * \param trigger the channels that sets the condition
   * \param filtered  the channels that are filtered by the condition
   */
  virtual void setConditionalFilter(std::vector<channel_t> trigger, std::vector<channel_t> filtered) = 0;

  /**
   * \brief fetches the configuration of the conditional filter
   *
   * see setConditionalFilter
   */
  virtual std::vector<channel_t> getConditionalFilterTrigger() = 0;

  /**
   * \brief fetches the configuration of the conditional filter
   *
   * see setConditionalFilter
   */
  virtual std::vector<channel_t> getConditionalFilterFiltered() = 0;

  /**
   * \brief enables or disables the filter on the FPGA board.
   * @deprecated use setConditionalFilter
   *
   * The filter disables transmission of nontriggered tags on channel 7.
   * This is a deprecated specialization of setConditionalFilter.
   */
  virtual void setFilter(bool state) = 0;

  /**
   * \brief returns the filter state on the FPGA board
   * @deprecated use getConditionalFilter*
   *
   * The laserfilter disables transmission of nontriggered tags on channel 7.
   * This is a deprecated specialization of getConditionalFilter*.
   *
   */
  virtual bool getFilter() = 0;


  /**
   * \brief enables or disables the normalization of the distribution.
   *
   * Refer the Manual for a description of this function.
   */
  virtual void setNormalization(bool state) = 0;

  /**
   * \brief returns the the normalization of the distribution.
   *
   * Refer the Manual for a description of this function.
   */
  virtual bool getNormalization() = 0;

  /**
   * \brief sets the maximum USB buffer size
   *
   * This option controls the maximum buffer size of the USB connection.
   * This can be used to balance low input latency vs high (peak) throughput.
   *
   * \param size the maximum buffer size in events
   */
  virtual void setHardwareBufferSize(int size) = 0;

  /**
   * \brief queries the size of the USB queue
   *
   * See setHardwareBufferSize for more information.
   *
   * \return the actual size of the USB queue in events
   */
  virtual int getHardwareBufferSize() = 0;

  /**
   * \brief set the deadtime between two edges on the same channel.
   *
   * This function sets the user configureable deadtime. The requested time will
   * be rounded to the nearest multiple of the clock time. The deadtime will also
   * be clamped to device specific limitations.
   *
   * As the actual deadtime will be altered, the real value will be returned.
   *
   * \param channel channel to be configured
   * \param deadtime new deadtime
   * \return the real configured deadtime
   */
  virtual timestamp_t setDeadtime(channel_t channel, timestamp_t deadtime) = 0;

  /**
   * \brief get the deadtime between two edges on the same channel.
   *
   * This function gets the user configureable deadtime.
   *
   * \param channel channel to be queried
   * \return the real configured deadtime
   */
  virtual timestamp_t getDeadtime(channel_t channel) = 0;

  /**
   * \brief Divides the amount of transmitted edge per channel
   *
   * This filter decimates the events on a given channel by a specified.
   * factor. So for a divider n, every nth event is transmitted through
   * the filter and n-1 events are skipped between consecutive 
   * transmitted events. If a conditional filter is also active, the event
   * divider is applied after the conditional filter, so the conditional
   * is applied to the complete event stream and only events which pass the
   * conditional filter are forwarded to the divider.
   *
   * As it is a hardware filter, it reduces the required USB bandwidth and
   * CPU processing power, but it cannot be configured for virtual channels.
   *
   * \param channel channel to be configured
   * \param divider new divider, must be smaller than 65536
   */
  virtual void setEventDivider(channel_t channel, unsigned int divider) = 0;

  /**
   * \brief Returns the factor of the dividing filter
   *
   * See setEventDivider for further details.
   *
   * \param channel channel to be queried
   * \return the configured divider
   */
  virtual unsigned int getEventDivider(channel_t channel) = 0;

  /**
   * \brief register a FPGA channel.
   *
   * Only events on previously registered channels will be transfered over
   * the communication channel.
   *
   * \param channel  the channel
   */
  virtual void registerChannel(channel_t channel) = 0;

  /**
   * \brief release a previously registered channel.
   *
   * \param channel   the channel
   */
  virtual void unregisterChannel(channel_t channel) = 0;

  /**
   * \brief enable the calibration on a channel.
   *
   * This will connect or disconnect the channel with the on-chip uncorrelated signal generator.
   *
   * \param channel  the channel
   * \param enabled  enabled / disabled flag
   */
  virtual void setTestSignal(channel_t channel, bool enabled) = 0;
  virtual void setTestSignal(std::vector<channel_t> channel, bool enabled) = 0;

  /**
   * \brief fetch the status of the test signal generator
   *
   * \param channel   the channel
   */
  virtual bool getTestSignal(channel_t channel) = 0;

  /**
   * \brief runs a calibrations based on the on-chip uncorrelated signal generator.
   */
  GET_DATA_1D(autoCalibration, double, array_out, virtual) = 0;

  /**
  * \brief identifies the hardware by serial number
  */
  virtual std::string getSerial() = 0;

  /**
  * \brief identifies the hardware by Time Tagger Model
  */
  virtual std::string getModel() = 0;

  /**
  * \brief Fetch the configured numbering scheme for this TimeTagger object
  *
  * Please see setTimeTaggerChannelNumberScheme() for details.
  */
  virtual int getChannelNumberScheme() = 0;

  /**
  * \brief returns the minumum and the maximum voltage of the DACs as a trigger reference
  */
  virtual std::vector<double> getDACRange() = 0;

  /**
   * \brief get internal calibration data
   */
  GET_DATA_2D(getDistributionCount, long long, array_out, virtual) = 0;

  /**
   * \brief get internal calibration data
   */
  GET_DATA_2D(getDistributionPSecs, timestamp_t, array_out, virtual) = 0;

  /**
   *  \brief fetch the amount of channels
   *
   * @deprecated Use getChannelList instead.
   */
  virtual channel_t getChannels() = 0;

  /**
   * \brief fetch a vector of all physical input channel ids
   * 
   * The function returns the channel of all rising and falling edges.
   * For example for the Time Tagger 20 (8 input channels) 
   * TT_CHANNEL_NUMBER_SCHEME_ZERO: {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15}
   * and for
   * TT_CHANNEL_NUMBER_SCHEME_ONE: {-8,-7,-6,-5,-4,-3,-2,-1,1,2,3,4,5,6,7,8}
   * 
   * TT_CHANNEL_RISING_EDGES returns only the rising edges
   * SCHEME_ONE: {1,2,3,4,5,6,7,8}
   * and
   * TT_CHANNEL_FALLING_EDGES resturn only the falling edges
   * SCHEME_ONE: {-1,-2,-3,-4,-5,-6,-7,-8}
   * which are the invertedChannels of the rising edges.
   */
#define TT_CHANNEL_RISING_AND_FALLING_EDGES  0
#define TT_CHANNEL_RISING_EDGES 1
#define TT_CHANNEL_FALLING_EDGES 2
  virtual std::vector<channel_t> getChannelList(int type = TT_CHANNEL_RISING_AND_FALLING_EDGES) = 0;

  /**
   * \brief get the falling channel id for a raising channel and vice versa
   */
  virtual channel_t getInvertedChannel(channel_t channel) = 0;

  /**
   * \brief compares the provided channel with CHANNEL_UNUSED
   *
   * But also keeps care about the channel number scheme and selects
   * either CHANNEL_UNUSED or CHANNEL_UNUSED_OLD
   */
  virtual bool isUnusedChannel(channel_t channel) = 0;

  /**
   * \brief fetch the duration of each clock cycle in picoseconds
   */
  virtual timestamp_t getPsPerClock() = 0;


  /**
   * \brief get overflow count
   *
   * Get the number of communication overflows occured
   * 
   */
  virtual long long getOverflows() = 0;

  /**
   * \brief clear overflow counter
   *
   * Sets the overflow counter to zero
   */
  virtual void clearOverflows() = 0;

  /**
   * \brief get and clear overflow counter
   *
   * Get the number of communication overflows occured and sets them to zero
   */
  virtual long long getOverflowsAndClear() = 0;

  /**
   * \brief Sync the timetagger pipeline, so that all started iterators and their enabled channels are ready
   */
  virtual void sync() = 0;

  /**
  * \brief Return the hardware version of the PCB board. Version 0 is everything before mid 2018
  * and with the channel configuration ZERO. version >= 1 is channel configuration ONE
  */
  virtual std::string getPcbVersion() = 0;

  /**
   * \brief Return an unique identifier for the applied firmware.
   *
   * This function returns a comma separated list of the firmware version with
   * - the device identifier: TT-20 or TT-Ultra
   * - the firmware identifier: FW 3
   * - optional the timestamp of the assembling of the firmware
   * - the firmware indentifier of the USB chip: OK 1.30
   * eg "TT-Ultra, FW 3, TS 2018-11-13 22:57:32, OK 1.30"
   */
  virtual std::string getFirmwareVersion() = 0;

  /**
  * \brief Show the status of the sensor data from the FPGA and peripherals on the console
  */
  virtual std::string getSensorData() = 0;

  /**
  * \brief Enforce a state to the LEDs
  * 0: led_status[R]      16: led_status[R] - mux
  * 1: led_status[B]      17: led_status[B] - mux
  * 2: led_status[G]      18: led_status[G] - mux
  * 3: led_power[R]       19: led_power[R]  - mux
  * 4: led_power[B]       20: led_power[B]  - mux
  * 5: led_power[G]       21: led_power[G]  - mux
  * 6: led_clock[R]       22: led_clock[R]  - mux
  * 7: led_clock[B]       23: led_clock[B]  - mux
  * 8: led_clock[G]       24: led_clock[G]  - mux
  */
  virtual void setLED(uint32_t bitmask) = 0;


  typedef std::function<void(_Iterator*)> IteratorCallback;
  typedef std::map<_Iterator*, IteratorCallback> IteratorCallbackMap;

  /**
   * \brief Run synchronized callbacks for a list of iterators
   *
   * This method has a list of callbacks for a list of iterators.
   * Those callbacks are called for a synchronized data set, but in parallel.
   * They are called from an internal worker thread.
   * As the data set is synchronized, this creates a bottleneck for one
   * worker thread, so only fast and non-blocking callbacks are allowed.
   *
   * \param callbacks Map of callbacks per iterator
   * \param block Shall this method block until all callbacks are finished
   *
   */
  virtual void runSynchronized(const IteratorCallbackMap& callbacks, bool block = true) = 0;

private:
  // Used by _Iterator to add itself
  virtual _Iter *addIterator(_Iterator *it) = 0;

  // allocate a new virtual output channel
  virtual channel_t getNewVirtualChannel() = 0;
};


/**
 * \brief a single event on a channel
 *
 * Channel events are passed from the backend to registered iterators
 * by the _Iterator::next() callback function.
 *
 * A Tag describes a single event on a channel.
 */
struct Tag {
  /// when set, there was an overflow on the communication channel.
  bool overflow;
  /// the channel number
  channel_t channel;
  /// the timestamp on the event, in picoseconds
  timestamp_t time;
};

/**
 * \brief Base class for all iterators
 *
 *
 */
class TT_API _Iterator {
  friend class _TimeTagger;
private:
  // Abstract class
  _Iterator() = delete;

  // Non Copyable
  _Iterator(const _Iterator&) = delete;
  _Iterator& operator=(const _Iterator&) = delete;
  void clearWithoutLock();

protected:
  /**
   * \brief standard constructor
   *
   * will register with the TimeTagger backend.
   */
  _Iterator(TimeTagger* tagger);

public:
  /**
   * \brief destructor
   *
   * will stop and unregister prior finalization.
   */
  virtual ~_Iterator();

  /**
   * \brief start the iterator
   *
   * The default behavior for iterators is to start automatically on creation.
   */
  virtual void start();

  /**
   * \brief start the iterator, and stops it after the capture_duration
   * \param capture_duration capture duration until the meassurement is stopped
   * \param clear resets the data aquired
   *
   * When the startFor is called before the previous measurement has ended and the clear 
   * parameter is set to false, then the passed capture_duration will be added on top to the current max_capture_duration
   */
  void startFor(timestamp_t capture_duration, bool clear = true);

  /**
   * \brief stop the iterator
   *
   * The iterator is put into the STOPPED state, but will still be registered with the backend.
   */
  virtual void stop();

  /**
   * \brief clear Iterator state.
   */
  void clear();

  /**
   * \brief query the Iterator state.
   *
   * Fetches if this iterator is running.
   */
  bool isRunning();

  /**
   * \brief query the evaluation time
   *
   * Query the total capture duration since the last call to clear.
   * This might have a wrong amount of time if there were some overflows within this range.
   * \return capture duration of the data
   */
  timestamp_t getCaptureDuration();

protected:
  /**
   * \brief register a channel
   *
   * Only channels registered by any iterator attached to a backend are delivered over the usb.
   *
   * \param channel  the channel
   */
  void registerChannel(channel_t channel);

  /**
   * \brief unregister a channel
   *
   * \param channel  the channel
   */
  void unregisterChannel(channel_t channel);

  /**
   * \brief allocate a new virtual output channel for this iterator
   */
  channel_t getNewVirtualChannel();

   /**
   * \brief clear Iterator state.
   *
   * Each Iterator should implement the clear_impl() method to reset
   * its internal state.
   * The clear_impl() function is guarded by the update lock.
   */
  virtual void clear_impl() {};

  /**
   * \brief callback when the measurement class is started
   *
   * This function is guarded by the update lock.
   */
  virtual void on_start() {};

  /**
   * \brief callback when the measurement class is stopped
   *
   * This function is guarded by the update lock.
   */
  virtual void on_stop() {};

  /**
   * \brief aquire update lock
   *
   * All mutable operations on a iterator are guarded with an update mutex.
   * Implementers are adviced to lock() an iterator, whenever internal state
   * is queried or changed.
   */
  void lock();

  /**
   * \brief release update lock
   *
   * see lock()
   */
  void unlock();

  /**
   * \brief update iterator state
   *
   * Each Iterator must implement the next_impl() method.
   * The next_impl() function is guarded by the update lock.
   *
   * The backend delivers each Tag on each registered channel
   * to this callback function.
   *
   * \param incoming_tags block of events
   * \param begin_time earliest event in the block
   * \param end_time begin_time of the next block, not including in this block
   * \return if the content of this block was modified
   */
  virtual bool next_impl(std::vector<Tag>& incoming_tags, timestamp_t begin_time, timestamp_t end_time) = 0;

  /// list of channels used by the iterator
  std::set<channel_t> channels_registered;

  /// running state of the iterator
  bool running;

  TimeTagger* tagger;

  timestamp_t capture_duration; // duration the iterator has already captured data

private:
  void next(std::vector<Tag>& incoming_tags, timestamp_t begin_time, timestamp_t end_time);

  _Iter *iter;
  timestamp_t max_capture_duration; // capture duration at which the .stop() method will be called, <0 for infinity
};

#endif /* TIMETAGGER_H_ */
