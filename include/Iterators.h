/*
This file is part of Time Tagger software defined digital data acquisition.

Copyright (C) 2011-2019 Swabian Instruments
All Rights Reserved

Unauthorized copying of this file is strictly prohibited.
*/

#ifdef LIBTIMETAGGER_EXPORTS
#define TT_API __declspec(dllexport) 
#pragma warning (disable : 4251)
#else
#if defined(__linux) || defined(SWIG) || defined(NOEXPORT)
#define TT_API
#else
#define TT_API __declspec(dllimport) 
#pragma warning (disable : 4251)
#endif
#endif

#ifndef TT_ITERATORS_H_
#define TT_ITERATORS_H_

#include <queue>
#include <vector>
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdint.h>
#include <map>
#include <deque>
#include <set>
#include <list>
#include <mutex>
#include <array>

#include "TimeTagger.h"

/**
 * \defgroup ITERATOR base iterators
 * \brief base iterators for photon counting applications
 */

/**
 * \ingroup ITERATOR
 *
 * \brief Combine some channels in a virtual channel which has a tick for each tick in the input channels
 *
 * This iterator can be used to get aggregation channels, eg if you want to monitor the countrate
 * of the sum of two channels.
 */
class TT_API Combiner : public _Iterator {
public:
  /**
   * \brief construct a combiner
   *
   * \param tagger    reference to a TimeTagger
   * \param channels    vector of channels to combine
   */
  Combiner(TimeTagger* tagger, std::vector<channel_t> channels);

  ~Combiner();

  /**
   * \brief get sum of counts
   *
   * For reference, this iterators sums up how much ticks are generated because of which input channel.
   * So this functions returns an array with one value per input channel.
   *
   */
  GET_DATA_1D(getData, long long, array_out,);

  /**
   * \brief the new virtual channel
   *
   * This function returns the new allocated virtual channel.
   * It can be used now in any new iterator.
   *
   */
  channel_t getChannel();

protected:
  bool next_impl(std::vector<Tag>& incoming_tags, timestamp_t begin_time, timestamp_t end_time) override;
  void clear_impl() override;

private:
  const std::vector<channel_t> channels;
  std::vector<long long> data;
  channel_t chan_out;

  std::vector<Tag> mirror;
};

/**
 * \ingroup ITERATOR
 *
 * \brief a combiner which calculates the mean value of all monitored channel
 *
 * This iterator is a combiner which triggers a virtual event when every selected
 * channel was triggered within the max_delay interval. The mean value of all
 * input timestamps will be used for the virtual event.
 * So this function can be used to average some channels to get a more accurate result.
 *
 * This feature is only supported on the Time Tagger Ultra 18.
 */
class TT_API AverageChannel : public _Iterator {
public:
  /**
   * \brief constructs a new averaging combiner
   *
   * \param tagger reference to a TimeTagger
   * \param input_channel the physical input channel
   * \param combined_channels the list of inputs for averaging the event, the input_channel will be muxed internally to the combined_channels
   */
  AverageChannel(TimeTagger* tagger, channel_t input_channel, std::vector<channel_t> combined_channels);
  ~AverageChannel();
  channel_t getChannel();

protected:
  bool next_impl(std::vector<Tag>& incoming_tags, timestamp_t begin_time, timestamp_t end_time) override;
  void clear_impl() override;

private:
  void flushQueue(timestamp_t time);

  timestamp_t max_delay;
  channel_t chan_out;
  std::vector<channel_t> combined_channels;
  std::vector<timestamp_t> events;
  std::vector<Tag> buffer;
  std::vector<Tag> mirror;

  class TagCompare {
  public:
    bool operator()(const Tag& a, const Tag& b) const {
      return a.time - b.time > 0; // Note: this isn't the same as a>b because of the s64 overflow
    }
  };
  std::priority_queue<Tag, std::vector<Tag>, TagCompare> delayed_buffer;
};

/**
 * \ingroup ITERATOR
 *
 * \brief a simple counter where external marker signals determine the bins
 *
 * Counter with external signals that trigger beginning and end of each counter
 * accumulation. This can be used to implement counting triggered by a pixel
 * clock and gated counting. The thread waits for the first time tag on the
 * 'begin_channel', then begins counting time tags on the 'click_channel'.
 * It ends counting when a tag on the 'end_chanel' is detected.
 *
 */
class TT_API CountBetweenMarkers : public _Iterator {
public:
  /**
   * \brief constructor of CountBetweenMarkers
   *
   *
   * @param tagger         reference to a TimeTagger
   * @param click_channel  channel that increases the count
   * @param begin_channel  channel that triggers beginning of counting and stepping to the next value
   * @param end_channel    channel that triggers end of counting
   * @param n_values       the number of counter values to be stored
   */
  CountBetweenMarkers(TimeTagger* tagger, channel_t click_channel, channel_t begin_channel, channel_t end_channel = CHANNEL_UNUSED, int n_values = 1000);

  ~CountBetweenMarkers();

  /**
   * \brief tbd
   *
   */
  bool ready();

  /**
   * \brief tbd
   */
  GET_DATA_1D(getData, int, array_out,);

  /**
   * \brief fetches the widths of each bins
   */
  GET_DATA_1D(getBinWidths, timestamp_t, array_out,);

  /**
   * \brief fetches the starting time of each bin
   */
  GET_DATA_1D(getIndex, timestamp_t, array_out,);

protected:
  bool next_impl(std::vector<Tag>& incoming_tags, timestamp_t begin_time, timestamp_t end_time) override;
  void clear_impl() override;

private:
  std::vector<int> data;
  std::vector<timestamp_t> binwidths;
  std::vector<timestamp_t> begin_clicks;

  channel_t click_channel;
  channel_t begin_channel;
  channel_t end_channel;
  int n_values;

  int state;
  bool stopped;
  timestamp_t start_time;
  timestamp_t first_start_time;
};


/**
 * \ingroup ITERATOR
 *
 * \brief a simple counter on one or more channels
 *
 * Counter with fixed binwidth and circular buffer output. This class
 * is suitable to generate a time trace of the count rate on one or more channels.
 * The thread repeatedly counts clicks on a single channel over a
 * given time interval and stores the results in a two-dimensional array.
 * The array is treated as a circular buffer. I.e., once the array is
 * full, each new value shifts all previous values one element to the left.
 *
 */
class TT_API Counter : public _Iterator {
public:
  /**
   * \brief construct a counter
   *
   * \param tagger       reference to a TimeTagger
   * \param channels     channels to count on
   * \param binwidth     counts are accumulated for binwidth picoseconds
   * \param n_values     number of counter values stored (for each channel)
   */
  Counter(TimeTagger* tagger, std::vector<channel_t> channels, timestamp_t binwidth = 1000000000, int n_values = 1);

  ~Counter();

  /**
   * \brief get counts
   *
   * the counts are copied to a newly allocated allocated memory, an the
   * pointer to this location is returned.
   *
   */
  GET_DATA_2D(getData, int, array_out,);

  GET_DATA_1D(getIndex, timestamp_t, array_out,);

protected:
  bool next_impl(std::vector<Tag>& incoming_tags, timestamp_t begin_time, timestamp_t end_time) override;
  void clear_impl() override;
  void on_start() override;

private:
  std::vector<int> data;

  const std::vector<channel_t> channels;

  timestamp_t binwidth;

  int n_values;

  int pos;

  timestamp_t counter_start_time;

  void setBinPosition(timestamp_t time);
};

/**
* \ingroup ITERATOR
*
* \brief a coincidence monitor for one or more channel groups
*
* Monitor coincidences for given coincidence groups passed by the constructor.
* A coincidence is hereby defined as for a given coincidence group
* a) the incoming is part of this group
* b) at least tag arrived within the coincidenceWindow [ps] for all other channels of this coincidence group
* Each coincidence will create a virtual event.
* The block of event IDs for those coincidence group can be fetched.
*/
class TT_API Coincidences : public _Iterator {
public:
  /**
  * \brief construct a Coincidences
  *
  * \param tagger		            reference to a TimeTagger
  * \param coincidenceGroups    a vector of channels defining the coincidences
  * \param coincidenceWindow    the size of the coincidence window in picoseconds
  */
  Coincidences(TimeTagger * tagger, std::vector<std::vector<channel_t>> coincidenceGroups, timestamp_t coincidenceWindow);

  ~Coincidences();

  /**
   * \brief fetches the block of virtual channels for those coincidence groups
   */
  std::vector<channel_t> getChannels();
  
  void setCoincidenceWindow(timestamp_t coincidenceWindow);

protected:
  bool next_impl(std::vector<Tag>& incoming_tags, timestamp_t begin_time, timestamp_t end_time) override;

private:
  std::vector<Tag> mirror; // Tag mirror for injecting new tags

  struct ChannelData {
    timestamp_t edge; // for every input channel one
    int index; // mapping of channels (don't use the real channel number) - see later for details
    std::set<size_t> updateCoincidence; // pre calculated block of which coincidences have to be tested when a certain tag arrives
  };
  std::map<channel_t, ChannelData> channelMap;

  struct CoincidenceData {
    uint64_t mask; // pre calculated binary representation of the coincidences
    channel_t output_channel; // for the output events
  };
  std::vector<CoincidenceData> coincidenceVector;

  timestamp_t coincidenceWindow; // in ps
};

/**
 * \ingroup ITERATOR
 *
 * \brief a coincidence monitor for one or more channel groups
 *
 * Monitor coincidences for a given channel groups passed by the constructor.
 * A coincidence is event is detected when all slected channels have a click 
 * within the given coincidenceWindow [ps]
 * The coincidence will create a virtual events on a virtual channel with
 * the channel number provided by getChannel().
 * For multiple coincidence channel combinations use the class Coincidences
 * which outperformes multiple instances of Conincdence.
 */
class TT_API Coincidence : public Coincidences {
public:
  /**
  * \brief construct a coincidence
  *
  * \param tagger    reference to a TimeTagger
  * \param channels    vector of channels to match
  * \param coincidenceWindow    max distance between all clicks for a coincidence [ps]
  */
  Coincidence(TimeTagger* tagger, std::vector<channel_t> channels, timestamp_t coincidenceWindow = 1000)
  : Coincidences(tagger, {channels}, coincidenceWindow) {}

  /**
  * \brief virtual channel which contains the coincidences
  */
  channel_t getChannel() {
    return getChannels()[0];
  }
};

/**
 * \ingroup ITERATOR
 * \brief count rate on one or more channels
 *
 * Measures the average count rate on one or more channels. Specifically, it counts
 * incoming clicks and determines the time between the initial click and the latest click.
 * The number of clicks divided by the time corresponds to the average countrate
 * since the initial click.
 *
 */
class TT_API Countrate : public _Iterator {
public:

  /**
   * \brief constructor of Countrate
   *
   * @param tagger    reference to a TimeTagger
   * @param channels    the channels to count on
   */
  Countrate(TimeTagger* tagger, std::vector<channel_t> channels);

  ~Countrate();

  /**
   * \brief get the count rates
   *
   * Returns the average rate of events per second per channel as an array.
   */
  GET_DATA_1D(getData, double, array_out,);

  /**
   * \brief get the total amount of events
   *
   * Returns the total amount of events per channel as an array.
   */
  GET_DATA_1D(getCountsTotal, long long, array_out,);

protected:
  bool next_impl(std::vector<Tag>& incoming_tags, timestamp_t begin_time, timestamp_t end_time) override;
  void clear_impl() override;
  void on_start() override;

private:
  const std::vector<channel_t> channels;

  timestamp_t now;
  timestamp_t startzeit;
  std::vector<long long> events;       // number of events counted from the last clear/overflow
  std::vector<long long> events_total; // number of events counted from the last clear
};

/**
 * \ingroup ITERATOR
 *
 * \brief a simple delayed queue
 *
 * A simple first-in first-out queue of delayed event timestamps.
 */
class TT_API DelayedChannel : public _Iterator {
public:

  /**
  * \brief constructor of a DelayedChannel
  *
  * \param tagger                reference to a TimeTagger
  * \param input_channel         channel which is delayed
  * \param delay                 amount of time to delay, must be positive
  */
  DelayedChannel(TimeTagger* tagger, channel_t input_channel, unsigned long long delay);

  ~DelayedChannel();

  /**
   * \brief the new virtual channel
   *
   * This function returns the new allocated virtual channel.
   * It can be used now in any new iterator.
   *
   */
  channel_t getChannel();

protected:
  bool next_impl(std::vector<Tag>& incoming_tags, timestamp_t begin_time, timestamp_t end_time) override;
  void on_start() override;

private:
  std::vector<Tag> modifiedTagStream; // Tag mirror for injecting new tags

  std::queue<Tag> delayedTags;
  const channel_t input_channel;
  const channel_t output_channel;
  timestamp_t delay;
public:
  /**
  * \brief set the delay time delay for the cloned tags in the virtual channel 0 <= t < 2^63
  *
  * Note: When the delay is the same or greater than the previous value all incoming tags will be visible at virtual channel.
  * By applying a shorter delay time, the tags stored in the local buffer will be flushed and won't be visible in the virtual channel.
  */
  void setDelay(unsigned long long delay);
};

/**
* \ingroup ITERATOR
*
* \brief An input channel is gated by a gate channel.
*
* Note: The gate is edge sensitive and not level sensitive. That means that the gate will transfer data only when an appropriate level change is detected on the gate_start_channel.
*/
class TT_API GatedChannel : public _Iterator {
public:

  /**
  * \brief constructor of a GatedChannel
  *
  * \param tagger                reference to a TimeTagger
  * \param input_channel         channel which is gated
  * \param gate_start_channel    channel on which a signal detected will start the transmission of the input_channel through the gate
  * \param gate_stop_channel     channel on which a signal detected will stop the transmission of the input_channel through the gate
  */
  GatedChannel(TimeTagger* tagger, channel_t input_channel, channel_t gate_start_channel, channel_t gate_stop_channel);

  ~GatedChannel();

  /**
  * \brief the new virtual channel
  *
  * This function returns the new allocated virtual channel.
  * It can be used now in any new iterator.
  *
  */
  channel_t getChannel();

protected:
  bool next_impl(std::vector<Tag>& incoming_tags, timestamp_t begin_time, timestamp_t end_time) override;

private:
  std::vector<Tag> modifiedTagStream; // Tag mirror for injecting new tags

  const channel_t input_channel;
  const channel_t gate_start_channel;
  const channel_t gate_stop_channel;
  const channel_t output_channel;
  
  bool transferTags = false;
};

/**
* \ingroup ITERATOR
*
* \brief The signal of an input channel is scaled up to a higher frequency according to the multiplier passed as a parameter.
*
* The FrequencyMultiplier inserts copies the original input events from the input_channel and adds additional events to match the upscaling factor. 
* The algorithm used assumes a constant frequency and calculates out of the last two incoming events linearly the intermediate timestamps to match the upscaled frequency given by the multiplier parameter.
*
* The FrequencyMultiplier can be used to restore the actual frequency applied to an input_channel which was reduces via the EventDivider to lower the effective data rate. 
* For example a 80 MHz laser sync signal can be scaled down via setEventDivider(..., 80) to 1 MHz (hardware side) and an 80 MHz signal can be restored via FrequencyMultiplier(..., 80) on the software side with some loss in precision.
* The FrequencyMultiplier is an alternative way to reduce the data rate in comparison to the EventFilter, which has a higher precision but can be more difficult to use.
*/
class TT_API FrequencyMultiplier : public _Iterator {
public:
  /**
  * \brief constructor of a FrequencyMultiplier
  *
  * \param tagger                reference to a TimeTagger
  * \param input_channel         channel on which the upscaling of the frequency is based on
  * \param multiplier            frequency upscaling factor
  */
  FrequencyMultiplier(TimeTagger* tagger, channel_t input_channel, int multiplier);

  ~FrequencyMultiplier();

  channel_t getChannel();
  int getMultiplier();

protected:
  bool next_impl(std::vector<Tag>& incoming_tags, timestamp_t begin_time, timestamp_t end_time) override;

private:
  std::vector<Tag> modified_tag_stream; // Tag mirror for injecting new tags

  timestamp_t getNextExtrapolatedTag();
  void insertExtrapolatedTag();

  const channel_t input_channel;
  const channel_t output_channel;
  const int multiplier;
  timestamp_t last_input_event;
  timestamp_t second_last_input_event;
  timestamp_t next_extrapolated_tag_time;
  int extrapolated_couter;
  void reset();
};

/**
 * \ingroup ITERATOR
 *
 * \brief a simple event queue
 *
 * A simple Iterator, just keeping a first-in first-out queue of event timestamps.
 *
 * @deprecated use TimeTagStream
 */
class TT_API Iterator : public _Iterator {
public:
  /**
   * \brief standard constructor
   *
   * @param tagger        the backend
   * @param channel       the channel to get events from
   */
  Iterator(TimeTagger* tagger, channel_t channel);

  ~Iterator();

  /**
   * \brief get next timestamp
   *
   * get the next timestamp from the queue.
   */
  timestamp_t next();

  /**
   * \brief get queue size
   */
  size_t size();

protected:
  bool next_impl(std::vector<Tag>& incoming_tags, timestamp_t begin_time, timestamp_t end_time) override;
  void clear_impl() override;

private:
  std::queue<timestamp_t> q;
  channel_t channel;
};

class TimeTagStream;
class TT_API TimeTagStreamBuffer {
  friend class TimeTagStream;
public:
  GET_DATA_1D(getOverflows, unsigned char, array_out, );
  GET_DATA_1D(getChannels, channel_t, array_out, );
  GET_DATA_1D(getTimestamps, timestamp_t, array_out, );

  size_t size;
  bool hasOverflows;
  timestamp_t tStart;
  timestamp_t tGetData;

private:
  std::vector<unsigned char> tagOverflows;
  std::vector<channel_t> tagChannels;
  std::vector<timestamp_t> tagTimestamps;
};

/**
* \ingroup ITERATOR
* \brief access the time tag stream
*/
class TT_API TimeTagStream : public _Iterator {
public:
  /**
  * \brief constructor of a TimeTagStream thread
  *
  * Gives access to the time tag stream
  *
  * @param tagger      reference to a TimeTagger
  * @param n_max_events    maximum number of tags stored
  * @param channels      channels which are dumped to the file (when empty or not passed all active channels are dumped)
  */
  TimeTagStream(TimeTagger* tagger, int n_max_events, std::vector<channel_t> channels = {});

  /**
  * \brief tbd
  *
  */
  ~TimeTagStream();

  /**
  * \brief get incoming time tags 
  *
  * All incoming time tags are stored in a buffer (max size: max_tags). The buffer is cleared after retrieving the data with getData()
  *
  */

  /**
  * \brief return the number of stored tags
  *
  */
  size_t getCounts();

  /**
  * \brief fetches all stored tags and clears the internal state
  */
  TimeTagStreamBuffer getData();

protected:
  bool next_impl(std::vector<Tag>& incoming_tags, timestamp_t begin_time, timestamp_t end_time) override;
  void clear_impl() override;

private:
  const std::vector<channel_t> channels;
  int n_max_tags;
  timestamp_t currentTime;
  bool hasOverflows;
  timestamp_t tStart;
  std::vector<unsigned char> tagOverflows;
  std::vector<channel_t> tagChannels;
  std::vector<timestamp_t> tagTimestamps;
};

/**
 * \ingroup ITERATOR
 * \brief dump all time tags to a file
 *
 */
class TT_API Dump : public _Iterator {
public:
  /**
   * \brief constructor of a Dump thread
   *
   * @param tagger        reference to a TimeTagger
   * @param filename      name of the file to dump to
   * @param max_tags      stop after this number of tags has been dumped
   * @param channels      channels which are dumped to the file (when empty or not passed all active channels are dumped)
   */
  Dump(TimeTagger* tagger, std::string filename, size_t max_tags, std::vector<channel_t> channels = {});

  /**
   * \brief tbd
   *
   */
  ~Dump();

protected:
  bool next_impl(std::vector<Tag>& incoming_tags, timestamp_t begin_time, timestamp_t end_time) override;
  void clear_impl() override;
  void on_start() override;
  void on_stop() override;

private:
  std::ofstream output_file;
  std::string filename;
  size_t count, max_tags;
  const std::vector<channel_t> channels;
};

/**
 * @ingroup ITERATOR
 *
 * \brief simple start-stop measurement
 *
 * This class performs a start-stop measurement between two channels
 * and stores the time differences in a histogram. The histogram resolution
 * is specified beforehand (binwidth) but the histogram range is unlimited.
 * It is adapted to the largest time difference that was detected. Thus
 * all pairs of subsequent clicks are registered.
 *
 * Be aware, on long-running measurements this may considerably slow down
 * system performance and even crash the system entirely when attached to an
 * unsuitable signal source.
 *
 */
class TT_API StartStop : public _Iterator {
public:

  /**
   * \brief constructor of StartStop
   *
   * @param tagger                reference to a TimeTagger
   * @param click_channel         channel for stop clicks
   * @param start_channel         channel for start clicks
   * @param binwidth              width of one histogram bin in ps
   */
  StartStop(TimeTagger* tagger, channel_t click_channel, channel_t start_channel=CHANNEL_UNUSED, timestamp_t binwidth=1000);

  ~StartStop();

  GET_DATA_2D(getData, timestamp_t, array_out,);

protected:
  bool next_impl(std::vector<Tag>& incoming_tags, timestamp_t begin_time, timestamp_t end_time) override;
  void clear_impl() override;
  void on_start() override;

private:
  std::map<timestamp_t,size_t> data;
  channel_t click_channel;
  channel_t start_channel;
  timestamp_t binwidth;
  timestamp_t last;
};


/**
 * @ingroup ITERATOR
 * \brief Accumulates the time differences between clicks on two channels in one or more histograms.
 *
 * A multidimensional histogram measurement with the option up to include three additional channels
 * that control how to step through the indices of the histogram array. This is a very powerful and
 * generic measurement. You can use it to record cross-correlation, lifetime measurements, fluorescence
 * lifetime imaging and many more measurements based on pulsed excitation. Specifically, the measurement
 * waits for a tag on the ‘start_channel’, then measures the time difference between the start tag and
 * all subsequent tags on the ‘click_channel’ and stores them in a histogram. If no ‘start_channel’ is
 * specified, the ‘click_channel’ is used as ‘start_channel’ corresponding to an auto-correlation
 * measurement. The histogram has a number ‘n_bins’ of bins of bin width ‘binwidth’. Clicks that fall
 * outside the histogram range are discarded. Data accumulation is performed independently for all start
 * tags. This type of measurement is frequently referred to as ‘multiple start, multiple stop’
 * measurement and corresponds to a full auto- or cross-correlation measurement.
 *
 * The data obtained from subsequent start tags can be accumulated into the same histogram (one-
 * dimensional measurement) or into different histograms (two-dimensional measurement). In this way, you
 * can perform more general two-dimensional time-difference measurements. The parameter ‘n_histograms’
 * specifies the number of histograms. After each tag on the ‘next_channel’, the histogram index is
 * incremented by one and reset to zero after reaching the last valid index. The measurement starts with
 * the first tag on the ‘next_channel’.
 *
 * You can also provide a synchronization trigger that resets the histogram index by specifying a
 * ‘sync_channel’. The measurement starts when a tag on the ‘sync_channel’ arrives with a subsequent tag
 * on ‘next_channel’. When a rollover occurs, the accumulation is stopped until the next sync and
 * subsequent next signal. A sync signal before a rollover will stop the accumulation, reset the
 * histogram index and a subsequent signal on the ‘next_channel’ starts the accumulation again.
 *
 * Typically, you will run the measurement indefinitely until stopped by the user. However, it is also
 * possible to specify the maximum number of rollovers of the histogram index. In this case the
 * measurement stops when the number of rollovers has reached the specified value. This means that for
 * both a one-dimensional and for a two-dimensional measurement, it will measure until the measurement
 * went through the specified number of rollovers / sync tags.
 */
class TT_API TimeDifferences : public _Iterator {
public:
  /**
  * \brief constructor of a TimeDifferences measurement
  *
  *
  * \param tagger                reference to a TimeTagger
  * \param click_channel         channel that increments the count in a bin
  * \param start_channel         channel that sets start times relative to which clicks on the click channel are measured
  * \param next_channel          channel that increments the histogram index
  * \param sync_channel          channel that resets the histogram index to zero
  * \param binwidth              width of one histogram bin in ps
  * \param n_bins                number of bins in each histogram
  * \param n_histograms          number of histograms
  */
  TimeDifferences(TimeTagger* tagger, channel_t click_channel, channel_t start_channel=CHANNEL_UNUSED, channel_t next_channel=CHANNEL_UNUSED, channel_t sync_channel=CHANNEL_UNUSED, timestamp_t binwidth=1000, int n_bins=1000, int n_histograms=1);

  ~TimeDifferences();

  /**
  * \brief returns a two-dimensional array of size ‘n_bins’ by ‘n_histograms’ containing the histograms
  */
  GET_DATA_2D(getData, int, array_out,);

  /**
  * \brief returns a vector of size ‘n_bins’ containing the time bins in ps
  */
  GET_DATA_1D(getIndex, timestamp_t, array_out,);

  /**
  * \brief set the number of rollovers at which the measurement stops integrating
  *
  * \param max_counts             maximum number of sync/next clicks
  */
  void setMaxCounts(uint64_t max_counts);

  /**
  * \brief returns the number of rollovers (histogram index resets)
  */
  uint64_t getCounts();

  /**
  * \brief returns ‘true’ when the required number of rollovers set by ‘setMaxCounts’ has been reached
  */
  bool ready();

protected:
  bool next_impl(std::vector<Tag>& incoming_tags, timestamp_t begin_time, timestamp_t end_time) override;
  void clear_impl() override;
  void on_start() override;

private:
  template<typename TimeDelta, bool binning> void process_tags(const std::vector<Tag>& incoming_tags);

  struct _PulsedEdge {
    timestamp_t edge;
    int histogram;
  };

  channel_t click_channel, start_channel, next_channel, sync_channel;
  bool next_channel_unused, sync_channel_unused;
  int n_bins, n_histograms;
  timestamp_t binwidth;

  std::vector<int> data;

  std::deque<_PulsedEdge> edge;

  bool waiting_for_sync;
  int current_histogram;
  uint64_t counts, max_counts;
};

/**
 * @ingroup ITERATOR
 * \brief Accumulates the time differences between clicks on two channels in a multi-dimensional histogram.
 *
 * This is a multidimensional implementation of the TimeDifferences measurement class. Please read their documentation first.
 *
 * This measurement class extends the TimeDifferences interface for a multidimensional amount of histograms.
 * It captures many multiple start - multiple stop histograms, but with many asynchronous next_channel triggers.
 * After each tag on each next_channel, the histogram index of the associated dimension is
 * incremented by one and reset to zero after reaching the last valid index.  The elements of the parameter n_histograms
 * specifies the number of histograms per dimension. The accumulation starts when next_channel has been triggered on all dimensions.
 *
 * You should provide a synchronization trigger by specifying a sync_channel per dimension.
 * It will stop the accumulation when an associated histogram index rollover occurs.
 * A sync event will also stop the accumulation, reset the histogram index of the associated dimension,
 * and a subsequent event on the corresponding next_channel starts the accumulation again.
 * The synchronization is done asynchronous, so an event on the next_channel increases the histogram index even if the accumulation is stopped.
 * The accumulation starts when a tag on the sync_channel arrives with a subsequent tag on next_channel for all dimensions.
 *
 * Please use setInputDelay to adjust the latency of all channels. In general, the order of the provided triggers including maximum jitter should be:
 * old start trigger -- all sync triggers -- all next triggers -- new start trigger
 */
class TT_API TimeDifferencesND : public _Iterator {
public:
  /**
  * \brief constructor of a TimeDifferencesND measurement
  *
  *
  * \param tagger                reference to a TimeTagger
  * \param click_channel         channel that increments the count in a bin
  * \param start_channel         channel that sets start times relative to which clicks on the click channel are measured
  * \param next_channels         vector of channels that increments the histogram index
  * \param sync_channels         vector of channels that resets the histogram index to zero
  * \param n_histograms          vector of numbers of histograms per dimension
  * \param binwidth              width of one histogram bin in ps
  * \param n_bins                number of bins in each histogram
  */
  TimeDifferencesND(TimeTagger* tagger, channel_t click_channel, channel_t start_channel, std::vector<channel_t> next_channels, std::vector<channel_t> sync_channels, std::vector<int> n_histograms, timestamp_t binwidth, size_t n_bins);

  ~TimeDifferencesND();

  /**
  * \brief returns a two-dimensional array of size n_bins by all n_histograms containing the histograms
  */
  GET_DATA_2D(getData, int, array_out,);

  /**
  * \brief returns a vector of size n_bins containing the time bins in ps
  */
  GET_DATA_1D(getIndex, timestamp_t, array_out,);

protected:
  bool next_impl(std::vector<Tag>& incoming_tags, timestamp_t begin_time, timestamp_t end_time) override;
  void clear_impl() override;
  void on_start() override;

private:
  struct PulsedEdge {
    timestamp_t edge;
    size_t current_position;
  };

  const channel_t click_channel, start_channel;
  const std::vector<channel_t> next_channels, sync_channels;
  std::vector<char> sync_channel_unused;
  const std::vector<int> n_histograms;
  const timestamp_t binwidth;
  const size_t n_bins;
  const size_t dim;

  std::deque<PulsedEdge> edge;

  std::vector<int> data;

  std::vector<char> waiting_for_sync;
  std::vector<int> current_histogram;
  size_t current_histogram_position;
  bool current_histogram_valid;
};

/**
 * @ingroup ITERATOR
 *
 * \brief Accumulate time differences into a histogram
 *
 * This is a simple multiple start, multiple stop measurement. This is a special
 * case of the more general 'TimeDifferences' measurement.
 *    Specifically, the thread waits for clicks on a first channel, the 'start channel',
 * then measures the time difference between the last start click and all subsequent
 * clicks on a second channel, the 'click channel', and stores them in a histogram.
 * The histogram range and resolution is specified by the number of bins and the binwidth.
 * Clicks that fall outside the histogram range are ignored.
 * Data accumulation is performed independently for all start clicks. This type of measurement
 * is frequently referred to as 'multiple start, multiple stop' measurement and corresponds to a
 * full auto- or cross-correlation measurement.
 */
class TT_API Histogram {
public:
  /**
  * \brief constructor of a Histogram measurement
  *
  *
  * \param tagger                reference to a TimeTagger
  * \param click_channel         channel that increments the count in a bin
  * \param start_channel         channel that sets start times relative to which clicks on the click channel are measured
  * \param binwidth              width of one histogram bin in ps
  * \param n_bins                number of bins in the histogram
  */
  Histogram(TimeTagger* tagger, channel_t click_channel, channel_t start_channel = CHANNEL_UNUSED, timestamp_t binwidth = 1000, int n_bins = 1000);

  void start();
  void startFor(timestamp_t capture_duration, bool clear = true);
  void stop();
  void clear();
  timestamp_t getCaptureDuration();

  bool isRunning();

  GET_DATA_1D(getData, int, array_out,);

  GET_DATA_1D(getIndex, timestamp_t, array_out,);

private:
  TimeDifferences td;
};

/**
* @ingroup ITERATOR
*
* \brief Accumulate time differences into a histogram with logarithmic increasing bin sizes
*
* This is a multiple start, multiple stop measurement, and works the very same way as the histogram measurement but with logarithmic 
* increasing bin widths.
* After initializing the measurement (or after an overflow) no data is accumulated in the histogram until the full histogram duration has
* passed to ensure a balanced count accumulation over the full histogram.
* 
*/
class TT_API HistogramLogBins : public _Iterator {
public:
	/**
	* \brief constructor of a HistogramLogBins measurement
	*
	* \param tagger                reference to a TimeTagger
	* \param click_channel         channel that increments the count in a bin
	* \param start_channel         channel that sets start times relative to which clicks on the click channel are measured
	* \param exp_start             exponent for the lowest time diffrences in the histogram: 10^exp_start s, lowest exp_start: -12 => 1ps
	* \param exp_stop              exponent for the highest time diffrences in the histogram: 10^exp_stop s
	* \param n_bins                total number of bins in the histogram
	*/
	HistogramLogBins(TimeTagger* tagger, channel_t click_channel, channel_t start_channel, double exp_start, double exp_stop, int n_bins);
	~HistogramLogBins();

	// returns the absolute counts for the bins
	GET_DATA_1D(getData, unsigned long long, array_out, );

	// returns the counts normalized by the binwidth of each bin
	GET_DATA_1D(getDataNormalized, double, array_out, );

	// resturns the edges of the bins in ps
	GET_DATA_1D(getBinEdges, timestamp_t, array_out, );

protected:
	bool next_impl(std::vector<Tag>& incoming_tags, timestamp_t begin_time, timestamp_t end_time) override;
	void clear_impl() override;

private:
	timestamp_t start_time;

	channel_t click_channel, start_channel;

	void _clear(bool clearData);
		
	// histogram data
	std::vector<unsigned long long> data;

	// queue for the click tags
	std::deque<timestamp_t> tag_time_queue;

	// binedges (size: data + 1)
	std::vector<timestamp_t> binedges;

	// inserted_events counts how many events have been inserted in total to the tag_time_queue 
	// the value is not decreased if something is removed out of the queue
	long long inserted_events = 0;

	// binedge_tagindex stores the pointer (binedge_tagindex[n] + inserted_events) for n'th bin. The tag adressed is the most recent tag arrived which is still in the n'th bin
	std::vector<long long> binedge_tagindex;
};


/**
 * @ingroup ITERATOR
 *
 * \brief Fluorescence lifetime imaging
 *
 * Successively acquires n histograms (one for each pixel in the image), where
 * each histogram is determined by the number of bins and the binwidth.
 * Clicks that fall outside the histogram range are ignored.
 *
 * Fluorescence-lifetime imaging microscopy or FLIM is an imaging technique for producing an image based on
 * the differences in the exponential decay rate of the fluorescence from a fluorescent sample.
 *
 * Fluorescence lifetimes can be determined in the time domain by using a pulsed source. When a population
 * of fluorophores is excited by an ultrashort or delta pulse of light, the time-resolved fluorescence will
 * decay exponentially.
 *
 */
class TT_API Flim {
public:
  /**
  * \brief constructor of a FLIM measurement
  *
  *
  * \param tagger                reference to a TimeTagger
  * \param click_channel         channel that increments the count in a bin
  * \param start_channel         channel that sets start times relative to which clicks on the click channel are measured
  * \param next_channel          channel that increments the pixel
  * \param binwidth              width of one histogram bin in ps
  * \param n_bins                number of bins in each histogram
  * \param n_pixels              number of pixels
  */
  Flim(TimeTagger* tagger, channel_t click_channel, channel_t start_channel, channel_t next_channel, timestamp_t binwidth = 1000, int n_bins = 1000, int n_pixels=1);

  void start();
  void startFor(timestamp_t capture_duration, bool clear = true);
  void stop();
  void clear();
  timestamp_t getCaptureDuration();

  GET_DATA_2D(getData, int, array_out,);

  GET_DATA_1D(getIndex, timestamp_t, array_out,);

private:
  TimeDifferences td;
};

/**
 * @ingroup ITERATOR
 *
 * \brief cross-correlation between two channels
 *
 * Accumulates time differences between clicks on two channels into
 * a histogram, where all ticks are considered both as start and stop
 * clicks and both positive and negative time differences are considered.
 * The histogram is determined by the number of total bins and the binwidth.
 *
 */
class TT_API Correlation : public _Iterator {
public:
  /**
  * \brief constructor of a correlation measurement
  *
  * If channel_2 is left empty or set to CHANNEL_UNUSED, an auto-correlation measurement is performed.
  * This is the same as setting channel_2 = channel_1.
  *
  * \param tagger                reference to a TimeTagger
  * \param channel_1             first click channel
  * \param channel_2             second click channel
  * \param binwidth              width of one histogram bin in ps
  * \param n_bins                the number of bins in the resulting histogram
  */
  Correlation(TimeTagger* tagger, channel_t channel_1, channel_t channel_2 = CHANNEL_UNUSED, timestamp_t binwidth = 1000, int n_bins = 1000);

  ~Correlation();

  /**
  * \brief returns a one-dimensional array of size n_bins containing the histogram
  */
  GET_DATA_1D(getData, int, array_out, );

  /**
  * \brief get the histogram - normalized such that a perfectly uncorrelated signals would be flat at a height of one
  */
  GET_DATA_1D(getDataNormalized, double, array_out, );

  /**
   * \brief returns a vector of size n_bins containing the time bins in ps
   */
  GET_DATA_1D(getIndex, timestamp_t, array_out,);

protected:
  bool next_impl(std::vector<Tag>& incoming_tags, timestamp_t begin_time, timestamp_t end_time) override;
  void clear_impl() override;

private:
  template<typename TimeDelta, bool binning> void process_tags(const std::vector<Tag>& incoming_tags);

  struct _PulsedEdge {
    timestamp_t edge;
  };

  std::array<channel_t,2> channels;
  int n_bins;
  timestamp_t binwidth;
  timestamp_t half_window_size;

  std::vector<int> data;

  std::array<std::deque<_PulsedEdge>, 2> edges;

  std::array<uint64_t, 2> total_clicks;
};


enum State {
  UNKNOWN,
  HIGH,
  LOW,
};
struct Event {
  timestamp_t time;
  State state;
};
/**
 * @ingroup ITERATOR
 * \brief
 *
 */
class TT_API Scope : public _Iterator {
public:

  /**
  * \brief constructor of a Scope measurement
  *
  * \param tagger                reference to a TimeTagger
  * \param event_channels        channels which are captured
  * \param trigger_channel       channel that starts a new trace
  * \param window_size           window time of each trace
  * \param n_traces              amount of traces (n_traces < 1, automatic retrigger)
  * \param n_max_events          maximum number of tags in each trace
  */
  Scope(TimeTagger* tagger, std::vector<channel_t> event_channels, channel_t trigger_channel, timestamp_t window_size=1000000000, int n_traces=1, int n_max_events=1000);

  ~Scope();

  bool ready();

  int triggered();

  std::vector<std::vector<Event>> getData();

  timestamp_t getWindowSize();

protected:
  bool next_impl(std::vector<Tag>& incoming_tags, timestamp_t begin_time, timestamp_t end_time) override;
  void clear_impl() override;

private:
  struct ActiveTrace {
    timestamp_t time;
    int trace;
  };
  std::deque<ActiveTrace> traces;
  std::vector<State> current_states;
  int next_trace;

  std::vector<std::vector<Event>> events; // [trace*channels + channel][event_id]
  std::vector<std::vector<Event>> eventsBuffer; // [trace*channels + channel][event_id]

  bool retrigger; // restart the measurement after it has finished

  std::vector<channel_t> event_channels; // channel0, channel0_falling, channel1, channel1_falling, ...
  channel_t trigger_channel;
  timestamp_t window_size;
  int n_traces;
  size_t n_max_events;
  size_t channels;
  std::vector<timestamp_t> deadtime;
  void reset();
};

/**
* \ingroup ITERATOR
* \brief start, stop and clear several measurements synchronized
*
* For the case that several measurements should be started, stopped or cleared at the very same time,
* a SynchronizedMeasurements object can be create to which all the measurements (also called iterators)
* can be registered with .registerMeasurement(measurement).
* Calling .stop(), .start() or .clear() on the SynchronizedMeasurements object will call the respective
* method on each of the registered measurements at the very same time. That means that all measurements
* taking part will have processed the very same time tags.
*
**/
class TT_API SynchronizedMeasurements {
public:
  /**
  * \brief construct a SynchronizedMeasurements object
  *
  * \param tagger reference to a TimeTagger
  */
  SynchronizedMeasurements(TimeTagger* tagger);

  /**
  * \brief register a measurement (iterator) to the SynchronizedMeasurements-group.
  *
  * All available methods called on the SynchronizedMeasurements will happen at the very same time for all the registered measurements.
  */
  void registerMeasurement(_Iterator* measurement);

  /**
  * \brief clear all registered measurements synchronously
  */
  void clear();

  /**
  * \brief start all registered measurements synchronously
  */
  void start();

  /**
  * \brief stop all registered measurements synchronously
  */
  void stop();

  /**
  * \brief start all registered measurements synchronously, and stops them after the capture_duration
  */
  void startFor(timestamp_t capture_duration, bool clear = true);

protected:
  /**
  * \brief run a callback on all registered measurements synchronously
  *
  * Please keep in mind that the callback is copied for each measurement.
  * So please avoid big captures.
  */
  void runCallback(TimeTagger::IteratorCallback callback, bool block = true);

private:
  std::set<_Iterator*> registered_measurements;
  std::mutex measurements_mutex;
  TimeTagger* tagger;
};

/**
 * \ingroup ITERATOR
 *
 * \brief a virtual CFD implementation which returns the mean time between a raising and a falling pair of edges
 *
 */
class TT_API ConstantFractionDiscriminator : public _Iterator {
public:

  /**
  * \brief constructor of a ConstantFractionDiscriminator
  *
  * \param tagger                reference to a TimeTagger
  * \param channels              list of channels for the CFD, the formers of the raising+falling pairs must be given
  * \param search_window         interval for the CFD window, must be positive
  */
  ConstantFractionDiscriminator(TimeTagger* tagger, std::vector<channel_t> channels, timestamp_t search_window);

  ~ConstantFractionDiscriminator();

  /**
   * \brief the list of new virtual channels
   *
   * This function returns the list of new allocated virtual channels.
   * It can be used now in any new measurement class.
   */
  std::vector<channel_t> getChannels();

protected:
  bool next_impl(std::vector<Tag>& incoming_tags, timestamp_t begin_time, timestamp_t end_time) override;
  void on_start() override;

private:
  // Configuration state
  const timestamp_t window;
  const timestamp_t delay;
  std::vector<channel_t> output_channels;

  // State of every CFD input channel
  // For performance, store both the state of the first event and of the second event within the same map.
  // So half of the data might be unused, but we only need one map lookup.
  struct CFDState {
    // state of the first received event
    bool first_event = false;
    bool event_detected = false;
    timestamp_t last_event;

    // state of the second received event
    bool second_event = false;
    CFDState* former_channel = nullptr;
    channel_t output_channel = CHANNEL_UNUSED;
  };
  std::map<channel_t, CFDState> channel_to_cfd_state_map;

  // Tag mirror for injecting new tags
  std::vector<Tag> out_tags;

  // priority output queue for a merge sort of the input stream and the CFD events
  class TagCompare {
  public:
    bool operator()(const Tag& a, const Tag& b) const {
      return a.time - b.time > 0; // Note: this isn't the same as a>b because of the s64 overflow
    }
  };
  std::priority_queue<Tag, std::vector<Tag>, TagCompare> delayed_tags;
};

#endif /* TT_ITERATORS_H_ */
