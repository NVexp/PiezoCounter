#QT       += core gui
QT       += core gui printsupport
QT       += datavisualization
QT       += multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

CONFIG += c++17

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Sora/cnn.cpp \
    Sora/control.cpp \
    Sora/sora.cpp \
    Sora/preparelearningdata.cpp \
    Sora/evalfit.cpp \
    # alglib
    Sora/alglib/alglibinternal.cpp \
    Sora/alglib/alglibmisc.cpp \
    Sora/alglib/ap.cpp \
    #alglib/fittingroutines.cpp \
    Sora/alglib/integration.cpp \
    Sora/alglib/interpolation.cpp \
    Sora/alglib/linalg.cpp \
    Sora/alglib/optimization.cpp \
    Sora/alglib/solvers.cpp \
    Sora/alglib/specialfunctions.cpp \
    Sora/alglib/fasttransforms.cpp \
    #
    arraypreparationsforpulsedmeasurements.cpp \
    common/ostools/spcm_md5.cpp \
    common/ostools/spcm_ostools_win.cpp \
    common/spcm_lib_card.cpp \
    common/spcm_lib_data.cpp \
    common/spcm_lib_thread.cpp \
    labjackt7.cpp \
    loadfilecreatmosaic.cpp \
    m2icard.cpp \
    m8195adev.cpp \
    magnetstage/magnetcontrolclass.cpp \
    magnetstage/magnetstagelimits.cpp \
    magnetstage/magnetstagesettings.cpp \
    magnetstage/visualization2dplot.cpp \
    magnetstage/visualization3dplot.cpp \
    main.cpp \
    mainwindow.cpp \
    pulsedmeasurementsanddata.cpp \
    QCPwithROI/qcpmovablestraightline.cpp \
    QCPwithROI/qcprectroi.cpp \
    QCPwithROI/qcpmovabletracer.cpp \
    QCPwithROI/qcpsequencedata.cpp \
    qcustomplot.cpp \
    qcustomplotwithroi.cpp \
    qseqeditor.cpp \
    sb5_file/sb5_file.cpp \
    scanandcount.cpp

HEADERS += \
    Sora/cnn.h \
    Sora/control.h \
    Sora/sora.h \
    Sora/preparelearningdata.h \
    Sora/evalfit.h \
    # alglib
    Sora/alglib/alglibinternal.h \
    Sora/alglib/alglibmisc.h \
    Sora/alglib/ap.h \
    #alglib/fittingroutines.h \
    Sora/alglib/integration.h \
    Sora/alglib/interpolation.h \
    Sora/alglib/linalg.h \
    Sora/alglib/optimization.h \
    Sora/alglib/solvers.h \
    Sora/alglib/specialfunctions.h \
    Sora/alglib/fasttransforms.h \
    #
    arraypreparationsforpulsedmeasurements.h \
    c_header/dlltyp.h \
    c_header/errors.h \
    c_header/regs.h \
    c_header/spcerr.h \
    c_header/spcm_drv.h \
    c_header/spectrum.h \
    common/ostools/spcm_md5.h \
    common/ostools/spcm_ostools.h \
    common/ostools/spcm_oswrap.h \
    common/spcm_lib_card.h \
    common/spcm_lib_data.h \
    common/spcm_lib_thread.h \
    include/Iterators.h \
    include/PI_GCS2_DLL.h \
    include/TimeTagger.h \
    m2icard.h \
    stdafx.h \
    labjackt7.h \
    loadfilecreatmosaic.h \
    m8195adev.h \
    magnetstage/magnetcontrolclass.h \
    magnetstage/magnetstagelimits.h \
    magnetstage/magnetstagesettings.h \
    magnetstage/visualization2dplot.h \
    magnetstage/visualization3dplot.h \
    mainwindow.h \
    pulsedmeasurementsanddata.h \
    QCPwithROI/qcpmovablestraightline.h \
    QCPwithROI/qcprectroi.h \
    QCPwithROI/qcpmovabletracer.h \
    QCPwithROI/qcpsequencedata.h \
    qcustomplot.h \
    qcustomplotwithroi.h \
    qseqeditor.h \
    sb5_file/sb5_file.h \
    scanandcount.h

FORMS += \
    Sora/control.ui \
    Sora/sora.ui \
    Sora/preparelearningdata.ui \
    magnetstage/magnetstagelimits.ui \
    magnetstage/magnetstagesettings.ui \
    magnetstage/visualization2dplot.ui \
    magnetstage/visualization3dplot.ui \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

unix:!macx|win32: LIBS += -L$$PWD/libs_x64/ -lTimeTagger

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/libs_x64/TimeTagger.lib
else:unix:!macx|win32-g++: PRE_TARGETDEPS += $$PWD/libs_x64/libTimeTagger.a

unix:!macx|win32: LIBS += -L$$PWD/libs_x64/ -lPI_GCS2_DLL_x64

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/libs_x64/PI_GCS2_DLL_x64.lib
else:unix:!macx|win32-g++: PRE_TARGETDEPS += $$PWD/libs_x64/libPI_GCS2_DLL_x64.a

Debug:QMAKE_CXXFLAGS += /MT
Release:QMAKE_CXXFLAGS += /MT

win32: LIBS += -L$$PWD/libs_x64/ -lLabJackM_x64

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/libs_x64/LabJackM_x64.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/libs_x64/libLabJackM_x64.a

# Frequency Tab
win32: LIBS += -L$$PWD/'../../../../../../Program Files/IVI Foundation/IVI/Lib_x64/msc/' -lKtM8195

INCLUDEPATH += $$PWD/'../../../../../../Program Files/IVI Foundation/IVI/Include'
DEPENDPATH += $$PWD/'../../../../../../Program Files/IVI Foundation/IVI/Include'

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/'../../../../../../Program Files/IVI Foundation/IVI/Lib_x64/msc/KtM8195.lib'
else:win32-g++: PRE_TARGETDEPS += $$PWD/'../../../../../../Program Files/IVI Foundation/IVI/Lib_x64/msc/libKtM8195.a'

# Pulsed Stuff

win32: LIBS += -L$$PWD/c_header/ -lspcm_win64_msvcpp

INCLUDEPATH += $$PWD/c_header
DEPENDPATH += $$PWD/c_header

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/c_header/spcm_win64_msvcpp.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/c_header/libspcm_win64_msvcpp.a

# Magnetstage

win32: LIBS += -L$$PWD/libs_x64/ -llibximc

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/libs_x64/libximc.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/libs_x64/liblibximc.a
