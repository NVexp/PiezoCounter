#ifndef LOADFILECREATMOSAIC_H
#define LOADFILECREATMOSAIC_H

#include <QMainWindow>

#include <iostream>
#include <algorithm>
#include <fstream>
#include <chrono>
#include <ctime>
#include <iomanip>
#include <QMessageBox>
#include <QTimer>
#include <utility>
#include <filesystem>
#include <direct.h>



class QCustomPlotWithROI;

typedef struct dataFromFile
{
    int Xstep;
    int Ystep;
    int Npx_X;
    int Npx_Y;
    int XPos;
    int YPos;
    int ZPos;
    std::vector<double> kcountsPerSec;
    std::string fileNameLoaded2D;
    std::string PathLoaded2D;
    std::string fileNameLoaded3D;
    std::string PathLoaded3D;

} dataFromFile;


class LoadFileCreatMosaic
{

public:
    LoadFileCreatMosaic(QCustomPlotWithROI* plot, QCustomPlotWithROI *image3D);
    LoadFileCreatMosaic();

    std::string getFileNameOfLoaded2D();
    std::string getPathOfLoaded2D();
    std::string getFileNameOfLoaded3D();
    std::string getPathOfLoaded3D();

    //functions for single Scan
    void readDataFromDatFile(QString fileName);
    void loadDataFromSingleScan(std::string fileName);
    //functions for Z Scan
    void loadDataFromZScan(std::string folderName);
    void readDataFromDatFile3D(QString filename);

    dataFromFile currentDataFromFile;
    QCustomPlotWithROI* image2DPlot;
    QCustomPlotWithROI* image3DPlot;

    QVector<QVector<double>> currentPlotValues;

};

#endif // LOADFILECREATMOSAIC_H
