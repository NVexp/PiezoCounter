#include "sora.h"
#include "ui_sora.h"

#include<iostream>
#include<QFile>
#include<QFileInfo>
#include<QTextStream>
#include<QMessageBox>
#include<QFileDialog>
#include<QDir>
#include<QObject>
#include<QList>
#include<QTimer>
#include<QProgressDialog>

#include<thread>
#include"cnn.h"
#include"preparelearningdata.h"
#include"evalfit.h"

Sora::Sora(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Sora)
{
    ui->setupUi(this);

    // set title of the window
    this->setWindowTitle("Control");

    // prepare color map
    // configure axis rect
    ui->SoraColorMapRecognition->axisRect()->setupFullAxesBox(true);
    ui->SoraColorMapLearning->axisRect()->setupFullAxesBox(true);

    // set up the QCPColorMap:
    colorMapRecognition = new QCPColorMap(ui->SoraColorMapRecognition->xAxis, ui->SoraColorMapRecognition->yAxis);
    colorMapLearning = new QCPColorMap(ui->SoraColorMapLearning->xAxis, ui->SoraColorMapLearning->yAxis);


    ui->SoraColorMapRecognition->xAxis->setLabel("x");
    ui->SoraColorMapRecognition->yAxis->setLabel("y");
    ui->SoraColorMapLearning->xAxis->setLabel("x");
    ui->SoraColorMapLearning->yAxis->setLabel("y");


    // add a color scale to Recognation Tab:
    colorScaleRecognition = new QCPColorScale(ui->SoraColorMapRecognition);
    ui->SoraColorMapRecognition->plotLayout()->addElement(0, 1, colorScaleRecognition); // add it to the right of the main axis rect
    colorScaleRecognition->setType(QCPAxis::atRight); // scale shall be vertical bar with tick/axis labels right (actually atRight is already the default)
    colorScaleRecognition->axis()->setLabel("PhotonCount");

    // add a color scale to Learning Tab:
    colorScaleLearning = new QCPColorScale(ui->SoraColorMapLearning);
    ui->SoraColorMapLearning->plotLayout()->addElement(0, 1, colorScaleLearning); // add it to the right of the main axis rect
    colorScaleLearning->setType(QCPAxis::atRight); // scale shall be vertical bar with tick/axis labels right (actually atRight is already the default)
    colorScaleLearning->axis()->setLabel("PhotonCount");


    // connect progressbar and current package number with
    // ann with bias
    connect(ann_var, SIGNAL(changePackage(int)), this, SLOT(update_SoraLearnCurrentNb(int)));
    connect(ann_var, SIGNAL(changeEpoch(int)), ui->SoraLearnLearningProgressBar, SLOT(setValue(int)));
    connect(ann_var,SIGNAL(finish(bool)), this, SLOT(enabledButtons(bool)));

    // ann without bias
    connect(annwoBias_var, SIGNAL(changePackage(int)), this, SLOT(update_SoraLearnCurrentNb(int)));
    connect(annwoBias_var, SIGNAL(changeEpoch(int)), ui->SoraLearnLearningProgressBar, SLOT(setValue(int)));
    connect(annwoBias_var,SIGNAL(finish(bool)), this, SLOT(enabledButtons(bool)));

    // cnn with bias
    connect(cnn_var, SIGNAL(changePackage(int)), this, SLOT(update_SoraLearnCurrentNb(int)));
    connect(cnn_var, SIGNAL(changeEpoch(int)), ui->SoraLearnLearningProgressBar, SLOT(setValue(int)));
    connect(cnn_var,SIGNAL(finish(bool)), this, SLOT(enabledButtons(bool)));

    // cnn without bias
    connect(cnnwoBias_var, SIGNAL(changePackage(int)), this, SLOT(update_SoraLearnCurrentNb(int)));
    connect(cnnwoBias_var, SIGNAL(changeEpoch(int)), ui->SoraLearnLearningProgressBar, SLOT(setValue(int)));
    connect(cnnwoBias_var,SIGNAL(finish(bool)), this, SLOT(enabledButtons(bool)));

    // connect click function to the NV-center list
    connect(ui->SoraNVRecListNVCenter, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(on_listItem_clicked(QListWidgetItem*)));

    //connect QTimer
    refocusTimer = new QTimer(this);
    connect(refocusTimer, SIGNAL(timeout()), SLOT(refocusRoutine()));
    Scan2DTimer = new QTimer(this);
    connect(Scan2DTimer, SIGNAL(timeout()), SLOT(start_2DScan_chrackterization()));
    ODMRScanTimer = new QTimer(this);
    connect(ODMRScanTimer, SIGNAL(timeout()), SLOT(start_ODMR_chrackterization()));

    magStageState = new QTimer(this);
    connect(magStageState, SIGNAL(timeout()), SLOT(moveMagnetTest()));


    /* TODO
     * funktion hinzufügen, welche schumutz raus filtert (wahrscheinlich über die anzahl von counts)
     * höhenmessung für das beste Signal
     */
}

Sora::~Sora()
{
    delete ui;
}


//#######################################################################
//########################## Load Data: START ###########################
//#######################################################################

// get current loaded data
void Sora::setData(QVector<double> data, int nbPixelX, int nbPixelY, double resolution, double xPos, double yPos){

    dataOfFileForSora_var->resolution.append(resolution/1000.);
    dataOfFileForSora_var->nb_pixel_X.append(nbPixelX);
    dataOfFileForSora_var->nb_pixel_Y.append(nbPixelY);
    dataOfFileForSora_var->pos_X.append(xPos/1000.);
    dataOfFileForSora_var->pos_Y.append(yPos/1000.);

    // Removal of the snake patter for better processing
    dataOfFileForSora_var->kCountsPerSec.append(prepareDataFromFile(data, dataOfFileForSora_var->nb_pixel_X[dataOfFileForSora_var->nb_pixel_X.size()-1], dataOfFileForSora_var->nb_pixel_Y[dataOfFileForSora_var->nb_pixel_Y.size()-1]));

    showData();
}

// determine the path and read and show data
void Sora::on_SoraNVRecLoad2DScan_clicked()
{
    // delete data of the previous file
    dataOfFileForSora_var->kCountsPerSec.clear();
    dataOfFileForSora_var->resolution.clear();
    dataOfFileForSora_var->nb_pixel_X.clear();
    dataOfFileForSora_var->nb_pixel_Y.clear();
    dataOfFileForSora_var->pos_X.clear();
    dataOfFileForSora_var->pos_Y.clear();

    // delete predicted ROI's
    predictDataROI.clear();

    // delete all rectangles
    ui->SoraColorMapRecognition->removeAllROIs(true);

    // delete the list widget
    ui->SoraNVRecListNVCenter->clear();

    //QString fileName = QFileDialog::getOpenFileName(this, "Open file", "E:/data","Text files (*.dat)");
    QString fileName = QFileDialog::getOpenFileName(this, "Open file", "C:/Users/plate/OneDrive/Universitaet/Masterarbeit","Text files (*.dat)");
    std::cout << fileName.toStdString() << std::endl;
    currentFilePath = fileName;
    if (!fileName.isEmpty()){
        // load data
        loadDataFromFile(fileName);
        // show data in the colormap
        showData();
    }

    // If there are ROI's from the previous image, they must be deleted
    ui->SoraColorMapRecognition->removeAllROIs(true);
    ui->SoraColorMapRecognition->replot();
    ui->SoraColorMapLearning->removeAllROIs(true);
    ui->SoraColorMapLearning->replot();
}

// load the data of the given filename
void Sora::loadDataFromFile(QString cur_fileName){
    QFile file(cur_fileName);
    QVector<double> cur_data;

    QFileInfo getFilenName_var(cur_fileName);

    if(file.open(QIODevice::ReadOnly)){
        QTextStream DataFromFile(&file);
        int lineCounter = 1;
        QString line;
        while(!DataFromFile.atEnd()){
            line = DataFromFile.readLine();
            // line 1 and 2 are text lines without import information
            if(lineCounter == 2){
                QStringList splitLine = line.split("\t");
                dataOfFileForSora_var->resolution.append(splitLine[0].toInt()/1000.);
                dataOfFileForSora_var->nb_pixel_X.append(splitLine[2].toInt());
                dataOfFileForSora_var->nb_pixel_Y.append(splitLine[3].toInt());
                dataOfFileForSora_var->pos_X.append(splitLine[4].toInt()/1000.);
                dataOfFileForSora_var->pos_Y.append(splitLine[5].toInt()/1000.);
                dataOfFileForSora_var->pos_Z.append(splitLine[6].toInt()/1000.);

            }
            if(lineCounter>4){
                QStringList splitLine = line.split("\t");
                cur_data.append(splitLine[2].toDouble()/1000.0);
            }
            lineCounter++;
        }
        file.close();

        // Removal of the snake patter for better processing
        dataOfFileForSora_var->kCountsPerSec.append(prepareDataFromFile(cur_data, dataOfFileForSora_var->nb_pixel_X[dataOfFileForSora_var->nb_pixel_X.size()-1], dataOfFileForSora_var->nb_pixel_Y[dataOfFileForSora_var->nb_pixel_Y.size()-1]));

    }else{
        std::cout << "loading fails. Please check the file!" << std::endl;
    }
}

// undo the snake pattern
QVector<double> Sora::prepareDataFromFile(QVector<double> nonScaledData, int nb_pixel_X, int nb_pixel_Y){
    // undo the snake pattern
    QVector<double> kcountsPerSec_woSnake, scaledData;
    for (int yIndex=0; yIndex<nb_pixel_Y; ++yIndex){
      for (int xIndex = (yIndex%2==0? 0: nb_pixel_Y-1); xIndex>=0 && xIndex < nb_pixel_Y;yIndex%2==0? ++xIndex: --xIndex){
          kcountsPerSec_woSnake.append(nonScaledData[(yIndex*nb_pixel_X)+xIndex]);
      }
    }

    scaledData = kcountsPerSec_woSnake;

    return scaledData;
}

// show the data on the Recognition and Learning Colormap
void Sora::showData(){

    // set up the QCPColorMap:
    colorMapRecognition->data()->setSize(dataOfFileForSora_var->nb_pixel_X[0], dataOfFileForSora_var->nb_pixel_Y[0]);
    colorMapLearning->data()->setSize(dataOfFileForSora_var->nb_pixel_X[0], dataOfFileForSora_var->nb_pixel_Y[0]);

    // calculate the limits using the first element (only important when a folder is loaded)
    double x_lowerLimit = (dataOfFileForSora_var->pos_X[0] - round(dataOfFileForSora_var->nb_pixel_X[0]/2)*dataOfFileForSora_var->resolution[0]);
    double x_upperLimit = (dataOfFileForSora_var->pos_X[0] + round(dataOfFileForSora_var->nb_pixel_X[0]/2)*dataOfFileForSora_var->resolution[0]);
    double y_lowerLimit = (dataOfFileForSora_var->pos_Y[0] - round(dataOfFileForSora_var->nb_pixel_Y[0]/2)*dataOfFileForSora_var->resolution[0]);
    double y_upperLimit = (dataOfFileForSora_var->pos_Y[0] + round(dataOfFileForSora_var->nb_pixel_Y[0]/2)*dataOfFileForSora_var->resolution[0]);

    colorMapRecognition->data()->setRange(QCPRange(x_lowerLimit, x_upperLimit), QCPRange(y_lowerLimit, y_upperLimit));
    colorMapLearning->data()->setRange(QCPRange(x_lowerLimit, x_upperLimit), QCPRange(y_lowerLimit, y_upperLimit));

    int pxIndex=0;
    double x, y, z;
    for (int yIndex=0; yIndex<dataOfFileForSora_var->nb_pixel_Y[0]; ++yIndex){
      for(int xIndex=0;xIndex<dataOfFileForSora_var->nb_pixel_X[0]; xIndex++){
        // set cell coordinates
        colorMapRecognition->data()->cellToCoord(xIndex, yIndex, &x, &y);
        colorMapLearning->data()->cellToCoord(xIndex, yIndex, &x, &y);
        z = dataOfFileForSora_var->kCountsPerSec[0][pxIndex];
        pxIndex++;
        // fill the colorMap with the number of counts
        colorMapRecognition->data()->setCell(xIndex, yIndex, z);
        colorMapLearning->data()->setCell(xIndex, yIndex, z);
      }
    }

    // use pixels with hard borders
    colorMapRecognition->setInterpolate(false);
    colorMapLearning->setInterpolate(false);

    // associate the color map with the color scale
    colorMapRecognition->setColorScale(colorScaleRecognition);
    colorMapLearning->setColorScale(colorScaleLearning);

    // set the gradient
    colorMapRecognition->setGradient(QCPColorGradient::gpPolar);
    colorMapLearning->setGradient(QCPColorGradient::gpPolar);

    // rescale the data dimension (color) such that all data points lie in the span visualized by the color gradient:
    colorMapRecognition->rescaleDataRange();
    colorMapLearning->rescaleDataRange();

    // make sure the axis rect and color scale synchronize their bottom and top margins (so they line up):
    QCPMarginGroup *marginGroupRecognition = new QCPMarginGroup(ui->SoraColorMapRecognition);
    ui->SoraColorMapRecognition->axisRect()->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroupRecognition);
    colorScaleRecognition->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroupRecognition);
    QCPMarginGroup *marginGroupLearning = new QCPMarginGroup(ui->SoraColorMapLearning);
    ui->SoraColorMapLearning->axisRect()->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroupLearning);
    colorScaleLearning->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroupLearning);


    // rescale the key (x) and value (y) axes so the whole color map is visible:
    ui->SoraColorMapRecognition->xAxis->setRange(x_lowerLimit, x_upperLimit);
    ui->SoraColorMapRecognition->yAxis->setRange(y_lowerLimit, y_upperLimit);
    ui->SoraColorMapLearning->xAxis->setRange(x_lowerLimit, x_upperLimit);
    ui->SoraColorMapLearning->yAxis->setRange(y_lowerLimit, y_upperLimit);

    // replot
    ui->SoraColorMapRecognition->replot();
    ui->SoraColorMapLearning->replot();
}

//#######################################################################
//########################### Load Data: END ############################
//#######################################################################



//#######################################################################
//########################## Edit ROI's: START ##########################
//#######################################################################

// shows the ROI legend (nb of ROI, position of ROI and height and wide of the ROI)
void Sora::on_SoraNVRecEnableROILegend_clicked(bool checked)
{
    ui->SoraColorMapRecognition->enableROILegend(checked);
    ui->SoraColorMapRecognition->replot();

    ui->SoraColorMapLearning->enableROILegend(checked);
    ui->SoraColorMapLearning->replot();
}

// updates predictDataROI vector if the Data is changed (ROI gets new positions/wide/heigths)
// this vector is then used from Sora or for learning
void Sora::on_SoraNVRecConfirmEditing_clicked()
{
    QVector<double> curPredictDataROI;
    // delete elements of the current positions and list
    predictDataROI.clear();
    ui->SoraNVRecListNVCenter->clear();

    // gets the new positions using qcustomplot
    for(int i=0; i<ui->SoraColorMapRecognition->ROInumber(); i++){
        curPredictDataROI.clear();
        curPredictDataROI.append(ui->SoraColorMapRecognition->getROIposition(i+1).x());
        curPredictDataROI.append(ui->SoraColorMapRecognition->getROIposition(i+1).y());
        curPredictDataROI.append(ui->SoraColorMapRecognition->getROIsize(i+1).x());
        curPredictDataROI.append(ui->SoraColorMapRecognition->getROIsize(i+1).y());
        predictDataROI.append(curPredictDataROI);
    }

    // update the list
    for(int i=0; i<ui->SoraColorMapRecognition->ROIStore.size(); i++){
        ui->SoraNVRecListNVCenter->addItem(QString::number(i+1) + ": position (" + QString::number(predictDataROI[i][0]) + "/" + QString::number(predictDataROI[i][1]) + ")mum, Bounding Box (" + QString::number(predictDataROI[i][2]*1000.) + "/" + QString::number(predictDataROI[i][3]*1000.) + ")nm");
    }
}

// removes the last ROI
void Sora::on_SoraNVRecRemoveLastROI_clicked()
{
    ui->SoraColorMapRecognition->removeROI();
    ui->SoraColorMapRecognition->replot();

    ui->SoraColorMapLearning->removeROI();
    ui->SoraColorMapLearning->replot();
}

// guide how to delete the selected ROI
void Sora::on_SoraNVRecRemoveSelectedROI_clicked()
{
    QMessageBox removeSelcROI;
    removeSelcROI.setText("To delete the selected ROI, select it and right click on it. Select 'delete ROI' to delete it.");
    removeSelcROI.exec();
}

// removes all ROI's
void Sora::on_SoraNVRecRemoveAllROI_clicked()
{
    ui->SoraColorMapRecognition->removeAllROIs(true);
    ui->SoraColorMapRecognition->replot();

    ui->SoraColorMapLearning->removeAllROIs(true);
    ui->SoraColorMapLearning->replot();
}

// add a ROI at a certain position
void Sora::on_SoraNVRecAddROI_clicked()
{
    if(!dataOfFileForSora_var->kCountsPerSec.isEmpty()){
        // add a ROI at the middle of the colormap with an heigth and wide of 300nm
        ui->SoraColorMapRecognition->addROI({dataOfFileForSora_var->pos_X[0],dataOfFileForSora_var->pos_Y[0]}, 0.3, 0.3);
        ui->SoraColorMapRecognition->enableROILegend(ui->SoraNVRecEnableROILegend->isChecked());
        ui->SoraColorMapRecognition->replot();

        ui->SoraColorMapLearning->addROI({dataOfFileForSora_var->pos_X[0],dataOfFileForSora_var->pos_Y[0]}, 0.3, 0.3);
        ui->SoraColorMapLearning->enableROILegend(ui->SoraNVRecEnableROILegend->isChecked());
        ui->SoraColorMapLearning->replot();
    }
}

//#######################################################################
//########################### Edit ROI's: END ###########################
//#######################################################################



//#######################################################################
//####################### Learning Process: START #######################
//#######################################################################

// load folder with learning data
void Sora::on_SoraLearnLoadFolder_clicked()
{
    // delete data of the prevous file
    dataOfFileForSora_var->kCountsPerSec.clear();
    dataOfFileForSora_var->resolution.clear();
    dataOfFileForSora_var->nb_pixel_X.clear();
    dataOfFileForSora_var->nb_pixel_Y.clear();
    dataOfFileForSora_var->pos_X.clear();
    dataOfFileForSora_var->pos_Y.clear();

    // delete predicted ROI's
    predictDataROI.clear();

    // delete all rectangles
    ui->SoraColorMapRecognition->removeAllROIs(true);

    // delete the list widget
    ui->SoraNVRecListNVCenter->clear();

    QString folderName = QFileDialog::getExistingDirectory(this, "Open directory", "E:/data", QFileDialog::ShowDirsOnly);


    if(!folderName.isEmpty()){
        QDir dir3D = folderName;
        dir3D.setNameFilters(QStringList()<<"*.dat");
        // Only stops when all files with the extension '*.dat' have been read and saved
        foreach(QFileInfo var, dir3D.entryInfoList()){
            if(var.isFile()){
                loadDataFromFile(var.absoluteFilePath());
            }
        }
        // show the first image data in the colormap
        showData();
    }

}

// load single image as learning data (for documantation see 'on_SoraNVRecLoad2DScan_clicked()')
void Sora::on_SoraLearnLoadImage_clicked()
{
    on_SoraNVRecLoad2DScan_clicked();
}

// disable all buttons which can't be used during learn process
void Sora::enabledButtons(bool state)
{
    // Learning Tab
    ui->SoraLearnStartLearningprocess->setEnabled(state);
    ui->SoraLearnSaveLearnParameters->setEnabled(state);
    ui->SoraLearnDeleteNetwork->setEnabled(state);
    ui->SoraNVRecPredict->setEnabled(state);
    ui->SoraLearnGD->setEnabled(state);
    ui->SoraRecNNType->setEnabled(state);
    ui->SoraLearnNNType->setEnabled(state);
}

// for the learning process, the data that is to be used must be processed beforehand.
// this can be done by the AI ​​or by hand.The following function enables such a preparation using another GUI
void Sora::on_SoraLearnPrepareData_clicked()
{
    // check if a file is loaded
    if (dataOfFileForSora_var->kCountsPerSec.isEmpty()){
        QMessageBox emptyVector;
        emptyVector.setText("Please load data to prepare!");
        emptyVector.exec();
    }else{
        // creates new object (preparelearningdata class)
        prepareLearningData *prepareLearningData_var = new prepareLearningData();

        // create a vector with max and min of x and y (needed for the learning window)
        QVector<double> range;
        range.append(dataOfFileForSora_var->pos_X[0] - round(dataOfFileForSora_var->nb_pixel_X[0]/2)*dataOfFileForSora_var->resolution[0]);
        range.append(dataOfFileForSora_var->pos_X[0] + round(dataOfFileForSora_var->nb_pixel_X[0]/2)*dataOfFileForSora_var->resolution[0]);
        range.append(dataOfFileForSora_var->pos_Y[0] - round(dataOfFileForSora_var->nb_pixel_Y[0]/2)*dataOfFileForSora_var->resolution[0]);
        range.append(dataOfFileForSora_var->pos_Y[0] + round(dataOfFileForSora_var->nb_pixel_Y[0]/2)*dataOfFileForSora_var->resolution[0]);

        // transfer of file data
        prepareLearningData_var->setLearnData(dataOfFileForSora_var->kCountsPerSec, dataOfFileForSora_var->resolution, dataOfFileForSora_var->nb_pixel_X, dataOfFileForSora_var->nb_pixel_Y,"C:/Users/plate/OneDrive/ControlProgram/MasterProgramm/CNN/");
        // transfer of range and predicted data (if available)
        prepareLearningData_var->setROIData(predictDataROI, range);

        prepareLearningData_var->show();
    }
}

// starts the learning process
void Sora::on_SoraLearnStartLearningprocess_clicked()
{
    // ann with bias
    if(ui->SoraLearnNNType->currentIndex() == 0){
        ann_var->stopLearning = false;

        // learning process started depending on the choosen gradient decent
        // stochastic gradient decent
        if(ui->SoraLearnGD->currentIndex() == 0){
            ui->SoraLearnCurrentNb->setText(QString::number(0) + "/" + QString::number(ui->SoraLearnNbImages->value()));
            std::thread SGB(&CNN::learnNN_SGB, ann_var, ui->SoraLearnEpochs->value(), ui->SoraLearnNbImages->value(), ui->SoraLearnLearningRate->value());
            SGB.detach();
        // mini batch gradient decent (recommended)
        }else if (ui->SoraLearnGD->currentIndex() == 1) {
            ui->SoraLearnCurrentNb->setText(QString::number(0) + "/" + QString::number(ui->SoraLearnNbPackages->value()));
            std::thread SGBD(&CNN::learnNN_MBGD, ann_var, ui->SoraLearnEpochs->value(), ui->SoraLearnNbPackages->value(), ui->SoraLearnNbPerPackage->value(), ui->SoraLearnLearningRate->value());
            SGBD.detach();
        }
    // ann without bias
    }else if(ui->SoraLearnNNType->currentIndex() == 1){
        annwoBias_var->stopLearning = false;

        // learning process started depending on the choosen gradient decent
        // stochastic gradient decent
        if(ui->SoraLearnGD->currentIndex() == 0){
            ui->SoraLearnCurrentNb->setText(QString::number(0) + "/" + QString::number(ui->SoraLearnNbImages->value()));
            std::thread SGB(&CNN::learnNN_SGB, annwoBias_var, ui->SoraLearnEpochs->value(), ui->SoraLearnNbImages->value(), ui->SoraLearnLearningRate->value());
            SGB.detach();
        // mini batch gradient decent (recommended)
        }else if (ui->SoraLearnGD->currentIndex() == 1) {
            ui->SoraLearnCurrentNb->setText(QString::number(0) + "/" + QString::number(ui->SoraLearnNbPackages->value()));
            std::thread SGBD(&CNN::learnNN_MBGD, annwoBias_var, ui->SoraLearnEpochs->value(), ui->SoraLearnNbPackages->value(), ui->SoraLearnNbPerPackage->value(), ui->SoraLearnLearningRate->value());
            SGBD.detach();
        }
    // cnn with bias
    }else if(ui->SoraLearnNNType->currentIndex() == 2){
        cnn_var->stopLearning = false;

        // learning process started depending on the choosen gradient decent
        // stochastic gradient decent
        if(ui->SoraLearnGD->currentIndex() == 0){
            ui->SoraLearnCurrentNb->setText(QString::number(0) + "/" + QString::number(ui->SoraLearnNbImages->value()));
            std::thread SGB(&CNN::learnNN_SGB, cnn_var, ui->SoraLearnEpochs->value(), ui->SoraLearnNbImages->value(), ui->SoraLearnLearningRate->value());
            SGB.detach();
        // mini batch gradient decent (recommended)
        }else if (ui->SoraLearnGD->currentIndex() == 1) {
            ui->SoraLearnCurrentNb->setText(QString::number(0) + "/" + QString::number(ui->SoraLearnNbPackages->value()));
            std::thread SGBD(&CNN::learnNN_MBGD, cnn_var, ui->SoraLearnEpochs->value(), ui->SoraLearnNbPackages->value(), ui->SoraLearnNbPerPackage->value(), ui->SoraLearnLearningRate->value());
            SGBD.detach();
        }
    // cnn without bias
    }else if(ui->SoraLearnNNType->currentIndex() == 3){
        cnnwoBias_var->stopLearning = false;

        // learning process started depending on the choosen gradient decent
        // stochastic gradient decent
        if(ui->SoraLearnGD->currentIndex() == 0){
            ui->SoraLearnCurrentNb->setText(QString::number(0) + "/" + QString::number(ui->SoraLearnNbImages->value()));
            std::thread SGB(&CNN::learnNN_SGB, cnnwoBias_var, ui->SoraLearnEpochs->value(), ui->SoraLearnNbImages->value(), ui->SoraLearnLearningRate->value());
            SGB.detach();
        // mini batch gradient decent (recommended)
        }else if (ui->SoraLearnGD->currentIndex() == 1) {
            ui->SoraLearnCurrentNb->setText(QString::number(0) + "/" + QString::number(ui->SoraLearnNbPackages->value()));
            std::thread SGBD(&CNN::learnNN_MBGD, cnnwoBias_var, ui->SoraLearnEpochs->value(), ui->SoraLearnNbPackages->value(), ui->SoraLearnNbPerPackage->value(), ui->SoraLearnLearningRate->value());
            SGBD.detach();
        }
    }

    enabledButtons(false);
}

// save the new weights after learning
void Sora::on_SoraLearnSaveLearnParameters_clicked()
{
    // ann with bias
    if(ui->SoraLearnNNType->currentIndex() == 0){
        ann_var->saveNeuralNetwork();
    // ann without bias
    }else if(ui->SoraLearnNNType->currentIndex() == 1){
        annwoBias_var->saveNeuralNetwork();
    // cnn with bias
    }else if(ui->SoraLearnNNType->currentIndex() == 2){
        cnn_var->saveNeuralNetwork();
    // cnn without bias
    }else if(ui->SoraLearnNNType->currentIndex() == 3){
        cnnwoBias_var->saveNeuralNetwork();
    }
}

// delete the current selected CNN (not recommended!!)
void Sora::on_SoraLearnDeleteNetwork_clicked()
{
    // ann with bias
    if(ui->SoraLearnNNType->currentIndex() == 0){
        ann_var->resetNeuralNetwork();
    // ann without bias
    }else if(ui->SoraLearnNNType->currentIndex() == 1){
        annwoBias_var->resetNeuralNetwork();
    // cnn with bias
    }else if(ui->SoraLearnNNType->currentIndex() == 2){
        cnn_var->resetNeuralNetwork();
    // cnn without bias
    }else if(ui->SoraLearnNNType->currentIndex() == 3){
        cnnwoBias_var->resetNeuralNetwork();
    }
}

// stop learning process
void Sora::on_SoraLearnStopLearningprocess_clicked()
{
    // ann with bias
    if(ui->SoraLearnNNType->currentIndex() == 0){
        ann_var->stopLearning = true;
    // ann without bias
    }else if(ui->SoraLearnNNType->currentIndex() == 1){
        annwoBias_var->stopLearning = true;
    // cnn with bias
    }else if(ui->SoraLearnNNType->currentIndex() == 2){
        cnn_var->stopLearning = true;
    // cnn without bias
    }else if(ui->SoraLearnNNType->currentIndex() == 3){
        cnnwoBias_var->stopLearning = true;
    }

    QMessageBox stop;
    stop.setText("learning process stopped");
    stop.exec();
}

// help be choosing the gradient decent
void Sora::on_SoraLearnGDHelp_clicked()
{
    QMessageBox help;
    help.setText("Stochastic Gradient Descent:\nUse one (randomly picked) sample for a forward pass and than adjust weights\n \nMini Batch Gradient Descent:\nUse a batch of (randomly picked) samples for a forward pass and than adjust weights");
    help.exec();
}

// update current Number of package
void Sora::update_SoraLearnCurrentNb(int curNb)
{
    ui->SoraLearnCurrentNb->setText(QString::number(curNb) + "/" + QString::number(ui->SoraLearnNbPackages->value()));
}

//#######################################################################
//######################## Learning Process: END ########################
//#######################################################################



//#######################################################################
//###################### CNN/ANN Predicting: START ######################
//#######################################################################

// use the data of the current colormap for an CNN/ANN prediction
void Sora::on_SoraNVRecPredict_clicked()
{
    if(dataOfFileForSora_var->kCountsPerSec.size() != 0){
        // calls function which searches for NV centers depending on the Neural Network in the colormap and then saves them in predictDataROI
        // ann with bias
        if(ui->SoraRecNNType->currentIndex() == 0){
            predictDataROI = ann_var->predicted(dataOfFileForSora_var->kCountsPerSec[0], dataOfFileForSora_var->nb_pixel_X[0], dataOfFileForSora_var->nb_pixel_Y[0], (int)(dataOfFileForSora_var->resolution[0]*1000));
        // ann without bias
        }else if(ui->SoraRecNNType->currentIndex() == 1){
            predictDataROI = annwoBias_var->predicted(dataOfFileForSora_var->kCountsPerSec[0], dataOfFileForSora_var->nb_pixel_X[0], dataOfFileForSora_var->nb_pixel_Y[0], (int)(dataOfFileForSora_var->resolution[0]*1000));
        // cnn with bias
        }else if(ui->SoraRecNNType->currentIndex() == 2){
            predictDataROI = cnn_var->predicted(dataOfFileForSora_var->kCountsPerSec[0], dataOfFileForSora_var->nb_pixel_X[0], dataOfFileForSora_var->nb_pixel_Y[0], (int)(dataOfFileForSora_var->resolution[0]*1000));
        // cnn without bias
        }else if(ui->SoraRecNNType->currentIndex() == 3){
            predictDataROI = cnnwoBias_var->predicted(dataOfFileForSora_var->kCountsPerSec[0], dataOfFileForSora_var->nb_pixel_X[0], dataOfFileForSora_var->nb_pixel_Y[0], (int)(dataOfFileForSora_var->resolution[0]*1000));
        }
        /* predictDataROI[*][0]: x-position of the predicted NV center
         * predictDataROI[*][1]: y-position of the predicted NV center
         * predictDataROI[*][2]: wide of the bounding box
         * predictDataROI[*][3]: height of the bounding box
         */

        if(!predictDataROI.isEmpty()){
            // deletes the list of the previous guessed NV centers
            ui->SoraNVRecListNVCenter->clear();
            // for each new guess from the CNN, the previous guess in form of ROI's must be deleted
            if(!ui->SoraColorMapRecognition->ROIStore.isEmpty()){
                ui->SoraColorMapRecognition->removeAllROIs(true);
            }

            // the CNN does not work with the position of the scan on the diamond. The first pixel (bottom right) is in the 0/0 position for the CNN.
            // therefore, the positions on the respective scan must be determined. For 0/0 applies:
            double xStart = (dataOfFileForSora_var->pos_X[0] - round(dataOfFileForSora_var->nb_pixel_X[0]/2)*dataOfFileForSora_var->resolution[0]);
            double yStart = (dataOfFileForSora_var->pos_Y[0] - round(dataOfFileForSora_var->nb_pixel_Y[0]/2)*dataOfFileForSora_var->resolution[0]);

            for(int i=0; i<predictDataROI.size(); i++){
                // for simpler calculations, the CNN uses nanometers instead of micrometers. Therefore, you have to convert from nanometers to micrometers.
                predictDataROI[i][0]=xStart + predictDataROI[i][0]/1000.;
                predictDataROI[i][1]=yStart + predictDataROI[i][1]/1000.;
                predictDataROI[i][2]=predictDataROI[i][2]/1000.;
                predictDataROI[i][3]=predictDataROI[i][3]/1000.;

                // creates ROI on the colormap
                ui->SoraColorMapRecognition->addROI({predictDataROI[i][0], predictDataROI[i][1]}, predictDataROI[i][2], predictDataROI[i][3]);
                ui->SoraColorMapLearning->addROI({predictDataROI[i][0], predictDataROI[i][1]}, predictDataROI[i][2], predictDataROI[i][3]);

                // creates a list with all guessed NV centers (ROI number, position and bounding box)
                ui->SoraNVRecListNVCenter->addItem(QString::number(ui->SoraColorMapRecognition->ROInumber()) + ": position (" + QString::number(predictDataROI[i][0]) + "/" + QString::number(predictDataROI[i][1]) + ")mum, Bounding Box (" + QString::number(predictDataROI[i][2]*1000) + "/" + QString::number(predictDataROI[i][3]*1000) + ")nm");

            }

        }else{
            std::cout << "No NV center found!" << std::endl;
        }

        // default to false to get a better overview
        ui->SoraColorMapRecognition->enableROILegend(false);
        ui->SoraColorMapLearning->enableROILegend(false);

        ui->SoraColorMapRecognition->replot();
        ui->SoraColorMapLearning->replot();
    }else{
        QMessageBox noImage;
        noImage.setText("Please load a picture to predict!");
        noImage.exec();
    }
}

// colors the ROI selected in list
void Sora::on_listItem_clicked(QListWidgetItem *item){
    //look for the selected item in the list
    for(int i=0; i<ui->SoraColorMapRecognition->ROIStore.size(); i++){
        if(ui->SoraNVRecListNVCenter->item(i) == item){
            // if the item exists, look for the appropriate ROI
            for(int j=0; j<ui->SoraColorMapRecognition->ROIStore.size(); j++){
                ui->SoraColorMapRecognition->ROIStore[j]->setSelected(false);
            }

            // color the ROI as if it was selected
            ui->SoraColorMapRecognition->ROIStore[i]->setSelected(true);
            ui->SoraColorMapRecognition->replot();
            break;
        }
    }
}

// updates the list after changing ROI position/size
// the list vector is used for Sora or learning
void Sora::on_SoraNVRecConfirmList_clicked()
{
    on_SoraNVRecConfirmEditing_clicked();
}

// determine the median using the sum of the points
QVector<double> Sora::medianDetermination(double posX, double posY, double res, int nbPixelX, int nbPixelY, QVector<double> counts)
{
    // determine the average
    double sum=0;
    for(int i=0; i<counts.size(); i++){
        sum+=counts[i];
    }
    double average = sum/counts.size();

    // set all values below average - 5 to zero and bigger than it to 1
    int nbPixel1=0;
    for(int i=0; i<counts.size(); i++){
        if(counts[i]<(average+10)){
            counts[i]=0;
        }else{
            counts[i]=1;
            nbPixel1++;
        }
    }

    QVector<double> position(2);
    // determine x
    int sumX=0;
    int sumY=0;
    for(int i=1; i<nbPixelX+1; i++){
        for(int j=1; j<nbPixelY+1; j++){
            sumY+=(int)counts[i+((j-1)*nbPixelX)-1];
        }
        sumX+=i*sumY;
        sumY=0;
    }

    position[0] = (posX-res*(nbPixelX/2.)) + (sumX/nbPixel1)*res;

    // detrmine y
    sumX=0;
    sumY=0;
    for(int i=1; i<nbPixelY+1; i++){
        for(int j=1; j<nbPixelX+1; j++){
            sumY+=(int)counts[((i-1)*nbPixelX)+j-1];
        }
        sumX+=i*sumY;
        sumY=0;
    }

    position[1] = (posY-res*(nbPixelY/2.)) + (sumX/nbPixel1)*res;

    return position;
}

// determine the size of the ROI for the learning process
QVector<double> Sora::ROISizeDetermination(double res, int nbPixelX, int nbPixelY, QVector<double> counts)
{
    // determine the average
    double sumAv=0;
    for(int i=0; i<counts.size(); i++){
        sumAv+=counts[i];
    }
    double average = sumAv/counts.size();

    // set all values below average - 5 to zero and bigger than it to 1
    int nbPixel1=0;
    for(int i=0; i<counts.size(); i++){
        if(counts[i]<(average+10)){
            counts[i]=0;
        }else{
            counts[i]=1;
            nbPixel1++;
        }
    }

    QVector<double> ROIsize(2);
    // determine ROI size in x
    int ROI_X=0;
    int sum=0;
    for(int i=1; i<nbPixelY+1; i++){
        for(int j=1; j<nbPixelX+1; j++){
            sum+=(int)counts[((i-1)*nbPixelX)+j-1];
        }
        if(sum>ROI_X){
            ROI_X=sum;
        }
        sum=0;
    }

    ROIsize[0] = ROI_X*res;

    // determine ROI size in x
    int ROI_Y=0;
    sum=0;
    for(int i=1; i<nbPixelX+1; i++){
        for(int j=1; j<nbPixelY+1; j++){
            sum+=(int)counts[i+((j-1)*nbPixelX)-1];
        }
        if(sum>ROI_Y){
            ROI_Y=sum;
        }
        sum=0;
    }

    ROIsize[1]=ROI_Y*res;

    return ROIsize;
}

//#######################################################################
//####################### CNN/ANN Predicting: END #######################
//#######################################################################



//#######################################################################
//########################## Synchronize: START #########################
//#######################################################################

// Synchronize the ROI's of the learning colormap with the recognition colormap
void Sora::on_SoraLearningSynchronize_clicked()
{
    QVector<double> curPredictDataROI;

    // delete elements of the current positions, colormaps and list
    predictDataROI.clear();
    ui->SoraNVRecListNVCenter->clear();

    if(!ui->SoraColorMapRecognition->ROIStore.isEmpty()){
        ui->SoraColorMapRecognition->removeAllROIs(true);
    }

    // gets the new positions using qcustomplot
    for(int i=0; i<ui->SoraColorMapLearning->ROInumber(); i++){
        curPredictDataROI.clear();
        curPredictDataROI.append(ui->SoraColorMapLearning->getROIposition(i+1).x());
        curPredictDataROI.append(ui->SoraColorMapLearning->getROIposition(i+1).y());
        curPredictDataROI.append(ui->SoraColorMapLearning->getROIsize(i+1).x());
        curPredictDataROI.append(ui->SoraColorMapLearning->getROIsize(i+1).y());
        predictDataROI.append(curPredictDataROI);
    }

    // update the list
    for(int i=0; i<ui->SoraColorMapLearning->ROIStore.size(); i++){
        ui->SoraNVRecListNVCenter->addItem(QString::number(i+1) + ": position (" + QString::number(predictDataROI[i][0]) + "/" + QString::number(predictDataROI[i][1]) + ")mum, Bounding Box (" + QString::number(predictDataROI[i][2]*1000.) + "/" + QString::number(predictDataROI[i][3]*1000.) + ")nm");
    }

    // update recognition colormap
    for(int i=0; i<predictDataROI.size(); i++){
        ui->SoraColorMapRecognition->addROI({predictDataROI[i][0], predictDataROI[i][1]}, predictDataROI[i][2], predictDataROI[i][3]);
    }
    ui->SoraColorMapRecognition->enableROILegend(false);
    ui->SoraColorMapRecognition->replot();
}

// Synchronize the ROI's of the recognition colormap with the learning colormap
void Sora::on_SoraRecognitionSynchronize_clicked()
{
    QVector<double> curPredictDataROI;

    // delete elements of the current positions, colormaps and list
    predictDataROI.clear();
    ui->SoraNVRecListNVCenter->clear();

    if(!ui->SoraColorMapLearning->ROIStore.isEmpty()){
        ui->SoraColorMapLearning->removeAllROIs(true);
    }

    // gets the new positions using qcustomplot
    for(int i=0; i<ui->SoraColorMapRecognition->ROInumber(); i++){
        curPredictDataROI.clear();
        curPredictDataROI.append(ui->SoraColorMapRecognition->getROIposition(i+1).x());
        curPredictDataROI.append(ui->SoraColorMapRecognition->getROIposition(i+1).y());
        curPredictDataROI.append(ui->SoraColorMapRecognition->getROIsize(i+1).x());
        curPredictDataROI.append(ui->SoraColorMapRecognition->getROIsize(i+1).y());
        predictDataROI.append(curPredictDataROI);
    }

    // update the list
    for(int i=0; i<ui->SoraColorMapRecognition->ROIStore.size(); i++){
        ui->SoraNVRecListNVCenter->addItem(QString::number(i+1) + ": position (" + QString::number(predictDataROI[i][0]) + "/" + QString::number(predictDataROI[i][1]) + ")mum, Bounding Box (" + QString::number(predictDataROI[i][2]*1000.) + "/" + QString::number(predictDataROI[i][3]*1000.) + ")nm");
    }

    // update learning colormap
    for(int i=0; i<predictDataROI.size(); i++){
        ui->SoraColorMapLearning->addROI({predictDataROI[i][0], predictDataROI[i][1]}, predictDataROI[i][2], predictDataROI[i][3]);
    }
    ui->SoraColorMapLearning->enableROILegend(false);
    ui->SoraColorMapLearning->replot();
}

//#######################################################################
//########################### Synchronize: END ##########################
//#######################################################################



//#######################################################################
//######################## Measure Routine: START #######################
//#######################################################################

// set measure states for...
// ...2D Scan
void Sora::state2DScan(bool state)
{
    curState2DScan = state;
    std::cout << "2D Scan finished" << std::endl;
}

// ... magnet stage
void Sora::stateMagnetStage(bool state)
{
    curMagnetState = state;
    if(state){
        std::cout << "true 1" << std::endl;
    }else{
        std::cout << "false" << std::endl;
    }
}

// refocus on the NV center
void Sora::refocusRoutine()
{
    // scan parameter
    int nbPixelX = 10, nbPixelY = 10;
    int stepsizeX = 100, stepsizeY = 100;

    // z count number
    QVector<double> totalCounts(5);

    // FIRST: set scan parameter and start the scan
    if(!refocusStates[0]){
        // set scan parameter
        emit set2DScanParameters(nbPixelX, nbPixelY, stepsizeX, stepsizeY, 1, 2, "XY");
        Sleep(250);
        refocusStates[0] = true;
        refocusWindow->setValue(11);
        std::cout << "scan parameters set" << std::endl;

    }else if(!refocusStates[1]==true && refocusStates[0]==true){
        // start the scan
        emit checkLaser();
        emit setPermWord();

        curState2DScan = false;
        emit start2DScan();
        std::cout << "2D Scan starts" << std::endl;
        refocusStates[1] = true;

        refocusWindow->setValue(22);

    // SECOND: predict the position of the NV Center
    }else if(!refocusStates[2]==true && refocusStates[1]==true && curState2DScan==true){
        // get current position
        std::vector<double> curPositions = emit getPositions();
        // get current counts vector
        std::vector<double> counts = emit getCounts();

        /* ############################# comment out of outputs #############################
        std::cout << "curPos size: " << curPositions.size() << std::endl;
        std::cout << "counts size: " << counts.size() << std::endl;
        std::cout << "X: " << curPositions[0] << ", Y: " << curPositions[1] << ", Z: " << curPositions[2] << std::endl;
        std::cout << "counts size: " << counts.size() << std::endl;
        std::cout << "counts[0]: " << counts[0] << std::endl;
        ############################# comment out of outputs #############################*/


        QVector<QVector<double>> guessPositions;
        QVector<double> inputDataWOSnake;

        // undo the snake pattern
        for(int yIndex=0; yIndex<nbPixelY; ++yIndex){
            for(int xIndex = (yIndex%2==0? 0: nbPixelY-1); xIndex>=0 && xIndex<nbPixelY; yIndex%2==0? ++xIndex: --xIndex){
                inputDataWOSnake.append(counts[(yIndex*nbPixelX)+xIndex]);
            }
        }

        // predicted the positions using the CNN
        guessPositions = cnnwoBias_var->predicted(inputDataWOSnake, nbPixelX, nbPixelY, stepsizeX);

        std::cout << "guess Position size: " << guessPositions.size() << std::endl;

        // calculate global positions
        guessPositions[0][0] += curPositions[0]-((nbPixelX/2.)*stepsizeX);
        guessPositions[0][1] += curPositions[1]-((nbPixelY/2.)*stepsizeY);

        std::cout << "global Positions: " << guessPositions[0][0] << ", " << guessPositions[0][1] << std::endl;

        // THIRD: set the X-, Y- position
        emit newPosition(guessPositions[0][0], guessPositions[0][1], curPositions[2]);
        Sleep(500);

        /*############################# comment out of outputs #############################
        std::cout << "Aufmerksamkeit!" << std::endl;
        std::cout << "X: " << guessPositions[0][0] << std::endl;
        std::cout << "Y: " << guessPositions[0][1] << std::endl;
        std::cout << "Z: " << curPositions[2] << std::endl;
        ############################# comment out of outputs #############################*/

        refocusStates[2]=true;

        refocusWindow->setValue(33);

    // FOURTH: set the Z- position
    // scan 5 images (5x5) with different Z- positions and compere there counts
    }else if(!refocusStates[3]==true && refocusStates[2]==true){
        // set scan parameters
        emit set2DScanParameters(5, 5, 60, 60, 1, 2, "XY");
        Sleep(250);

        // get current position
        std::vector<double> curPositions = emit getPositions();
        Sleep(250);

        // set new position
        emit newPosition(curPositions[0], curPositions[1], (curPositions[2]-200));

        /*############################# comment out of outputs #############################
        std::cout << "\nX: " << curPositions[0] << ", Y: " << curPositions[1] << ", Z: " << curPositions[2] << std::endl;
        std::cout << "X: " << curPositions[0] << ", Y: " << curPositions[1] << ", Z: " << curPositions[2]-200 << std::endl;
        ############################# comment out of outputs #############################*/

        Sleep(250);

        // start 2D scan
        curState2DScan = false;
        emit start2DScan();
        std::cout << "2D Scan starts" << std::endl;

        refocusStates[3]=true;

        refocusWindow->setValue(44);

    }else if(!refocusStates[4]==true && refocusStates[3]==true && curState2DScan==true){
        // get counts and sum them up
        std::vector<double> counts = emit getCounts();
        Sleep(250);
        double curTotalCounts = 0;
        // sum up all counts
        for(int j=0; j<counts.size(); j++){
            curTotalCounts += counts[j];
        }

        curSavedValues.append(curTotalCounts);

        refocusStates[4]=true;

        refocusWindow->setValue(55);

    }else if(!refocusStates[5]==true && refocusStates[4]==true){
        // get current position
        std::vector<double> curPositions = emit getPositions();
        Sleep(250);

        // set new position
        emit newPosition(curPositions[0], curPositions[1], (curPositions[2]+100));

        /*############################# comment out of outputs #############################
        std::cout << "\nX: " << curPositions[0] << ", Y: " << curPositions[1] << ", Z: " << curPositions[2] << std::endl;
        std::cout << "X: " << curPositions[0] << ", Y: " << curPositions[1] << ", Z: " << curPositions[2]+100 << std::endl;
        ############################# comment out of outputs #############################*/

        Sleep(250);

        // start 2D scan
        curState2DScan = false;
        emit start2DScan();
        std::cout << "2D Scan starts" << std::endl;

        refocusStates[5]=true;

        refocusWindow->setValue(55+2*curSavedValues.size());

    }else if(!refocusStates[6]==true && refocusStates[5]==true && curState2DScan==true){
        // get counts and sum them up
        std::vector<double> counts = emit getCounts();
        Sleep(250);
        double curTotalCounts = 0;
        // sum up all counts
        for(int j=0; j<counts.size(); j++){
            curTotalCounts += counts[j];
        }

        //149032

        curSavedValues.append(curTotalCounts);

        if(curSavedValues.size()<5){
            refocusStates[5]=false;
        }else{
            // get current position
            std::vector<double> curPositions = emit getPositions();
            Sleep(250);

            int bestZposition = std::max_element(curSavedValues.begin(), curSavedValues.end())-curSavedValues.begin();
            std::cout << "best Z position: " << bestZposition << std::endl;

            std::cout << "z positions" << std::endl;
            for(int i=0; i<curSavedValues.size(); i++){
                std::cout << curSavedValues[i] << std::endl;

            }

            int guessZposition = curPositions[2] - 500 + (bestZposition*100);
            std::cout << "guess Z position: " << guessZposition << std::endl;

            std::cout << "\nX: " << curPositions[0] << ", Y: " << curPositions[1] << ", Z: " << guessZposition << std::endl;
            emit newPosition(curPositions[0], curPositions[1], guessZposition);

            refocusStates[6]=true;

            refocusWindow->setValue(66);
        }
    // FITH: refocus using the classical methode
    }else if(!refocusStates[7]==true && refocusStates[6]==true){
        emit set2DScanParameters(20, 20, 40, 40, 1, 2, "XY");
        Sleep(250);

        // start 2D scan
        curState2DScan = false;
        emit start2DScan();

        refocusStates[7]=true;

        refocusWindow->setValue(77);

    }else if(!refocusStates[8]==true && refocusStates[7]==true && curState2DScan==true){
        // get counts and sum them up
        std::vector<double> counts = emit getCounts();
        Sleep(250);

        // get current position
        std::vector<double> curPositions = emit getPositions();
        Sleep(250);

        QVector<double> medianPos;
        medianPos = medianDetermination(curPositions[0], curPositions[1], 40, 20, 20, QVector<double>::fromStdVector(counts));

        /*############################# comment out of outputs #############################
        std::cout << "\nX: " << medianPos[0] << ", Y: " << medianPos[1] << ", Z: " << curPositions[2] << std::endl;
        ############################# comment out of outputs #############################*/

        emit newPosition(medianPos[0], medianPos[1], curPositions[2]);

        refocusStates[8]=true;

        refocusWindow->setValue(88);

    }else if(refocusStates[8]){
        refocusTimer->stop();
        refocusWindow->setValue(100);
        refocusWindow->close();
        std::cout << "refocus done" << std::endl;
        emit AIuse(false);
    }else{
        //std::cout << "wait" << std::endl;
    }

}
void Sora::refocus()
{
    emit AIuse(true);

    curSavedValues.clear();

    refocusStates.resize(9);
    refocusStates.fill(false);

    refocusTimer->start(500);

    refocusWindow = new QProgressDialog();
    refocusWindow->setRange(0,100);
    refocusWindow->setLabelText("I'll refocus to the ROi. Please wait a moment :)");
    refocusWindow->setWindowTitle("Sora");
    refocusWindow->exec();
    std::cout << "start refocus" << std::endl;





    /*
    // FIRST: set scan parameter and start the scan
    // set scan parameter
    int nbPixelX = 15, nbPixelY = 15;
    int stepsizeX = 100, stepsizeY = 100;

    emit set2DScanParameters(nbPixelX, nbPixelY, stepsizeX, stepsizeY);

    QString X_str = "X";
    QString Y_str = "Y";

    // fill wavGenTableLine
    emit setWavGenTableLine(0, nbPixelX, nbPixelY, X_str);
    emit setWavGenTableLine(1, nbPixelX, nbPixelY, Y_str);

    // start the scan
    emit checkLaser();
    emit setPermWord();
    curState2DScan = false;
    emit start2DScan();
    // wait until the measurement is done
    while(!curState2DScan){
        Sleep(250);
    }

    // get current position
    std::vector<double> curPositions = emit getPositions();
    // get current counts vector
    std::vector<double> counts = emit getCounts();
    //std::cout << "X: " << curPositions[0] << ", Y: " << curPositions[1] << ", Z: " << curPositions[2] << std::endl;
    //std::cout << "counts size: " << counts.size() << std::endl;
    //std::cout << "counts[0]: " << counts[0] << std::endl;

    // SECOND: predict the position of the NV Center
    QVector<QVector<double>> guessPositions;
    QVector<double> inputDataWOSnake;

    // undo the snake pattern
    for(int yIndex=0; yIndex<nbPixelY; ++yIndex){
        for(int xIndex = (yIndex%2==0? 0: nbPixelY-1); xIndex>=0 && xIndex<nbPixelY; yIndex%2==0? ++xIndex: --xIndex){
            inputDataWOSnake.append(counts[(yIndex*nbPixelX)+xIndex]);
        }
    }

    // predicted the positions using the CNN
    guessPositions = cnnwoBias_var->predicted(inputDataWOSnake, nbPixelX, nbPixelY, stepsizeX);

    // calculate global positions
    guessPositions[0][0] += curPositions[0]-((nbPixelX/2.)*stepsizeX);
    guessPositions[0][1] += curPositions[1]-((nbPixelY/2.)*stepsizeY);

    //std::cout << guessPositions[0][0] << ", " << guessPositions[0][1] << std::endl;

    // THIRD: set the X-, Y- position
    emit newPosition(guessPositions[0][0], guessPositions[0][1], curPositions[2]);
    //std::cout << "Aufmerksamkeit!" << std::endl;
    //std::cout << "X: " << guessPositions[0][0] << std::endl;
    //std::cout << "Y: " << guessPositions[0][1] << std::endl;
    //std::cout << "Z: " << curPositions[2] << std::endl;

    //std::vector<double> curNewPositions = emit getPositions();
    //std::cout << "X: " << curNewPositions[0] << ", Y: " << curNewPositions[1] << ", Z: " << curNewPositions[2] << std::endl;

    // FOURTH: set the Z- position
    // scan 5 images (5x5) with different Z- positions and compere there counts
    emit set2DScanParameters(5, 5, 50, 50);

    QVector<double> totalCounts(5);
    double curNbCounts;
    std::vector<double> curCounts;
    int curZposition = curPositions[2]-200;

    for(int i=0; i<5; i++){
        emit newPosition(guessPositions[0][0], guessPositions[0][1], curZposition);
        // wait to be sure of being at the right position
        Sleep(500);
        // start 2D scan
        curState2DScan = false;
        emit start2DScan();
        // wait until the measurement is done
        while(!curState2DScan){
            Sleep(250);
        }

        curNbCounts = 0;
        curCounts = emit getCounts();
        // sum up all counts
        for(int j=0; j<curCounts.size(); j++){
            curNbCounts += curCounts[j];
        }
        //std::cout << "nb Counts: " << curNbCounts << std::endl;
        totalCounts[i] = curNbCounts;
        curCounts.clear();

        curZposition += 100;
    }

    int bestZposition = std::max_element(totalCounts.begin(), totalCounts.end())-totalCounts.begin();
    int guessZposition = curPositions[2] - 300 + (bestZposition*150);

    emit newPosition(guessPositions[0][0], guessPositions[0][1], guessZposition);

    // refocus using the classical methode
    emit set2DScanParameters(20, 20, 40, 40);
    counts = emit getCounts();

    QVector<double> medianPos;
    //medianPos = medianDetermination(guessPositions[0][0], guessPositions[0][1], 40, 20, 20, counts);
    //emit newPosition(medianPos[0], medianPos[1], guessZposition);



    //std::cout << "best z position: " << guessZposition << std::endl;

    emit AIuse(false);
*/
    std::cout << "refocused!" << std::endl;
}


// save count data (2D Scan, ODMR, Rabi)
void Sora::saveMeasureRoutineData(QVector<QVector<double>> dataCounts, QVector<double> dataInformation, QString pathAndFileName){
    // create a header in the file and name the fitparameter
        QVector<QString> fileInformation;
        if(pathAndFileName.contains("2DScan")){
            fileInformation.append("Stepsize X/Stepsize Y/X Px number/Y Px number/Pos X/Pos Y/Pos Z");
            fileInformation.append(QString::number(dataInformation[0]) +"\t"+ QString::number(dataInformation[1]) +"\t"+ QString::number(dataInformation[2]) +"\t"+ QString::number(dataInformation[3]) +"\t"+ QString::number(dataInformation[4]) +"\t"+ QString::number(dataInformation[5]) +"\t"+ QString::number(dataInformation[6]));
            fileInformation.append("X	 Y	 Counts/second");
            fileInformation.append("");
        }else if(pathAndFileName.contains("ODMRScan")){
            std::cout << "work in progress" << std::endl;
        }else if(pathAndFileName.contains("Rabi")){
            std::cout << "work in progress" << std::endl;
            // hier auch FFT??
        }

        // create a file with header and data
        QFile file(pathAndFileName);

        file.open(QIODevice::WriteOnly);
            QTextStream out(&file);
            for(int i=0; i<fileInformation.size(); i++){
                out << fileInformation[i] << "\n";
            }
            for(int i=0; i<dataInformation[2]; i++){
                for(int j=0; j<dataInformation[3]; j++){
                    // x-position
                    if(i%2==0)
                    {
                        out << dataInformation[4]-(int)(((dataInformation[2]-1)*dataInformation[0])/2.0)+(j*dataInformation[0]) << "\t";
                    }else{
                        out << dataInformation[4]-(int)(((dataInformation[2]-1)*dataInformation[0])/2.0)+((dataInformation[2]-(j+1))*dataInformation[0]) << "\t";
                    }
                    // y-position
                    out << dataInformation[5]-(int)(((dataInformation[3]-1)*dataInformation[1])/2.0)+(i*dataInformation[1]) << "\t";
                    // counts/second
                    out << dataCounts[0][i*dataInformation[2]+j]*1000 << "\n";
                }
                //out << "\n";
            }

        file.close();
}

// measure routines for...
// ... 2D Scan
void Sora::start_2DScan_chrackterization()
{

}

// ... ODMR
void Sora::start_ODMR_chrackterization()
{
    // set AWG mode to triggered
    if(!ODMRStates[0]){
        std::cout << "set triggerd mode" << std::endl;
        emit setAWGTriggerMode("Triggered");
        Sleep(500);
        ODMRStates[0] = true;

    // move the magnet stage to the position 0 deg, 0 deg, 50mm (small motor, big motor, distance motor)
    }else if(!ODMRStates[1]){
        std::cout << "start moving magnetstage" << std::endl;
        curMagnetState = false;
        emit setMagnetStage(49.63, 0, 0); // distance motor (not 50 because the scale ist between 0 and 49.63), small motor, big motor
        ODMRStates[1] = true;

    // upload scan data to the AWG
    }else if(ODMRStates[2]==false && curMagnetState==true){
        std::cout << "set scan data" << std::endl;
    // set scan data
    int loopNb = 100;
    double exitTime = 1000.;
    double freqStart = 2600.;
    double freqEnd = 3200.;
    int stepNb = 200;
    ODMRStates[2] = true;
    // stop timer if the measure routine is done
    }else if(ODMRStates[2]){
        std::cout << "QTimer stops" << std:: endl;
        ODMRScanTimer->stop();
    }else{
        // do nothing!
        std::cout << "wait" << std::endl;
    }
    // start to upload data


    // start measurement

    // read measure values

    // save data in a file
    //std::cout << "ODMR: work in progress" << std::endl;

    // prepare cw ODMR
    /*int loopNb = 100;
    double excitTime = 1000.;
    double freqStart = 2400.;
    double freqEnd = 3400.;
    int stepNb = 500;*/

    /*
    int loopNb = 100;
    double excitTime = 1000.;
    double freqStart = 2800.;
    double freqEnd = 2900.;
    int stepNb = 100;
    curODMRUploadState = true;
    emit loadCwODMR(loopNb, excitTime, freqStart, freqEnd, stepNb);
    while(curODMRUploadState){
        Sleep(500);
    }

    // start cw ODMR
    curCwODMRState = true;
    emit startCwODMR(loopNb);
    while(curCwODMRState){
        Sleep(500);
    }*/

  //  std::cout << "ende gut alles gut" << std::endl;
}

// ... NV axis
//#######################################################################
//######################### Measure Routine: END ########################
//#######################################################################


void Sora::on_test_clicked()
{
    /*
    QVector<double> test;
    test = cnnwoBias_var->rescaleSizeInput(dataOfFileForSora_var->kCountsPerSec[0],dataOfFileForSora_var->nb_pixel_X[0], dataOfFileForSora_var->nb_pixel_Y[0]);

    dataOfFileForSora_var->kCountsPerSec[0] = test;
    dataOfFileForSora_var->nb_pixel_X[0] = 2*dataOfFileForSora_var->nb_pixel_X[0]-1;
    dataOfFileForSora_var->nb_pixel_Y[0] = 2*dataOfFileForSora_var->nb_pixel_Y[0]-1;
    dataOfFileForSora_var->resolution[0] = dataOfFileForSora_var->resolution[0]/2.;

    showData();*/

    /*QVector<double> pos = medianDetermination(dataOfFileForSora_var->pos_X[0], dataOfFileForSora_var->pos_Y[0], dataOfFileForSora_var->resolution[0], dataOfFileForSora_var->nb_pixel_X[0], dataOfFileForSora_var->nb_pixel_Y[0], dataOfFileForSora_var->kCountsPerSec[0]);

    QVector<double> ROI = ROISizeDetermination(dataOfFileForSora_var->resolution[0], dataOfFileForSora_var->nb_pixel_X[0], dataOfFileForSora_var->nb_pixel_Y[0], dataOfFileForSora_var->kCountsPerSec[0]);

    ui->SoraColorMapRecognition->addROI({pos[0], pos[1]}, ROI[0], ROI[1]);
    ui->SoraColorMapRecognition->enableROILegend(ui->SoraNVRecEnableROILegend->isChecked());
    //ui->SoraColorMapRecognition->rescaleDataRange();
    ui->SoraColorMapRecognition->replot();
    */
    int nbPixelX = 15, nbPixelY = 15;
    int stepsizeX = 100, stepsizeY = 100;

    emit set2DScanParameters(nbPixelX, nbPixelY, stepsizeX, stepsizeY, 1, 2, "XY");

    emit start2DScan();
    std::cout << "I've finished my work" << std::endl;



}

// update the state of a 2D scan
void Sora::stateNVMeasurment(bool state){
    curMeasurmentState = state;
}


// update the state if the ODMR data is uploaded
void Sora::stateODMRUpload(bool state){
    curODMRUploadState = state;
}

void Sora::stateCwODMR(bool state){
    curCwODMRState = state;
}

void Sora::start_chrackterization(QString scanType)
{
    emit AIuse(true);
    // prepare Laser
    emit checkLaser();
    emit setPermWord();
    Sleep(1000);

    // save current positions
    std::vector<double> curPositions;
    // save current counts
    std::vector<double> counts;

    // scan all possible NV centers
    for(int i=0; i<ui->SoraColorMapRecognition->ROIStore.size(); i++){
        // FIRST: 2D Scan
        // create a folder for the NV center
        QDir().mkdir(curPath+"/Sora"+"/ROI"+QString::number(i+1));

        // go to the position of the ROI
        curPositions = emit getPositions();
        emit newPosition(predictDataROI[i][0]*1000, predictDataROI[i][1]*1000, curPositions[0]);
        Sleep(500);
        std::cout << "X: " << predictDataROI[i][0]*1000 << std::endl;
        std::cout << "Y: " << predictDataROI[i][1]*1000 << std::endl;
        std::cout << "Z: " << curPositions[2] << std::endl;


        // focus on the NV center (X, Y, Z)
        refocus();

        // start a 2D scan
        emit set2DScanParameters(20, 20, 40, 40, 1, 2, "XY");
        Sleep(500);
        curState2DScan = false;
        emit start2DScan();
        // wait until the measurement is done
        while(!curState2DScan){
            Sleep(250);
        }
        std::cout << "measurement done!" << std::endl;

        // get values for the measured counts
        counts = emit getCounts();
        Sleep(500);
        std::cout << "size of data(400): " << counts.size() << std::endl;
        if(counts.size()>0){
            std::cout << counts[0] << std::endl;
        }

        // save data in a file
        QVector<QVector<double>> dataCounts;
        dataCounts.append(QVector<double>::fromStdVector(counts));
        QVector<double> dataInformation = {40, 40, 20, 20, predictDataROI[i][0]*1000, predictDataROI[i][1]*1000, curPositions[2]};
        QString pathAndFileName = curPath+"/Sora"+"/ROI"+QString::number(i+1)+"/2DScan.dat";
        saveMeasureRoutineData(dataCounts, dataInformation, pathAndFileName);


        // SECOND: ODMR Scan
        // if the scan type is equal to ODMRScan the ROI gets further investigated using ODMR
        if(scanType == "ODMRScan"){
            ODMRStates.resize(5);
            ODMRStates.fill(false);
            // start timer...
            start_ODMR_chrackterization();
        }


        // THRID: NV axis Scan
        // check if the ROI is a NV center or not (if not the scanType magFieldScan and rabiScan is't possible)
        // if the scan type is equal to magFieldScan the direction of the NV axis is determined
        if(scanType == "magFieldScan"){
            std::cout << "mag Field: work in progress" << std::endl;
        }


        // FOURTH: Rabi Scan
        // if the scan type is equal to rabiScan the rabi time is determined
        if(scanType == "rabiScan"){
            std::cout << "Rabi: work in progress" << std::endl;
        }

    }
    /*####
    int centerPositionROIX, centerPositionROIY, centerPositionROIZ;

    // set the wavetable and wait until done
    emit setWavetabel(20,20,40,40,1,2);
    Sleep(2000);

    for(int i=0; i<ui->SoraColorMapRecognition->ROIStore.size(); i++){
        // FIRST: find ROI's and scan the parameter
        QPointF position = ui->SoraColorMapRecognition->getROIposition(i+1);
        centerPositionROIX= (int)(position.x()*1000);
        centerPositionROIY= (int)(position.y()*1000);
        centerPositionROIZ= (int)(dataOfFileForSora_var->pos_Z[0]*1000);
        curMeasurmentState = true;
        emit startScanNVOnly(centerPositionROIX,centerPositionROIY,centerPositionROIZ);
        while(curMeasurmentState){
            Sleep(500);
        }####*/

        /*
        //SECOND: confirm ROI's as NV centers using ODMR
        // set Magnet to the position 0 deg, 0 deg, 50mm (small motor, big motor, distance motor)
        curMagnetState = true;
        emit setDistanceValue(49.63); // not 50 because the scale ist between 0 and 49.63
        emit setSmallAngleValue(5);
        emit setBigAngleValue(5);
        emit startMagnetstage();
        while(curMagnetState){
            Sleep(500);
        }*/

        // prepare cw ODMR
        /*int loopNb = 100;
        double excitTime = 1000.;
        double freqStart = 2400.;
        double freqEnd = 3400.;
        int stepNb = 500;*/

        /*
        int loopNb = 100;
        double excitTime = 1000.;
        double freqStart = 2800.;
        double freqEnd = 2900.;
        int stepNb = 100;
        curODMRUploadState = true;
        emit loadCwODMR(loopNb, excitTime, freqStart, freqEnd, stepNb);
        while(curODMRUploadState){
            Sleep(500);
        }

        // start cw ODMR
        curCwODMRState = true;
        emit startCwODMR(loopNb);
        while(curCwODMRState){
            Sleep(500);
        }*/

      //  std::cout << "ende gut alles gut" << std::endl;

    //}

    //emit reset parameter like x,y,z as well as define the origin values of the program
    // emit wavetable back to orign values (vllt lösen durch ein signal welches auf button im MainWindow zugreift oder so ähnlich)


    std::cout << "\nSora is finished!" << std::endl;
    emit AIuse(false);
}

bool Sora::moveMagnetTest()
{
    //emit setMagnetStage(0, 0, 49.63);
    if(!curMeasure){
        //emit setMagnetStage(49.63, 0, 0);
        std::cout << "1" << std::endl;
        emit setMagnetStage(49.63, 0, 0);
        //std::thread thread_scan_NVOnly(&Sora::start_ODMR_chrackterization, this);
        //thread_scan_NVOnly.detach();

        curMeasure = true;
    }
    std::cout << "2" << std::endl;
    if(curMagnetState){
        magStageState->stop();
        std::cout << "done" << std::endl;
        curMeasure = false;
    }
    /*if(curMagnetState){
        return true;
    }else{
        return false;
    }*/
    return false;
}


void Sora::on_SoraNVRecStartInvestigation_clicked()
{
    std::cout << "work in progress" << std::endl;
    std::cout << curPath.toStdString() << std::endl;

    if(!QDir(curPath+"/Sora").exists()){
        QDir().mkdir(curPath+"/Sora");
    }

    ODMRStates.resize(5);
    ODMRStates.fill(false);

    curMagnetState = false;
    ODMRScanTimer->start(1000);
    //std::thread thread_scan_NVOnly(&Sora::start_ODMR_chrackterization, this);
    //thread_scan_NVOnly.detach();


    /*if(!predictDataROI.isEmpty()){
        // create folder for the current Scan
        bool newFolder = true;
        int index = 1;
        while(newFolder){
            if(!QDir(curPath+"/Sora"+"/Scan"+QString::number(index)).exists()){
                QDir().mkdir(curPath+"/Sora"+"/Scan"+QString::number(index));
                QString curPathToROI = curPath+"/Sora"+"/Scan"+QString::number(index);
                newFolder = false;
            }else{
                index++;
            }
        }

        // save used data image
        QVector<double> dataInformation = {dataOfFileForSora_var->resolution[0], dataOfFileForSora_var->resolution[0], (double)dataOfFileForSora_var->nb_pixel_X[0], (double)dataOfFileForSora_var->nb_pixel_Y[0], dataOfFileForSora_var->pos_X[0], dataOfFileForSora_var->pos_Y[0], dataOfFileForSora_var->pos_Z[0]};
        QString pathAndFileName = curPathToROI+"/global2DScan.dat";
        saveMeasureRoutineData(dataOfFileForSora_var->kCountsPerSec, dataInformation, pathAndFileName);

        // start investing the ROI's
        //start_chrackterization("Scan2D");

        //std::thread thread_start_chrackterization(&Sora::start_chrackterization, this, "Scan2D");
        //thread_start_chrackterization.detach();
    }else{
        QMessageBox error;
        error.setText("No ROI's to invest");
        error.exec();
    }*/
    /*
    std::thread thread_scan_NVOnly(&Sora::Scan_NVOnly, this);
    thread_scan_NVOnly.detach();
    */
}
