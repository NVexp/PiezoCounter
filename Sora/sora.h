#ifndef SORA_H
#define SORA_H

#include <QMainWindow>
#include <QTimer>

#include<qcustomplot.h>
#include "cnn.h"
#include "evalfit.h"

typedef struct dataOfFileForSora{
    QVector<double> resolution; // stepsize between the pixels (X and Y has to be equal)
    QVector<int> nb_pixel_X;
    QVector<int> nb_pixel_Y;
    QVector<double> pos_X;
    QVector<double> pos_Y;
    QVector<double> pos_Z;
    QVector<QVector<double>> kCountsPerSec;
}dataOfFileForSora;

namespace Ui {
class Sora;
}

class Sora : public QMainWindow
{
    Q_OBJECT

public:
    explicit Sora(QWidget *parent = nullptr);
    ~Sora();

    // object which includes the Neural Network
    // CNN = Convolutional Neural Network
    // CNNwoBias = Convolutional Neural Network without Bias
    // ANN = Artificial Neural Network
    // ANNwoBias = Artificial Neural Network without Bias
    CNN *cnn_var = new CNN("C:/Users/DiamondExp/Documents/Qt projects/PiezoScanPlusCounter/Sora/CNN/", "cnn");
    CNN *cnnwoBias_var = new CNN("C:/Users/DiamondExp/Documents/Qt projects/PiezoScanPlusCounter/Sora/CNN/", "cnnwoBias");

    CNN *ann_var = new CNN("C:/Users/DiamondExp/Documents/Qt projects/PiezoScanPlusCounter/Sora/CNN/", "ann");
    CNN *annwoBias_var = new CNN("C:/Users/DiamondExp/Documents/Qt projects/PiezoScanPlusCounter/Sora/CNN/", "annwoBias");

    /*
    CNN *cnn_var = new CNN("C:/Users/plate/OneDrive/ControlProgram/MasterProgramm/CNN/", "cnn");
    CNN *cnnwoBias_var = new CNN("C:/Users/plate/OneDrive/ControlProgram/MasterProgramm/CNN/", "cnnwoBias");

    CNN *ann_var = new CNN("C:/Users/plate/OneDrive/ControlProgram/MasterProgramm/CNN/", "ann");
    CNN *annwoBias_var = new CNN("C:/Users/plate/OneDrive/ControlProgram/MasterProgramm/CNN/", "annwoBias");
*/
    // saves the read data of the file
    dataOfFileForSora *dataOfFileForSora_var = new dataOfFileForSora();

    // evaluate data
    EvalFit *evalFit_var = new EvalFit();

    // Load Data
    void setData(QVector<double> data, int nbPixelX, int nbPixelY, double resolution, double xPos, double yPos);

    // current path
    QString curPath;
    QString curPathToROI;

signals:
    void AIuse(bool state);
    // Measure Routines
    // set 2D scan parameters
    void set2DScanParameters(int nbPixelX, int nbPixelY, int stepsizeX, int stepsizeY, int IDAxis1, int IDAxis2, QString ScanAxis);
    void setWavGenTableLine(int id, int nbPixelX, int nbPixelY, QString X_str); //??

    // start 2D
    void checkLaser();
    void setPermWord();
    void start2DScan();

    // get current positions
    std::vector<double> getPositions();
    // get current counts
    std::vector<double> getCounts();
    // go to position
    void newPosition(int xPos, int yPos, int zPos);

    // set magnetstage parameters
    void setMagnetStage(double smallMotor, double bigMotor, double distanceMotor);
    void startMagnetstage();//###
    void setDistanceValue(double val);//###
    void setSmallAngleValue(double val);//###
    void setBigAngleValue(double val);//###

    // set AWG scan parameters
    void setAWGTriggerMode(const QString &mode);
    void uploadCwODMRParameter(int loopNb, double excitTime, double freqStart, double freqEnd, int stepNb);


    // signals that indicate the learning process
    // 2DScan
    void startScanNVOnly(int centerPositionROIX, int centerPositionROIY, int centerPositionROIZ);
    void setWavetabel(int Lx, int Ly, int Xstep, int Ystep);


    // ODMR measurement
    void loadCwODMR(int loopNb, double excitTime, double freqStart, double freqEnd, int stepNb);//###

    // cw ODMR
    void startCwODMR(int loopNb);
    void setCwODMRParameters(int loobNb, double excitTime, double freqStart, double freqEnd, int stepNb);

public slots:
    void state2DScan(bool state);
    void stateNVMeasurment(bool state);
    void stateMagnetStage(bool state);
    void stateODMRUpload(bool state);
    void stateCwODMR(bool state);

    //Measure Routine
    void refocus();

    bool moveMagnetTest();

private slots:

    // Load Data START
    void on_SoraNVRecLoad2DScan_clicked();
    void showData();
    // Load Data END

    // Edit Tab START
    void on_SoraNVRecEnableROILegend_clicked(bool checked);
    void on_SoraNVRecConfirmEditing_clicked();
    void on_SoraNVRecRemoveLastROI_clicked();
    void on_SoraNVRecRemoveSelectedROI_clicked();
    void on_SoraNVRecRemoveAllROI_clicked();
    void on_SoraNVRecAddROI_clicked();
    // Edit Tab END

    // Learning Tab START
    void on_SoraLearnLoadImage_clicked();
    void on_SoraLearnLoadFolder_clicked();
    void on_SoraLearnPrepareData_clicked();
    void on_SoraLearnStartLearningprocess_clicked();
    void on_SoraLearnSaveLearnParameters_clicked();
    void on_SoraLearnDeleteNetwork_clicked();
    void on_SoraLearnStopLearningprocess_clicked();
    void on_SoraLearnGDHelp_clicked();
    void enabledButtons(bool state);
    void update_SoraLearnCurrentNb(int curNb);
    // Learning Tab END

    // Predicting Tab START
    void on_SoraNVRecPredict_clicked();
    void on_listItem_clicked(QListWidgetItem *item);
    void on_SoraNVRecConfirmList_clicked();
    QVector<double> medianDetermination(double posX, double posY, double res, int nbPixelX, int nbPixelY, QVector<double> counts);
    QVector<double> ROISizeDetermination(double res, int nbPixelX, int nbPixelY, QVector<double> counts);
    // Predicting Tab START

    // Synchronize START
    void on_SoraLearningSynchronize_clicked();
    void on_SoraRecognitionSynchronize_clicked();
    // Synchronize END

    void on_test_clicked();

    // Start Sora
    void refocusRoutine();
    void start_chrackterization(QString scanType);
    void start_2DScan_chrackterization();
    void start_ODMR_chrackterization();
    void on_SoraNVRecStartInvestigation_clicked();


private:
    // Load Data START
    void loadDataFromFile(QString cur_fileName);
    QVector<double> prepareDataFromFile(QVector<double> nonScaledData, int nb_pixel_X, int nb_pixel_Y);
    // Load Data END

    // Measure Routine
    void saveMeasureRoutineData(QVector<QVector<double>> dataCounts, QVector<double> dataInformation, QString pathAndFileName);

    // measure states
    bool curState2DScan = false;

    // Start Sora
    bool curMeasurmentState;
    bool curMagnetState;
    bool curODMRUploadState;
    bool curCwODMRState;

    Ui::Sora *ui;

    QCPColorMap *colorMapRecognition;
    QCPColorScale *colorScaleRecognition;
    QCPColorMap *colorMapLearning;
    QCPColorScale *colorScaleLearning;

    QCPItemRect *ROIData;

    QVector<QVector<double>> predictDataROI;
    QString currentFilePath;
    QVector<double> curSavedValues;

    // QTimer
    QTimer *refocusTimer;
    QTimer *Scan2DTimer;
    QTimer *ODMRScanTimer;

    QTimer *magStageState;

    QProgressDialog *refocusWindow;

    // measurement states
    QVector<bool> refocusStates;
    QVector<bool> ODMRStates;
    bool curMeasure = false;
};

#endif // SORA_H
