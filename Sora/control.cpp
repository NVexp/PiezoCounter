#include "control.h"
#include "ui_control.h"

#include<iostream>
#include<QFile>
#include<QFileInfo>
#include<QTextStream>
#include<QMessageBox>
#include<QFileDialog>
#include<QDir>
#include<QObject>
#include<QList>
#include<QTimer>

#include<thread>
#include"cnn.h"
#include"preparelearningdata.h"
#include"evalfit.h"

Control::Control(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Control)
{
    ui->setupUi(this);

    // set title of the window
    this->setWindowTitle("Control");

    // prepare color map
    // configure axis rect:
    ui->ControlColorMapRecognition->axisRect()->setupFullAxesBox(true);
    ui->ControlColorMapLearning->axisRect()->setupFullAxesBox(true);

    // set up the QCPColorMap:
    colorMapRecognition = new QCPColorMap(ui->ControlColorMapRecognition->xAxis, ui->ControlColorMapRecognition->yAxis);
    colorMapLearning = new QCPColorMap(ui->ControlColorMapLearning->xAxis, ui->ControlColorMapLearning->yAxis);


    ui->ControlColorMapRecognition->xAxis->setLabel("x");
    ui->ControlColorMapRecognition->yAxis->setLabel("y");
    ui->ControlColorMapLearning->xAxis->setLabel("x");
    ui->ControlColorMapLearning->yAxis->setLabel("y");


    // add a color scale to Recognation Tab:
    colorScaleRecognition = new QCPColorScale(ui->ControlColorMapRecognition);
    ui->ControlColorMapRecognition->plotLayout()->addElement(0, 1, colorScaleRecognition); // add it to the right of the main axis rect
    colorScaleRecognition->setType(QCPAxis::atRight); // scale shall be vertical bar with tick/axis labels right (actually atRight is already the default)
    colorScaleRecognition->axis()->setLabel("PhotonCount");

    // add a color scale to Learning Tab:
    colorScaleLearning = new QCPColorScale(ui->ControlColorMapLearning);
    ui->ControlColorMapLearning->plotLayout()->addElement(0, 1, colorScaleLearning); // add it to the right of the main axis rect
    colorScaleLearning->setType(QCPAxis::atRight); // scale shall be vertical bar with tick/axis labels right (actually atRight is already the default)
    colorScaleLearning->axis()->setLabel("PhotonCount");


    // connect progressbar and current package number with
    // ann with bias
    connect(ann_var, SIGNAL(changePackage(int)), this, SLOT(update_ControlLearnCurrentNb(int)));
    connect(ann_var, SIGNAL(changeEpoch(int)), ui->ControlLearnLearningProgressBar, SLOT(setValue(int)));
    connect(ann_var,SIGNAL(finish(bool)), this, SLOT(enabledButtons(bool)));

    // ann without bias
    connect(annwoBias_var, SIGNAL(changePackage(int)), this, SLOT(update_ControlLearnCurrentNb(int)));
    connect(annwoBias_var, SIGNAL(changeEpoch(int)), ui->ControlLearnLearningProgressBar, SLOT(setValue(int)));
    connect(annwoBias_var,SIGNAL(finish(bool)), this, SLOT(enabledButtons(bool)));

    // cnn with bias
    connect(cnn_var, SIGNAL(changePackage(int)), this, SLOT(update_ControlLearnCurrentNb(int)));
    connect(cnn_var, SIGNAL(changeEpoch(int)), ui->ControlLearnLearningProgressBar, SLOT(setValue(int)));
    connect(cnn_var,SIGNAL(finish(bool)), this, SLOT(enabledButtons(bool)));

    // cnn without bias
    connect(cnnwoBias_var, SIGNAL(changePackage(int)), this, SLOT(update_ControlLearnCurrentNb(int)));
    connect(cnnwoBias_var, SIGNAL(changeEpoch(int)), ui->ControlLearnLearningProgressBar, SLOT(setValue(int)));
    connect(cnnwoBias_var,SIGNAL(finish(bool)), this, SLOT(enabledButtons(bool)));

    // connect click function to the NV-center list
    connect(ui->ControlNVRecListNVCenter, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(on_listItem_clicked(QListWidgetItem*)));

    //connect QTimer
    Scan2DTimer = new QTimer(this);
    connect(Scan2DTimer, SIGNAL(timeout()), SLOT(start_2DScan_chrackterization()));
    ODMRScanTimer = new QTimer(this);
    connect(ODMRScanTimer, SIGNAL(timeout()), SLOT(start_ODMR_chrackterization()));

    magStageState = new QTimer(this);
    connect(magStageState, SIGNAL(timeout()), SLOT(moveMagnetTest()));

    /* TODO
     * funktion hinzufügen, welche schumutz raus filtert (wahrscheinlich über die anzahl von counts)
     * höhenmessung für das beste Signal
     */
}

Control::~Control()
{
    delete ui;
}


//#######################################################################
//########################## Load Data: START ###########################
//#######################################################################

// get current loaded data
void Control::setData(QVector<double> data, int nbPixelX, int nbPixelY, double resolution, double xPos, double yPos){

    dataOfFile_var->resolution.append(resolution/1000.);
    dataOfFile_var->nb_pixel_X.append(nbPixelX);
    dataOfFile_var->nb_pixel_Y.append(nbPixelY);
    dataOfFile_var->pos_X.append(xPos/1000.);
    dataOfFile_var->pos_Y.append(yPos/1000.);

    // Removal of the snake patter for better processing
    dataOfFile_var->kCountsPerSec.append(prepareDataFromFile(data, dataOfFile_var->nb_pixel_X[dataOfFile_var->nb_pixel_X.size()-1], dataOfFile_var->nb_pixel_Y[dataOfFile_var->nb_pixel_Y.size()-1]));

    showData();
}

// determine the path and read and show data
void Control::on_ControlNVRecLoad2DScan_clicked()
{
    // delete data of the previous file
    dataOfFile_var->kCountsPerSec.clear();
    dataOfFile_var->resolution.clear();
    dataOfFile_var->nb_pixel_X.clear();
    dataOfFile_var->nb_pixel_Y.clear();
    dataOfFile_var->pos_X.clear();
    dataOfFile_var->pos_Y.clear();

    // delete predicted ROI's
    predictDataROI.clear();

    // delete all rectangles
    ui->ControlColorMapRecognition->removeAllROIs(true);

    // delete the list widget
    ui->ControlNVRecListNVCenter->clear();

    QString fileName = QFileDialog::getOpenFileName(this, "Open file", "E:/data","Text files (*.dat)");
    std::cout << fileName.toStdString() << std::endl;
    currentFilePath = fileName;
    if (!fileName.isEmpty()){
        // load data
        loadDataFromFile(fileName);
        // show data in the colormap
        showData();
    }

    // If there are ROI's from the previous image, they must be deleted
    ui->ControlColorMapRecognition->removeAllROIs(true);
    ui->ControlColorMapRecognition->replot();
    ui->ControlColorMapLearning->removeAllROIs(true);
    ui->ControlColorMapLearning->replot();
}

// load the data of the given filename
void Control::loadDataFromFile(QString cur_fileName){
    QFile file(cur_fileName);
    QVector<double> cur_data;

    QFileInfo getFilenName_var(cur_fileName);

    if(file.open(QIODevice::ReadOnly)){
        QTextStream DataFromFile(&file);
        int lineCounter = 1;
        QString line;
        while(!DataFromFile.atEnd()){
            line = DataFromFile.readLine();
            // line 1 and 2 are text lines without import information
            if(lineCounter == 2){
                QStringList splitLine = line.split("\t");
                dataOfFile_var->resolution.append(splitLine[0].toInt()/1000.);
                dataOfFile_var->nb_pixel_X.append(splitLine[2].toInt());
                dataOfFile_var->nb_pixel_Y.append(splitLine[3].toInt());
                dataOfFile_var->pos_X.append(splitLine[4].toInt()/1000.);
                dataOfFile_var->pos_Y.append(splitLine[5].toInt()/1000.);
                dataOfFile_var->pos_Z.append(splitLine[6].toInt()/1000.);

            }
            if(lineCounter>4){
                QStringList splitLine = line.split("\t");
                cur_data.append(splitLine[2].toDouble()/1000.0);
            }
            lineCounter++;
        }
        file.close();

        // Removal of the snake patter for better processing
        dataOfFile_var->kCountsPerSec.append(prepareDataFromFile(cur_data, dataOfFile_var->nb_pixel_X[dataOfFile_var->nb_pixel_X.size()-1], dataOfFile_var->nb_pixel_Y[dataOfFile_var->nb_pixel_Y.size()-1]));

    }else{
        std::cout << "loading fails. Please check the file!" << std::endl;
    }
}

// undo the snake pattern
QVector<double> Control::prepareDataFromFile(QVector<double> nonScaledData, int nb_pixel_X, int nb_pixel_Y){
    // undo the snake pattern
    QVector<double> kcountsPerSec_woSnake, scaledData;
    for (int yIndex=0; yIndex<nb_pixel_Y; ++yIndex){
      for (int xIndex = (yIndex%2==0? 0: nb_pixel_Y-1); xIndex>=0 && xIndex < nb_pixel_Y;yIndex%2==0? ++xIndex: --xIndex){
          kcountsPerSec_woSnake.append(nonScaledData[(yIndex*nb_pixel_X)+xIndex]);
      }
    }

    scaledData = kcountsPerSec_woSnake;

    return scaledData;
}

// show the data on the Recognition and Learning Colormap
void Control::showData(){

    // set up the QCPColorMap:
    colorMapRecognition->data()->setSize(dataOfFile_var->nb_pixel_X[0], dataOfFile_var->nb_pixel_Y[0]);
    colorMapLearning->data()->setSize(dataOfFile_var->nb_pixel_X[0], dataOfFile_var->nb_pixel_Y[0]);

    // calculate the limits using the first element (only important when a folder is loaded)
    double x_lowerLimit = (dataOfFile_var->pos_X[0] - round(dataOfFile_var->nb_pixel_X[0]/2)*dataOfFile_var->resolution[0]);
    double x_upperLimit = (dataOfFile_var->pos_X[0] + round(dataOfFile_var->nb_pixel_X[0]/2)*dataOfFile_var->resolution[0]);
    double y_lowerLimit = (dataOfFile_var->pos_Y[0] - round(dataOfFile_var->nb_pixel_Y[0]/2)*dataOfFile_var->resolution[0]);
    double y_upperLimit = (dataOfFile_var->pos_Y[0] + round(dataOfFile_var->nb_pixel_Y[0]/2)*dataOfFile_var->resolution[0]);

    colorMapRecognition->data()->setRange(QCPRange(x_lowerLimit, x_upperLimit), QCPRange(y_lowerLimit, y_upperLimit));
    colorMapLearning->data()->setRange(QCPRange(x_lowerLimit, x_upperLimit), QCPRange(y_lowerLimit, y_upperLimit));

    int pxIndex=0;
    double x, y, z;
    for (int yIndex=0; yIndex<dataOfFile_var->nb_pixel_Y[0]; ++yIndex){
      for(int xIndex=0;xIndex<dataOfFile_var->nb_pixel_X[0]; xIndex++){
        // set cell coordinates
        colorMapRecognition->data()->cellToCoord(xIndex, yIndex, &x, &y);
        colorMapLearning->data()->cellToCoord(xIndex, yIndex, &x, &y);
        z = dataOfFile_var->kCountsPerSec[0][pxIndex];
        pxIndex++;
        // fill the colorMap with the number of counts
        colorMapRecognition->data()->setCell(xIndex, yIndex, z);
        colorMapLearning->data()->setCell(xIndex, yIndex, z);
      }
    }

    // use pixels with hard borders
    colorMapRecognition->setInterpolate(false);
    colorMapLearning->setInterpolate(false);

    // associate the color map with the color scale
    colorMapRecognition->setColorScale(colorScaleRecognition);
    colorMapLearning->setColorScale(colorScaleLearning);

    // set the gradient
    colorMapRecognition->setGradient(QCPColorGradient::gpPolar);
    colorMapLearning->setGradient(QCPColorGradient::gpPolar);

    // rescale the data dimension (color) such that all data points lie in the span visualized by the color gradient:
    colorMapRecognition->rescaleDataRange();
    colorMapLearning->rescaleDataRange();

    // make sure the axis rect and color scale synchronize their bottom and top margins (so they line up):
    QCPMarginGroup *marginGroupRecognition = new QCPMarginGroup(ui->ControlColorMapRecognition);
    ui->ControlColorMapRecognition->axisRect()->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroupRecognition);
    colorScaleRecognition->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroupRecognition);
    QCPMarginGroup *marginGroupLearning = new QCPMarginGroup(ui->ControlColorMapLearning);
    ui->ControlColorMapLearning->axisRect()->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroupLearning);
    colorScaleLearning->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroupLearning);


    // rescale the key (x) and value (y) axes so the whole color map is visible:
    ui->ControlColorMapRecognition->xAxis->setRange(x_lowerLimit, x_upperLimit);
    ui->ControlColorMapRecognition->yAxis->setRange(y_lowerLimit, y_upperLimit);
    ui->ControlColorMapLearning->xAxis->setRange(x_lowerLimit, x_upperLimit);
    ui->ControlColorMapLearning->yAxis->setRange(y_lowerLimit, y_upperLimit);

    // replot
    ui->ControlColorMapRecognition->replot();
    ui->ControlColorMapLearning->replot();
}

//#######################################################################
//########################### Load Data: END ############################
//#######################################################################



//#######################################################################
//########################## Edit ROI's: START ##########################
//#######################################################################

// shows the ROI legend (nb of ROI, position of ROI and height and wide of the ROI)
void Control::on_ControlNVRecEnableROILegend_clicked(bool checked)
{
    ui->ControlColorMapRecognition->enableROILegend(checked);
    ui->ControlColorMapRecognition->replot();

    ui->ControlColorMapLearning->enableROILegend(checked);
    ui->ControlColorMapLearning->replot();
}

// updates predictDataROI vector if the Data is changed (ROI gets new positions/wide/heigths)
// this vector is then used from control or for learning
void Control::on_ControlNVRecConfirmEditing_clicked()
{
    QVector<double> curPredictDataROI;
    // delete elements of the current positions and list
    predictDataROI.clear();
    ui->ControlNVRecListNVCenter->clear();

    // gets the new positions using qcustomplot
    for(int i=0; i<ui->ControlColorMapRecognition->ROInumber(); i++){
        curPredictDataROI.clear();
        curPredictDataROI.append(ui->ControlColorMapRecognition->getROIposition(i+1).x());
        curPredictDataROI.append(ui->ControlColorMapRecognition->getROIposition(i+1).y());
        curPredictDataROI.append(ui->ControlColorMapRecognition->getROIsize(i+1).x());
        curPredictDataROI.append(ui->ControlColorMapRecognition->getROIsize(i+1).y());
        predictDataROI.append(curPredictDataROI);
    }

    // update the list
    for(int i=0; i<ui->ControlColorMapRecognition->ROIStore.size(); i++){
        ui->ControlNVRecListNVCenter->addItem(QString::number(i+1) + ": position (" + QString::number(predictDataROI[i][0]) + "/" + QString::number(predictDataROI[i][1]) + ")mum, Bounding Box (" + QString::number(predictDataROI[i][2]*1000.) + "/" + QString::number(predictDataROI[i][3]*1000.) + ")nm");
    }
}

// removes the last ROI
void Control::on_ControlNVRecRemoveLastROI_clicked()
{
    ui->ControlColorMapRecognition->removeROI();
    ui->ControlColorMapRecognition->replot();

    ui->ControlColorMapLearning->removeROI();
    ui->ControlColorMapLearning->replot();
}

// guide how to delete the selected ROI
void Control::on_ControlNVRecRemoveSelectedROI_clicked()
{
    QMessageBox removeSelcROI;
    removeSelcROI.setText("To delete the selected ROI, select it and right click on it. Select 'delete ROI' to delete it.");
    removeSelcROI.exec();
}

// removes all ROI's
void Control::on_ControlNVRecRemoveAllROI_clicked()
{
    ui->ControlColorMapRecognition->removeAllROIs(true);
    ui->ControlColorMapRecognition->replot();

    ui->ControlColorMapLearning->removeAllROIs(true);
    ui->ControlColorMapLearning->replot();
}

// add a ROI at a certain position
void Control::on_ControlNVRecAddROI_clicked()
{
    if(!dataOfFile_var->kCountsPerSec.isEmpty()){
        // add a ROI at the middle of the colormap with an heigth and wide of 300nm
        ui->ControlColorMapRecognition->addROI({dataOfFile_var->pos_X[0],dataOfFile_var->pos_Y[0]}, 0.3, 0.3);
        ui->ControlColorMapRecognition->enableROILegend(ui->ControlNVRecEnableROILegend->isChecked());
        ui->ControlColorMapRecognition->replot();

        ui->ControlColorMapLearning->addROI({dataOfFile_var->pos_X[0],dataOfFile_var->pos_Y[0]}, 0.3, 0.3);
        ui->ControlColorMapLearning->enableROILegend(ui->ControlNVRecEnableROILegend->isChecked());
        ui->ControlColorMapLearning->replot();
    }
}

//#######################################################################
//########################### Edit ROI's: END ###########################
//#######################################################################



//#######################################################################
//####################### Learning Process: START #######################
//#######################################################################

// load folder with learning data
void Control::on_ControlLearnLoadFolder_clicked()
{
    // delete data of the prevous file
    dataOfFile_var->kCountsPerSec.clear();
    dataOfFile_var->resolution.clear();
    dataOfFile_var->nb_pixel_X.clear();
    dataOfFile_var->nb_pixel_Y.clear();
    dataOfFile_var->pos_X.clear();
    dataOfFile_var->pos_Y.clear();

    // delete predicted ROI's
    predictDataROI.clear();

    // delete all rectangles
    ui->ControlColorMapRecognition->removeAllROIs(true);

    // delete the list widget
    ui->ControlNVRecListNVCenter->clear();

    QString folderName = QFileDialog::getExistingDirectory(this, "Open directory", "E:/data", QFileDialog::ShowDirsOnly);


    if(!folderName.isEmpty()){
        QDir dir3D = folderName;
        dir3D.setNameFilters(QStringList()<<"*.dat");
        // Only stops when all files with the extension '*.dat' have been read and saved
        foreach(QFileInfo var, dir3D.entryInfoList()){
            if(var.isFile()){
                loadDataFromFile(var.absoluteFilePath());
            }
        }
        // show the first image data in the colormap
        showData();
    }

}

// load single image as learning data (for documantation see 'on_ControlNVRecLoad2DScan_clicked()')
void Control::on_ControlLearnLoadImage_clicked()
{
    on_ControlNVRecLoad2DScan_clicked();
}

// disable all buttons which can't be used during learn process
void Control::enabledButtons(bool state)
{
    // Learning Tab
    ui->ControlLearnStartLearningprocess->setEnabled(state);
    ui->ControlLearnSaveLearnParameters->setEnabled(state);
    ui->ControlLearnDeleteNetwork->setEnabled(state);
    ui->ControlNVRecPredict->setEnabled(state);
    ui->ControlLearnGD->setEnabled(state);
    ui->ControlRecNNType->setEnabled(state);
    ui->ControlLearnNNType->setEnabled(state);
}

// for the learning process, the data that is to be used must be processed beforehand.
// this can be done by the AI ​​or by hand.The following function enables such a preparation using another GUI
void Control::on_ControlLearnPrepareData_clicked()
{
    // check if a file is loaded
    if (dataOfFile_var->kCountsPerSec.isEmpty()){
        QMessageBox emptyVector;
        emptyVector.setText("Please load data to prepare!");
        emptyVector.exec();
    }else{
        // creates new object (preparelearningdata class)
        prepareLearningData *prepareLearningData_var = new prepareLearningData();

        // create a vector with max and min of x and y (needed for the learning window)
        QVector<double> range;
        range.append(dataOfFile_var->pos_X[0] - round(dataOfFile_var->nb_pixel_X[0]/2)*dataOfFile_var->resolution[0]);
        range.append(dataOfFile_var->pos_X[0] + round(dataOfFile_var->nb_pixel_X[0]/2)*dataOfFile_var->resolution[0]);
        range.append(dataOfFile_var->pos_Y[0] - round(dataOfFile_var->nb_pixel_Y[0]/2)*dataOfFile_var->resolution[0]);
        range.append(dataOfFile_var->pos_Y[0] + round(dataOfFile_var->nb_pixel_Y[0]/2)*dataOfFile_var->resolution[0]);

        // transfer of file data
        prepareLearningData_var->setLearnData(dataOfFile_var->kCountsPerSec, dataOfFile_var->resolution, dataOfFile_var->nb_pixel_X, dataOfFile_var->nb_pixel_Y,"C:/Users/plate/OneDrive/ControlProgram/MasterProgramm/CNN/");
        // transfer of range and predicted data (if available)
        prepareLearningData_var->setROIData(predictDataROI, range);

        prepareLearningData_var->show();
    }
}

// starts the learning process
void Control::on_ControlLearnStartLearningprocess_clicked()
{
    // ann with bias
    if(ui->ControlLearnNNType->currentIndex() == 0){
        ann_var->stopLearning = false;

        // learning process started depending on the choosen gradient decent
        // stochastic gradient decent
        if(ui->ControlLearnGD->currentIndex() == 0){
            ui->ControlLearnCurrentNb->setText(QString::number(0) + "/" + QString::number(ui->ControlLearnNbImages->value()));
            std::thread SGB(&CNN::learnNN_SGB, ann_var, ui->ControlLearnEpochs->value(), ui->ControlLearnNbImages->value(), ui->ControlLearnLearningRate->value());
            SGB.detach();
        // mini batch gradient decent (recommended)
        }else if (ui->ControlLearnGD->currentIndex() == 1) {
            ui->ControlLearnCurrentNb->setText(QString::number(0) + "/" + QString::number(ui->ControlLearnNbPackages->value()));
            std::thread SGBD(&CNN::learnNN_MBGD, ann_var, ui->ControlLearnEpochs->value(), ui->ControlLearnNbPackages->value(), ui->ControlLearnNbPerPackage->value(), ui->ControlLearnLearningRate->value());
            SGBD.detach();
        }
    // ann without bias
    }else if(ui->ControlLearnNNType->currentIndex() == 1){
        annwoBias_var->stopLearning = false;

        // learning process started depending on the choosen gradient decent
        // stochastic gradient decent
        if(ui->ControlLearnGD->currentIndex() == 0){
            ui->ControlLearnCurrentNb->setText(QString::number(0) + "/" + QString::number(ui->ControlLearnNbImages->value()));
            std::thread SGB(&CNN::learnNN_SGB, annwoBias_var, ui->ControlLearnEpochs->value(), ui->ControlLearnNbImages->value(), ui->ControlLearnLearningRate->value());
            SGB.detach();
        // mini batch gradient decent (recommended)
        }else if (ui->ControlLearnGD->currentIndex() == 1) {
            ui->ControlLearnCurrentNb->setText(QString::number(0) + "/" + QString::number(ui->ControlLearnNbPackages->value()));
            std::thread SGBD(&CNN::learnNN_MBGD, annwoBias_var, ui->ControlLearnEpochs->value(), ui->ControlLearnNbPackages->value(), ui->ControlLearnNbPerPackage->value(), ui->ControlLearnLearningRate->value());
            SGBD.detach();
        }
    // cnn with bias
    }else if(ui->ControlLearnNNType->currentIndex() == 2){
        cnn_var->stopLearning = false;

        // learning process started depending on the choosen gradient decent
        // stochastic gradient decent
        if(ui->ControlLearnGD->currentIndex() == 0){
            ui->ControlLearnCurrentNb->setText(QString::number(0) + "/" + QString::number(ui->ControlLearnNbImages->value()));
            std::thread SGB(&CNN::learnNN_SGB, cnn_var, ui->ControlLearnEpochs->value(), ui->ControlLearnNbImages->value(), ui->ControlLearnLearningRate->value());
            SGB.detach();
        // mini batch gradient decent (recommended)
        }else if (ui->ControlLearnGD->currentIndex() == 1) {
            ui->ControlLearnCurrentNb->setText(QString::number(0) + "/" + QString::number(ui->ControlLearnNbPackages->value()));
            std::thread SGBD(&CNN::learnNN_MBGD, cnn_var, ui->ControlLearnEpochs->value(), ui->ControlLearnNbPackages->value(), ui->ControlLearnNbPerPackage->value(), ui->ControlLearnLearningRate->value());
            SGBD.detach();
        }
    // cnn without bias
    }else if(ui->ControlLearnNNType->currentIndex() == 3){
        cnnwoBias_var->stopLearning = false;

        // learning process started depending on the choosen gradient decent
        // stochastic gradient decent
        if(ui->ControlLearnGD->currentIndex() == 0){
            ui->ControlLearnCurrentNb->setText(QString::number(0) + "/" + QString::number(ui->ControlLearnNbImages->value()));
            std::thread SGB(&CNN::learnNN_SGB, cnnwoBias_var, ui->ControlLearnEpochs->value(), ui->ControlLearnNbImages->value(), ui->ControlLearnLearningRate->value());
            SGB.detach();
        // mini batch gradient decent (recommended)
        }else if (ui->ControlLearnGD->currentIndex() == 1) {
            ui->ControlLearnCurrentNb->setText(QString::number(0) + "/" + QString::number(ui->ControlLearnNbPackages->value()));
            std::thread SGBD(&CNN::learnNN_MBGD, cnnwoBias_var, ui->ControlLearnEpochs->value(), ui->ControlLearnNbPackages->value(), ui->ControlLearnNbPerPackage->value(), ui->ControlLearnLearningRate->value());
            SGBD.detach();
        }
    }

    enabledButtons(false);
}

// save the new weights after learning
void Control::on_ControlLearnSaveLearnParameters_clicked()
{
    // ann with bias
    if(ui->ControlLearnNNType->currentIndex() == 0){
        ann_var->saveNeuralNetwork();
    // ann without bias
    }else if(ui->ControlLearnNNType->currentIndex() == 1){
        annwoBias_var->saveNeuralNetwork();
    // cnn with bias
    }else if(ui->ControlLearnNNType->currentIndex() == 2){
        cnn_var->saveNeuralNetwork();
    // cnn without bias
    }else if(ui->ControlLearnNNType->currentIndex() == 3){
        cnnwoBias_var->saveNeuralNetwork();
    }
}

// delete the current selected CNN (not recommended!!)
void Control::on_ControlLearnDeleteNetwork_clicked()
{
    // ann with bias
    if(ui->ControlLearnNNType->currentIndex() == 0){
        ann_var->resetNeuralNetwork();
    // ann without bias
    }else if(ui->ControlLearnNNType->currentIndex() == 1){
        annwoBias_var->resetNeuralNetwork();
    // cnn with bias
    }else if(ui->ControlLearnNNType->currentIndex() == 2){
        cnn_var->resetNeuralNetwork();
    // cnn without bias
    }else if(ui->ControlLearnNNType->currentIndex() == 3){
        cnnwoBias_var->resetNeuralNetwork();
    }
}

// stop learning process
void Control::on_ControlLearnStopLearningprocess_clicked()
{
    // ann with bias
    if(ui->ControlLearnNNType->currentIndex() == 0){
        ann_var->stopLearning = true;
    // ann without bias
    }else if(ui->ControlLearnNNType->currentIndex() == 1){
        annwoBias_var->stopLearning = true;
    // cnn with bias
    }else if(ui->ControlLearnNNType->currentIndex() == 2){
        cnn_var->stopLearning = true;
    // cnn without bias
    }else if(ui->ControlLearnNNType->currentIndex() == 3){
        cnnwoBias_var->stopLearning = true;
    }

    QMessageBox stop;
    stop.setText("learning process stopped");
    stop.exec();
}

// help be choosing the gradient decent
void Control::on_ControlLearnGDHelp_clicked()
{
    QMessageBox help;
    help.setText("Stochastic Gradient Descent:\nUse one (randomly picked) sample for a forward pass and than adjust weights\n \nMini Batch Gradient Descent:\nUse a batch of (randomly picked) samples for a forward pass and than adjust weights");
    help.exec();
}

// update current Number of package
void Control::update_ControlLearnCurrentNb(int curNb)
{
    ui->ControlLearnCurrentNb->setText(QString::number(curNb) + "/" + QString::number(ui->ControlLearnNbPackages->value()));
}

//#######################################################################
//######################## Learning Process: END ########################
//#######################################################################



//#######################################################################
//###################### CNN/ANN Predicting: START ######################
//#######################################################################

// use the data of the current colormap for an CNN/ANN prediction
void Control::on_ControlNVRecPredict_clicked()
{
    if(dataOfFile_var->kCountsPerSec.size() != 0){
        // calls function which searches for NV centers depending on the Neural Network in the colormap and then saves them in predictDataROI
        // ann with bias
        if(ui->ControlRecNNType->currentIndex() == 0){
            predictDataROI = ann_var->predicted(dataOfFile_var->kCountsPerSec[0], dataOfFile_var->nb_pixel_X[0], dataOfFile_var->nb_pixel_Y[0], (int)(dataOfFile_var->resolution[0]*1000));
        // ann without bias
        }else if(ui->ControlRecNNType->currentIndex() == 1){
            predictDataROI = annwoBias_var->predicted(dataOfFile_var->kCountsPerSec[0], dataOfFile_var->nb_pixel_X[0], dataOfFile_var->nb_pixel_Y[0], (int)(dataOfFile_var->resolution[0]*1000));
        // cnn with bias
        }else if(ui->ControlRecNNType->currentIndex() == 2){
            predictDataROI = cnn_var->predicted(dataOfFile_var->kCountsPerSec[0], dataOfFile_var->nb_pixel_X[0], dataOfFile_var->nb_pixel_Y[0], (int)(dataOfFile_var->resolution[0]*1000));
        // cnn without bias
        }else if(ui->ControlRecNNType->currentIndex() == 3){
            predictDataROI = cnnwoBias_var->predicted(dataOfFile_var->kCountsPerSec[0], dataOfFile_var->nb_pixel_X[0], dataOfFile_var->nb_pixel_Y[0], (int)(dataOfFile_var->resolution[0]*1000));
        }
        /* predictDataROI[*][0]: x-position of the predicted NV center
         * predictDataROI[*][1]: y-position of the predicted NV center
         * predictDataROI[*][2]: wide of the bounding box
         * predictDataROI[*][3]: height of the bounding box
         */

        if(!predictDataROI.isEmpty()){
            // deletes the list of the previous guessed NV centers
            ui->ControlNVRecListNVCenter->clear();
            // for each new guess from the CNN, the previous guess in form of ROI's must be deleted
            if(!ui->ControlColorMapRecognition->ROIStore.isEmpty()){
                ui->ControlColorMapRecognition->removeAllROIs(true);
            }

            // the CNN does not work with the position of the scan on the diamond. The first pixel (bottom right) is in the 0/0 position for the CNN.
            // therefore, the positions on the respective scan must be determined. For 0/0 applies:
            double xStart = (dataOfFile_var->pos_X[0] - round(dataOfFile_var->nb_pixel_X[0]/2)*dataOfFile_var->resolution[0]);
            double yStart = (dataOfFile_var->pos_Y[0] - round(dataOfFile_var->nb_pixel_Y[0]/2)*dataOfFile_var->resolution[0]);

            for(int i=0; i<predictDataROI.size(); i++){
                // for simpler calculations, the CNN uses nanometers instead of micrometers. Therefore, you have to convert from nanometers to micrometers.
                predictDataROI[i][0]=xStart + predictDataROI[i][0]/1000.;
                predictDataROI[i][1]=yStart + predictDataROI[i][1]/1000.;
                predictDataROI[i][2]=predictDataROI[i][2]/1000.;
                predictDataROI[i][3]=predictDataROI[i][3]/1000.;

                // creates ROI on the colormap
                ui->ControlColorMapRecognition->addROI({predictDataROI[i][0], predictDataROI[i][1]}, predictDataROI[i][2], predictDataROI[i][3]);
                ui->ControlColorMapLearning->addROI({predictDataROI[i][0], predictDataROI[i][1]}, predictDataROI[i][2], predictDataROI[i][3]);

                // creates a list with all guessed NV centers (ROI number, position and bounding box)
                ui->ControlNVRecListNVCenter->addItem(QString::number(ui->ControlColorMapRecognition->ROInumber()) + ": position (" + QString::number(predictDataROI[i][0]) + "/" + QString::number(predictDataROI[i][1]) + ")mum, Bounding Box (" + QString::number(predictDataROI[i][2]*1000) + "/" + QString::number(predictDataROI[i][3]*1000) + ")nm");

            }

        }else{
            std::cout << "No NV center found!" << std::endl;
        }

        // default to false to get a better overview
        ui->ControlColorMapRecognition->enableROILegend(false);
        ui->ControlColorMapLearning->enableROILegend(false);

        ui->ControlColorMapRecognition->replot();
        ui->ControlColorMapLearning->replot();
    }else{
        QMessageBox noImage;
        noImage.setText("Please load a picture to predict!");
        noImage.exec();
    }
}

// colors the ROI selected in list
void Control::on_listItem_clicked(QListWidgetItem *item){
    //look for the selected item in the list
    for(int i=0; i<ui->ControlColorMapRecognition->ROIStore.size(); i++){
        if(ui->ControlNVRecListNVCenter->item(i) == item){
            // if the item exists, look for the appropriate ROI
            for(int j=0; j<ui->ControlColorMapRecognition->ROIStore.size(); j++){
                ui->ControlColorMapRecognition->ROIStore[j]->setSelected(false);
            }

            // color the ROI as if it was selected
            ui->ControlColorMapRecognition->ROIStore[i]->setSelected(true);
            ui->ControlColorMapRecognition->replot();
            break;
        }
    }
}

// updates the list after changing ROI position/size
// the list vector is used for Control or learning
void Control::on_ControlNVRecConfirmList_clicked()
{
    on_ControlNVRecConfirmEditing_clicked();
}

//#######################################################################
//####################### CNN/ANN Predicting: END #######################
//#######################################################################



//#######################################################################
//########################## Synchronize: START #########################
//#######################################################################

// Synchronize the ROI's of the learning colormap with the recognition colormap
void Control::on_ControlLearningSynchronize_clicked()
{
    QVector<double> curPredictDataROI;

    // delete elements of the current positions, colormaps and list
    predictDataROI.clear();
    ui->ControlNVRecListNVCenter->clear();

    if(!ui->ControlColorMapRecognition->ROIStore.isEmpty()){
        ui->ControlColorMapRecognition->removeAllROIs(true);
    }

    // gets the new positions using qcustomplot
    for(int i=0; i<ui->ControlColorMapLearning->ROInumber(); i++){
        curPredictDataROI.clear();
        curPredictDataROI.append(ui->ControlColorMapLearning->getROIposition(i+1).x());
        curPredictDataROI.append(ui->ControlColorMapLearning->getROIposition(i+1).y());
        curPredictDataROI.append(ui->ControlColorMapLearning->getROIsize(i+1).x());
        curPredictDataROI.append(ui->ControlColorMapLearning->getROIsize(i+1).y());
        predictDataROI.append(curPredictDataROI);
    }

    // update the list
    for(int i=0; i<ui->ControlColorMapLearning->ROIStore.size(); i++){
        ui->ControlNVRecListNVCenter->addItem(QString::number(i+1) + ": position (" + QString::number(predictDataROI[i][0]) + "/" + QString::number(predictDataROI[i][1]) + ")mum, Bounding Box (" + QString::number(predictDataROI[i][2]*1000.) + "/" + QString::number(predictDataROI[i][3]*1000.) + ")nm");
    }

    // update recognition colormap
    for(int i=0; i<predictDataROI.size(); i++){
        ui->ControlColorMapRecognition->addROI({predictDataROI[i][0], predictDataROI[i][1]}, predictDataROI[i][2], predictDataROI[i][3]);
    }
    ui->ControlColorMapRecognition->enableROILegend(false);
    ui->ControlColorMapRecognition->replot();
}

// Synchronize the ROI's of the recognition colormap with the learning colormap
void Control::on_ControlRecognitionSynchronize_clicked()
{
    QVector<double> curPredictDataROI;

    // delete elements of the current positions, colormaps and list
    predictDataROI.clear();
    ui->ControlNVRecListNVCenter->clear();

    if(!ui->ControlColorMapLearning->ROIStore.isEmpty()){
        ui->ControlColorMapLearning->removeAllROIs(true);
    }

    // gets the new positions using qcustomplot
    for(int i=0; i<ui->ControlColorMapRecognition->ROInumber(); i++){
        curPredictDataROI.clear();
        curPredictDataROI.append(ui->ControlColorMapRecognition->getROIposition(i+1).x());
        curPredictDataROI.append(ui->ControlColorMapRecognition->getROIposition(i+1).y());
        curPredictDataROI.append(ui->ControlColorMapRecognition->getROIsize(i+1).x());
        curPredictDataROI.append(ui->ControlColorMapRecognition->getROIsize(i+1).y());
        predictDataROI.append(curPredictDataROI);
    }

    // update the list
    for(int i=0; i<ui->ControlColorMapRecognition->ROIStore.size(); i++){
        ui->ControlNVRecListNVCenter->addItem(QString::number(i+1) + ": position (" + QString::number(predictDataROI[i][0]) + "/" + QString::number(predictDataROI[i][1]) + ")mum, Bounding Box (" + QString::number(predictDataROI[i][2]*1000.) + "/" + QString::number(predictDataROI[i][3]*1000.) + ")nm");
    }

    // update learning colormap
    for(int i=0; i<predictDataROI.size(); i++){
        ui->ControlColorMapLearning->addROI({predictDataROI[i][0], predictDataROI[i][1]}, predictDataROI[i][2], predictDataROI[i][3]);
    }
    ui->ControlColorMapLearning->enableROILegend(false);
    ui->ControlColorMapLearning->replot();
}

//#######################################################################
//########################### Synchronize: END ##########################
//#######################################################################



//#######################################################################
//######################## Measure Routine: START #######################
//#######################################################################

// set measure states for...
// ...2D Scan
void Control::state2DScan(bool state)
{
    curState2DScan = state;
}

// ... magnet stage
void Control::stateMagnetStage(bool state)
{
    curMagnetState = state;
    if(state){
        std::cout << "true 1" << std::endl;
    }else{
        std::cout << "false" << std::endl;
    }
}

// refocus on the NV center
void Control::refocus()
{
    emit AIuse(true);
    // FIRST: set scan parameter and start the scan
    // set scan parameter
    int nbPixelX = 15, nbPixelY = 15;
    int stepsizeX = 100, stepsizeY = 100;

    emit set2DScanParameters(nbPixelX, nbPixelY, stepsizeX, stepsizeY);

    QString X_str = "X";
    QString Y_str = "Y";

    // fill wavGenTableLine
    emit setWavGenTableLine(0, nbPixelX, nbPixelY, X_str);
    emit setWavGenTableLine(1, nbPixelX, nbPixelY, Y_str);

    // start the scan
    emit checkLaser();
    emit setPermWord();
    curState2DScan = false;
    emit start2DScan();
    // wait until the measurement is done
    while(!curState2DScan){
        Sleep(250);
    }

    // get current position
    std::vector<double> curPositions = emit getPositions();
    // get current counts vector
    std::vector<double> counts = emit getCounts();
    //std::cout << "X: " << curPositions[0] << ", Y: " << curPositions[1] << ", Z: " << curPositions[2] << std::endl;
    //std::cout << "counts size: " << counts.size() << std::endl;
    //std::cout << "counts[0]: " << counts[0] << std::endl;

    // SECOND: predict the position of the NV Center
    QVector<QVector<double>> guessPositions;
    QVector<double> inputDataWOSnake;

    // undo the snake pattern
    for(int yIndex=0; yIndex<nbPixelY; ++yIndex){
        for(int xIndex = (yIndex%2==0? 0: nbPixelY-1); xIndex>=0 && xIndex<nbPixelY; yIndex%2==0? ++xIndex: --xIndex){
            inputDataWOSnake.append(counts[(yIndex*nbPixelX)+xIndex]);
        }
    }

    // predicted the positions using the CNN
    guessPositions = cnnwoBias_var->predicted(inputDataWOSnake, nbPixelX, nbPixelY, stepsizeX);

    // calculate global positions
    guessPositions[0][0] += curPositions[0]-((nbPixelX/2.)*stepsizeX);
    guessPositions[0][1] += curPositions[1]-((nbPixelY/2.)*stepsizeY);

    //std::cout << guessPositions[0][0] << ", " << guessPositions[0][1] << std::endl;

    // THIRD: set the X-, Y- position
    emit newPosition(guessPositions[0][0], guessPositions[0][1], curPositions[2]);
    /*std::cout << "Aufmerksamkeit!" << std::endl;
    std::cout << "X: " << guessPositions[0][0] << std::endl;
    std::cout << "Y: " << guessPositions[0][1] << std::endl;
    std::cout << "Z: " << curPositions[2] << std::endl;

    std::vector<double> curNewPositions = emit getPositions();
    std::cout << "X: " << curNewPositions[0] << ", Y: " << curNewPositions[1] << ", Z: " << curNewPositions[2] << std::endl;
*/
    // FOURTH: set the Z- position
    // scan 5 images (5x5) with different Z- positions and compere there counts
    emit set2DScanParameters(5, 5, 50, 50);

    QVector<double> totalCounts(5);
    double curNbCounts;
    std::vector<double> curCounts;
    int curZposition = curPositions[2]-200;

    for(int i=0; i<5; i++){
        emit newPosition(guessPositions[0][0], guessPositions[0][1], curZposition);
        // wait to be sure of being at the right position
        Sleep(500);
        // start 2D scan
        curState2DScan = false;
        emit start2DScan();
        // wait until the measurement is done
        while(!curState2DScan){
            Sleep(250);
        }

        curNbCounts = 0;
        curCounts = emit getCounts();
        // sum up all counts
        for(int j=0; j<curCounts.size(); j++){
            curNbCounts += curCounts[j];
        }
        //std::cout << "nb Counts: " << curNbCounts << std::endl;
        totalCounts[i] = curNbCounts;
        curCounts.clear();

        curZposition += 100;
    }

    int bestZposition = std::max_element(totalCounts.begin(), totalCounts.end())-totalCounts.begin();
    int guessZposition = curPositions[2] - 300 + (bestZposition*150);

    emit newPosition(guessPositions[0][0], guessPositions[0][1], guessZposition);

    //std::cout << "best z position: " << guessZposition << std::endl;

    emit AIuse(false);

    std::cout << "refocused!" << std::endl;
}

// save count data (2D Scan, ODMR, Rabi)
void Control::saveMeasureRoutineData(QVector<QVector<double>> dataCounts, QVector<double> dataInformation, QString pathAndFileName){
    // create a header in the file and name the fitparameter
        QVector<QString> fileInformation;
        if(pathAndFileName.contains("2DScan")){
            fileInformation.append("Stepsize X/Stepsize Y/X Px number/Y Px number/Pos X/Pos Y/Pos Z");
            fileInformation.append(QString::number(dataInformation[0]) +"\t"+ QString::number(dataInformation[1]) +"\t"+ QString::number(dataInformation[2]) +"\t"+ QString::number(dataInformation[3]) +"\t"+ QString::number(dataInformation[4]) +"\t"+ QString::number(dataInformation[5]) +"\t"+ QString::number(dataInformation[6]));
            fileInformation.append("X	 Y	 Counts/second");
            fileInformation.append("");
        }else if(pathAndFileName.contains("ODMRScan")){
            std::cout << "work in progress" << std::endl;
        }else if(pathAndFileName.contains("Rabi")){
            std::cout << "work in progress" << std::endl;
            // hier auch FFT??
        }

        // create a file with header and data
        QFile file(pathAndFileName);

        file.open(QIODevice::WriteOnly);
            QTextStream out(&file);
            for(int i=0; i<fileInformation.size(); i++){
                out << fileInformation[i] << "\n";
            }
            for(int i=0; i<dataInformation[2]; i++){
                for(int j=0; j<dataInformation[3]; j++){
                    // x-position
                    if(i%2==0)
                    {
                        out << dataInformation[4]-(int)(((dataInformation[2]-1)*dataInformation[0])/2.0)+(j*dataInformation[0]) << "\t";
                    }else{
                        out << dataInformation[4]-(int)(((dataInformation[2]-1)*dataInformation[0])/2.0)+((dataInformation[2]-(j+1))*dataInformation[0]) << "\t";
                    }
                    // y-position
                    out << dataInformation[5]-(int)(((dataInformation[3]-1)*dataInformation[1])/2.0)+(i*dataInformation[1]) << "\t";
                    // counts/second
                    out << dataCounts[0][i*dataInformation[2]+j]*1000 << "\n";
                }
                //out << "\n";
            }

        file.close();
}

// measure routines for...
// ... 2D Scan
void Control::start_2DScan_chrackterization()
{

}

// ... ODMR
void Control::start_ODMR_chrackterization()
{
    // set AWG mode to triggered
    if(!ODMRStates[0]){
        std::cout << "set triggerd mode" << std::endl;
        emit setAWGTriggerMode("Triggered");
        Sleep(500);
        ODMRStates[0] = true;

    // move the magnet stage to the position 0 deg, 0 deg, 50mm (small motor, big motor, distance motor)
    }else if(!ODMRStates[1]){
        std::cout << "start moving magnetstage" << std::endl;
        curMagnetState = false;
        emit setMagnetStage(49.63, 0, 0); // distance motor (not 50 because the scale ist between 0 and 49.63), small motor, big motor
        ODMRStates[1] = true;

    // upload scan data to the AWG
    }else if(ODMRStates[2]==false && curMagnetState==true){
        std::cout << "set scan data" << std::endl;
    // set scan data
    int loopNb = 100;
    double exitTime = 1000.;
    double freqStart = 2600.;
    double freqEnd = 3200.;
    int stepNb = 200;
    ODMRStates[2] = true;
    // stop timer if the measure routine is done
    }else if(ODMRStates[2]){
        std::cout << "QTimer stops" << std:: endl;
        ODMRScanTimer->stop();
    }else{
        // do nothing!
        std::cout << "wait" << std::endl;
    }
    // start to upload data


    // start measurement

    // read measure values

    // save data in a file
    //std::cout << "ODMR: work in progress" << std::endl;

    // prepare cw ODMR
    /*int loopNb = 100;
    double excitTime = 1000.;
    double freqStart = 2400.;
    double freqEnd = 3400.;
    int stepNb = 500;*/

    /*
    int loopNb = 100;
    double excitTime = 1000.;
    double freqStart = 2800.;
    double freqEnd = 2900.;
    int stepNb = 100;
    curODMRUploadState = true;
    emit loadCwODMR(loopNb, excitTime, freqStart, freqEnd, stepNb);
    while(curODMRUploadState){
        Sleep(500);
    }

    // start cw ODMR
    curCwODMRState = true;
    emit startCwODMR(loopNb);
    while(curCwODMRState){
        Sleep(500);
    }*/

  //  std::cout << "ende gut alles gut" << std::endl;
}

// ... NV axis
//#######################################################################
//######################### Measure Routine: END ########################
//#######################################################################


void Control::on_test_clicked()
{
    QVector<double> test;
    test = cnnwoBias_var->rescaleSizeInput(dataOfFile_var->kCountsPerSec[0],dataOfFile_var->nb_pixel_X[0], dataOfFile_var->nb_pixel_Y[0]);

    dataOfFile_var->kCountsPerSec[0] = test;
    dataOfFile_var->nb_pixel_X[0] = 2*dataOfFile_var->nb_pixel_X[0]-1;
    dataOfFile_var->nb_pixel_Y[0] = 2*dataOfFile_var->nb_pixel_Y[0]-1;
    dataOfFile_var->resolution[0] = dataOfFile_var->resolution[0]/2.;

    showData();
}

// update the state of a 2D scan
void Control::stateNVMeasurment(bool state){
    curMeasurmentState = state;
}


// update the state if the ODMR data is uploaded
void Control::stateODMRUpload(bool state){
    curODMRUploadState = state;
}

void Control::stateCwODMR(bool state){
    curCwODMRState = state;
}

void Control::start_chrackterization(QString scanType)
{
    emit AIuse(true);
    // prepare Laser
    emit checkLaser();
    emit setPermWord();
    Sleep(1000);

    // save current positions
    std::vector<double> curPositions;
    // save current counts
    std::vector<double> counts;

    // scan all possible NV centers
    for(int i=0; i<ui->ControlColorMapRecognition->ROIStore.size(); i++){
        // FIRST: 2D Scan
        // create a folder for the NV center
        QDir().mkdir(curPath+"/Sora"+"/ROI"+QString::number(i+1));

        // go to the position of the ROI
        curPositions = emit getPositions();
        emit newPosition(predictDataROI[i][0]*1000, predictDataROI[i][1]*1000, curPositions[0]);
        Sleep(500);
        std::cout << "X: " << predictDataROI[i][0]*1000 << std::endl;
        std::cout << "Y: " << predictDataROI[i][1]*1000 << std::endl;
        std::cout << "Z: " << curPositions[2] << std::endl;


        // focus on the NV center (X, Y, Z)
        refocus();

        // start a 2D scan
        emit set2DScanParameters(20, 20, 40, 40);
        Sleep(500);
        curState2DScan = false;
        emit start2DScan();
        // wait until the measurement is done
        while(!curState2DScan){
            Sleep(250);
        }
        std::cout << "measurement done!" << std::endl;

        // get values for the measured counts
        counts = emit getCounts();
        Sleep(500);
        std::cout << "size of data(400): " << counts.size() << std::endl;
        if(counts.size()>0){
            std::cout << counts[0] << std::endl;
        }

        // save data in a file
        QVector<QVector<double>> dataCounts;
        dataCounts.append(QVector<double>::fromStdVector(counts));
        QVector<double> dataInformation = {40, 40, 20, 20, predictDataROI[i][0]*1000, predictDataROI[i][1]*1000, curPositions[2]};
        QString pathAndFileName = curPath+"/Sora"+"/ROI"+QString::number(i+1)+"/2DScan.dat";
        saveMeasureRoutineData(dataCounts, dataInformation, pathAndFileName);


        // SECOND: ODMR Scan
        // if the scan type is equal to ODMRScan the ROI gets further investigated using ODMR
        if(scanType == "ODMRScan"){
            ODMRStates.resize(5);
            ODMRStates.fill(false);
            // start timer...
            start_ODMR_chrackterization();
        }


        // THRID: NV axis Scan
        // check if the ROI is a NV center or not (if not the scanType magFieldScan and rabiScan is't possible)
        // if the scan type is equal to magFieldScan the direction of the NV axis is determined
        if(scanType == "magFieldScan"){
            std::cout << "mag Field: work in progress" << std::endl;
        }


        // FOURTH: Rabi Scan
        // if the scan type is equal to rabiScan the rabi time is determined
        if(scanType == "rabiScan"){
            std::cout << "Rabi: work in progress" << std::endl;
        }

    }
    /*####
    int centerPositionROIX, centerPositionROIY, centerPositionROIZ;

    // set the wavetable and wait until done
    emit setWavetabel(20,20,40,40,1,2);
    Sleep(2000);

    for(int i=0; i<ui->ControlColorMapRecognition->ROIStore.size(); i++){
        // FIRST: find ROI's and scan the parameter
        QPointF position = ui->ControlColorMapRecognition->getROIposition(i+1);
        centerPositionROIX= (int)(position.x()*1000);
        centerPositionROIY= (int)(position.y()*1000);
        centerPositionROIZ= (int)(dataOfFile_var->pos_Z[0]*1000);
        curMeasurmentState = true;
        emit startScanNVOnly(centerPositionROIX,centerPositionROIY,centerPositionROIZ);
        while(curMeasurmentState){
            Sleep(500);
        }####*/

        /*
        //SECOND: confirm ROI's as NV centers using ODMR
        // set Magnet to the position 0 deg, 0 deg, 50mm (small motor, big motor, distance motor)
        curMagnetState = true;
        emit setDistanceValue(49.63); // not 50 because the scale ist between 0 and 49.63
        emit setSmallAngleValue(5);
        emit setBigAngleValue(5);
        emit startMagnetstage();
        while(curMagnetState){
            Sleep(500);
        }*/

        // prepare cw ODMR
        /*int loopNb = 100;
        double excitTime = 1000.;
        double freqStart = 2400.;
        double freqEnd = 3400.;
        int stepNb = 500;*/

        /*
        int loopNb = 100;
        double excitTime = 1000.;
        double freqStart = 2800.;
        double freqEnd = 2900.;
        int stepNb = 100;
        curODMRUploadState = true;
        emit loadCwODMR(loopNb, excitTime, freqStart, freqEnd, stepNb);
        while(curODMRUploadState){
            Sleep(500);
        }

        // start cw ODMR
        curCwODMRState = true;
        emit startCwODMR(loopNb);
        while(curCwODMRState){
            Sleep(500);
        }*/

      //  std::cout << "ende gut alles gut" << std::endl;

    //}

    //emit reset parameter like x,y,z as well as define the origin values of the program
    // emit wavetable back to orign values (vllt lösen durch ein signal welches auf button im MainWindow zugreift oder so ähnlich)


    std::cout << "\nSora is finished!" << std::endl;
    emit AIuse(false);
}

bool Control::moveMagnetTest()
{
    //emit setMagnetStage(0, 0, 49.63);
    if(!curMeasure){
        //emit setMagnetStage(49.63, 0, 0);
        std::cout << "1" << std::endl;
        emit setMagnetStage(49.63, 0, 0);
        //std::thread thread_scan_NVOnly(&Control::start_ODMR_chrackterization, this);
        //thread_scan_NVOnly.detach();

        curMeasure = true;
    }
    std::cout << "2" << std::endl;
    if(curMagnetState){
        magStageState->stop();
        std::cout << "done" << std::endl;
        curMeasure = false;
    }
    /*if(curMagnetState){
        return true;
    }else{
        return false;
    }*/
    return false;
}


void Control::on_ControlNVRecStartControl_clicked()
{
    std::cout << "work in progress" << std::endl;
    std::cout << curPath.toStdString() << std::endl;

    if(!QDir(curPath+"/Sora").exists()){
        QDir().mkdir(curPath+"/Sora");
    }

    ODMRStates.resize(5);
    ODMRStates.fill(false);

    curMagnetState = false;
    ODMRScanTimer->start(1000);
    //std::thread thread_scan_NVOnly(&Control::start_ODMR_chrackterization, this);
    //thread_scan_NVOnly.detach();


    /*if(!predictDataROI.isEmpty()){
        // create folder for the current Scan
        bool newFolder = true;
        int index = 1;
        while(newFolder){
            if(!QDir(curPath+"/Sora"+"/Scan"+QString::number(index)).exists()){
                QDir().mkdir(curPath+"/Sora"+"/Scan"+QString::number(index));
                QString curPathToROI = curPath+"/Sora"+"/Scan"+QString::number(index);
                newFolder = false;
            }else{
                index++;
            }
        }

        // save used data image
        QVector<double> dataInformation = {dataOfFile_var->resolution[0], dataOfFile_var->resolution[0], (double)dataOfFile_var->nb_pixel_X[0], (double)dataOfFile_var->nb_pixel_Y[0], dataOfFile_var->pos_X[0], dataOfFile_var->pos_Y[0], dataOfFile_var->pos_Z[0]};
        QString pathAndFileName = curPathToROI+"/global2DScan.dat";
        saveMeasureRoutineData(dataOfFile_var->kCountsPerSec, dataInformation, pathAndFileName);

        // start investing the ROI's
        //start_chrackterization("Scan2D");

        //std::thread thread_start_chrackterization(&Control::start_chrackterization, this, "Scan2D");
        //thread_start_chrackterization.detach();
    }else{
        QMessageBox error;
        error.setText("No ROI's to invest");
        error.exec();
    }*/
    /*
    std::thread thread_scan_NVOnly(&Control::Scan_NVOnly, this);
    thread_scan_NVOnly.detach();
    */
}
