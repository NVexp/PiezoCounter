#ifndef EVALFIT_H
#define EVALFIT_H

#include <QObject>
#include <QVector>

class EvalFit
{
public:
    EvalFit();

    // start fitting dependig on the type (Lorenztian, Gaussian, dampedSine) using NN or alglib
    QVector<double> startFit(QVector<double> xVal, QVector<double> yVal, int epoch, QString type);
    void setUserFitParameter(QVector<double> fitParams);
    // returns maxima of an grid using gaussian
    QVector<double> gridMaximaGaussian(QVector<double> xVal, QVector<double> yVal, QVector<QVector<double>> zVal);

    // fast fourier transformation
    QVector<QVector<double>> FFT(QVector<double> xVal, QVector<double> yVal);
    bool checkOscillation(QVector<double> xVal, QVector<double> yVal);
    // check if and how many peaks are in the spectra
    int nbOfPeaks(QVector<double> xVal, QVector<double> yVal);

    int test(int number);
    // depending if the user or the AI uses the class diffrent outputs are made
    bool AIusage = false;

private:
    // User fit parameter
    QVector<double> userFitParams;

    // guess fit parameters
    QVector<double> guessFitParametersDampedSine(QVector<double> xVal, QVector<double> yVal, int checkpoint);
    QVector<double> guessFitParamsLorentzian(QVector<double> xVal, QVector<double> yVal);
    QVector<double> guessFitParamsDoubleLorentzian(QVector<double> xVal, QVector<double> yVal);
    QVector<double> guessFitParamsGaussian(QVector<double> xVal, QVector<double> yVal);

    // fitting via neural network learning process
    double loss_squere(QVector<double> y_true, QVector<double> y_prediction);
    QVector<double> SGD_dampedSineLearning(QVector<double> xVal, QVector<double> yVal, int epochs, int checkpoint);
    QVector<double> SGD_dampedSine(QVector<double> xVal, QVector<double> yVal, int epochs, int checkpoint);
    QVector<double> SGD_lorentzian(QVector<double> xVal, QVector<double> yVal, int epochs);
    QVector<double> SGD_doubleLorentzian(QVector<double> xVal, QVector<double> yVal, int epochs);

    // fitting via alglib (levenberg marquardt alogrithm)
    QVector<double> LM_dampedSine(QVector<double> xVal, QVector<double> yVal);
    QVector<double> LM_lorentzian(QVector<double> xVal, QVector<double> yVal);
    QVector<double> LM_doubleLorentzian(QVector<double> xVal, QVector<double> yVal);
    QVector<double> LM_gaussian(QVector<double> xVal, QVector<double> yVal);

};

#endif // EVALFIT_H
