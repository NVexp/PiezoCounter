#ifndef CNN_H
#define CNN_H

#include<QVector>
#include<QObject>


class CNN : public QObject
{
     Q_OBJECT
public:
    CNN(QString weight_path, QString type);

    // CNN/ANN Predicting START
    QVector<QVector<double>> predicted(QVector<double> inputData, int nb_pixel_X, int nb_pixel_Y, int resolution);
    // CNN/ANN Predicting END


    // // learn ANN START
    void learnNN_SGB(int epoche, int nb_images, double learning_rate);
    void learnNN_MBGD(int epoche, int nb_packages, int nb_per_package, double learning_rate);
    // if stopLearning is true the learn process stopped
    bool stopLearning = false;
    // // learn ANN END


    void saveNeuralNetwork();
    void resetNeuralNetwork();

    QVector<double> rescaleSizeInput(QVector<double> inputData, int nb_pixel_X, int nb_pixel_Y);

    // just as an test
    QVector<double> getCurData(QVector<double> inputData); //###########################

signals:
    // signals that indicate the learning process
    void changeEpoch(int val);
    void changePackage(int val);
    void finish(bool state);

private:
    //file directory
    QString weight_path;

    // type of neural network:
    // - ann: artificial neural network
    // - cnn: convolutional neural network
    QString type;


    // CNN/ANN Predicting START
    // prepare input
    QVector<double> prepareInput(QVector<double> inputData);
    QVector<double> rescaleInput(QVector<double> inputData, int nb_pixel_X, int nb_pixel_Y);
    //###QVector<double> rescaleSizeInput(QVector<double> inputData, int nb_pixel_X, int nb_pixel_Y);
    // determine output (guess)
    QVector<double> calculateOutput(QVector<double> input);
    // CNN/ANN Predicting START


    //filter
    QVector<QVector<double>> filter1, filter2, filter3, filter4;//, centerFilter;

    //convolution Layer
    QVector<double> afterFilterData;
    // save the position of the element from the max pooling for the learning process
    QVector<QVector<int>> positionMaxPooling;

    // feature Extraction (convolution/ pooling) START
    QVector<double> convolution(QVector<double> input);
    QVector<double> useFilter(QString filter, QVector<double> paddingImageData);
    QVector<double> convolutionPadding(QVector<double> input);
    QVector<double> maxPooling2x2(QVector<double> input);
    // feature Extraction (convolution/ pooling) START


    // classification (new weights/ ANN) START
    void ann();
    // classification (new weights/ ANN) END


    // YOLO algorithm START
    QVector<QVector<double>> YOLO5x5(QVector<double> input);
    QVector<QVector<QVector<double>>> YOLO20x20(QVector<double> input, int nb_pixel_X, int nb_pixel_Y);
    QVector<QVector<double>> nonMaxSuppression(QVector<QVector<double>> input);
    // YOLO algorithm END


    // learning_rate
    double learning_rate;

    // data which are read from files in order to learn the network
    // input = colormap data
    // output = information like position (x/ y) and bounding box (height/ wide)
    QVector<QVector<double>> learnDataInput, learnDataOutput;
    // current input for ann (just classification) or cnn (just feature extraction)
    QVector<double> inputLayer_ann, inputLayer_cnn, y_true;

    // learn ANN/CNN START
    QVector<double> newWeights_outputLayer(QVector<double> hiddenLayerOut);
    QVector<double> newWeights_hiddenLayer2(QVector<double> delta_Etot_h2out);
    QVector<double> newWeights_hiddenLayer1(QVector<double> delta_Etot_h1out);
    QVector<double> weight_sum(QVector<double> input, QVector<QVector<double>> weights, QVector<double> bias);
    QVector<QVector<double>> newFilter_convolutiona1(QVector<double> delta_Output, QVector<int> errorPosition, QVector<QVector<double>> curFilter);
    // learn ANN/CNN END


    // activation functions + derivation START
    QVector<double> sigmoid(QVector<double> weight_sum);
    QVector<double> sigmoid_derivation(QVector<double> input);
    QVector<double> reLu(QVector<double> weight_sum);
    QVector<double> reLu_derivation(QVector<double> input);
    QVector<double> leakyReLu(QVector<double> weight_sum);
    QVector<double> leakyReLu_derivation(QVector<double> input);
    // activation functions + derivation END


    // loss functions START
    double sparse_categorical_crossentropy(QVector<double> y_true, QVector<double> y_predicted);
    QVector<double> sparse_categorical_crossentropy_derivation(QVector<double> y_true, QVector<double> y_predicted);
    double square_error(QVector<double> y_true, QVector<double> y_predicted);
    QVector<double> square_error_derivation(QVector<double> y_true, QVector<double> y_predicted);
    // loss functions END


    // weights and bias of each Layer START
    // first hidden Layer
    QVector<double> hiddenLayer1_out;
    QVector<QVector<double>> hiddenLayer1_weights, delta_hiddenLayer1_weights;
    QVector<double> hiddenLayer1_bias, delta_hiddenLayer1_bias;

    // second hidden Layer
    QVector<double> hiddenLayer2_out;
    QVector<QVector<double>> hiddenLayer2_weights, delta_hiddenLayer2_weights;
    QVector<double> hiddenLayer2_bias, delta_hiddenLayer2_bias;

    // output Layer
    QVector<double> outputLayer_out;
    QVector<QVector<double>>outputLayer_weights, delta_outputLayer_weights;
    QVector<double> outputLayer_bias, delta_outputLayer_bias;
    // weights and bias of each Layer END

    // fill weights, bias and filters if the neural network is reseted
    void fillRandom_NormalDistribution();
    void resetFilters();

    // read and write START
    void readLearnDataFromFile();
    bool readFiltersFromFile(QString fileName, QVector<QVector<double>> cur_filter);
    bool readWeightsBiasFromFile(QString fileNameWeights, QString fileNameBias, int nb_input, int nb_output, QVector<QVector<double>> weights, QVector<QVector<double>> delta_weights, QVector<double> bias);
    void saveWeightsBiasInFile(QString fileNameWeights, QString fileNameBias, QVector<QVector<double>> weights, QVector<double> bias);
    void saveFilterInFile(QString fileName, QVector<QVector<double>> filterData);
    // read and write END

};

#endif // CNN_H
