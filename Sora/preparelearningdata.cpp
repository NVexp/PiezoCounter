#include "preparelearningdata.h"
#include "ui_preparelearningdata.h"

prepareLearningData::prepareLearningData(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::prepareLearningData)
{
    ui->setupUi(this);

    //set title of the window
    this->setWindowTitle("Create learn data");

    // prepare color map
    // configure axis rect:
    ui->PrepLearnDataControlTotalColorMap->axisRect()->setupFullAxesBox(true);
    ui->PrepLearnDataControl5x5ColorMap->axisRect()->setupFullAxesBox(true);

    // set up the QCPColorMap:
    totalColorMap = new QCPColorMap(ui->PrepLearnDataControlTotalColorMap->xAxis, ui->PrepLearnDataControlTotalColorMap->yAxis);
    cellColorMap = new QCPColorMap(ui->PrepLearnDataControl5x5ColorMap->xAxis, ui->PrepLearnDataControl5x5ColorMap->yAxis);


    ui->PrepLearnDataControlTotalColorMap->xAxis->setLabel("x");
    ui->PrepLearnDataControlTotalColorMap->yAxis->setLabel("y");
    ui->PrepLearnDataControl5x5ColorMap->xAxis->setLabel("x");
    ui->PrepLearnDataControl5x5ColorMap->yAxis->setLabel("y");


    // add a color scale to Recognation Tab:
    totalColorScale = new QCPColorScale(ui->PrepLearnDataControlTotalColorMap);
    ui->PrepLearnDataControlTotalColorMap->plotLayout()->addElement(0, 1, totalColorScale); // add it to the right of the main axis rect
    totalColorScale->setType(QCPAxis::atRight); // scale shall be vertical bar with tick/axis labels right (actually atRight is already the default)
    totalColorScale->axis()->setLabel("PhotonCount");

    // add a color scale to Learning Tab:
    cellColorScale = new QCPColorScale(ui->PrepLearnDataControl5x5ColorMap);
    ui->PrepLearnDataControl5x5ColorMap->plotLayout()->addElement(0, 1, cellColorScale); // add it to the right of the main axis rect
    cellColorScale->setType(QCPAxis::atRight); // scale shall be vertical bar with tick/axis labels right (actually atRight is already the default)
    cellColorScale->axis()->setLabel("PhotonCount");


    connect(ui->PrepLearnDataControlTotalColorMap, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(changeCellStateViaMouseclick(QMouseEvent*)));


    /* TODO
     * on_PrepLearnDataControlSaveCurrentCell_clicked (name der Datei ist TEST)
     *
     */
}

prepareLearningData::~prepareLearningData()
{
    // deletes all items when the window is closed
    ui->PrepLearnDataControlTotalColorMap->removeAllROIs(true);
    if(!rectGrid.isEmpty()){
        for(int i=0; i<inputDataScaled.size(); i++){
            for(int j=rectGrid.size(); j>0; j--){
                ui->PrepLearnDataControlTotalColorMap->removeItem(rectGrid[i][j-1]);
                ui->PrepLearnDataControlTotalColorMap->removeItem(rectGridLabel[i][j-1]);
            }
        }
    }
    selectedCellData.clear();
    curSelectedCell=0;

    // delete ui
    delete ui;
}

// receives individual images or entire folders of data and then processes them
void prepareLearningData::setLearnData(QVector<QVector<double>> data, QVector<double> resolution, QVector<int> nbPixelX, QVector<int> nbPixelY, QString path)
{
    // enable the ROI legend (nb of ROI/ wide/ height)
    ui->PrepLearnDataControlTotalColorMap->enableROILegend(false);

    std::cout << "resolution: " << resolution[0] << std::endl;

    // save data to global vectors
    this->inputData = data;
    this->nbPixelX = nbPixelX;
    this->nbPixelY = nbPixelY;
    this->resolution = resolution;
    this->path = path;

    for(int i=0; i<data.size(); i++){
        // if the resolution of an image is bigger than in the range of 35-70 than the resolution has to be scaled to it
        std::cout << "lala: " << data.size() << std::endl;
        while(resolution[i] > 0.07){
            data[i] = rescaleSizeInput(data[i], nbPixelX[i], nbPixelY[i]);
            // to get the new resolution the old one has to divid by two
            resolution[i] = (int)(resolution[i]/2.);
            // the new number of pixels are twice as big - 1
            nbPixelX[i] = 2*nbPixelX[i];//-1
            nbPixelY[i] = 2*nbPixelY[i];//-1
        }
    }
    std::cout << "heir" << std::endl;

    // index describes the current number of the image
    curDataIndex = 0;

    // get nb_imagesParts X and Y from the given images!
    nbCellSectionX.resize(data.size());
    nbCellSectionY.resize(data.size());

    // divides the image into 5x5 px areas
    for(int i=0; i<data.size(); i++){
        nbCellSectionX[i] = ceil((double)nbPixelX[i]/5.);
        nbCellSectionY[i] = ceil((double)nbPixelY[i]/5.);

        std::cout << "nb section X: " << nbCellSectionX[i] << std::endl;
        std::cout << "nb section Y: " << nbCellSectionY[i] << std::endl;
    }

    // creates the number of cells with the associated legend
    rectGrid.resize(data.size());
    rectGridLabel.resize(data.size());
    for(int i=0; i<data.size(); i++){

        // resize the vectors
        rectGrid[i].resize(nbCellSectionX[0]*nbCellSectionY[0]);
        rectGridLabel[i].resize(nbCellSectionX[0]*nbCellSectionY[0]);

        // creates for every cell section n rectangel and a label
        for(int j=0; j<(nbCellSectionX[0]*nbCellSectionY[0]); j++){
            rectGrid[i][j] = new QCPItemRect(ui->PrepLearnDataControlTotalColorMap);
            rectGridLabel[i][j] = new QCPItemText(ui->PrepLearnDataControlTotalColorMap);
        }
    }

    // scale the given data between 0 and 1
    inputDataScaled.clear();
    for(int i=0; i<data.size(); i++){
        inputDataScaled.append(prepareData(data[i], i));
    }

    // saves which rectangels are selected (true = selected, false = not selected)
    selectedRect.resize((int)(inputDataScaled[curDataIndex].size()/25.)); // 25 pixels (5x5 = 25)
    selectedRect.fill(false);

    std::cout << "vor map" << std::endl;

    showTotalColorMap(true);

    std::cout << "nach map" << std::endl;
    // if a folder is loaded the current image is shown in a text label
    ui->PrepLearnDataControlNbImage->setText("image: "+QString::number(curDataIndex+1)+"/"+QString::number(inputDataScaled.size()));

}

// update the total colormap
void prepareLearningData::showTotalColorMap(bool resize)
{
    totalColorMap->data()->setSize(nbPixelX[curDataIndex], nbPixelY[curDataIndex]);

    // set ranges
    double x_lowerLimit = 0;
    double x_upperLimit = nbPixelX[curDataIndex]*(resolution[curDataIndex]);
    double y_lowerLimit = 0;
    double y_upperLimit = nbPixelY[curDataIndex]*(resolution[curDataIndex]);

    totalColorMap->data()->setRange(QCPRange(x_lowerLimit, x_upperLimit), QCPRange(y_lowerLimit, y_upperLimit));

    // fill colormap
    int pxIndex=0;
    double x, y, z;
    for (int yIndex=0; yIndex<nbPixelY[curDataIndex]; ++yIndex)
    {
      for(int xIndex=0;xIndex<nbPixelX[curDataIndex]; xIndex++)
      {
        totalColorMap->data()->cellToCoord(xIndex, yIndex, &x, &y);
        z = inputData[curDataIndex][pxIndex];
        pxIndex++;

        totalColorMap->data()->setCell(xIndex, yIndex, z);
      }
    }

    totalColorMap->setInterpolate(false);
    totalColorMap->setColorScale(totalColorScale); // associate the color map with the color scale
    totalColorMap->setGradient(QCPColorGradient::gpPolar);

    // rescale the data dimension (color) such that all data points lie in the span visualized by the color gradient:
    totalColorMap->rescaleDataRange();

    // make sure the axis rect and color scale synchronize their bottom and top margins (so they line up):
    QCPMarginGroup *marginGroup = new QCPMarginGroup(ui->PrepLearnDataControlTotalColorMap);
    ui->PrepLearnDataControlTotalColorMap->axisRect()->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);
    totalColorScale->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);

    // set rectangle grid and cell name (cell number)
    for(int j=0; j<(int)(nbPixelY[curDataIndex]/5.); j++){
        for(int i=0; i<(int)(nbPixelX[curDataIndex]/5.); i++){
            if(selectedRect[(j*nbCellSectionX[curDataIndex])+i] == true){
                rectGrid[curDataIndex][(j*nbCellSectionX[curDataIndex])+i]->setPen(QPen(Qt::white, 6));
            }else{
                rectGrid[curDataIndex][(j*nbCellSectionX[curDataIndex])+i]->setPen(QPen(Qt::black, 6));
            }
            rectGrid[curDataIndex][(j*nbCellSectionX[curDataIndex])+i]->topLeft->setCoords(((i%nbCellSectionX[curDataIndex])*(resolution[curDataIndex]*nbPixelX[curDataIndex]/nbCellSectionX[curDataIndex]))+0.007,((j%nbCellSectionY[curDataIndex])*(resolution[curDataIndex]*nbPixelY[curDataIndex]/nbCellSectionY[curDataIndex]))+(resolution[curDataIndex]*nbPixelY[curDataIndex]/nbCellSectionY[curDataIndex])-0.007);
            rectGrid[curDataIndex][(j*nbCellSectionX[curDataIndex])+i]->bottomRight->setCoords(((i%nbCellSectionX[curDataIndex])*(resolution[curDataIndex]*nbPixelX[curDataIndex]/nbCellSectionX[curDataIndex]))+(resolution[curDataIndex]*nbPixelX[curDataIndex]/nbCellSectionX[curDataIndex])-0.007,(j%nbCellSectionY[curDataIndex])*(resolution[curDataIndex]*nbPixelY[curDataIndex]/nbCellSectionY[curDataIndex])+0.007);

            if(ui->PrepLearnDataControlSetGrid->isChecked()){
                rectGrid[curDataIndex][(j*nbCellSectionX[curDataIndex])+i]->setVisible(true);
            }else{
                rectGrid[curDataIndex][(j*nbCellSectionX[curDataIndex])+i]->setVisible(false);
            }
            rectGrid[curDataIndex][(j*nbCellSectionX[curDataIndex])+i]->setSelectable(false);

            rectGridLabel[curDataIndex][(j*nbCellSectionX[curDataIndex])+i]->position->setCoords(((i%nbCellSectionX[curDataIndex])*(resolution[curDataIndex]*nbPixelX[curDataIndex]/nbCellSectionX[curDataIndex]))+0.1,((j%nbCellSectionY[curDataIndex])*(resolution[curDataIndex]*nbPixelY[curDataIndex]/nbCellSectionY[curDataIndex]))+(resolution[curDataIndex]*nbPixelY[curDataIndex]/nbCellSectionY[curDataIndex])-0.05);
            rectGridLabel[curDataIndex][(j*nbCellSectionX[curDataIndex])+i]->setText("cell " + QString::number((j*nbCellSectionX[curDataIndex])+i+1));
            rectGridLabel[curDataIndex][(j*nbCellSectionX[curDataIndex])+i]->setColor(Qt::white);
            rectGridLabel[curDataIndex][(j*nbCellSectionX[curDataIndex])+i]->setSelectable(false);
            rectGridLabel[curDataIndex][(j*nbCellSectionX[curDataIndex])+i]->setPen(Qt::NoPen);

            if(ui->PrepLearnDataControlSetLegendGrid->isChecked()){
                rectGridLabel[curDataIndex][(j*nbCellSectionX[curDataIndex])+i]->setVisible(true);
            }else{
                rectGridLabel[curDataIndex][(j*nbCellSectionX[curDataIndex])+i]->setVisible(false);
            }
        }
    }

    // rescale the key (x) and value (y) axes so the whole color map is visible if resize is true
    if(resize){
        ui->PrepLearnDataControlTotalColorMap->xAxis->setRange(x_lowerLimit, x_upperLimit);
        ui->PrepLearnDataControlTotalColorMap->yAxis->setRange(y_lowerLimit, y_upperLimit);
    }

    ui->PrepLearnDataControlTotalColorMap->replot();
}


//####################################################################################
//############################# Prepare Input Data: START ############################
//####################################################################################

// prepare the input data section per section and scale them
QVector<double> prepareLearningData::prepareData(QVector<double> data, int curCellSection){

    // the total image is divided in 20x20 pixel parts. due to bleeched areas, smaller sections are considered for accuracy
    QVector<double> cur_inputData, resizedInputData, scaledInputData;
    QVector<QVector<double>> curScaledInputData, scaledInputDataIndex;


    // FIRST: scaling to an image divisible by 20
    // calculate the number of 20x20px cells in the incoming image (always round up)
    int nb20x20CellsSectionsX = ceil((double)nbPixelX[curCellSection]/20.);
    int nb20x20CellsSectionsY = ceil((double)nbPixelY[curCellSection]/20.);

    // if the number of pixels in x / y direction is not divisible by 20, they have to bee rescaled
    if((nbPixelX[curCellSection]%20)!=0 || (nbPixelY[curCellSection]%20)!=0){
        // calculate the average
        double average = 0;
        for(int i=0; i<data.size(); i++){
            average += data[i];
        }
        average /= data.size();

        // set the missing pixels to the average
        for(int i=0; i<nbPixelX[curCellSection]; i++){
            for(int j=0; j< nbPixelX[curCellSection]; j++){
                resizedInputData.append(data[(i*nbPixelX[curCellSection])+j]);
            }
            for(int k=0; k<((nb20x20CellsSectionsX*20)-nbPixelX[curCellSection]); k++){
                resizedInputData.append(average);
            }
        }
        for(int l=0; l<(((nb20x20CellsSectionsY*20)-nbPixelY[curCellSection])*(nb20x20CellsSectionsX*20)); l++){
            resizedInputData.append(average);
        }
    }else{
        resizedInputData = data;
    }


    // SECOND: fill every 20x20 pixel image
    for(int imageY=0; imageY<nb20x20CellsSectionsY; imageY++){
        for(int imageX=0; imageX<nb20x20CellsSectionsX; imageX++){
            // gets the data for the 20x20px images
            for(int i=0; i<20; i++){
                for(int j=0; j<20; j++){
                    cur_inputData.append(resizedInputData[j+(imageX*20)+(i*nb20x20CellsSectionsX*20)+(imageY*nb20x20CellsSectionsX*20*20)]);
                }
            }
            curScaledInputData.append(scaleData(cur_inputData));
            cur_inputData.clear();
        }
    }


    // THIRD: puts the individual parts of the image back together
    int indexX=0, indexY=0;
    for(int imagesY=0; imagesY<nb20x20CellsSectionsY; imagesY++){
        for(int j=0; j<20; j++){
            if(indexY < nbPixelY[curCellSection]){
                for(int imagesX=0; imagesX<nb20x20CellsSectionsX; imagesX++){
                    for(int i=0; i<20;i++){
                        if(indexX < nbPixelX[curCellSection]){
                            scaledInputData.append(curScaledInputData[imagesX+(imagesY*nb20x20CellsSectionsX)][i+(j*20)]);
                            indexX++;
                        }else{
                            indexX++;
                        }
                    }
                }
                indexX = 0;
                indexY++;
            }else{
                break;
            }
        }
    }

    scaledInputDataIndex.append(scaledInputData);

    return scaledInputData;
}

// scale the data between 0 and 1
QVector<double> prepareLearningData::scaleData(QVector<double> input)
{
    QVector<double> output;

    // calculate the average
    double average = 0;
    for(int i=0; i<input.size(); i++){
        average += input[i];
    }
    average /= input.size();

    // set everything lower than average +5 to zero
    for(int i=0; i<input.size(); i++){
        if(input[i] < (average+5)) input[i]=0.;
    }

    // scale the data between 0 and 1 so that the NN hasn't high numbers
    output.resize(input.size());
    double min = *std::min_element(input.begin(),input.end());
    double max = *std::max_element(input.begin(),input.end()) - min;
    for(int i=0; i<input.size(); i++){
        output[i] = (((input[i] - min))/max);
    }

    return output;
}


// sum up two vectors and divide the result by 2
QVector<double> betweenLearnVectors(QVector<double> a, QVector<double> b){
    QVector<double> c;
    for(int i=0; i<a.size(); i++){
        c.append((a[i]+b[i])/2.);
    }
    return c;
}

// instead of using several networks for different resolutions, the resolution is scaled between 40 and 70
QVector<double> prepareLearningData::rescaleSizeInput(QVector<double> inputData, int nb_pixel_X, int nb_pixel_Y){
    // vector with the current data
    QVector<QVector<double>> rescaledSizeX, rescaledSizeY;
    QVector<double> curRow, rescaledSizeData;

    // add the values to the rows (x- direction)
    int index=0;
    for(int i=0; i< nb_pixel_Y; i++){
        for(int j=0; j<2*nb_pixel_X; j++){ //-1
            if(index == nb_pixel_X){
                curRow.append(inputData[i*nb_pixel_X+index-1]);
            }else if(j%2==0){
                curRow.append(inputData[i*nb_pixel_X+index]);
                ++index;
            }else{
                curRow.append((inputData[i*nb_pixel_X+index-1]+inputData[i*nb_pixel_X+index])/2.);
            }
        }
        index=0;
        rescaledSizeX.append(curRow);
        curRow.clear();

    }

    // add the values to the colums (y- direction)
    index=0;
    for(int i=0; i<2*rescaledSizeX.size()-1; i++){
        if(i%2==0){
            rescaledSizeY.append(rescaledSizeX[index]);
            index++;
        }else{
            rescaledSizeY.append(betweenLearnVectors(rescaledSizeX[index-1],rescaledSizeX[index]));
        }
    }

    // form an Vector out of an vector<vector>
    for(int i=0; i<rescaledSizeY.size(); i++){
        for(int j=0; j<rescaledSizeY[i].size(); j++){
            rescaledSizeData.append(rescaledSizeY[i][j]);
        }
    }
    std::cout << "rescaledSizeData: " << rescaledSizeData.size() << std::endl;

    return rescaledSizeData;
}
//####################################################################################
//############################## Prepare Input Data: END #############################
//####################################################################################


//####################################################################################
//################################# Edit ROI's: START ################################
//####################################################################################

// shows the ROI legend (nb of ROI, position of ROI and height and wide of the ROI)
void prepareLearningData::on_PrepLearnDataControlSetLegendROI_clicked(bool checked)
{
    ui->PrepLearnDataControlTotalColorMap->enableROILegend(checked);
    ui->PrepLearnDataControlTotalColorMap->replot();
}

// removes all ROI's
void prepareLearningData::on_PrepLearnDataControlRemoveAllROI_clicked()
{
    ui->PrepLearnDataControlTotalColorMap->removeAllROIs(true);
    ui->PrepLearnDataControlTotalColorMap->replot();
}

// guide how to delete the selected ROI
void prepareLearningData::on_PrepLearnDataControlRemoveSelectedROI_clicked()
{
    QMessageBox removeSelcROI;
    removeSelcROI.setText("To delete the selected ROI, select it and right click on it. Select 'delete ROI' to delete it.");
    removeSelcROI.exec();
}

// removes the last ROI
void prepareLearningData::on_PrepLearnDataControlRemoveLastROI_clicked()
{
    ui->PrepLearnDataControlTotalColorMap->removeROI();
    ui->PrepLearnDataControlTotalColorMap->replot();
}

// add a ROI at a certain position
void prepareLearningData::on_PrepLearnDataControlAddROI_clicked()
{
    // calculate the center position
    double x_center = nbPixelX[curDataIndex]*(resolution[curDataIndex])/2.;
    double y_center = nbPixelY[curDataIndex]*(resolution[curDataIndex])/2.;

    // add a ROI at the middle of the colormap with an heigth and wide of 300nm
    ui->PrepLearnDataControlTotalColorMap->addROI({x_center, y_center}, 0.3, 0.3);
    ui->PrepLearnDataControlTotalColorMap->enableROILegend(ui->PrepLearnDataControlSetLegendROI->isChecked());
    ui->PrepLearnDataControlTotalColorMap->replot();
}

// if the position of the ROI's is changed, the rectangles are adapted
void prepareLearningData::on_PrepLearnDataControlConfirmROI_clicked()
{
    // delete the data of the current ROI positions
    selectedCellData.clear();

    // set the number of selected cells to zero
    curSelectedCell=0;

    // set all choosen rectangels to zero
    selectedRect.fill(false);

    // determine the associated cells
    selectROIcells();
}

// if more than one image was transferred, this function changes to the previous picture
void prepareLearningData::on_PrepLearnDataControlPreviousImage_clicked()
{
    if(curDataIndex > 0){
        --curDataIndex;
        // show the current number of the image in a label
        ui->PrepLearnDataControlNbImage->setText(QString::number(curDataIndex+1)+"/"+QString::number(inputData.size()));

        // remove all ROI's
        on_PrepLearnDataControlRemoveAllROI_clicked();

        // reset checkbox
        ui->PrepLearnDataControlSetGrid->setChecked(false);
        ui->PrepLearnDataControlSetLegendGrid->setChecked(false);
        ui->PrepLearnDataControlSetLegendROI->setChecked(false);

        // reset grid
        for(int i=0; i<rectGrid[curDataIndex].size();i++){
            rectGrid[curDataIndex+1][i]->setVisible(false);
            rectGridLabel[curDataIndex+1][i]->setVisible(false);
        }

        // reset ROI information
        selectedCellData.clear();
        curSelectedCell=0;
        selectedRect.fill(false);


        showTotalColorMap(true);
    }
}

// if more than one image was transferred, this function changes to the next picture
void prepareLearningData::on_PrepLearnDataControlNextImage_clicked()
{
    if(curDataIndex < inputData.size()-1){
        ++curDataIndex;
        // show the current number of the image in a label
        ui->PrepLearnDataControlNbImage->setText(QString::number(curDataIndex+1)+"/"+QString::number(inputData.size()));

        // remove all ROI's
        on_PrepLearnDataControlRemoveAllROI_clicked();

        // reset checkbox
        ui->PrepLearnDataControlSetGrid->setChecked(false);
        ui->PrepLearnDataControlSetLegendGrid->setChecked(false);
        ui->PrepLearnDataControlSetLegendROI->setChecked(false);

        // reset grid
        for(int i=0; i<rectGrid[curDataIndex].size();i++){
            rectGrid[curDataIndex-1][i]->setVisible(false);
            rectGridLabel[curDataIndex-1][i]->setVisible(false);
        }

        // reset ROI information
        selectedCellData.clear();
        curSelectedCell=0;
        selectedRect.fill(false);


        showTotalColorMap(true);

    }
}
//####################################################################################
//################################## Edit ROI's: END #################################
//####################################################################################


//####################################################################################
//############################# split in 5x5 ROI's: START ############################
//####################################################################################

// set the grid legend
void prepareLearningData::on_PrepLearnDataControlSetLegendGrid_clicked()
{
    showTotalColorMap(true);
}
 // set the grid rectangels
void prepareLearningData::on_PrepLearnDataControlSetGrid_clicked()
{
    showTotalColorMap(true);
}

// calculate and save the current positions of the ROI's (!!! diffrent ranges for x and y leeds to different new positions)
void prepareLearningData::setROIData(QVector<QVector<double>> ROIData, QVector<double> range){
    if(!ROIData.isEmpty()){

        double xRange, yRange, newXPosition, newYPosition;
        // sets the new range
        xRange = range[1]-range[0];
        yRange = range[3]-range[2];

        for(int i=0; i<ROIData.size(); i++){
            // determine the position in this color map
            newXPosition = (ROIData[i][0]-range[0])/xRange*nbPixelX[curDataIndex]*(resolution[curDataIndex]);
            newYPosition = (ROIData[i][1]-range[2])/yRange*nbPixelX[curDataIndex]*(resolution[curDataIndex]);
            ui->PrepLearnDataControlTotalColorMap->addROI({newXPosition, newYPosition}, ROIData[i][2], ROIData[i][3]);
        }
        selectROIcells();
    }
}

// selects and marks the cells that belong to an ROI
void prepareLearningData::selectROIcells(){
    // goes through all ROI's individually
    for(int i=0; i<ui->PrepLearnDataControlTotalColorMap->ROInumber(); i++){
        std::cout << "ROI: " << i << std::endl;

        // get global coordinates from the colormap
        double posNVx = ui->PrepLearnDataControlTotalColorMap->getROIposition(i+1).x();
        double posNVy = ui->PrepLearnDataControlTotalColorMap->getROIposition(i+1).y();
        double wideNV = ui->PrepLearnDataControlTotalColorMap->getROIsize(i+1).x();
        double heightNV = ui->PrepLearnDataControlTotalColorMap->getROIsize(i+1).y();

        // searches for 5x5 cells which belong to a possible NV center
        if(posNVx > 0 && posNVx < nbPixelX[curDataIndex]*resolution[curDataIndex] && posNVy > 0 && posNVy < nbPixelY[curDataIndex]*resolution[curDataIndex]){

            // determine the belonging cell id und their cell coordinates (local coordinates)
            int cellX = (int)(ceil(posNVx/(5*resolution[curDataIndex])));
            double posXcell = posNVx-(cellX-1)*5*resolution[curDataIndex];

            int cellY = (int)(ceil(posNVy/(5*resolution[curDataIndex])));
            double posYcell = posNVy-(cellY-1)*5*resolution[curDataIndex];

            // sets the found cell to 'true' (needed for update the colormap)
            selectedRect[(((cellY-1)*nbCellSectionX[curDataIndex])+cellX)-1] = true;

            // saves data of the cell
            QVector<double> cellData;
            cellData.append((((cellY-1)*nbCellSectionX[curDataIndex])+cellX));      // cell id
            cellData.append(posXcell/(5*resolution[curDataIndex]));                 // x position (if the position of the ROI is outside values below 0 or bigger than 1 are possible)
            cellData.append(posYcell/(5*resolution[curDataIndex]));                 // y position (if the position of the ROI is outside values below 0 or bigger than 1 are possible)
            cellData.append(heightNV/(5*resolution[curDataIndex]));                 // height of the bounding box (if the bounding box is bigger than one cell than the number is bigger than 1)
            cellData.append(wideNV/(5*resolution[curDataIndex]));                   // wide of the bounding box (if the bounding box is bigger than one cell than the number is bigger than 1)

            // append the data of the 5x5 cell to 'cellData' vector
            for(int i=0; i<5; i++){
                for(int j=0; j<5; j++){
                    cellData.append(inputDataScaled[curDataIndex][((cellY-1)*5*nbPixelX[curDataIndex])+(i*nbPixelX[curDataIndex])+((cellX-1)*5)+j]);
                }
            }

            // appand the data of the 5x5 cell to an vector vector which saves all selected 5x5 cells
            selectedCellData.append(cellData);

            // increase the number of cells
            curSelectedCell++;

            std::cout << "before nachbarn" << std::endl;
            // if the ROI is big enough or the center is on the edge, then it can happen that a large part is in another 5x5 cell
            // this is than searched for and also marked and saved in 'selectedCellData'
            ceckNeighboringCell(cellX, cellY, posXcell/(5*resolution[curDataIndex]), posYcell/(5*resolution[curDataIndex]),wideNV/(5*resolution[curDataIndex]),heightNV/(5*resolution[curDataIndex]));
            std::cout << "after nachbarn" << std::endl;

            // shows the data of the first 5x5 cell ('cellColorMap')
            showSectionColormap();
            std::cout << "nach section colormap" << std::endl;

            // color the marked 5x5 cells in the total colormap
            showTotalColorMap(true);
            std::cout << "nach total colormap" << std::endl;
        }
    }
}

// if the ROI is big enough or the center is on the edge, then it can happen that a large part is in another 5x5 cell
// this is than searched for and also marked and saved in 'selectedCellData'
void prepareLearningData::ceckNeighboringCell(int cellX, int cellY, double posXcell, double posYcell, double wide, double height){

    // determine the cell number depending on the corner
    int cellXdirection, cellYdirection, newCellX, newCellY;
    QVector<double> posXdirectionCell, posYdirectionCell;

    // looks at the cell on the left
    if(posXcell < 0.5 && cellX > 1){
        // calculate the total cell number
        cellXdirection = (((cellY-1)*nbCellSectionX[curDataIndex])+cellX)-1;
        // calculate the cell number in the current row/colum
        newCellX = cellX-1;

        // if the ROI takes 40% of the neighbor cell the cell is also used to learn
        if(((wide/2.)-posXcell) > 0.4){
            posXdirectionCell.append(1 + posXcell);
            posXdirectionCell.append(posYcell);
            // increase the number of cells
            curSelectedCell++;
        }

    // looks at the cell on the right
    }else if(posXcell >= 0.5 && cellX < nbCellSectionX[curDataIndex]){
        // calculate the total cell number
        cellXdirection = (((cellY-1)*nbCellSectionX[curDataIndex])+cellX)+1;
        // calculate the cell number in the current row/colum
        newCellX = cellX+1;

        // if the ROI takes 40% of the neighbor cell the cell is also used to learn
        if(((wide/2.)-(1-posXcell)) > 0.4){
            posXdirectionCell.append( 0 - (1-posXcell));
            posXdirectionCell.append(posYcell);
            // increase the number of cells
            curSelectedCell++;
        }

    // neither the left nor the right cell require the conditions
    }else{
        cellXdirection = 0;
    }

    // looks at the cell above
    if(posYcell < 0.5 && cellY > 1){
        // calculate the total cell number
        cellYdirection = (((cellY-2)*nbCellSectionX[curDataIndex])+cellX);
        // calculate the cell number in the current row/colum
        newCellY = cellY-1;

        // if the ROI takes 40% of the neighbor cell the cell is also used to learn
        if(((height/2.)-posYcell) > 0.4){
            posYdirectionCell.append(posXcell);
            posYdirectionCell.append(1 + posYcell);
            // increase the number of cells
            curSelectedCell++;
        }

    // looks at the cell below
    }else if(posYcell >= 0.5 && cellY < nbCellSectionY[curDataIndex]){
        // calculate the total cell number
        cellYdirection = (((cellY)*nbCellSectionX[curDataIndex])+cellX);
        // calculate the cell number in the current row/colum
        newCellY = cellY+1;

        // if the ROI takes 40% of the neighbor cell the cell is also used to learn
        if(((height/2.)-(1-posYcell)) > 0.4){
            posYdirectionCell.append(posXcell);
            posYdirectionCell.append(0 - (1-posYcell));
            // increase the number of cells
            curSelectedCell++;
        }

    // neither upper nor lower cell require the conditions
    }else{
        cellYdirection = 0;
    }

    // include the data in the learning Vector (left/ right)
    if(!posXdirectionCell.isEmpty()){
        // sets the found cell to 'true' (needed for update the colormap)
        selectedRect[cellXdirection-1] = true;
        // saves data of the cell
        QVector<double> curCellData;
        curCellData.append(cellXdirection);                                             // cell id
        curCellData.append(posXdirectionCell[0]);                                       // x position (if the position of the ROI is outside values below 0 or bigger than 1 are possible)
        curCellData.append(posXdirectionCell[1]);                                       // y position (if the position of the ROI is outside values below 0 or bigger than 1 are possible)
        curCellData.append(height);                                                     // height of the bounding box (if the bounding box is bigger than one cell than the number is bigger than 1)
        curCellData.append(wide);                                                       // wide of the bounding box (if the bounding box is bigger than one cell than the number is bigger than 1)

        // append the data of the 5x5 cell to 'cellData' vector
        for(int i=0; i<5; i++){
            for(int j=0; j<5; j++){
                curCellData.append(inputDataScaled[curDataIndex][((cellY-1)*5*nbPixelX[curDataIndex])+(i*nbPixelX[curDataIndex])+((newCellX-1)*5)+j]);
            }
        }

        // appand the data of the 5x5 cell to an vector vector which saves all selected 5x5 cells
        selectedCellData.append(curCellData);
    }

    // include the data in the learning Vector (up/ down)
    if(!posYdirectionCell.isEmpty()){
        // sets the found cell to 'true' (needed for update the colormap)
        selectedRect[cellYdirection-1] = true;
        // saves data of the cell
        QVector<double> curCellData;
        curCellData.append(cellYdirection);                                             // cell id
        curCellData.append(posYdirectionCell[0]);                                       // x position (if the position of the ROI is outside values below 0 or bigger than 1 are possible)
        curCellData.append(posYdirectionCell[1]);                                       // y position (if the position of the ROI is outside values below 0 or bigger than 1 are possible)
        curCellData.append(height);                                                     // height of the bounding box (if the bounding box is bigger than one cell than the number is bigger than 1)
        curCellData.append(wide);                                                       // wide of the bounding box (if the bounding box is bigger than one cell than the number is bigger than 1)

        // append the data of the 5x5 cell to 'cellData' vector
        for(int i=0; i<5; i++){
            for(int j=0; j<5; j++){
                curCellData.append(inputDataScaled[curDataIndex][((newCellY-1)*5*nbPixelX[curDataIndex])+(i*nbPixelX[curDataIndex])+((cellX-1)*5)+j]);
            }
        }

        // appand the data of the 5x5 cell to an vector vector which saves all selected 5x5 cells
        selectedCellData.append(curCellData);
    }
}

// show the current 5x5 cell at the cell colormap
void prepareLearningData::showSectionColormap(){
    // 5x5 colormap
    int sizeCM=5;
    cellColorMap->data()->setSize(sizeCM,sizeCM);
    //cellColorMap->data()->setRange(QCPRange(0,sizeCM*resolution[curDataIndex]),QCPRange(0,sizeCM*resolution[curDataIndex]));
    cellColorMap->data()->setRange(QCPRange(0,1),QCPRange(0,1));

    // sets data of the colormap
    int pxIndex=0;
    double x, y, z;
    for (int yIndex=0; yIndex<sizeCM; ++yIndex)
    {
      for(int xIndex=0;xIndex<sizeCM; xIndex++)
      {
        cellColorMap->data()->cellToCoord(xIndex, yIndex, &x, &y);
        z = selectedCellData[curSelectedCell-1][pxIndex+5];
        pxIndex++;

        cellColorMap->data()->setCell(xIndex, yIndex, z);
      }
    }

    cellColorMap->setInterpolate(false);
    cellColorMap->setColorScale(cellColorScale);  // associate the color map with the color scale
    cellColorMap->setGradient(QCPColorGradient::gpPolar);

    // rescale the data dimension (color) such that all data points lie in the span visualized by the color gradient:
    cellColorMap->rescaleDataRange();

    // make sure the axis rect and color scale synchronize their bottom and top margins (so they line up):
    QCPMarginGroup *cellMarginGroup = new QCPMarginGroup(ui->PrepLearnDataControl5x5ColorMap);
    ui->PrepLearnDataControl5x5ColorMap->axisRect()->setMarginGroup(QCP::msBottom|QCP::msTop, cellMarginGroup);
    cellColorScale->setMarginGroup(QCP::msBottom|QCP::msTop, cellMarginGroup);

    // rescale the key (x) and value (y) axes so the whole color map is visible:
    //ui->PrepLearnDataControl5x5ColorMap->xAxis->setRange(0, sizeCM*resolution[curDataIndex]);
    //ui->PrepLearnDataControl5x5ColorMap->yAxis->setRange(0, sizeCM*resolution[curDataIndex]);
    ui->PrepLearnDataControl5x5ColorMap->xAxis->setRange(0, 1);
    ui->PrepLearnDataControl5x5ColorMap->yAxis->setRange(0, 1);

    ui->PrepLearnDataControl5x5ColorMap->replot();

    // display the values at the GUI
    ui->PrepLearnDataControlXPosition->setValue(selectedCellData[curSelectedCell-1][1]);
    ui->PrepLearnDataControlYPosition->setValue(selectedCellData[curSelectedCell-1][2]);
    ui->PrepLearnDataControlHeight->setValue(selectedCellData[curSelectedCell-1][3]);
    ui->PrepLearnDataControlWide->setValue(selectedCellData[curSelectedCell-1][4]);

    // display the current cell number
    ui->PrepLearnDataControlCurrentCellLabel->setText("current cell number: " + QString::number(selectedCellData[curSelectedCell-1][0]));

}

// when a cell is clicked, it is either marked or unmarked depending on the state
void prepareLearningData::changeCellStateViaMouseclick(QMouseEvent *event){

    if(ui->PrepLearnDataControlTotalColorMap->aPressed){
        // get coordinates of the click position in the colormap
        double xCoord = ui->PrepLearnDataControlTotalColorMap->xAxis->pixelToCoord(event->pos().x());
        double yCoord = ui->PrepLearnDataControlTotalColorMap->yAxis->pixelToCoord(event->pos().y());

        int imageX=0;
        int imageY=0;
        // determine the number of the image (x axis)
        for(int i=0; i<nbCellSectionX[curDataIndex]+1; i++){
            if(xCoord < (i*resolution[curDataIndex]*5)){
                imageX=i-1;
                break;
            }
        }

        // determine the number of the image (y axis)
        for(int i=0; i<nbCellSectionY[curDataIndex]+1; i++){
            if(yCoord < (i*resolution[curDataIndex]*5)){
                imageY=i-1;
                break;
            }
        }

        // marks the cell as false if it has already been selected
        if(selectedRect[imageY*nbCellSectionX[curDataIndex]+imageX] == true){
            selectedRect[imageY*nbCellSectionX[curDataIndex]+imageX] = false;
            for(int i=0; i<selectedCellData.size(); i++){
                if(selectedCellData[i][0] == (imageY*nbCellSectionX[curDataIndex]+imageX+1)){

                    // remove the data from the 'selectedCellData' vector vector
                    selectedCellData.remove(i);

                    // update the current cell id
                    ui->PrepLearnDataControlCurrentCellLabel->setText("current cell number: " + QString::number((imageY*nbCellSectionX[curDataIndex]+imageX)));

                    // decrease the number of cells
                    curSelectedCell--;
                    break;
                }
            }
        // marks the cell as true if it hasn't already been selected
        }else{
            selectedRect[imageY*nbCellSectionX[curDataIndex]+imageX] = true;
            // add the cell data to the 'selectedCellData' vector vector
            selectedCellData.append(addCellData((imageY*nbCellSectionX[curDataIndex]+imageX)+1, imageX, imageY));

            // shows the selected cell in the colormap
            curSelectedCell = selectedCellData.size();
            showSectionColormap();

            // update the current cell id
            ui->PrepLearnDataControlCurrentCellLabel->setText("current cell number: " + QString::number((imageY*nbCellSectionX[curDataIndex]+imageX)+1));
        }
        showTotalColorMap(false);
    }
}

// adds the data of the clicked rectangle
QVector<double> prepareLearningData::addCellData(int nb_cell, int imageX, int imageY){
    QVector<double> cellData;

    // to interact with the cells the need to be classified
    cellData.append(nb_cell);                                               // cell id

    // checks whether an ROI exists
    if(!ui->PrepLearnDataControlTotalColorMap->ROIStore.isEmpty()){
        // calculate the center of the clicked ROI
        double centerX = (imageX+1)*5*resolution[curDataIndex] - 2.5*resolution[curDataIndex];
        double centerY = (imageY+1)*5*resolution[curDataIndex] - 2.5*resolution[curDataIndex];

        // search the ROI with the shortest distance
        double posROIX, posROIY, distance=1000, newDistance;
        int id = 0;
        for(int i=0; i<ui->PrepLearnDataControlTotalColorMap->ROIStore.size(); i++){
            posROIX = ui->PrepLearnDataControlTotalColorMap->getROIposition(i+1).x();
            posROIY = ui->PrepLearnDataControlTotalColorMap->getROIposition(i+1).y();

            newDistance = qSqrt(pow(centerX-posROIX,2)+pow(centerY-posROIY,2));
            if(newDistance < distance){
                distance = newDistance;
                id = i+1;
            }
        }

        // get global position of the closest ROI
        double posROIx = ui->PrepLearnDataControlTotalColorMap->getROIposition(id).x();
        double posROIy = ui->PrepLearnDataControlTotalColorMap->getROIposition(id).y();

        // determine local coordinates
        double posXcell = posROIx-imageX*5*resolution[curDataIndex];
        double posYcell = posROIy-imageY*5*resolution[curDataIndex];

        // get height and wide of the closest ROI
        double wideNV = ui->PrepLearnDataControlTotalColorMap->getROIsize(id).x();
        double heightNV = ui->PrepLearnDataControlTotalColorMap->getROIsize(id).y();

        // saves data of the cell
        cellData.append(posXcell/(5*resolution[curDataIndex]));                 // x position (if the position of the ROI is outside values below 0 or bigger than 1 are possible)
        cellData.append(posYcell/(5*resolution[curDataIndex]));                 // y position (if the position of the ROI is outside values below 0 or bigger than 1 are possible)
        cellData.append(heightNV/(5*resolution[curDataIndex]));                 // height of the bounding box (if the bounding box is bigger than one cell than the number is bigger than 1)
        cellData.append(wideNV/(5*resolution[curDataIndex]));                   // wide of the bounding box (if the bounding box is bigger than one cell than the number is bigger than 1)

        // if there is no ROI, the rectangle cannot be assigned to any, so the position, height and width are set to zero
    }else{
        for(int i=0; i<4; i++){
            cellData.append(0);
        }
    }

    for(int i=0; i<5; i++){
        for(int j=0; j<5; j++){
            cellData.append(inputDataScaled[curDataIndex][(imageY*5*nbPixelX[curDataIndex])+(i*nbPixelX[curDataIndex])+(imageX*5)+j]);
        }
    }

    return cellData;
}

// if more than one 5x5 exist, this function changes to the previous picture
void prepareLearningData::on_PrepLearnDataControlPreviousCell_clicked()
{
    // ceck whether no rectangels have been selected or whether the first one has already been selected
    if(!selectedCellData.isEmpty() && (curSelectedCell-1) > 0){
        curSelectedCell--;

        showSectionColormap();
        // display the current cell number
        ui->PrepLearnDataControlCurrentCellLabel->setText("current cell number: " + QString::number(selectedCellData[curSelectedCell-1][0]));
    }
}

// if more than one 5x5 exist, this function changes to the next picture
void prepareLearningData::on_PrepLearnDataControlNextCell_clicked()
{
    // ceck whether no rectangels have been selected or whether the last one has already been selected
    if(!selectedCellData.isEmpty() && (curSelectedCell+0) < selectedCellData.size()){
        curSelectedCell++;

        showSectionColormap();
        // display the current cell number
        ui->PrepLearnDataControlCurrentCellLabel->setText("current cell number: " + QString::number(selectedCellData[curSelectedCell-1][0]));
    }
}

// jump to the first cell
void prepareLearningData::on_PrepLearnDataControlFirstCell_clicked()
{
    // check if rectangels have been selected
    if(!selectedCellData.isEmpty()){
        curSelectedCell = 1;

        showSectionColormap();
        // display the current cell number
        ui->PrepLearnDataControlCurrentCellLabel->setText("current cell number: " + QString::number(selectedCellData[curSelectedCell-1][0]));
    }
}

// save the data of a rectangle in a training file
void prepareLearningData::on_PrepLearnDataControlSaveCurrentCell_clicked()
{
    // check if rectangels exists
    if(!selectedCellData.isEmpty() && curSelectedCell > 0){
        QFile file(path + "TrainingData/5x5trainDataTEST.dat");

        /* file:
         * 0-5 characterisation of the 4 possible ROI's/ NV centers
               - 0: NVCenter 1: yes, 0: no
               - 1: positionX/normFactor (resacel between 0 and 1 to meke it easier for the neural network)
               - 2: positionY/normFactor
               - 3: Bounding Box height
               - 4: Bounding Box wide
         * 5-30: normalized image data
         */

        file.open(QIODevice::WriteOnly | QIODevice::Append);
            QTextStream out(&file);
            for(int i=0; i<30; i++){
                // save if this is a NV center...
                if(i==0 && ui->PrepLearnDataControlNVCenterYesNo->isChecked()){
                    out << 1 << "\t";
                // ... or not
                }else if(i==0 && !ui->PrepLearnDataControlNVCenterYesNo->isChecked()){
                    out << 0 << "\t";
                // save the rest of the data
                }else if(i<29){
                    out << selectedCellData[curSelectedCell-1][i] << "\t";
                }else{
                    out << selectedCellData[curSelectedCell-1][i] << "\n";
                }
            }

            file.close();
            std::cout << "data saved!" << std::endl;
    }
}

// save the data of all rectangls in a training file
void prepareLearningData::on_PrepLearnDataControlSaveAllCell_clicked()
{
    if(!selectedCellData.isEmpty() && curSelectedCell > 0){
        // save the current selected cell
        int saveCurSelectedCell = curSelectedCell;

        // starts with the first cell and ends with the last cell
        curSelectedCell = 1;
        do{
            on_PrepLearnDataControlSaveCurrentCell_clicked();
            curSelectedCell++;
        }while(curSelectedCell != selectedCellData.size()+1);

        // reset the curSelectedCell
        curSelectedCell = saveCurSelectedCell;
    }
}

//####################################################################################
//############################# split in 5x5 ROI's: START ############################
//####################################################################################

