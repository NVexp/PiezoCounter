#ifndef PREPARELEARNINGDATA_H
#define PREPARELEARNINGDATA_H

#include <QWidget>
#include "qcustomplot.h"

namespace Ui {
class prepareLearningData;
}

class prepareLearningData : public QWidget
{
    Q_OBJECT

public:
    explicit prepareLearningData(QWidget *parent = nullptr);
    ~prepareLearningData();

    void setLearnData(QVector<QVector<double>> data, QVector<double> resolution, QVector<int> nbPixelX, QVector<int> nbPixelY, QString path);

    // split in 5x5 ROI's
    void setROIData(QVector<QVector<double>> data, QVector<double> range);

private slots:
    void showTotalColorMap(bool resize);

    // Edit Tab START
    void on_PrepLearnDataControlSetLegendROI_clicked(bool checked);
    void on_PrepLearnDataControlRemoveAllROI_clicked();
    void on_PrepLearnDataControlRemoveSelectedROI_clicked();
    void on_PrepLearnDataControlRemoveLastROI_clicked();
    void on_PrepLearnDataControlAddROI_clicked();
    void on_PrepLearnDataControlConfirmROI_clicked();
    void on_PrepLearnDataControlPreviousImage_clicked();
    void on_PrepLearnDataControlNextImage_clicked();
    // Edit Tab END

    // split in 5x5 ROI's START
    void on_PrepLearnDataControlSetLegendGrid_clicked();
    void on_PrepLearnDataControlSetGrid_clicked();
    void showSectionColormap();
    void changeCellStateViaMouseclick(QMouseEvent *event);
    void on_PrepLearnDataControlPreviousCell_clicked();
    void on_PrepLearnDataControlNextCell_clicked();
    void on_PrepLearnDataControlFirstCell_clicked();
    void on_PrepLearnDataControlSaveCurrentCell_clicked();
    void on_PrepLearnDataControlSaveAllCell_clicked();
    // split in 5x5 ROI's END

private:
    Ui::prepareLearningData *ui;

    QCPColorMap *totalColorMap;
    QCPColorScale *totalColorScale;
    QCPColorMap *cellColorMap;
    QCPColorScale *cellColorScale;

    // Prepare Input Data START
    // data
    QVector<QVector<double>> inputData, inputDataScaled;
    QVector<double> resolution;
    QVector<int> nbPixelX, nbPixelY;
    // numbers of 5x5 cell sections in x and y
    QVector<int> nbCellSectionX, nbCellSectionY;
    //current image
    int curDataIndex;


    QVector<double> prepareData(QVector<double> data, int curCellSection);
    QVector<double> scaleData(QVector<double> input);
    QVector<double> rescaleSizeInput(QVector<double> inputData, int nb_pixel_X, int nb_pixel_Y);
    // Prepare Input Data END



    // split in 5x5 ROI's START
    // rectangels
    QVector<QVector<QCPItemRect*>> rectGrid;
    QVector<QVector<QCPItemText*>> rectGridLabel;
    // selected rectangles
    QVector<bool> selectedRect;
    // current data of the 5x5 cell section
    QVector<QVector<double>> selectedCellData;
    int curSelectedCell=0;
    // path to training data
    QString path;

    void selectROIcells();
    void ceckNeighboringCell(int cellX, int cellY, double posXcell, double posYcell, double wide, double height);
    QVector<double> addCellData(int nb_cell, int imageX, int imageY);
    // split in 5x5 ROI's END
};

#endif // PREPARELEARNINGDATA_H
