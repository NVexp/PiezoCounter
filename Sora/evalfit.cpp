#include "evalfit.h"
#include <iostream>

#include <QVector>
#include <QtMath>
#include <QMessageBox>

#include "stdafx.h"
#include <numeric>

#include "alglib/interpolation.h"
#include "alglib/fasttransforms.h"

EvalFit::EvalFit()
{

}

// select fit function and fit algorithm
QVector<double> EvalFit::startFit(QVector<double> xVal, QVector<double> yVal, int epoch, QString type)
{
    QVector<double> fitParameter;
    // returns an error if exists
    QMessageBox errorMB;

    // determine fit parameter depending on the fit function and type
    if(type == "NN_dampedSine"){
        // check whether there is an oscillation or not using FFT
        if(checkOscillation(xVal, yVal)){
            fitParameter = SGD_dampedSine(xVal, yVal, epoch, 2);
        }else{
            if(!AIusage){
                errorMB.setText("there is no oscillation!");
                errorMB.exec();
            }
            fitParameter.append(0);
        }
    }else if(type == "LM_dampedSine"){
        // check whether there is an oscillation or not using FFT
        if(checkOscillation(xVal, yVal)){
            fitParameter = LM_dampedSine(xVal, yVal);
        }else{
            if(!AIusage){
                errorMB.setText("there is no oscillation!");
                errorMB.exec();
            }
            fitParameter.append(0);
        }

    }else if(type == "NN_lorentzian"){
        fitParameter = SGD_lorentzian(xVal, yVal, epoch);
    }else if(type == "LM_lorentzian"){
        fitParameter = LM_lorentzian(xVal, yVal);

    }else if(type == "NN_doubleLorentzian"){
        fitParameter = SGD_doubleLorentzian(xVal, yVal, epoch);
    }else if(type == "LM_doubleLorentzian"){
        fitParameter = LM_doubleLorentzian(xVal, yVal);

    }else if(type == "NN_chooseLorentzian"){
        // check how many peaks are in the spectra
        int nbPeaks = nbOfPeaks(xVal, yVal);
        if(nbPeaks == 1){
            fitParameter = SGD_lorentzian(xVal, yVal, epoch);
        }else if(nbPeaks == 2){
            fitParameter = SGD_doubleLorentzian(xVal, yVal, epoch);
        }else{
            if(!AIusage){
                if(nbPeaks == 0){
                    errorMB.setText("there are no peaks!");
                }else{
                    errorMB.setText("there are more then two peaks! (" + QString::number(nbPeaks) + " peaks)");
                }
            }
            errorMB.exec();
            fitParameter.append(0);
        }
    }else if(type == "LM_chooseLorentzian"){
        // check how many peaks are in the spectra
        int nbPeaks = nbOfPeaks(xVal, yVal);
        if(nbPeaks == 1){
            fitParameter = LM_lorentzian(xVal, yVal);
        }else if(nbPeaks == 2){
            fitParameter = LM_doubleLorentzian(xVal, yVal);
        }else{
            if(!AIusage){
                if(nbPeaks == 0){
                    errorMB.setText("there are no peaks!");
                }else{
                    errorMB.setText("there are more then two peaks! (" + QString::number(nbPeaks) + " peaks)");
                }
            }
            errorMB.exec();
            fitParameter.append(0);
        }

    }

    // clear the user fit parameter -> reset to automatic mode
    userFitParams.clear();

    // return fit parameter
    return fitParameter;
}

// set user fit parameters
void EvalFit::setUserFitParameter(QVector<double> fitParams)
{
    userFitParams = fitParams;
}

// returns maxima of an grid using gaussian
QVector<double> EvalFit::gridMaximaGaussian(QVector<double> xVal, QVector<double> yVal, QVector<QVector<double>> zVal)
{
    QVector<double> fitParameters, xMax, yMax, finalPosition(2);
    double max;

    // determine best position of x using y
   for(int i=0; i<xVal.size(); i++){
        fitParameters = LM_gaussian(yVal, zVal[i]);
        // append the maximum of the guassian
        max = (1/(fitParameters[0]*sqrt(2*alglib::pi())))+fitParameters[2];
        yMax.append(max);
    }
    fitParameters = LM_gaussian(xVal, yMax);
    finalPosition[0] = fitParameters[1];

    // determine best position of y using x
    QVector<QVector<double>> zValReverse;
    QVector<double> curCol;
    for(int i=0; i<zVal.size(); i++){
        for(int j=0; j<zVal[0].size(); j++){
            curCol.append(zVal[j][i]);
        }
        zValReverse.append(curCol);
        curCol.clear();
    }
    for(int i=0; i<yVal.size(); i++){
         fitParameters = LM_gaussian(xVal, zValReverse[i]);
         // append the maximum of the guassian
         max = (1/(fitParameters[0]*sqrt(2*alglib::pi())))+fitParameters[2];
         xMax.append(max);
    }
    fitParameters = LM_gaussian(yVal, xMax);
    finalPosition[1] = fitParameters[1];

    std::cout << "final position: " << finalPosition[0] << ", " << finalPosition[1] << std::endl;
    return finalPosition;
}

// fast fourier transformation
QVector<QVector<double>> EvalFit::FFT(QVector<double> xVal, QVector<double> yVal)
{
    QVector<QVector<double>> coordFFT;

    // save the y Values in an alglib vector
    alglib::real_1d_array y_fft;
    y_fft.setcontent(yVal.size(),&(yVal[0]));
    alglib::complex_1d_array f;

    // saves the complex values after Fast Fourier Transformation
    fftr1d(y_fft,f);

    // determine the absolute values after FFT
    QVector<double> fft_abs;
    for(int i=0; i<yVal.size(); i++){
        fft_abs.append((2./xVal.size())*qSqrt(pow(f[i].x,2)+pow(f[i].y,2)));
    }

    // determine the belonging frequencies
    double stepsize = xVal[1]-xVal[0];
    QVector<double> freq(xVal.size());
    int index = xVal.size()/2;

    for(int i=0; i<xVal.size(); i++){
        // it must be differentiated whether xVal-size is even or odd
        if(xVal.size()%2 == 0){
            if(i<(xVal.size()/2)-1){
                freq[i] = (1./(xVal.size()*stepsize))*i*1000;
            }else{
                freq[i] = -index/(xVal.size()*stepsize)*1000;
                index--;
            }
        }else{
            if(i<((xVal.size()-1)/2)){
                freq[i] = (1./(xVal.size()*stepsize))*i*1000;
            }else{
                freq[i] = -index/(xVal.size()*stepsize)*1000;
                index--;
            }
        }
    }

    // remove the frequency at 0
    fft_abs[0]=0;
    fft_abs[fft_abs.size()-1]=0;

    // turn unit of omega into MHz
    /*for(int i=0; i< freq.size(); i++){
        freq[i]/=(2*M_PI);
    }*/

/*
    double amp = *std::max_element(fft_abs.begin(),fft_abs.end());
    int indexAmp = std::max_element(fft_abs.begin(),fft_abs.end())-fft_abs.begin();
    double omega = freq[indexAmp];
    std::cout << "amp: " << amp << std::endl;
    std::cout << "index amp: " << indexAmp << std::endl;
    std::cout << "omega: " << omega << std::endl;
*/
    coordFFT.append(freq);
    coordFFT.append(fft_abs);

    return coordFFT;
}

// decides whether there is an oscillation or not using FFT
bool EvalFit::checkOscillation(QVector<double> xVal, QVector<double> yVal)
{
    QVector<QVector<double>> coordFFT = FFT(xVal, yVal);
    QVector<double> fft=coordFFT[1];

    // decides whether there is an oscillation
    bool osciMax = false;
    bool osciPos = false;

    double max = *std::max_element(fft.begin(),fft.end());
    double sum = std::accumulate(fft.begin(),fft.end(),0.0);
    double mean = sum/fft.size();

    // FIRST: if the maximum is higher than four times the mean than there is an oscillation
    if(max > (4*mean)){
        osciMax = true;
    }

    // SECOND: if more than points are between the maxima <0 and maxima >0
    // split frequency in bigger and smaller than 0
    QVector<double> biggerZero, smallerZero;

    for(int i=0; i<coordFFT[0].size(); i++){
        if(coordFFT[0][i]>=0){
            biggerZero.append(fft[i]);
        }else{
            smallerZero.append(fft[i]);
        }
    }

    // determine the position of the maxima
    int indexR = std::max_element(biggerZero.begin(), biggerZero.end())-biggerZero.begin();
    int indexL = std::max_element(smallerZero.begin(), smallerZero.end())-smallerZero.begin();

    if((indexR+(smallerZero.size()-indexL))>6){
        osciPos = true;
    }

    if(osciMax==true && osciPos==true){
        return true;
    }else{
        return false;
    }
}

QVector<double> findLocalMax(QVector<double> xVal, QVector<double> yVal, double lowerVal, double upperVal)
{
    QVector<double> result(2);
    result[0]=0; //value of maxima
    result[1]=0; //x position of the maxima

    for(int i=0; i<xVal.size(); i++){
        if(xVal[i]>lowerVal && xVal[i]<upperVal){
            if(yVal[i]>result[0]){
                result[0]=yVal[i];
                result[1]=xVal[i];
            }
        }
    }

    return result;
}

QVector<double> findLocalMin(QVector<double> xVal, QVector<double> yVal, double lowerVal, double upperVal)
{
    QVector<double> result(2);
    result[0]=1000; // value of minima
    result[1]=1000; // x position of the minima

    for(int i=0; i<xVal.size(); i++){
        if(xVal[i]>lowerVal && xVal[i]<upperVal){
            if(yVal[i]<result[0]){
                result[0]=yVal[i];
                result[1]=xVal[i];
            }
        }
    }

    return result;
}

// decides how many peaks are in the spectra
int EvalFit::nbOfPeaks(QVector<double> xVal, QVector<double> yVal)
{
    // determine offset
    double yValSum=0;
    for(int i=0; i<yVal.size(); i++){
        yValSum+=yVal[i];
    }
    double offset = yValSum/yVal.size();

    int nbOfIntervals = (int)((xVal[xVal.size()-1]-xVal[0])/20.);

    // number of elements in an interval
    int nbOfElements = (int)(20./(xVal[1]-xVal[0]));

    // saves the current interval
    int curInterval = 0;

    QVector<QVector<double>> peaks;
    QVector<double> curPeak;
    for(int i=0; i<nbOfIntervals; i++){
        curPeak = findLocalMin(xVal, yVal, xVal[0]+curInterval*20, xVal[0]+curInterval*20+20);
        if(curPeak[0] < (offset-0.05)){
            peaks.append(curPeak);
        }
        curInterval++;
    }

    // connect peaks which belongs together
    int nbPeaks = peaks.size()-1;
    for(int i=0; i<nbPeaks; i++){
        if((peaks[i+1][1]-peaks[i][1])<30){
            if(peaks[i][0]>peaks[i+1][0]){
                peaks.remove(i);
            }else{
                peaks.remove(i+1);
            }
            i--;
            nbPeaks--;
        }
    }

    return peaks.size();
}

//####################################################################################
//###########################  Guess Fit Parameters: START ###########################
//####################################################################################

QVector<double> EvalFit::guessFitParametersDampedSine(QVector<double> xVal, QVector<double> yVal, int checkpoint)
{
    QVector<double> guess_parameter(5);

    // guess amplitude
    double maxY = *std::max_element(yVal.constBegin(), yVal.constEnd());
    double minY = *std::min_element(yVal.constBegin(), yVal.constEnd());
    double amp_guess = (maxY-minY)/2;
    guess_parameter[0]=amp_guess;


    // guess offset
    double off_guess = maxY-amp_guess;
    guess_parameter[1]=off_guess;


    // guess omega
    // FIRST: determine places where the cosine goes over/under the offset
    QVector<double> x_down, x_up;
    QVector<double> y_down, y_up;

    for(int i=checkpoint; i<xVal.size(); i++){
        //break if the length of the vector is reached
        if(i+checkpoint+1 == xVal.size()){
            break;
        // checks whether the curve rises
        }else if(yVal[i]>off_guess && yVal[i+1]<off_guess){
            //to be sure that this isn't noise check two other points next to it
            if(yVal[i-checkpoint] > off_guess && yVal[i+checkpoint+1] < off_guess){
                x_down.append(xVal[i]+(xVal[i+1]-xVal[i])/2);
                y_down.append(yVal[i]-(yVal[i]-yVal[i+1])/2.);
            }
        // checks whether the curve falls
        }else if(yVal[i]<off_guess && yVal[i+1]>off_guess){
            //to be sure that this isn't noise check two other points next to it
            if(yVal[i-checkpoint] < off_guess && yVal[i+checkpoint+1] > off_guess){
                x_up.append(xVal[i]+(xVal[i+1]-xVal[i])/2);
                y_up.append(yVal[i+1]-(yVal[i+1]-yVal[i])/2.);
            }
        }
    }

    // SECOND: if the points are to close to each other one of them has to be removed
    for(int i=1; i<x_down.size(); i++){
        if((x_down[i-1]+200)>x_down[i]){
            x_down[i]=0;
            y_down[i]=0;
        }
    }

    for(int i=1; i<x_up.size(); i++){
        if((x_up[i-1]+200)>x_up[i]){
            x_up[i]=0;
            y_up[i]=0;
        }
    }

    //remove all zeros
    x_down.erase(std::remove(std::begin(x_down),std::end(x_down),0), std::end(x_down));
    y_down.erase(std::remove(std::begin(y_down),std::end(y_down),0), std::end(y_down));
    x_up.erase(std::remove(std::begin(x_up),std::end(x_up),0), std::end(x_up));
    y_up.erase(std::remove(std::begin(y_up),std::end(y_up),0), std::end(y_up));

    // THIRD: calculate the period
    int nb_down=0;
    double T_down=0.;
    for(int i=1; i<x_down.size();i++){
        T_down = T_down+x_down[i]-x_down[i-1];
        nb_down++;
    }
    T_down =T_down/nb_down;

    int nb_up=0;
    double T_up=0.;
    for(int i=1; i<x_up.size();i++){
        T_up = T_up+x_up[i]-x_up[i-1];
        nb_up++;
    }
    T_up=T_up/nb_up;

    double T=0.5*(T_down+T_up);

    // FOURTH: calculate guessed omega
    double w_guess=2*M_PI/T;
    guess_parameter[2]=w_guess;


    // guess phi
    double phi_guess=(std::fmod(x_up[0],T)/T)*2*M_PI;

    // phi has to be smaller than pi
    if(phi_guess>M_PI){
        phi_guess = qAbs(phi_guess-2*M_PI);
    }
    guess_parameter[3]=phi_guess;


    // guess tau
    if(x_down.size()>1){
        // FIRST: Determine the maximum of the first full sine
        QVector<double> sine1(2);
        if(x_down[0] > x_up[0]){
            sine1 = findLocalMax(xVal, yVal, x_up[0], x_down[0]);
        }else{
            sine1 = findLocalMax(xVal, yVal, x_up[0], x_down[1]);
        }

        //SECOND: Determine the maximum of the last full sine
        //check which vector is longer
        int lastSine=std::min(x_up.size(),x_down.size())-1;

        QVector<double> sine2(2);
        if(x_down[lastSine] > x_up[lastSine]){
            sine2 = findLocalMax(xVal, yVal, x_up[lastSine], x_down[lastSine]);
        }else{
            sine2 = findLocalMax(xVal, yVal, x_up[lastSine-1], x_down[lastSine]);
        }

        // THIRD: calculate tau1 (first sine) and tau2 (last sine)
        double factor = 0.5;
        double tau1 = -(qLn((sine1[0]-off_guess)/amp_guess)/sine1[1]);
        if(isnan(tau1)) factor=1.;
        double tau2 = -(qLn((sine2[0]-off_guess)/amp_guess)/sine2[1]);
        if(isnan(tau2)) factor=1.;

        double tau_guess = factor*(tau1+tau2);
        if(isnan(tau_guess)) tau_guess=0.00001;
        guess_parameter[4]=tau_guess;
    }else{
        guess_parameter[4]=0;
    }

    return guess_parameter;
}

QVector<double> EvalFit::guessFitParamsLorentzian(QVector<double> xVal, QVector<double> yVal)
{
    QVector<double> guessParameter(4);

    // determine amplitude
    double amp = *std::max_element(yVal.begin(), yVal.end())-*std::min_element(yVal.begin(), yVal.end());
    guessParameter[0] = amp;

    // set FWHM
    guessParameter[1] = 10.;

    // determine w0
    double w0 = xVal[std::min_element(yVal.begin(), yVal.end())-yVal.begin()];
    guessParameter[2] = w0;

    // determine offset
    double yValSum=0;
    for(int i=0; i<yVal.size(); i++){
        yValSum+=yVal[i];
    }
    double offset = yValSum/yVal.size();
    guessParameter[3] = offset;

    return guessParameter;
}

QVector<double> EvalFit::guessFitParamsDoubleLorentzian(QVector<double> xVal, QVector<double> yVal)
{
    nbOfPeaks(xVal, yVal);
    QVector<double> guessParameter(7);

    // determine offset
    double yValSum=0;
    for(int i=0; i<yVal.size(); i++){
        yValSum+=yVal[i];
    }
    double offset = yValSum/yVal.size();
    guessParameter[3] = offset;

    int nbOfIntervals = (int)((xVal[xVal.size()-1]-xVal[0])/20.);

    // number of elements in an interval
    int nbOfElements = (int)(20./(xVal[1]-xVal[0]));

    // saves the current interval
    int curInterval = 0;

    QVector<QVector<double>> peaks;
    QVector<double> curPeak;
    for(int i=0; i<nbOfIntervals; i++){
        curPeak = findLocalMin(xVal, yVal, xVal[0]+curInterval*20, xVal[0]+curInterval*20+20);
        if(curPeak[0] < (offset-0.05)){
            peaks.append(curPeak);
        }
        curInterval++;
    }

    // connect peaks which belongs together
    int nbPeaks = peaks.size()-1;
    for(int i=0; i<nbPeaks; i++){
        if((peaks[i+1][1]-peaks[i][1])<30){
            if(peaks[i][0]>peaks[i+1][0]){
                peaks.remove(i);
            }else{
                peaks.remove(i+1);
            }
            i--;
            nbPeaks--;
        }
    }


    // determine amplitude (peak 1)
    double amp1 = offset-peaks[0][0];
    guessParameter[0] = amp1;

    // determine amplitude (peak 2)
    double amp2 = offset-peaks[1][0];
    guessParameter[4] = amp2;

    // determine FHWM (peak 1)
    guessParameter[1] = 10.;

    // determine FHWM (peak 2)
    guessParameter[5] = 10.;

    // determine w0 (peak 1)
    guessParameter[2] = peaks[0][1];

    // determine w0 (peak 2)
    guessParameter[6] = peaks[1][1];

    return guessParameter;
}

QVector<double> EvalFit::guessFitParamsGaussian(QVector<double> xVal, QVector<double> yVal)
{
    QVector<double> guessParameter(3);

    // determine mu
    int indexMax = std::max_element(yVal.begin(), yVal.end()) - yVal.begin();
    guessParameter[1] = xVal[indexMax];

    // determine sigma
    guessParameter[0] = (xVal[indexMax] - xVal[0])/2;

    // determine offset
    double yValSum=0;
    for(int i=0; i<yVal.size(); i++){
        yValSum+=yVal[i];
    }
    double offset = yValSum/yVal.size();
    guessParameter[2] = offset;

    return guessParameter;
}

//####################################################################################
//############################  Guess Fit Parameters: END ############################
//####################################################################################



//####################################################################################
//##########################  Fit via Neural Network: START ##########################
//####################################################################################

// loss function
double EvalFit::loss_squere(QVector<double> y_true, QVector<double> y_prediction)
{
    double loss=0;
    for(int i=0; i<y_true.size(); i++){
        loss = loss + pow((y_true[i]-y_prediction[i]),2);
    }

    loss = loss/y_true.size();
    return loss;
}


// Damped Sine:
// damped sine function
QVector<double> dampedSine(QVector<double> xVal, QVector<double> fitParameters)
{
    QVector<double> func(xVal.size());

    for(int i=0; i<xVal.size(); i++){
        func[i]=fitParameters[0]*qExp(-fitParameters[4]*xVal[i])*qSin(fitParameters[2]*xVal[i]+fitParameters[3])+fitParameters[1];
    }

    return func;
}

// derivation after all fit parameters for the damped sine
QVector<double> dampedSine_der(QVector<double> xVal, QVector<double> fitParameters, QString derParameter)
{
    QVector<double> func(xVal.size());

    if(derParameter == "amp"){
        for(int i=0; i<xVal.size(); i++){
            func[i]=qExp(-fitParameters[4]*xVal[i])*qSin(fitParameters[2]*xVal[i]+fitParameters[3]);
        }
    }else if(derParameter == "off"){
        for(int i=0; i<xVal.size(); i++){
            func[i]=1;
        }
    }else if(derParameter == "omega"){
        for(int i=0; i<xVal.size(); i++){
            func[i]=fitParameters[0]*qExp(-fitParameters[4]*xVal[i])*qCos(fitParameters[2]*xVal[i]+fitParameters[3])*xVal[i];
        }
    }else if(derParameter == "phi"){
        for(int i=0; i<xVal.size(); i++){
            func[i]=fitParameters[0]*qExp(-fitParameters[4]*xVal[i])*qCos(fitParameters[2]*xVal[i]+fitParameters[3]);
        }
    }else if(derParameter == "tau"){
        for(int i=0; i<xVal.size(); i++){
            func[i]=-xVal[i]*fitParameters[0]*qExp(-fitParameters[4]*xVal[i])*qSin(fitParameters[2]*xVal[i]+fitParameters[3]);
        }
    }

    return func;
}

// SGB learning process for the damped sine
QVector<double> EvalFit::SGD_dampedSine(QVector<double> xVal, QVector<double> yVal, int epochs, int checkpoint)
{
    // use different checkpoints (guess parameters) for getting the smallest loss
    QVector<QVector<double>> fitParameters;
    QVector<double> out(xVal.size()), loss;

    for(int i=0; i<4; i++){
        fitParameters.append(SGD_dampedSineLearning(xVal, yVal, epochs, checkpoint));
        out = dampedSine(xVal, fitParameters[i]);
        loss.append(loss_squere(yVal,out));
        if(loss_squere(yVal, out) < 0.0005){
            break;
        }else{
            checkpoint++;
        }
    }

    // if all losses are smaller than 0.0005 the fit parameter with the smallest loss is choosen
    int smallLossIndex = std::min_element(loss.begin(),loss.end())-loss.begin();

    // turns unit of omega into MHz
    fitParameters[smallLossIndex][2] = fitParameters[0][2]*1000/(2*M_PI);

    return fitParameters[smallLossIndex];
}

// SGB learning function
QVector<double> EvalFit::SGD_dampedSineLearning(QVector<double> xVal, QVector<double> yVal, int epochs, int checkpoint)
{
    // guess fit Parameters for the damped Sine function
    QVector<double> fitParameters;
    /* fitParameters[0]: Amplitude
     * fitParameters[1]: Offset
     * fitParameters[2]: omega
     * fitParameters[3]: phi
     * fitParameters[4]: tau  */
    if(userFitParams.isEmpty()){
        fitParameters = guessFitParametersDampedSine(xVal,yVal,checkpoint);
    }else{
        fitParameters = userFitParams;
    }

    // use Neural Network to fit the function
    // define learning rates
    double lr_amp = 0.001;
    double lr_off = 0.01;
    double lr_w = 0.0001;
    double lr_phi = 0.1;
    double lr_tau = 0.00001;

    // current value for the derivation (der) and prediction (pre)
    double der,pre;
    double d_amp=0, d_off=0, d_w=0, d_phi=0, d_tau=0;
    int nb_val = xVal.size();

    // uses the neural network method to update the learning parameters
    for(int i=0; i<epochs; i++){

        // change omega w
        d_w = 0;
        for(int j=0; j<xVal.size();j++){
            pre = fitParameters[0]*qExp(-fitParameters[4]*xVal[j])*qSin(fitParameters[2]*xVal[j]+fitParameters[3])+fitParameters[1];
            der = fitParameters[0]*qExp(-fitParameters[4]*xVal[j])*qCos(fitParameters[2]*xVal[j]+fitParameters[3])*xVal[j];
            d_w = d_w + der*(pre-yVal[j]);
        }
        fitParameters[2] = fitParameters[2] - lr_w*(d_w/nb_val);

        // change phi
        d_phi = 0;
        for(int j=0; j<xVal.size();j++){
            pre = fitParameters[0]*qExp(-fitParameters[4]*xVal[j])*qSin(fitParameters[2]*xVal[j]+fitParameters[3])+fitParameters[1];
            der = fitParameters[0]*qExp(-fitParameters[4]*xVal[j])*qCos(fitParameters[2]*xVal[j]+fitParameters[3]);
            d_phi = d_phi + der*(pre-yVal[j]);
        }
        fitParameters[3] = fitParameters[3] - lr_phi*(d_phi/nb_val);

        // change amplitude
        d_amp = 0;
        for(int j=0; j<xVal.size();j++){
            pre = fitParameters[0]*qExp(-fitParameters[4]*xVal[j])*qSin(fitParameters[2]*xVal[j]+fitParameters[3])+fitParameters[1];
            der = qExp(-fitParameters[4]*xVal[j])*qSin(fitParameters[2]*xVal[j]+fitParameters[3]);
            d_amp = d_amp + der*(pre-yVal[j]);
        }
        fitParameters[0] = fitParameters[0] - lr_amp*(d_amp/nb_val);

        // change offset
        d_off = 0;
        for(int j=0; j<xVal.size();j++){
            pre = fitParameters[0]*qExp(-fitParameters[4]*xVal[j])*qSin(fitParameters[2]*xVal[j]+fitParameters[3])+fitParameters[1];
            d_off = d_off + (pre-yVal[j]);
        }
        fitParameters[1] = fitParameters[1] - lr_off*(d_off/nb_val);

        // change tau
        d_tau = 0;
        for(int j=0; j<xVal.size();j++){
            pre = fitParameters[0]*qExp(-fitParameters[4]*xVal[j])*qSin(fitParameters[2]*xVal[j]+fitParameters[3])+fitParameters[1];
            der = -xVal[j]*fitParameters[0]*qExp(-fitParameters[4]*xVal[j])*qSin(fitParameters[2]*xVal[j]+fitParameters[3]);
            d_tau = d_tau + der*(pre-yVal[j]);
        }
        fitParameters[4] = fitParameters[4] - lr_tau*(d_tau/nb_val);

        // print current fit parameters after 100 000 rounds
        if(i%100000==0 && AIusage==false){
            std::cout << "Epoche: " << i << ", w=" << fitParameters[2] << ", tau=" << 1/fitParameters[4]*1000 << ", phi=" << fitParameters[3] << ", A0=" << fitParameters[0] << ", off=" << fitParameters[1] << std::endl;
        }
    }

    return fitParameters;
}


// Single Loretzian:
// single lorentzian function
QVector<double> lorentzian(QVector<double> xVal, QVector<double> params)
{
    QVector<double> func(xVal.size());

    for(int i=0; i<xVal.size(); i++){
        func[i]=(-params[0]*pow(params[1],2))/(pow(xVal[i]-params[2],2)+pow(params[1],2))+params[3];
    }

    return func;
}

// derivation after all fit parameters for the lorentzian
QVector<double> lorentzian_der(QVector<double> xVal, QVector<double> params, QString derParameter)
{
    QVector<double> func(xVal.size());

    if(derParameter == "amp"){
        for(int i=0; i<xVal.size(); i++){
            func[i] = (- pow(params[1],2) )/( pow((xVal[i]-params[2]),2) + pow(params[1],2) );
        }
    }else if(derParameter == "FHWM"){
        for(int i=0; i<xVal.size(); i++){
            func[i] = ( (2*params[0]*pow(params[1],3))/ pow(( pow((xVal[i]-params[2]),2) + pow(params[1],2)),2) )-( (2*params[0]*params[1])/( pow((xVal[i]-params[2]),2) + pow(params[1],2) ) );
        }
    }else if(derParameter == "w0"){
        for(int i=0; i<xVal.size(); i++){
            func[i] = (- 2*params[0]*pow(params[1],2)*(xVal[i]-params[2]) )/pow(( pow((xVal[i]-params[2]),2) + pow(params[1],2) ) ,2);
        }
    }else if(derParameter == "off"){
        for(int i=0; i<xVal.size(); i++){
            func[i] = 1;
        }
    }

    return func;
}

// SGB learning process for the lorentzian
QVector<double> EvalFit::SGD_lorentzian(QVector<double> xVal, QVector<double> yVal, int epochs)
{
    // guess fit Parameters for the lorentzian
    QVector<double> fitParameters;
    /* fitParameters[0]: Amplitude (Peak 1)
     * fitParameters[1]: FHWM (Peak 1)
     * fitParamerets[2]: w0 (Peak 1)
     * fitParameters[3]: Offset  */
    if(userFitParams.isEmpty()){
        fitParameters = guessFitParamsLorentzian(xVal, yVal);
    }else{
        fitParameters = userFitParams;
    }

    // use Neural Network to fit the function
    // define learning rates
    double lr_amp = 2;
    double lr_FHWM = 7;
    double lr_w0 = 20;
    double lr_off = 0.1;

    double dAmp=0, dFHWM=0, dw0=0, dOff=0;

    // vectors for the derivation (der) and prediction (pre)
    QVector<double> der, pre;

    for(int i=0; i<epochs; i++){

        // change amplitude
        dAmp = 0;
        pre = lorentzian(xVal, fitParameters);
        der = lorentzian_der(xVal, fitParameters, "amp");
        for(int j=0; j<xVal.size(); j++){
            dAmp = dAmp + (der[j]*(pre[j]-yVal[j]));
        }
        fitParameters[0] = fitParameters[0]-(lr_amp*dAmp/xVal.size());

        // change FHWM
        dFHWM = 0;
        pre = lorentzian(xVal, fitParameters);
        der = lorentzian_der(xVal, fitParameters, "FHWM");
        for(int j=0; j<xVal.size(); j++){
            dFHWM = dFHWM + (der[j]*(pre[j]-yVal[j]));
        }
        fitParameters[1] = fitParameters[1]-(lr_FHWM*dFHWM/xVal.size());

        // change w0
        dw0 = 0;
        pre = lorentzian(xVal, fitParameters);
        der = lorentzian_der(xVal, fitParameters, "w0");
        for(int j=0; j<xVal.size(); j++){
            dw0 = dw0 + (der[j]*(pre[j]-yVal[j]));
        }
        fitParameters[2] = fitParameters[2]-(lr_w0*dw0/xVal.size());

        // change offset
        dOff = 0;
        pre = lorentzian(xVal, fitParameters);
        der = lorentzian_der(xVal, fitParameters, "off");
        for(int j=0; j<xVal.size(); j++){
            dOff = dOff + (der[j]*(pre[j]-yVal[j]));
        }
        fitParameters[3] = fitParameters[3]-(lr_off*dOff/xVal.size());

        // print current fit parameters after 100 000 rounds
        if(i%100000==0 && AIusage==false){
            std::cout << "Epoche: " << i << ", amplitude=" << fitParameters[0] << ", w0=" << fitParameters[2] << ", FHWM=" << fitParameters[1] << ", off=" << fitParameters[3] << std::endl;
        }

    }

    return fitParameters;
}


// Double Lorentzian
// double lorentzian function
QVector<double> doubleLorentzian(QVector<double> xVal, QVector<double> fitParameters)
{
    QVector<double> func(xVal.size());

    for(int i=0; i<xVal.size(); i++){
        func[i] = -(fitParameters[0]*pow(fitParameters[1],2)/(pow((xVal[i]-fitParameters[2]),2)+pow(fitParameters[1],2))+fitParameters[4]*pow(fitParameters[5],2)/(pow((xVal[i]-fitParameters[6]),2)+pow(fitParameters[5],2)))+fitParameters[3];
    }

    return func;
}

// derivation after all fit parameters for the double lorentzian
QVector<double> doubleLorentzian_der(QVector<double> xVal, QVector<double> params, QString derParameter)
{
    QVector<double> func(xVal.size());

    if(derParameter == "amp1"){
        for(int i=0; i<xVal.size(); i++){
            func[i] = (- pow(params[1],2) )/( pow((xVal[i]-params[2]),2) + pow(params[1],2) );
        }
    }else if(derParameter == "FHWM1"){
        for(int i=0; i<xVal.size(); i++){
            func[i] = ( (2*params[0]*pow(params[1],3))/ pow(( pow((xVal[i]-params[2]),2) + pow(params[1],2)),2) )-( (2*params[0]*params[1])/( pow((xVal[i]-params[2]),2) + pow(params[1],2) ) );
        }
    }else if(derParameter == "w01"){
        for(int i=0; i<xVal.size(); i++){
            func[i] = (- 2*params[0]*pow(params[1],2)*(xVal[i]-params[2]) )/pow(( pow((xVal[i]-params[2]),2) + pow(params[1],2) ) ,2);
        }
    }else if(derParameter == "off"){
        for(int i=0; i<xVal.size(); i++){
            func[i] = 1;
        }
    }else if(derParameter == "amp2"){
        for(int i=0; i<xVal.size(); i++){
            func[i] = (- pow(params[5],2) )/( pow((xVal[i]-params[6]),2) + pow(params[5],2) );
        }
    }else if(derParameter == "FHWM2"){
        for(int i=0; i<xVal.size(); i++){
            func[i] = ( (2*params[4]*pow(params[5],3))/ pow(( pow((xVal[i]-params[6]),2) + pow(params[5],2)),2) )-( (2*params[4]*params[5])/( pow((xVal[i]-params[6]),2) + pow(params[5],2) ) );
        }
    }else if(derParameter == "w02"){
        for(int i=0; i<xVal.size(); i++){
            func[i] = (- 2*params[4]*pow(params[5],2)*(xVal[i]-params[6]) )/pow(( pow((xVal[i]-params[6]),2) + pow(params[5],2) ) ,2);
        }
    }

    return func;
}

// SGB learning process for the double lorentzian
QVector<double> EvalFit::SGD_doubleLorentzian(QVector<double> xVal, QVector<double> yVal, int epochs)
{
    // guess fit Parameters for the double lorentzian
    QVector<double> fitParameters;
    /* fitParameters[0]: Amplitude (Peak 1)
     * fitParameters[1]: FHWM (Peak 1)
     * fitParamerets[2]: w0 (Peak 1)
     * fitParameters[4]: Amplitude (Peak 2)
     * fitParameters[5]: FHWM (Peak 2)
     * fitParamerets[6]: w0 (Peak 2)
     * fitParameters[3]: Offset  */
    if(userFitParams.isEmpty()){
        fitParameters = guessFitParamsDoubleLorentzian(xVal, yVal);
    }else{
        fitParameters = userFitParams;
    }

    // use Neural Network to fit the function
    // define learning rates
    double lr_amp1 = 5;
    double lr_amp2 = 5;
    double lr_FHWM1 = 10;
    double lr_FHWM2 = 10;
    double lr_w01 = 50;
    double lr_w02 = 50;
    double lr_off = 0.1;

    // current value for the derivation (der) and prediction (pre)
    QVector<double> der,pre;
    double d_amp1=0, d_amp2=0, d_FHWM1=0, d_FHWM2=0, d_w01=0, d_w02=0, d_off=0;
    int nb_val = xVal.size();

    // uses the neural network method to update the learning parameters
    for(int i=0; i<epochs; i++){

        // change amplitudes
        d_amp1 = 0;
        pre = doubleLorentzian(xVal, fitParameters);
        der = doubleLorentzian_der(xVal, fitParameters, "amp1");
        for(int j=0; j<xVal.size(); j++){
            d_amp1+=(der[j]*(pre[j]-yVal[j]));
        }
        //std::cout << "\nd_amp1: " << d_amp1 << std::endl;
        fitParameters[0]-=(lr_amp1*(d_amp1/nb_val));

        d_amp2 = 0;
        pre = doubleLorentzian(xVal, fitParameters);
        der = doubleLorentzian_der(xVal, fitParameters, "amp2");
        for(int j=0; j<xVal.size(); j++){
            d_amp2+=(der[j]*(pre[j]-yVal[j]));
        }
        //std::cout << "d_amp2: " << d_amp2 << std::endl;
        fitParameters[4]-=(lr_amp2*(d_amp2/nb_val));

        // change FHWMs
        d_FHWM1 = 0;
        pre = doubleLorentzian(xVal, fitParameters);
        der = doubleLorentzian_der(xVal, fitParameters, "FHWM1");
        for(int j=0; j<xVal.size(); j++){
            d_FHWM1+=(der[j]*(pre[j]-yVal[j]));
        }
        //std::cout << "d_FHWM1: " << d_FHWM1 << std::endl;
        fitParameters[1]-=(lr_FHWM1*(d_FHWM1/nb_val));

        d_FHWM2 = 0;
        pre = doubleLorentzian(xVal, fitParameters);
        der = doubleLorentzian_der(xVal, fitParameters, "FHWM2");
        for(int j=0; j<xVal.size(); j++){
            d_FHWM2+=(der[j]*(pre[j]-yVal[j]));
        }
        //std::cout << "d_FHWM2: " << d_FHWM2 << std::endl;
        fitParameters[5]-=(lr_FHWM2*(d_FHWM2/nb_val));

        // change w0s
        d_w01 = 0;
        pre = doubleLorentzian(xVal, fitParameters);
        der = doubleLorentzian_der(xVal, fitParameters, "w01");
        for(int j=0; j<xVal.size(); j++){
            d_w01+=(der[j]*(pre[j]-yVal[j]));
        }
        //std::cout << "d_w01: " << d_w01 << std::endl;
        fitParameters[2]-=(lr_w01*(d_w01/nb_val));

        // change w0s
        d_w02 = 0;
        pre = doubleLorentzian(xVal, fitParameters);
        der = doubleLorentzian_der(xVal, fitParameters, "w02");
        for(int j=0; j<xVal.size(); j++){
            d_w02+=(der[j]*(pre[j]-yVal[j]));
        }
        //std::cout << "d_w02: " << d_w02 << std::endl;
        fitParameters[6]-=(lr_w02*(d_w02/nb_val));

        // change offset
        // change w0s
        d_off = 0;
        pre = doubleLorentzian(xVal, fitParameters);
        der = doubleLorentzian_der(xVal, fitParameters, "off");
        for(int j=0; j<xVal.size(); j++){
            d_off+=(der[j]*(pre[j]-yVal[j]));
        }
        //std::cout << "d_off: " << d_off << std::endl;
        fitParameters[3]-=(lr_off*(d_off/nb_val));
        //std::cout << "\n" << std::endl;


        // print current fit parameters after 100 000 rounds
        if(i%100000==0 && AIusage==false){
            std::cout << "Epoche: " << i << std::endl;
            std::cout << "Peak 1: amplitude=" << fitParameters[0] << ", w0=" << fitParameters[2] << ", FHWM=" << fitParameters[1] << ", off=" << fitParameters[3] << std::endl;
            std::cout << "Peak 2: amplitude=" << fitParameters[4] << ", w0=" << fitParameters[6] << ", FHWM=" << fitParameters[5] << ", off=" << fitParameters[3] << std::endl;
        }
    }

    return fitParameters;
}

//####################################################################################
//###########################  Fit via Neural Network: END ###########################
//####################################################################################



//####################################################################################
//##############################  Fit via Alglib: START ##############################
//####################################################################################

// Damped Sine
void dampedSinFunction(const alglib::real_1d_array &c, const alglib::real_1d_array &x, double &func, void *ptr)
{
    // fit function where x is a position on X-axis and c is adjustable parameter
    //c[0] = amplitude, c[1] = omega, c[2] = phi, c[3] = tau, c[4] = offset
    func = c[0]*sin(c[1]*x[0]+c[2])*exp(-x[0]*c[3]) + c[4];
}

QVector<double> EvalFit::LM_dampedSine(QVector<double> xVal, QVector<double> yVal)
{

    alglib::real_2d_array x;
    x.setcontent(xVal.size(),1,&(xVal[0]));
    alglib::real_1d_array y;
    y.setcontent(yVal.size(),&(yVal[0]));

    // saves the final fit parameter
    QVector<double> finalFitParameter(5);

    //set fitparameters
    QVector<double> guessParameter(5);
    if(userFitParams.isEmpty() && checkOscillation(xVal,yVal)==true){
        guessParameter = guessFitParametersDampedSine(xVal,yVal,2);
        std::cout << "true" << std::endl;
    }else if(userFitParams.isEmpty() && checkOscillation(xVal,yVal)==false){
        std::cout << "false" << std::endl;
        if(!AIusage){
            QMessageBox errorDS;
            errorDS.setText("I dont think that this is a damped sine... if you're sure about the damped sine then use other or your own guess parameters");
            errorDS.exec();
        }
        finalFitParameter.fill(0);
        return finalFitParameter;
    }else{
        std::cout << "user" << std::endl;
        guessParameter = userFitParams;
    }
    std::cout << "amp: " << guessParameter[0] << std::endl;
    std::cout << "off: " << guessParameter[1] << std::endl;
    std::cout << "w: " << guessParameter[2] << std::endl;
    std::cout << "phi: " << guessParameter[3] << std::endl;
    std::cout << "tau: " << guessParameter[4] << std::endl;
    /* fitParameter[0]: Amplitude
     * fitParameter[1]: Offset
     * fitParameret[2]: omega
     * fitParameter[3]: phi
     * fitParameter[4]: tau  */

    std::vector<double> fitParameter = {guessParameter[0], guessParameter[2], guessParameter[3], guessParameter[4], guessParameter[1]};

    alglib::real_1d_array c;
    c.setcontent(fitParameter.size(),&(fitParameter[0]));

    alglib::lsfitstate state;
    double diffstep = 0.0001;
    double epsx = 0.000001;
    alglib::ae_int_t maxits = 0;
    alglib::ae_int_t info;
    alglib::lsfitreport rep;

    try{
        lsfitcreatef(x, y, c, diffstep, state);
        lsfitsetcond(state, epsx, maxits);
        alglib::lsfitfit(state, dampedSinFunction);
        lsfitresults(state, info, c, rep);

        finalFitParameter[0]=c[0];
        finalFitParameter[1]=c[4];
        // turn unit omega into MHz
        finalFitParameter[2]=(c[1]*1000)/(2*M_PI);
        finalFitParameter[3]=c[2];
        finalFitParameter[4]=c[3];
    } catch (...) {
        if(!AIusage){
            QMessageBox errorDS;
            errorDS.setText("I dont think that this is a damped sine... if you're sure about the damped sine then use other or your own guess parameters");
            errorDS.exec();
        }
        finalFitParameter.fill(0);
    }

    return finalFitParameter;
}


// Single Lorentzian
void lorentzianFunction(const alglib::real_1d_array &c, const alglib::real_1d_array &x, double &func, void *ptr)
{
    // fit function where x is a position on X-axis and c is adjustable parameter
    //c[0] = amplitude, c[1] = FHWM, c[2] = w0, c[3] = offset
    func = -(c[0]*pow(c[1],2))/(pow((x[0]-c[2]),2)+pow(c[1],2))+c[3];
}

QVector<double> EvalFit::LM_lorentzian(QVector<double> xVal, QVector<double> yVal)
{
    alglib::real_2d_array x;
    x.setcontent(xVal.size(),1,xVal.data());
    alglib::real_1d_array y;
    y.setcontent(yVal.size(),yVal.data());

    // set fitparameters
    QVector<double> guessParameter(4);
    if(userFitParams.isEmpty()){
        guessParameter = guessFitParamsLorentzian(xVal, yVal);
    }else{
        guessParameter = userFitParams;
    }
    /* fitParameter[0]: Amplitude
     * fitParameter[1]: FHWM
     * fitParameret[2]: w0
     * fitParameter[3]: Offset  */

    std::vector<double> fitParameter = {guessParameter[0], guessParameter[1], guessParameter[2], guessParameter[3]};

    alglib::real_1d_array c;
    c.setcontent(fitParameter.size(),&(fitParameter[0]));

    alglib::lsfitstate state;
    double diffstep = 0.0001;
    double epsx = 0.000001;
    alglib::ae_int_t maxits = 0;
    alglib::ae_int_t info;
    alglib::lsfitreport rep;

    // saves the final fit parameter
    QVector<double> finalFitParameter(4);

    try{
        lsfitcreatef(x, y, c, diffstep, state);
        lsfitsetcond(state, epsx, maxits);
        alglib::lsfitfit(state, lorentzianFunction);
        lsfitresults(state, info, c, rep);


        finalFitParameter[0]=c[0];
        finalFitParameter[1]=c[1];
        finalFitParameter[2]=c[2];
        finalFitParameter[3]=c[3];
    }catch(...){
        if(!AIusage){
            QMessageBox errorDS;
            errorDS.setText("I dont think that this is a lorentzian...if you're sure about the lorentzian then use other or your own guess parameters");
            errorDS.exec();
        }
        finalFitParameter.fill(0);
    }


    return finalFitParameter;
}


// Double Lorentzian
void doubleLorentzianFunction(const alglib::real_1d_array &c, const alglib::real_1d_array &x, double &func, void *ptr)
{
    // fit function where x is a position on X-axis and c is adjustable parameter
    //c[0] = amplitude (peak 1), c[1] = FHWM (peak 1), c[2] = w0 (peak 1), c[4] = amplitude (peak 2), c[5] = FHWM (peak 2), c[6] = w0 (peak 2), c[3] = offset
    func = -(c[0]*pow(c[1],2)/(pow((x[0]-c[2]),2)+pow(c[1],2))+c[4]*pow(c[5],2)/(pow((x[0]-c[6]),2)+pow(c[5],2)))+c[3];
}

QVector<double> EvalFit::LM_doubleLorentzian(QVector<double> xVal, QVector<double> yVal)
{
    alglib::real_2d_array x;
    x.setcontent(xVal.size(),1,&(xVal[0]));
    alglib::real_1d_array y;
    y.setcontent(yVal.size(),&(yVal[0]));
    alglib::real_1d_array c;

    // set fitparameters
    QVector<double> guessParameter(7);
    if(userFitParams.isEmpty()){
        guessParameter = guessFitParamsDoubleLorentzian(xVal, yVal);
    }else{
        guessParameter = userFitParams;
    }
    /* fitParameter[0]: Amplitude (Peak 1)
     * fitParameter[1]: FHWM (Peak 1)
     * fitParameret[2]: w0 (Peak 1)
     * fitParameter[4]: Amplitude (Peak 2)
     * fitParameter[5]: FHWM (Peak 2)
     * fitParameret[6]: w0 (Peak 2)
     * fitParameter[3]: Offset  */

    std::vector<double> fitParameter = {guessParameter[0],guessParameter[1],guessParameter[2],guessParameter[3],guessParameter[4],guessParameter[5],guessParameter[6]};

    c.setcontent(fitParameter.size(),&(fitParameter[0]));


    alglib::lsfitstate state;
    double diffstep = 0.0001;
    double epsx = 0.000001;
    alglib::ae_int_t maxits = 0;
    alglib::ae_int_t info;
    alglib::lsfitreport rep;

    // saves the final fit parameter
    QVector<double> finalFitParameter(7);


    try{
        lsfitcreatef(x, y, c, diffstep, state);
        lsfitsetcond(state, epsx, maxits);
        alglib::lsfitfit(state, doubleLorentzianFunction);
        lsfitresults(state, info, c, rep);

        finalFitParameter[0] = c[0];
        finalFitParameter[1] = c[1];
        finalFitParameter[2] = c[2];
        finalFitParameter[4] = c[4];
        finalFitParameter[5] = c[5];
        finalFitParameter[6] = c[6];
        finalFitParameter[3] = c[3];
    }catch(...){
        if(!AIusage){
            QMessageBox errorDS;
            errorDS.setText("I dont think that this is a double lorentzian... if you're sure about the double lorentzian then use other or your own guess parameters");
            errorDS.exec();
        }
        finalFitParameter.fill(0);
    }


    return finalFitParameter;
}


// Gaussian
void GaussianFunction(const alglib::real_1d_array &c, const alglib::real_1d_array &x, double &func, void *ptr){
    // fit function where x is a position on X-axis and c is adjustable parameter
    //c[0] = sigma, c[1] = mu, c[2] = offset
    func = (1/(c[0]*sqrt(2*alglib::pi())))*exp(-0.5*pow(((x[0]-c[1])/c[0]),2))+c[2];
}

QVector<double> EvalFit::LM_gaussian(QVector<double> xVal, QVector<double> yVal)
{
    alglib::real_2d_array x;
    x.setcontent(xVal.size(),1,&(xVal[0]));
    alglib::real_1d_array y;
    y.setcontent(yVal.size(),&(yVal[0]));

    //set fitparameters
    QVector<double> guessParameter(3);
    if(userFitParams.isEmpty()){
        guessParameter = guessFitParamsGaussian(xVal, yVal);
    }else{
        guessParameter = userFitParams;
    }
    /* fitParameter[0]: sigma
     * fitParameter[1]: mu
     * fitParameret[2]: offset */

    std::vector<double> fitParameter = {guessParameter[0], guessParameter[1], guessParameter[2]};

    alglib::real_1d_array c;
    c.setcontent(fitParameter.size(),&(fitParameter[0]));

    alglib::lsfitstate state;
    double diffstep = 0.0001;
    double epsx = 0.000001;
    alglib::ae_int_t maxits = 0;
    alglib::ae_int_t info;
    alglib::lsfitreport rep;

    // saves the final fit parameter
    QVector<double> finalFitParameter(3);


    try{
        lsfitcreatef(x, y, c, diffstep, state);
        lsfitsetcond(state, epsx, maxits);
        alglib::lsfitfit(state, GaussianFunction);
        lsfitresults(state, info, c, rep);

        finalFitParameter[0]=c[0];
        finalFitParameter[1]=c[1];
        finalFitParameter[2]=c[2];
    }catch(...){
        if(!AIusage){
            QMessageBox errorDS;
            errorDS.setText("I dont think that this is a gaussian... if you're sure about the guassian then use other or your own guess parameters");
            errorDS.exec();
        }
        finalFitParameter.fill(0);
    }

    return finalFitParameter;
}

//####################################################################################
//###############################  Fit via Alglib: END ###############################
//####################################################################################


int EvalFit::test(int number){
    std::cout << number+4 << std::endl;
    return 1;
}
