#ifndef CONTROL_H
#define CONTROL_H

#include <QMainWindow>
#include <QTimer>

#include<qcustomplot.h>
#include "cnn.h"
#include "evalfit.h"

typedef struct dataOfFile{
    QVector<double> resolution; // stepsize between the pixels (X and Y has to be equal)
    QVector<int> nb_pixel_X;
    QVector<int> nb_pixel_Y;
    QVector<double> pos_X;
    QVector<double> pos_Y;
    QVector<double> pos_Z;
    QVector<QVector<double>> kCountsPerSec;
}dataOfFile;

namespace Ui {
class Control;
}

class Control : public QMainWindow
{
    Q_OBJECT

public:
    explicit Control(QWidget *parent = nullptr);
    ~Control();

    // object which includes the Neural Network
    // CNN = Convolutional Neural Network
    // CNNwoBias = Convolutional Neural Network without Bias
    // ANN = Artificial Neural Network
    // ANNwoBias = Artificial Neural Network without Bias
    CNN *cnn_var = new CNN("C:/Users/DiamondExp/Documents/Qt projects/PiezoScanPlusCounter/Control/CNN/", "cnn");
    CNN *cnnwoBias_var = new CNN("C:/Users/DiamondExp/Documents/Qt projects/PiezoScanPlusCounter/Control/CNN/", "cnnwoBias");

    CNN *ann_var = new CNN("C:/Users/DiamondExp/Documents/Qt projects/PiezoScanPlusCounter/Control/CNN/", "ann");
    CNN *annwoBias_var = new CNN("C:/Users/DiamondExp/Documents/Qt projects/PiezoScanPlusCounter/Control/CNN/", "annwoBias");

    // saves the read data of the file
    dataOfFile *dataOfFile_var = new dataOfFile();

    // evaluate data
    EvalFit *evalFit_var = new EvalFit();

    // Load Data
    void setData(QVector<double> data, int nbPixelX, int nbPixelY, double resolution, double xPos, double yPos);

    // current path
    QString curPath;
    QString curPathToROI;

signals:
    void AIuse(bool state);
    // Measure Routines
    // set 2D scan parameters
    void set2DScanParameters(int nbPixelX, int nbPixelY, int stepsizeX, int stepsizeY);
    void setWavGenTableLine(int id, int nbPixelX, int nbPixelY, QString X_str);

    // start 2D
    void checkLaser();
    void setPermWord();
    void start2DScan();

    // get current positions
    std::vector<double> getPositions();
    // get current counts
    std::vector<double> getCounts();
    // go to position
    void newPosition(int xPos, int yPos, int zPos);

    // set magnetstage parameters
    void setMagnetStage(double smallMotor, double bigMotor, double distanceMotor);
    void startMagnetstage();//###
    void setDistanceValue(double val);//###
    void setSmallAngleValue(double val);//###
    void setBigAngleValue(double val);//###

    // set AWG scan parameters
    void setAWGTriggerMode(const QString &mode);
    void uploadCwODMRParameter(int loopNb, double excitTime, double freqStart, double freqEnd, int stepNb);


    // signals that indicate the learning process
    // 2DScan
    void startScanNVOnly(int centerPositionROIX, int centerPositionROIY, int centerPositionROIZ);
    void setWavetabel(int Lx, int Ly, int Xstep, int Ystep);


    // ODMR measurement
    void loadCwODMR(int loopNb, double excitTime, double freqStart, double freqEnd, int stepNb);//###

    // cw ODMR
    void startCwODMR(int loopNb);
    void setCwODMRParameters(int loobNb, double excitTime, double freqStart, double freqEnd, int stepNb);

public slots:
    void state2DScan(bool state);
    void stateNVMeasurment(bool state);
    void stateMagnetStage(bool state);
    void stateODMRUpload(bool state);
    void stateCwODMR(bool state);

    //Measure Routine
    void refocus();

    bool moveMagnetTest();

private slots:

    // Load Data START
    void on_ControlNVRecLoad2DScan_clicked();
    void showData();
    // Load Data END

    // Edit Tab START
    void on_ControlNVRecEnableROILegend_clicked(bool checked);
    void on_ControlNVRecConfirmEditing_clicked();
    void on_ControlNVRecRemoveLastROI_clicked();
    void on_ControlNVRecRemoveSelectedROI_clicked();
    void on_ControlNVRecRemoveAllROI_clicked();
    void on_ControlNVRecAddROI_clicked();
    // Edit Tab END

    // Learning Tab START
    void on_ControlLearnLoadImage_clicked();
    void on_ControlLearnLoadFolder_clicked();
    void on_ControlLearnPrepareData_clicked();
    void on_ControlLearnStartLearningprocess_clicked();
    void on_ControlLearnSaveLearnParameters_clicked();
    void on_ControlLearnDeleteNetwork_clicked();
    void on_ControlLearnStopLearningprocess_clicked();
    void on_ControlLearnGDHelp_clicked();
    void enabledButtons(bool state);
    void update_ControlLearnCurrentNb(int curNb);
    // Learning Tab END

    // Predicting Tab START
    void on_ControlNVRecPredict_clicked();
    void on_listItem_clicked(QListWidgetItem *item);
    void on_ControlNVRecConfirmList_clicked();
    // Predicting Tab START

    // Synchronize START
    void on_ControlLearningSynchronize_clicked();
    void on_ControlRecognitionSynchronize_clicked();
    // Synchronize END

    void on_test_clicked();

    // Start Control
    void start_chrackterization(QString scanType);
    void start_2DScan_chrackterization();
    void start_ODMR_chrackterization();
    void on_ControlNVRecStartControl_clicked();


private:
    // Load Data START
    void loadDataFromFile(QString cur_fileName);
    QVector<double> prepareDataFromFile(QVector<double> nonScaledData, int nb_pixel_X, int nb_pixel_Y);
    // Load Data END

    // Measure Routine
    void saveMeasureRoutineData(QVector<QVector<double>> dataCounts, QVector<double> dataInformation, QString pathAndFileName);

    // measure states
    bool curState2DScan = false;

    // Start Control
    bool curMeasurmentState;
    bool curMagnetState;
    bool curODMRUploadState;
    bool curCwODMRState;

    Ui::Control *ui;

    QCPColorMap *colorMapRecognition;
    QCPColorScale *colorScaleRecognition;
    QCPColorMap *colorMapLearning;
    QCPColorScale *colorScaleLearning;

    QCPItemRect *ROIData;

    QVector<QVector<double>> predictDataROI;
    QString currentFilePath;

    // QTimer
    QTimer *Scan2DTimer;
    QTimer *ODMRScanTimer;

    QTimer *magStageState;

    // measurement states
    QVector<bool> ODMRStates;
    bool curMeasure = false;
};

#endif // CONTROL_H
