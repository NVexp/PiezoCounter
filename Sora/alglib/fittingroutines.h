#ifndef FITTINGROUTINES_H
#define FITTINGROUTINES_H

#include <QVector>
#include "qcustomplot.h"

typedef struct fittingParametersLorentzian
{
    double w0;
    double w0_err;
    double FWHM;
    double FWHM_err;
    double amp;
    double amp_err;
    double offset;
    double offset_err;
} fittingParametersLorentzian;

typedef  struct fittingParametersDampedSin{
    double amp;
    double amp_err;
    double omegaMHz;
    double omegaMHz_err;
    double phi;
    double phi_err;
    double tau;
    double tau_err;
    double offset;
    double offset_err;
} fittingParametersDampedSin;

typedef struct fitEnvironment
{
    double startFreq;
    double stopFreq;
    int upperLimit;
    int lowerLimit;
    int steps;
} fitEnvironment;

class fittingroutines
{
public:
    fittingroutines(QCustomPlot *plotWavegentWindow);
    void setCurFilename(QString fileName);
    QString curFileName;
    void saveFitParametersinDatFile();
    QString curFittedKurveName;

    fittingParametersLorentzian Lorentzian1;
    fittingParametersLorentzian Lorentzian2;
    fittingParametersDampedSin dampedSinus;
    fitEnvironment fitEnvironment_var;

    void singleLorentzian(double w0, double FWHM, double amp, QVector<double> xValues, QVector<double> yValues, int lowerLimit, int upperLimit);
    void doubleLorentzian(double w0_1, double FWHM_1, double amp_1, double w0_2, double FWHM_2, double amp_2, QVector<double> xValues, QVector<double> yValues, int lowerLimit, int upperLimit);
    void dampedSin(double amp, double omega, double phi, double tau, QVector<double> xValues, QVector<double> yValues, int lowerLimit, int upperLimit);

    void plotFittedData(QVector<double> xValues, std::string functionType);

    QCustomPlot *wavegenPlot;

signals:
    void graphDataClear();
    void graphData(double valx, double valy);
    void plotGraph();

private:
    QVector<double> guessFitParametersDampedSine(QVector<double> valX, QVector<double> valY);

};

#endif // FITTINGROUTINES_H
