#include "fittingroutines.h"

#include <QVector>
#include <QFile>

#include "stdafx.h"
#include "alglib/interpolation.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>



fittingroutines::fittingroutines(QCustomPlot *plotWavegentWidget):
    wavegenPlot(plotWavegentWidget)
{
    /*Explanations
     * X        -   array[0..N-1,0..M-1], points (one row = one point)
     * Y        -   array[0..N-1], function values.
     * C        -   array[0..K-1], initial approximation to the solution,
     * N        -   number of points, N>1
     * M        -   dimension of space
     * K        -   number of parameters being fitted


     * DiffStep -   numerical differentiation step;
                    should not be very small or large;
                    large = loss of accuracy
                    small = growth of round-off errors
     * State    -   structure which stores algorithm state
     * EpsF     -   stopping criterion. Algorithm stops if
                    |F(k+1)-F(k)| <= EpsF*max{|F(k)|, |F(k+1)|, 1}
     * EpsX     -   >=0
                    The subroutine finishes its work if  on  k+1-th  iteration
                    the condition |v|<=EpsX is fulfilled, where:
                    * |.| means Euclidian norm
                    * v - scaled step vector, v[i]=dx[i]/s[i]
                    * dx - ste pvector, dx=X(k+1)-X(k)
                    * s - scaling coefficients set by LSFitSetScale()
     * MaxIts   -   maximum number of iterations. If MaxIts=0, the  number  of
                    iterations   is    unlimited.   Only   Levenberg-Marquardt
                    iterations  are  counted  (L-BFGS/CG  iterations  are  NOT
                    counted because their cost is very low compared to that of
                    LM).
     * Rep      -   optimization report. Following fields are set:
                    * Rep.TerminationType completetion code:
                    * RMSError          rms error on the (X,Y).
                    * AvgError          average error on the (X,Y).
                    * AvgRelError       average relative error on the non-zero Y
                    * MaxError          maximum error
                                       NON-WEIGHTED ERRORS ARE CALCULATED
                    * WRMSError         weighted rms error on the (X,Y).
      * Info    -   completetion code:
                    *  1    relative function improvement is no more than
                            EpsF.
                    *  2    relative step is no more than EpsX.
                    *  4    gradient norm is no more than EpsG
                    *  5    MaxIts steps was taken
                    *  7    stopping conditions are too stringent,
                            further improvement is impossible
*/
}

void fittingroutines::setCurFilename(QString fileName)
{
    curFileName = fileName;
}

void fittingroutines::saveFitParametersinDatFile(){

    if(curFittedKurveName == "singleLorentzian"){
        int nbOfFile = 0;
        do{
            nbOfFile++;
        }while(QFileInfo::exists(curFileName + "_SingleLorentzianFitParameters_" + QString::number(nbOfFile) + ".dat"));
        QFile file(curFileName + "_SingleLorentzianFitParameters_" + QString::number(nbOfFile) + ".dat");


        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
            return;

        QTextStream out(&file);
        out << "startFreq[MHz]:  \t" << fitEnvironment_var.startFreq << "\n";
        out << "stopFreq[MHz]:   \t" << fitEnvironment_var.stopFreq << "\n";
        out << "lower Limit[MHz]:\t" << fitEnvironment_var.lowerLimit << "\n";
        out << "upper Limit:     \t" << fitEnvironment_var.upperLimit << "\n";
        out << "steps:           \t" << fitEnvironment_var.steps << "\n \n \n";
        out << "Lorentzian 1: \n";
        out << "amplitude:\t" << Lorentzian1.amp << " pm " << Lorentzian1.amp_err << "\n";
        out << "FWHM[MHz]:\t" << Lorentzian1.FWHM<< " pm " << Lorentzian1.FWHM_err << "\n";
        out << "w0[MHz]:  \t" << Lorentzian1.w0 << " pm " << Lorentzian1.w0_err << "\n";
        out << "offset:   \t" << Lorentzian1.offset << " pm " << Lorentzian1.offset_err << "\n";

        std::cout << "File saved as: " << (curFileName + "_SingleLorentzianFitParameters_" + QString::number(nbOfFile) + ".dat").toStdString() << std::endl;

    }else if(curFittedKurveName == "doubleLorentzian"){
        int nbOfFile = 0;
        do{
            nbOfFile++;
        }while(QFileInfo::exists(curFileName + "_DoubleLorentzianFitParameters_" + QString::number(nbOfFile) + ".dat"));
        QFile file(curFileName + "_DoubleLorentzianFitParameters_" + QString::number(nbOfFile) + ".dat");

        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
            return;

        QTextStream out(&file);
        out << "startFreq[MHz]:  \t" << fitEnvironment_var.startFreq << "\n";
        out << "stopFreq[MHz]:   \t" << fitEnvironment_var.stopFreq << "\n";
        out << "lower Limit[MHz]:\t" << fitEnvironment_var.lowerLimit << "\n";
        out << "upper Limit:     \t" << fitEnvironment_var.upperLimit << "\n";
        out << "steps:           \t" << fitEnvironment_var.steps << "\n \n \n";
        out << "Lorentzian 1: \n";
        out << "amplitude:\t" << Lorentzian1.amp << " pm " << Lorentzian1.amp_err << "\n";
        out << "FWHM[MHz]:\t" << Lorentzian1.FWHM<< " pm " << Lorentzian1.FWHM_err << "\n";
        out << "w0[MHz]:  \t" << Lorentzian1.w0 << " pm " << Lorentzian1.w0_err << "\n";
        out << "offset:   \t" << Lorentzian1.offset << " pm " << Lorentzian1.offset_err << "\n";
        out << "Lorentzian 2: \n";
        out << "amplitude:\t" << Lorentzian2.amp << " pm " << Lorentzian2.amp_err << "\n";
        out << "FWHM[MHz]:\t" << Lorentzian2.FWHM<< " pm " << Lorentzian2.FWHM_err << "\n";
        out << "w0[MHz]:  \t" << Lorentzian2.w0 << " pm " << Lorentzian2.w0_err << "\n";
        out << "offset:   \t" << Lorentzian2.offset << " pm " << Lorentzian2.offset_err << "\n";

        std::cout << "File saved as: " << (curFileName + "_DoubleLorentzianFitParameters_" + QString::number(nbOfFile) + ".dat").toStdString() << std::endl;

    }else if(curFittedKurveName == "dampedSin"){
        int nbOfFile = 0;
        do{
            nbOfFile++;
        }while(QFileInfo::exists(curFileName + "_DumpedSinusFitParameters_" + QString::number(nbOfFile) + ".dat"));
        QFile file(curFileName + "_DumpedSinusFitParameters_" + QString::number(nbOfFile) + ".dat");

        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
            return;

        QTextStream out(&file);
        out << "startFreq[MHz]:  \t" << fitEnvironment_var.startFreq << "\n";
        out << "stopFreq[MHz]:   \t" << fitEnvironment_var.stopFreq << "\n";
        out << "lower Limit[MHz]:\t" << fitEnvironment_var.lowerLimit << "\n";
        out << "upper Limit:     \t" << fitEnvironment_var.upperLimit << "\n";
        out << "steps:           \t" << fitEnvironment_var.steps << "\n \n \n";
        out << "Dumped Sinus: \n";
        out << "amplitude:\t" << dampedSinus.amp << " pm " << dampedSinus.amp_err << "\n";
        out << "Omega[MHz]:\t" << dampedSinus.omegaMHz<< " pm " << dampedSinus.omegaMHz_err << "\n";
        out << "phi[rad]:  \t" << dampedSinus.phi << " pm " << dampedSinus.phi_err << "\n";
        out << "tau[ns]:  \t" << dampedSinus.tau << " pm " << dampedSinus.tau_err << "\n";
        out << "offset:   \t" << dampedSinus.offset << " pm " << dampedSinus.offset_err << "\n";

        std::cout << "File saved as: " << (curFileName + "_DumpedSinusFitParameters_" + QString::number(nbOfFile) + ".dat").toStdString() << std::endl;
    }
}


//defined functions

void singleLorentzianFunction(const alglib::real_1d_array &c, const alglib::real_1d_array &x, double &func, void *ptr)
{
    // fit function where x is a position on X-axis and c is adjustable parameter
    func = -(c[0]*pow(c[1],2))/(pow((x[0]-c[2]),2)+pow(c[1],2))+c[3];
}

void doubleLorentzianFunction(const alglib::real_1d_array &c, const alglib::real_1d_array &x, double &func, void *ptr)
{
    // fit function where x is a position on X-axis and c is adjustable parameter
    func = -(c[0]*pow(c[1],2)/(pow((x[0]-c[2]),2)+pow(c[1],2))+c[4]*pow(c[5],2)/(pow((x[0]-c[6]),2)+pow(c[5],2)))+c[3];
}

void dampedSinFunction(const alglib::real_1d_array &c, const alglib::real_1d_array &x, double &func, void *ptr)
{
    // fit function where x is a position on X-axis and c is adjustable parameter
    //c[0] = amplitude, c[1] = omega, c[2] = phi, c[3] = tau, c[4] = offset
    func = c[0]*sin(c[1]*x[0]+c[2])*exp(-x[0]*c[3]) + c[4];
}



void fittingroutines::singleLorentzian(double w0, double FWHM, double amp, QVector<double> xValues, QVector<double> yValues, int lowerLimit, int upperLimit){

    int stepsBeforeLowerLimit = (int)((lowerLimit-xValues[0])/(xValues[1] - xValues[0]));
    int numbOfSteps = (int)((upperLimit-lowerLimit)/(xValues[1] - xValues[0]));
    QVector<double> usedxValues(numbOfSteps), usedyValues(numbOfSteps);
    for (int i = 0; i<numbOfSteps; i++){
        usedxValues[i]= xValues[stepsBeforeLowerLimit+i];
        usedyValues[i]= yValues[stepsBeforeLowerLimit+i];
    }


    alglib::real_2d_array x;
    x.setcontent(usedxValues.size(),1,usedxValues.data());
    alglib::real_1d_array y;
    //y.setcontent(usedyValues.size(),&(usedyValues[0]));
    y.setcontent(usedyValues.size(),usedyValues.data());
    alglib::real_1d_array c;
    std::vector<double> fitParameters = {amp, FWHM, w0, *std::max_element(usedyValues.constBegin(), usedyValues.constEnd())};
    c.setcontent(fitParameters.size(),&(fitParameters[0]));


    alglib::lsfitstate state;
    double diffstep = 0.0001;
    double epsx = 0.000001;
    alglib::ae_int_t maxits = 0;
    alglib::ae_int_t info;
    alglib::lsfitreport rep;


    lsfitcreatef(x, y, c, diffstep, state);
    lsfitsetcond(state, epsx, maxits);
    alglib::lsfitfit(state, singleLorentzianFunction);
    lsfitresults(state, info, c, rep);

    Lorentzian1.amp = c[0];
    Lorentzian1.amp_err = rep.errpar[0];
    Lorentzian1.FWHM = abs(c[1]);
    Lorentzian1.FWHM_err = rep.errpar[1];
    Lorentzian1.w0 = c[2];
    Lorentzian1.w0_err = rep.errpar[2];
    Lorentzian1.offset = c[3];
    Lorentzian1.offset_err = rep.errpar[3];

    fitEnvironment_var.startFreq=xValues[0];
    fitEnvironment_var.stopFreq=xValues[xValues.size()-1];
    fitEnvironment_var.lowerLimit=lowerLimit;
    fitEnvironment_var.upperLimit=upperLimit;
    fitEnvironment_var.steps=xValues.size();

    plotFittedData(xValues, "singleLorentzian");
    curFittedKurveName = "singleLorentzian";
}

void fittingroutines::doubleLorentzian(double w0_1, double FWHM_1, double amp_1, double w0_2, double FWHM_2, double amp_2, QVector<double> xValues, QVector<double> yValues, int lowerLimit, int upperLimit)
{
    alglib::real_2d_array x;
    x.setcontent(xValues.size(),1,&(xValues[0]));
    alglib::real_1d_array y;
    y.setcontent(yValues.size(),&(yValues[0]));
    alglib::real_1d_array c;
    std::vector<double> fitParameters = {amp_1, FWHM_1, w0_1, *std::max_element(yValues.constBegin(), yValues.constEnd()), amp_2, FWHM_2, w0_2};
    c.setcontent(fitParameters.size(),&(fitParameters[0]));


    alglib::lsfitstate state;
    double diffstep = 0.0001;
    double epsx = 0.000001;
    alglib::ae_int_t maxits = 0;
    alglib::ae_int_t info;
    alglib::lsfitreport rep;


    lsfitcreatef(x, y, c, diffstep, state);
    lsfitsetcond(state, epsx, maxits);
    alglib::lsfitfit(state, doubleLorentzianFunction);
    lsfitresults(state, info, c, rep);

    Lorentzian1.amp = c[0];
    Lorentzian1.FWHM = abs(c[1]);
    Lorentzian1.w0 = c[2];
    Lorentzian1.offset = c[3];
    Lorentzian2.amp = c[4];
    Lorentzian2.FWHM = abs(c[5]);
    Lorentzian2.w0 = c[6];
    Lorentzian2.offset = c[3];

    fitEnvironment_var.startFreq=xValues[0];
    fitEnvironment_var.stopFreq=xValues[xValues.size()-1];
    fitEnvironment_var.lowerLimit=lowerLimit;
    fitEnvironment_var.upperLimit=upperLimit;
    fitEnvironment_var.steps=xValues.size();

    plotFittedData(xValues, "doubleLorentzian");
    curFittedKurveName = "doubleLorentzian";
}

void fittingroutines::dampedSin(double amp, double omegaMHz, double phi, double tau, QVector<double> xValues, QVector<double> yValues, int lowerLimit, int upperLimit)
{
    // norm y values for better fit
    /*double yMax = *std::max_element(yValues.constBegin(), yValues.constEnd());
    if(yMax!=1){
        for(int i=0;i<yValues.size();i++){
            yValues[i]=(yValues[i]/yMax);
        }
    }*/

    int stepsBeforeLowerLimit = (int)((lowerLimit-xValues[0])/(xValues[1] - xValues[0]));
    int numbOfSteps = (int)((upperLimit-lowerLimit)/(xValues[1] - xValues[0]));
    QVector<double> usedxValues(numbOfSteps), usedyValues(numbOfSteps);
    for (int i = 0; i<numbOfSteps; i++){
        usedxValues[i]= xValues[stepsBeforeLowerLimit+i];
        usedyValues[i]= yValues[stepsBeforeLowerLimit+i];
    }

    alglib::real_2d_array x;
    x.setcontent(usedxValues.size(),1,&(usedxValues[0]));
    alglib::real_1d_array y;
    y.setcontent(usedyValues.size(),&(usedyValues[0]));

    //set fitparameters
    QVector<double> guessParameter(5);
    guessParameter = guessFitParametersDampedSine(usedxValues,usedyValues);
    /* fitParameter[0]: Amplitude
     * fitParameter[1]: Offset
     * fitParameret[2]: omega
     * fitParameter[3]: phi
     * fitParameter[4]: tau  */

    if(amp == 0) amp=guessParameter[0];
    double off = guessParameter[1];
    if(omegaMHz == 0) omegaMHz = guessParameter[2];
    if(phi == 0) phi = guessParameter[3];
    if(tau == 0) tau = guessParameter[4];


    /*std::cout << "Amp: " << guessParameter[0] << std::endl;
    std::cout << "off: " << guessParameter[1] << std::endl;
    std::cout << "w: " << guessParameter[2] << std::endl;
    std::cout << "phi: " << guessParameter[3] << std::endl;
    std::cout << "tau: " << guessParameter[4] << std::endl;*/

    std::vector<double> fitParameter = {amp, omegaMHz, phi, tau, off};

    alglib::real_1d_array c;
    c.setcontent(fitParameter.size(),&(fitParameter[0]));

    //c.setcontent(fitParameters.size(),&(fitParameters[0]));

    //set some fitting parameters
    /*double ampGuess = (*std::max_element(usedyValues.constBegin(), usedyValues.constEnd())-*std::min_element(usedyValues.constBegin(), usedyValues.constEnd()))/2.;
    double offsetGuess = *std::max_element(usedyValues.constBegin(), usedyValues.constEnd())-ampGuess;
    if(amp == 0){
        std::vector<double> fitParameters = {ampGuess, omegaMHz/1000, phi, tau, offsetGuess};
        c.setcontent(fitParameters.size(),&(fitParameters[0]));
    }else{
        std::vector<double> fitParameters = {amp, omegaMHz/1000, phi, tau, offsetGuess};
        c.setcontent(fitParameters.size(),&(fitParameters[0]));
    }*/

    alglib::lsfitstate state;
    double diffstep = 0.0001;
    double epsx = 0.000001;
    alglib::ae_int_t maxits = 0;
    alglib::ae_int_t info;
    alglib::lsfitreport rep;

    lsfitcreatef(x, y, c, diffstep, state);
    lsfitsetcond(state, epsx, maxits);
    alglib::lsfitfit(state, dampedSinFunction);
    lsfitresults(state, info, c, rep);

    /*std::cout << "8" << std::endl;
    std::cout << c[0] << std::endl;
    std::cout << c[1] << std::endl;
    std::cout << c[2] << std::endl;
    std::cout << c[3] << std::endl;
    std::cout << c[4] << std::endl;*/
    dampedSinus.amp = c[0];
    dampedSinus.amp_err = rep.errpar[0];
    dampedSinus.omegaMHz = c[1];//*1000;
    dampedSinus.omegaMHz_err = rep.errpar[1];//*1000;
    dampedSinus.phi = c[2];
    dampedSinus.phi_err = rep.errpar[2];
    dampedSinus.tau = c[3];
    dampedSinus.tau_err = rep.errpar[3];
    dampedSinus.offset = c[4];
    dampedSinus. offset_err = rep.errpar[4];


    fitEnvironment_var.startFreq=xValues[0];
    fitEnvironment_var.stopFreq=xValues[xValues.size()-1];
    fitEnvironment_var.lowerLimit=lowerLimit;
    fitEnvironment_var.upperLimit=upperLimit;
    fitEnvironment_var.steps=xValues.size();

    plotFittedData(xValues, "dampedSin");
    curFittedKurveName = "dampedSin";
}


//####################################################################################
//###########################  Guess Fit Parameters: START ###########################
//####################################################################################

QVector<double> findLocalMax(QVector<int> valX, QVector<double> valY, double lowerVal, double upperVal){
    QVector<double> result(2);
    result[0]=0; //value of maxima
    result[1]=0; //x position of the maxima

    for(int i=0; i<valX.size(); i++){
        if(valX[i]>lowerVal && valX[i]<upperVal){
            if(valY[i]>result[0]){
                result[0]=valY[i];
                result[1]=valX[i];
            }
        }
    }

    return result;
}

// fit parameters for dumped sine
QVector<double> fittingroutines::guessFitParametersDampedSine(QVector<double> valXd, QVector<double> valY)
{
    QVector<int> valX(valXd.size());
    for(int i=0; i<valXd.size();i++){
        valX[i]=(int)valXd[i];
    }

    QVector<double> guess_parameter(5);
    int checkpoint = 2;

    // guess amplitude
    double maxY = *std::max_element(valY.constBegin(), valY.constEnd());
    double minY = *std::min_element(valY.constBegin(), valY.constEnd());
    double amp_guess = (maxY-minY)/2.;
    guess_parameter[0]=amp_guess;



    // guess offset
    double off_guess = maxY-amp_guess;
    guess_parameter[1]=off_guess;



    // guess omega
    // FIRST: determine places where the cosine goes over/under the offset
    QVector<double> x_down, x_up;
    QVector<double> y_down, y_up;

    for(int i=checkpoint; i<valX.size(); i++){
        //break if the length of the vector is reached
        if(i+checkpoint+1 == valX.size()){
            break;
        // checks whether the curve rises
        }else if(valY[i]>off_guess && valY[i+1]<off_guess){
            //to be sure that this isn't noise check two other points next to it
            if(valY[i-checkpoint] > off_guess && valY[i+checkpoint+1] < off_guess){
                x_down.append(valX[i]+(valX[i+1]-valX[i])/2);
                y_down.append(valY[i]-(valY[i]-valY[i+1])/2.);
            }
        // checks whether the curve falls
        }else if(valY[i]<off_guess && valY[i+1]>off_guess){
            //to be sure that this isn't noise check two other points next to it
            if(valY[i-checkpoint] < off_guess && valY[i+checkpoint+1] > off_guess){
                x_up.append(valX[i]+(valX[i+1]-valX[i])/2);
                y_up.append(valY[i+1]-(valY[i+1]-valY[i])/2.);
            }
        }
    }

    // SECOND: if the points are to close to each other one of them has to be removed
    for(int i=1; i<x_down.size(); i++){
        if((x_down[i-1]+200)>x_down[i]){
            x_down[i]=0;
            y_down[i]=0;
        }
    }

    for(int i=1; i<x_up.size(); i++){
        if((x_up[i-1]+200)>x_up[i]){
            x_up[i]=0;
            y_up[i]=0;
        }
    }

    //remove all zeros
    x_down.erase(std::remove(std::begin(x_down),std::end(x_down),0), std::end(x_down));
    y_down.erase(std::remove(std::begin(y_down),std::end(y_down),0), std::end(y_down));
    x_up.erase(std::remove(std::begin(x_up),std::end(x_up),0), std::end(x_up));
    y_up.erase(std::remove(std::begin(y_up),std::end(y_up),0), std::end(y_up));

    // THIRD: calculate the period
    int nb_down=0;
    double T_down=0.;
    for(int i=1; i<x_down.size();i++){
        T_down = T_down+x_down[i]-x_down[i-1];
        nb_down++;
    }
    T_down =T_down/nb_down;

    int nb_up=0;
    double T_up=0.;
    for(int i=1; i<x_up.size();i++){
        T_up = T_up+x_up[i]-x_up[i-1];
        nb_up++;
    }
    T_up=T_up/nb_up;

    double T=0.5*(T_down+T_up);

    // FOURTH: calculate guessed omega
    double w_guess=2*M_PI/T;
    guess_parameter[2]=w_guess;



    // guess phi
    double phi_guess=(std::fmod(x_up[0],T)/T)*2*M_PI;

    // phi has to be smaller than pi
    if(phi_guess>M_PI){
        phi_guess = qAbs(phi_guess-2*M_PI);
    }
    guess_parameter[3]=phi_guess;



    // guess tau
    // FIRST: Determine the maximum of the first full sine
    QVector<double> sine1(2);
    if(x_down[0] > x_up[0]){
        sine1 = findLocalMax(valX, valY, x_up[0], x_down[0]);
    }else{
        sine1 = findLocalMax(valX, valY, x_up[0], x_down[1]);
    }

    //SECOND: Determine the maximum of the last full sine
    //check which vector is longer
    int lastSine=std::min(x_up.size(),x_down.size())-1;

    QVector<double> sine2(2);
    if(x_down[lastSine] > x_up[lastSine]){
        sine2 = findLocalMax(valX, valY, x_up[lastSine], x_down[lastSine]);
    }else{
        sine2 = findLocalMax(valX, valY, x_up[lastSine-1], x_down[lastSine]);
    }

    // THIRD: calculate tau1 (first sine) and tau2 (last sine)
    double factor = 0.5;
    double tau1 = -(qLn((sine1[0]-off_guess)/amp_guess)/sine1[1]);
    if(isnan(tau1)) factor=1.;
    double tau2 = -(qLn((sine2[0]-off_guess)/amp_guess)/sine2[1]);
    if(isnan(tau2)) factor=1.;

    double tau_guess = factor*(tau1+tau2);
    if(isnan(tau_guess)) tau_guess=0.00001;
    guess_parameter[4]=tau_guess;

    return guess_parameter;
}

//####################################################################################
//############################  Guess Fit Parameters: END ############################
//####################################################################################


void fittingroutines::plotFittedData(QVector<double> xValues, std::string functionType){
    // generate some data:
    double stepSize = xValues[1]-xValues[0];
    int nbSteps = xValues.size();
    QVector<double> x(stepSize*nbSteps*10), y(stepSize*nbSteps*10);
    //emit(graphDataClear());
    for (int i=0; i<(stepSize*nbSteps*10); ++i)
    {
      x[i] = i*0.1+xValues[0];
      if(functionType=="singleLorentzian"){
          y[i] = -(Lorentzian1.amp*pow(Lorentzian1.FWHM,2))/(pow((x[i]-Lorentzian1.w0),2)+pow(Lorentzian1.FWHM,2))+Lorentzian1.offset;
      }else if(functionType=="doubleLorentzian"){
          y[i] = -(Lorentzian1.amp*pow(Lorentzian1.FWHM,2)/(pow((x[i]-Lorentzian1.w0),2)+pow(Lorentzian1.FWHM,2))+Lorentzian2.amp*pow(Lorentzian2.FWHM,2)/(pow((x[i]-Lorentzian2.w0),2)+pow(Lorentzian2.FWHM,2)))+Lorentzian1.offset;
      }else if(functionType=="dampedSin"){
          //y[i] = dampedSinus.amp*sin(dampedSinus.omegaMHz/1000*x[i]+dampedSinus.phi)*exp(-x[i]/dampedSinus.tau) + dampedSinus.offset;
          y[i] = dampedSinus.amp*sin(dampedSinus.omegaMHz*x[i]+dampedSinus.phi)*exp(-x[i]*dampedSinus.tau) + dampedSinus.offset;
      }
      //emit(graphData(x[i],y[i]));
    }
    //emit(plotGraph());

    wavegenPlot->graph(1)->setData(x, y);
    wavegenPlot->graph(1)->setPen(QPen(Qt::red));


    wavegenPlot->replot();
}
