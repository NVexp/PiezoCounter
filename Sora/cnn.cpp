#include "cnn.h"

#include<iostream>
#include<QVector>
#include<QtMath>
#include<QFile>
#include<QTextStream>
#include<QFileInfo>
#include<QMessageBox>

#include<stdlib.h>
#include<time.h>

#include<random>

CNN::CNN(QString weight_path, QString type)
{
    srand(time(NULL));

    // path to the files (CNN folder)
    this->weight_path = weight_path;

    /* saves type of the  used neural network
     * annwoBias : Artificial Neural Network without bais
     * ann       : Artificial Neural Network with bias
     * cnnwoBias : Convolutional Neural Network without Bias (recommended)
     * cnn       : Convolutional Neural Network with Bias
     * */
    this->type = type;

    // returns an error if the weights, bias or filters don't exist
    QVector<bool> no_error;

    // depending on which neural network has been selected, the weights and bias are loaded
    if(type == "ann" || type == "annwoBias"){

        // characteristaion
        no_error.append(readWeightsBiasFromFile("weights&bias/"+type+"/hiddenLayer1_weights_"+type+".dat", "weights&bias/"+type+"/hiddenLayer1_bias_"+type+".dat", 25, 17, hiddenLayer1_weights, delta_hiddenLayer1_weights, hiddenLayer1_bias));
        no_error.append(readWeightsBiasFromFile("weights&bias/"+type+"/hiddenLayer2_weights_"+type+".dat", "weights&bias/"+type+"/hiddenLayer2_bias_"+type+".dat", 17, 10, hiddenLayer2_weights, delta_hiddenLayer2_weights, hiddenLayer2_bias));
        no_error.append(readWeightsBiasFromFile("weights&bias/"+type+"/outputLayer_weights_"+type+".dat", "weights&bias/"+type+"/outputLayer_bias_"+type+".dat", 10, 5, outputLayer_weights, delta_outputLayer_weights, outputLayer_bias));
    }else if(type == "cnn" || type == "cnnwoBias"){

        // feature Extraction
        no_error.append(readFiltersFromFile("filters/filter1.dat",filter1));
        no_error.append(readFiltersFromFile("filters/filter2.dat",filter2));
        no_error.append(readFiltersFromFile("filters/filter3.dat",filter3));
        no_error.append(readFiltersFromFile("filters/filter4.dat",filter4));

        // characteristaion
        no_error.append(readWeightsBiasFromFile("weights&bias/"+type+"/hiddenLayer1_weights_"+type+".dat", "weights&bias/"+type+"/hiddenLayer1_bias_"+type+".dat", 36, 20, hiddenLayer1_weights, delta_hiddenLayer1_weights, hiddenLayer1_bias));
        no_error.append(readWeightsBiasFromFile("weights&bias/"+type+"/outputLayer_weights_"+type+".dat", "weights&bias/"+type+"/outputLayer_bias_"+type+".dat", 20, 5, outputLayer_weights, delta_outputLayer_weights, outputLayer_bias));

    }

    // looks for errors after loading
    bool no_error_found = true;
    for(int i=0; i<no_error.size(); i++){
        no_error_found = no_error[i];
        if(!no_error_found) break;
    }
    if(no_error_found){
        std::cout << "weights, bias (and filters) for " << type.toStdString() << " are loaded!" << std::endl;
    }else{
        std::cout << "error by loading weights, bias (or filters) for " << type.toStdString() << "!" << std::endl;
    }

    /* TODO:
     * filter learning function
     * change filter function
     * change filter size to 3x3
     * irgendwas stimmt mit den Input nicht (das inputlayer für die erste schicht ist glaube ich nicht das convolutional bei dem ann in der new weights function)
     * was ist mit dem Pooling?? (Error und output nachdem Filter sind unterschiedlich groß) (Internetseite!)
     */
}


//####################################################################################
//############################# CNN/ANN Predicting: START ############################
//####################################################################################

// guess NV-centers as well as position and wide and height of the bounding box
QVector<QVector<double>> CNN::predicted(QVector<double> nonScaledInputData, int nb_pixel_X, int nb_pixel_Y, int resolution)
{
    QVector<QVector<QVector<double>>> predictedOutcome;
    QVector<QVector<double>> NVpositions, NVpositionsNMS; // NMS = Non Max Suppression
    QVector<double> NVdata(5);

    // if the resolution of an image is bigger than in the range of 35-70 than the resolution has to be scaled to it
    while(resolution > 70){
        nonScaledInputData = rescaleSizeInput(nonScaledInputData, nb_pixel_X, nb_pixel_Y);
        // to get the new resolution the old one has to divid by two
        resolution = (int)(resolution/2.);
        // the new number of pixels are twice as big - 1
        nb_pixel_X = 2*nb_pixel_X-1;
        nb_pixel_Y = 2*nb_pixel_Y-1;
    }

    // if the number of pixels in x / y direction is not divisible by 20, they have to bee rescaled
    if((nb_pixel_X%20)!=0 || (nb_pixel_Y%20)!=0) nonScaledInputData = rescaleInput(nonScaledInputData, nb_pixel_X, nb_pixel_Y);

    // calculate the number of 20x20px cells in the incoming image (always round up)
    int nb_cellsX = ceil((double)nb_pixel_X/20.);
    int nb_cellsY = ceil((double)nb_pixel_Y/20.);

    // YOLO (You only look once)- algorythm
    // YOLO devids an bigger piscture into smaller ones which are each analysed by there own
    /* predictedOutcome       : number of 20x20 pixel parts in the image
     * predictedOutcome[*]    : number of 5x5 pixel in the 20x20 pixel part
     * predictedOutcome[*][*] : information like probability[0], x and y position[1][2], wide and heigth of the bounding box[3][4]
     */
    predictedOutcome = YOLO20x20(nonScaledInputData, nb_cellsX*20, nb_cellsY*20);

    // positions in the respective section and in the overall picture
    double cur_posX, cur_posY, tot_posX, tot_posY;

    // check whether NV centers are available
    if(predictedOutcome[0][0].size() != 0){
        for(int i=0; i<predictedOutcome.size(); i++){
            for(int j=0; j<16; j++){ // every 20x20px is devided in 16 5x5px images
                if(predictedOutcome[i][j][0] > 0.8){

                    // calculate the positions of the NV-center in the total image
                    // position in the 20x20px part of the image
                    cur_posX = (((j%4)*5)+(predictedOutcome[i][j][1]*5))*resolution;
                    cur_posY = (((j/4)*5)+(predictedOutcome[i][j][2]*5))*resolution;

                    // position in the total image
                    tot_posX = cur_posX+(i%nb_cellsX)*(resolution*20);
                    tot_posY = cur_posY+(i/nb_cellsY)*(resolution*20);

                    NVdata[0] = predictedOutcome[i][j][0];
                    NVdata[1] = tot_posX;
                    NVdata[2] = tot_posY;
                    NVdata[3] = predictedOutcome[i][j][3]*resolution*5;           // wide
                    NVdata[4] = predictedOutcome[i][j][4]*resolution*5;           // height

                    NVpositions.append(NVdata);
                }
            }
        }
    }

    // Non Max Suppression
    // because the image is divided into different parts, there are several bounding boxes for one NV center
    if(NVpositions.size()!=0) NVpositionsNMS = nonMaxSuppression(NVpositions);

    return NVpositionsNMS;
}

// prepares data for the neural network (scale)
QVector<double> CNN::prepareInput(QVector<double> inputData){
    // scales data between 0 and 1 in order to be able to calculate better
    QVector<double> scaledData;

    // calculate the average and set everything below and everything more than average+100 to zero
    double average = 0;
    for(int i=0; i<inputData.size(); i++){
        average += inputData[i];
    }

    average /= inputData.size();
    for(int i=0; i<inputData.size(); i++){
        if(inputData[i] < (average+5)) inputData[i]=-1.;
        if(inputData[i] > (average+100)) inputData[i]=-1.;
    }

    // scale the data between 0 and 1 so that the NN hasn't high numbers
    scaledData.resize(inputData.size());
    double min = *std::min_element(inputData.begin(),inputData.end());
    double max = *std::max_element(inputData.begin(),inputData.end()) - min;

    for(int i=0; i<inputData.size(); i++){
        scaledData[i] = (((inputData[i] - min))/max);
    }

    return scaledData;
}

// the YOLO algorithm always requires 20x20 image sections. If the transferred image does not have a number of pixels that can be divided by 20, it must be expanded.
QVector<double> CNN::rescaleInput(QVector<double> inputData, int nb_pixel_X, int nb_pixel_Y)
{
    // vector with the expanded data
    QVector<double> resizedInputData;

    // scaling to an image divisible by 20

    // calculate the number of 20x20px cells in the incoming image (always round up)
    int nb_cellsX = ceil((double)nb_pixel_X/20.);
    int nb_cellsY = ceil((double)nb_pixel_Y/20.);

    // scale between 0 and 1 using the average
    double average = 0;
    for(int i=0; i<inputData.size(); i++){
        average += inputData[i];
    }
    average /= inputData.size();

    // fills the remaining data with the average value (x and y)
    for(int i=0; i<nb_pixel_X; i++){
        for(int j=0; j< nb_pixel_X; j++){
            resizedInputData.append(inputData[(i*nb_pixel_X)+j]);
        }
        for(int k=0; k<((nb_cellsX*20)-nb_pixel_X); k++){
            resizedInputData.append(average);
        }
    }
    for(int l=0; l<(((nb_cellsY*20)-nb_pixel_Y)*(nb_cellsX*20)); l++){
        resizedInputData.append(average);
    }

    return resizedInputData;
}

// sum up two vectors and divide the result by 2
QVector<double> betweenVectors(QVector<double> a, QVector<double> b){
    QVector<double> c;
    for(int i=0; i<a.size(); i++){
        c.append((a[i]+b[i])/2.);
    }
    return c;
}

// instead of using several networks for different resolutions, the resolution is scaled between 40 and 70
QVector<double> CNN::rescaleSizeInput(QVector<double> inputData, int nb_pixel_X, int nb_pixel_Y){
    // vector with the current data
    QVector<QVector<double>> rescaledSizeX, rescaledSizeY;
    QVector<double> curRow, rescaledSizeData;

    // add the values to the rows (x- direction)
    int index=0;
    for(int i=0; i< nb_pixel_Y; i++){
        for(int j=0; j<2*nb_pixel_X-1; j++){
            if(j%2==0){
                curRow.append(inputData[i*nb_pixel_X+index]);
                ++index;
            }else{
                curRow.append((inputData[i*nb_pixel_X+index-1]+inputData[i*nb_pixel_X+index])/2.);
            }
        }
        index=0;
        rescaledSizeX.append(curRow);
        curRow.clear();

    }

    // add the values to the colums (y- direction)
    index=0;
    for(int i=0; i<2*rescaledSizeX.size()-1; i++){
        if(i%2==0){
            rescaledSizeY.append(rescaledSizeX[index]);
            index++;
        }else{
            rescaledSizeY.append(betweenVectors(rescaledSizeX[index-1],rescaledSizeX[index]));
        }
    }

    // form an Vector out of an vector<vector>
    for(int i=0; i<rescaledSizeY.size(); i++){
        for(int j=0; j<rescaledSizeY[i].size(); j++){
            rescaledSizeData.append(rescaledSizeY[i][j]);
        }
    }

    return rescaledSizeData;
}

// use the Neural Network the calculate the predicted values
QVector<double> CNN::calculateOutput(QVector<double> input){

    // vector with data on the probability, the position and the bounding box
    QVector<double> predictedOutcome;
    // CNN
    if(type == "cnn" || type == "cnnwoBias"){
        inputLayer_ann = convolution(input);
    }

    // ANN
    if(type == "ann"|| type == "annwoBias"){
        inputLayer_ann = input;
    }

    // calculate the outcome of the layers
    hiddenLayer1_out = reLu(weight_sum(inputLayer_ann,hiddenLayer1_weights,hiddenLayer1_bias));
    if(type == "ann"|| type == "annwoBias"){
        // second hidden layer rectifier linear unit
        hiddenLayer2_out = reLu(weight_sum(hiddenLayer1_out,hiddenLayer2_weights,hiddenLayer2_bias));
        // output layer leaky rectifier linear unit
        predictedOutcome = leakyReLu(weight_sum(hiddenLayer2_out,outputLayer_weights,outputLayer_bias));
    }else if(type == "cnn"|| type == "cnnwoBias"){
        // output layer leaky rectifier linear unit
        predictedOutcome = leakyReLu(weight_sum(hiddenLayer1_out,outputLayer_weights,outputLayer_bias));
    }

    // returns the predicted values
    return predictedOutcome;
}

//####################################################################################
//############################## CNN/ANN Predicting: END #############################
//####################################################################################


//####################################################################################
//################# feature Extraction (convolution/ pooling): START #################
//####################################################################################

// matrix multiplication
double matrixMulti(QVector<QVector<double>> input1, QVector<QVector<double>> input2){
    double product = 0;
    for(int i=0; i<input1.size(); i++){
        for(int j=0; j<input1[0].size(); j++){
            product += input1[j][i]*input2[i][j];
        }
    }
    return product/(input1.size()*input1[0].size());
}

// applies feature extraction
QVector<double> CNN::convolution(QVector<double> input){

    afterFilterData.resize(36);
    afterFilterData.fill(0);
    int nbOfFilter=0;

    // paddingImageData is a vector with further data left and rights as well as above and below
    QVector<double> paddingImageData = convolutionPadding(input);
    // saves the data after a filter has been applied to the colormap
    QVector<double> cur_Filter_Output;

    // applies a filter to the data
    cur_Filter_Output = useFilter("filter1", paddingImageData);
    // set every value below zero to zero (rectifier linear unit)
    cur_Filter_Output = reLu(cur_Filter_Output);
    // downsize the number of data using max pooling
    cur_Filter_Output = maxPooling2x2(cur_Filter_Output);

    // add data after a filter has been applied
    for(int i=0; i<(3*3); i++){
        afterFilterData[i+(nbOfFilter*0)] += cur_Filter_Output[i];
    }

    // same procedure as above
    cur_Filter_Output = useFilter("filter2", paddingImageData);
    cur_Filter_Output = reLu(cur_Filter_Output);
    cur_Filter_Output = maxPooling2x2(cur_Filter_Output);
    for(int i=0; i<(3*3); i++){
        afterFilterData[i+(nbOfFilter*1)] += cur_Filter_Output[i];
    }

    // same procedure as above
    cur_Filter_Output = useFilter("filter3", paddingImageData);
    cur_Filter_Output = reLu(cur_Filter_Output);
    cur_Filter_Output = maxPooling2x2(cur_Filter_Output);
    for(int i=0; i<(3*3); i++){
        afterFilterData[i+(nbOfFilter*2)] += cur_Filter_Output[i];
    }

    // same procedure as above
    cur_Filter_Output = useFilter("filter4", paddingImageData);
    cur_Filter_Output = reLu(cur_Filter_Output);
    cur_Filter_Output = maxPooling2x2(cur_Filter_Output);
    for(int i=0; i<(3*3); i++){
        afterFilterData[i+(nbOfFilter*3)] += cur_Filter_Output[i];
    }

    // after filter can be described as a new colormap
    return afterFilterData;
}

// apllies a filter to the data
QVector<double> CNN::useFilter(QString filter, QVector<double> paddingImageData)
{
    // vector for the currnet filter (depends of the filter type 'filter')
    QVector<QVector<double>> cur_filter;
    QVector<QVector<double>> cur_data = filter1; //just to fill the array with values
    // data after the filter has been applied
    QVector<double> output;

    // fills the array depending on which filter is to be used
    if(filter == "filter1"){
        cur_filter = filter1;
    }else if(filter == "filter2"){
        cur_filter = filter2;
    }else if(filter == "filter3"){
        cur_filter = filter3;
    }else if(filter == "filter4"){
        cur_filter = filter4;
    }

    // applies the filter
    int counter = 0;
    for(int i=0; i<33; i++){
        if((i!=0) && (i%((counter*7)+5)==0)){
            i += 2;
            counter++;
        }
        // determine the part of the image which is used for the output
        for(int j=0; j<3; j++){
            for(int k=0; k<3; k++){
                cur_data[j][k] = paddingImageData[(j*7)+k+i];
            }
        }
        output.append(matrixMulti(cur_data,cur_filter));
    }

    return output;
}

// adds further data left and right as well as above and below to the colormap
QVector<double> CNN::convolutionPadding(QVector<double> input)
{
    QVector<double> paddingImageData;

    // adds zeros to the right and left, above and below
    for(int i=0; i<7; i++){
        if(i==0 || i==6){
            for(int j=0; j<7; j++){
                paddingImageData.append(0.);
            }
        }else{
            for(int j=0; j<7; j++){
                if(j==0 || j==6){
                    paddingImageData.append(0.);
                }else{
                    paddingImageData.append(input[((i-1)*5)+j-1]);
                }
            }
        }
    }

    return paddingImageData;
}

// reduces the amount of data without losing any information
QVector<double> CNN::maxPooling2x2(QVector<double> input)
{
    QVector<double> pooling;
    QVector<double> output;
    QVector<double> scaledInput;
    // save the positions of the max element (for learning)
    QVector<int> positionMaxPoolingSquare;
    //positionMaxPooling.fill(0);

    int inputSqrt = qSqrt(input.size());

    // increases the array if it has an odd number of elements
    if(inputSqrt%2==1){
        int index = 0;
        for(int i=0; i<inputSqrt; i++){
            for(int j=0; j<inputSqrt; j++){
                scaledInput.append(input[index]);
                index++;
            }
            scaledInput.append(0);
        }
        for(int l=0; l<(inputSqrt+1); l++){
            scaledInput.append(0);
        }
    }
/*
    for(int i=0; i<6; i++){
        for(int j=0; j<6; j++){
            std::cout << scaledInput[(i*6)+j] << ", ";
        }
        std::cout << std::endl;
    }
*/
    // 2x2 pooling means that the maximum value is taken from a 2x2 square
    // then the box is moved two pixels further and also the maximum is taken from the 2x2 square and so on
    int counter=0;
    for(int i=0; i<9; i++){ // output has a size of 3x3 pixels
        if((i!=0) && (i%3==0)){
            counter++;
        }
        for(int j=0; j<2;j++){
            for(int k=0; k<2; k++){
                pooling.append(scaledInput[(j*6)+k+(counter*6)+(2*i)]);
                //std::cout << (j*6)+k+(counter*6) +(2*i) << ", ";
            }
            //std::cout << std::endl;
        }
        //std::cout << std::endl;
        output.append(*std::max_element(pooling.begin(),pooling.end()));
        positionMaxPoolingSquare.append(std::max_element(pooling.begin(),pooling.end()) - pooling.begin());
        pooling.clear();
    }
    /*
    std::cout << std::endl;
    std::cout << "size positionMax: " << positionMaxPoolingSquare.size() << std::endl;
    std::cout << std::endl;
    for(int i=0; i<positionMaxPoolingSquare.size(); i++){
        std::cout << positionMaxPoolingSquare[i] << ", ";
    }
    std::cout << "lalal" << std::endl;
    //std::cout << positionMaxPoolingSquare[42] << std::endl;
*/
    positionMaxPooling.append(positionMaxPoolingSquare);
    positionMaxPoolingSquare.clear();


    return output;
}

//####################################################################################
//################# feature Extraction (convolution/ pooling): START #################
//####################################################################################


//####################################################################################
//##################### classification (new weights/ ANN): START #####################
//####################################################################################

// divides the error of the first hidden layer into the errors of the individual filters
QVector<QVector<double>> splitError(QVector<double> errorVec, int sizeOfCurVec){
    QVector<QVector<double>> error;
    QVector<double> curErrorVec(sizeOfCurVec);

    for(int i=0; i<(errorVec.size()/sizeOfCurVec); i++){
        for(int j=0; j<sizeOfCurVec; j++){
            curErrorVec[j]=errorVec[(i*sizeOfCurVec)+j];
        }
        error.append(curErrorVec);
    }
    return error;
}

// applies the classification using an artificial neural network
void CNN::ann()
{
    // calculate the outcome of the hidden as well as output layers
    hiddenLayer1_out = reLu(weight_sum(inputLayer_ann,hiddenLayer1_weights,hiddenLayer1_bias));
    if(type == "ann" || type == "annwoBias"){
        hiddenLayer2_out = reLu(weight_sum(hiddenLayer1_out,hiddenLayer2_weights,hiddenLayer2_bias));
        outputLayer_out = leakyReLu(weight_sum(hiddenLayer2_out,outputLayer_weights,outputLayer_bias));

    }else if(type == "cnn" || type == "cnnwoBias"){
        outputLayer_out = leakyReLu(weight_sum(hiddenLayer1_out,outputLayer_weights,outputLayer_bias));
    }


    // calculate the new weights
    if(type == "ann" || type == "annwoBias"){
        QVector<double> delta_Etot_h2out = newWeights_outputLayer(hiddenLayer2_out);
        QVector<double> delta_Etot_h1out = newWeights_hiddenLayer2(delta_Etot_h2out);
        newWeights_hiddenLayer1(delta_Etot_h1out);
    }else if(type == "cnn" || type == "cnnwoBias"){
        QVector<double> delta_Etot_h1out = newWeights_outputLayer(hiddenLayer1_out);
        QVector<double> delta_E_convolution = newWeights_hiddenLayer1(delta_Etot_h1out);

        // calculate new filters
        // calculate size of output for each filter
        int sizeOfOutputError = delta_E_convolution.size()/4;
        QVector<QVector<double>> filterError = splitError(delta_E_convolution, sizeOfOutputError);
        filter1 = newFilter_convolutiona1(filterError[0], positionMaxPooling[0], filter1);
        filter2 = newFilter_convolutiona1(filterError[1], positionMaxPooling[1], filter2);
        filter3 = newFilter_convolutiona1(filterError[2], positionMaxPooling[2], filter3);
        filter4 = newFilter_convolutiona1(filterError[3], positionMaxPooling[3], filter4);

        positionMaxPooling.clear();
    }
}

//####################################################################################
//###################### classification (new weights/ ANN): END ######################
//####################################################################################


//####################################################################################
//##############################  YOLO algorithm: START ##############################
//####################################################################################

// YOLO (You only look once)- algorythm
// YOLO devids an bigger piscture into smaller ones which are each analysed by there own

// divides the large image into 20x20 sections and examines them for NV centers
QVector<QVector<QVector<double>>> CNN::YOLO20x20(QVector<double> input, int nb_pixel_X, int nb_pixel_Y)
{
    // the total image is divided in 20x20 pixel parts
    QVector<double> cur_inputData;
    QVector<QVector<QVector<double>>> predictedOutcome;


    // caclulate the number of images in x and y direction
    int nb_cellsX = ceil((double)nb_pixel_X/20.);
    int nb_cellsY = ceil((double)nb_pixel_Y/20.);

    // look for NV centers in every 20x20 pixel image
    for(int imageY=0; imageY<nb_cellsY; imageY++){
        for(int imageX=0; imageX<nb_cellsX; imageX++){
            // gets the data for the 20x20px images
            for(int i=0; i<20; i++){
                for(int j=0; j<20; j++){
                    cur_inputData.append(input[(imageY*20*nb_pixel_X)+(i*nb_pixel_X)+(imageX*20)+j]);
                }
            }
            // examines every 5x5 section of the 20x20 section
            predictedOutcome.append(YOLO5x5(prepareInput(cur_inputData)));
            cur_inputData.clear();
        }
    }

    // returns the guess parameter
    return predictedOutcome;
}

// divides the 20x20 sections in 5x5 sections and examines them for NV centers
QVector<QVector<double>> CNN::YOLO5x5(QVector<double> input)
{
    QVector<QVector<double>> predictedOutcome;
    QVector<double> cur_image_predictedOutcome;
    QVector<double> cur_inputData;

    // every 20x20px image is devided in 16 5x5px rectangels (4 in x and 4 in y)
    int nb_cellsX = 4;
    int nb_cellsY = 4;
    int nb_pixel_X = 20;

    // look for a NV centers in every 5x5 pixel image
    for(int imageY=0; imageY<nb_cellsY; imageY++){
        for(int imageX=0; imageX<nb_cellsX; imageX++){
            // gets the data for the 5x5px images
            for(int i=0; i<5; i++){
                for(int j=0; j<5; j++){
                    cur_inputData.append(input[(imageY*5*nb_pixel_X)+(i*nb_pixel_X)+(imageX*5)+j]);
                }
            }
            // predict the probability of NV-center or non NV-center
            predictedOutcome.append(calculateOutput(cur_inputData));
            cur_inputData.clear();
        }
    }
    // returns the guess parameter
    return predictedOutcome;
}

// combine vectors with diffrent elements (needed for the non max suppresion)
QVector<int> combineVectorsWithDifferentElements(QVector<int> input1, QVector<int> input2)
{
    QVector<int> output = input1;
    for(int i=0; i<input2.size(); i++){
        if(!(std::find(input1.begin(),input1.end(),input2[i]) != input1.end())) output.append(input2[i]);
    }

    return output;
}

// return the position of the hightest probability of a number of choosen bounding boxes
int getMaxPosition(QVector<double> input)
{
    int maxPosition = 0;
    double max = *std::max_element(input.begin(), input.end());
    for(int i=0; i<input.size(); i++){
        if(input[i] == max) maxPosition=i;
    }
    return maxPosition;
}

// every NV-center which is bigger than one 5x5px cell has multiple bounding boxe
// this function will choose the bounding box with the highest probability
QVector<QVector<double>> CNN::nonMaxSuppression(QVector<QVector<double>> input)
{
    QVector<QVector<double>> predictedPosition;
    QVector<QVector<int>> orderedGroup, combineGroup;
    QVector<int> curPredictedGroup;
    QVector<double> curNVdata(4);


    // form groups of bounding boxes which belongs to each other
    for(int i=0; i<input.size(); i++){

       // gets every bounding boxes which have their center in the bounding box of element i
        for(int j=0; j<input.size();j++){
            if(input[j][1]<(input[i][1]+input[i][3]/2.+10) && input[j][1]>(input[i][1]-input[i][3]/2.-10)
                    && input[j][2]<(input[i][2]+input[i][4]/2+10) && input[j][2]>(input[i][2]-input[i][4]/2.-10)){
                curPredictedGroup.append(j);
            }
        }
        orderedGroup.append(curPredictedGroup);
        curPredictedGroup.clear();
    }


    // form groups with all bounding boxes which belongs to each other
    QVector<int> selectInGroup;

    bool end = false;
    int index1 = 0, index2;
    do{
        index2=0;

        do{
            orderedGroup[index1] = combineVectorsWithDifferentElements(orderedGroup[index1],orderedGroup[orderedGroup[index1][index2]]);
            selectInGroup.append(orderedGroup[index1][index2]);
            index2++;
        }while(!(index2==orderedGroup[index1].size()));

        combineGroup.append(orderedGroup[index1]);

        for(int i=0; i<selectInGroup.size(); i++){
            if(std::find(selectInGroup.begin(),selectInGroup.end(),index1+1) != selectInGroup.end()) index1++;
        }
        if(index1==(orderedGroup.size()-1))end=true;
        index1++;

    }while(!end);

    // searches for the bounding box with the highest probability and saves its values ​​for the NV center
    for(int i=0; i<combineGroup.size(); i++){
        QVector<double> probability;
        for(int j=0;j<combineGroup[i].size();j++){
            probability.append(input[combineGroup[i][j]][0]);
        }
        int maxPosition = getMaxPosition(probability);
        curNVdata[0]=input[combineGroup[i][maxPosition]][1];
        curNVdata[1]=input[combineGroup[i][maxPosition]][2];
        curNVdata[2]=input[combineGroup[i][maxPosition]][3];
        curNVdata[3]=input[combineGroup[i][maxPosition]][4];
        predictedPosition.append(curNVdata);
    }

    return predictedPosition;
}

//####################################################################################
//###############################  YOLO algorithm: END ###############################
//####################################################################################



//####################################################################################
//###############################  learn ANN/CNN: START ##############################
//####################################################################################

// learn neural network using the stochastic gradient decent
void CNN::learnNN_SGB(int epoche, int nb_images, double learning_rate)
{
    // set the learning rate
    this->learning_rate = learning_rate;

    // read training data
    readLearnDataFromFile();

    // start learning process
    for(int i=0; i<nb_images; i++){
        // determine an radom image
        int random = rand()%(learnDataInput.size());

        // set output data
        y_true = learnDataOutput[random];

        // set input data
        // feature Extraction
        if(type == "cnn" || type == "cnnwoBias"){
            inputLayer_cnn = learnDataInput[random];
            inputLayer_ann = convolution(learnDataInput[random]);
        }

        // classification
        if(type == "ann" || type == "annwoBias"){
            inputLayer_ann = learnDataInput[random];
        }

        // upgrade progressbar in the GUI
        emit changePackage(i+1);

        // stop learning when the process has been canceled
        if(stopLearning){
            emit finish(true);
            return;
        }

        // learn the artificial neural network
        for(int j=0; j<epoche; j++){
            ann();
        }

        std::cout << "loss: " << square_error(y_true, outputLayer_out) << std::endl;
    }
    emit finish(true);
}

// learn neural network using the mini batch gradient decent
void CNN::learnNN_MBGD(int epoche, int nb_packages, int nb_per_package, double learning_rate)
{
    // set the learning rate
    this->learning_rate = learning_rate;

    // read training data
    readLearnDataFromFile();

    // save random numbers for the learning process
    QVector<int> image_package(nb_per_package);

    // start learning process
    for(int i=0; i<nb_packages; i++){

        //fills the packages with random numbers
        for(int j=0; j<nb_per_package; j++){
             image_package[j] = rand()%(learnDataInput.size());
        }

        for(int k=0; k<epoche; k++){
            for(int l=0; l<nb_per_package; l++){
                y_true = learnDataOutput[image_package[l]];

                // feature Extraction
                if(type == "cnn" || type == "cnnwoBias"){
                    inputLayer_cnn = learnDataInput[image_package[l]];
                    inputLayer_ann = convolution(learnDataInput[image_package[l]]);
                }

                // classification
                if(type == "ann" || type == "annwoBias"){
                    inputLayer_ann = learnDataInput[image_package[l]];
                }

                // learn the artificial neural network
                ann();

            }

            // upgrade progressbar in the GUI
            emit changeEpoch((int)((((double)k+1)/(double)epoche)*100));

            // stop learning when the process has been canceled
            if(stopLearning){
                emit finish(true);
                return;
            }
        }
        // upgrade the GUI
        emit changePackage(i+1);

        // determine the total loss
        double lossTotal=0;
        for(int m=0; m<nb_per_package; m++){
            lossTotal += square_error(learnDataOutput[image_package[m]], calculateOutput(learnDataInput[image_package[m]]));
        }
        std::cout << "\nloss total: " << lossTotal << std::endl;
    }
    emit finish(true);
}

// calculate the new weights of the ANN
// for the output layer
QVector<double> CNN::newWeights_outputLayer(QVector<double> hiddenLayerOut)
{
    // calculate the predicted output
    QVector<double> Oout = leakyReLu(weight_sum(hiddenLayerOut, outputLayer_weights, outputLayer_bias));                               // weight_sum(input, weights, bias) == Oinput!
    // calculate the error derivation (loss) of every output
    QVector<double> delta_E_Oout = square_error_derivation(y_true,Oout);
    // calculate the derivation between input und output of the output neuron
    QVector<double> delta_Oout_Oin = leakyReLu_derivation(weight_sum(hiddenLayerOut, outputLayer_weights, outputLayer_bias));           // weight_sum(input, weights, bias) == Oinput!


    // calculates the error derivation which is needed for the next Layer
    QVector<double> delta_Etot_h2out(outputLayer_weights.size());

    for(int i=0; i<outputLayer_weights.size(); i++){
        double cur_delta_Etot_h2out = 0;
        for(int j=0; j<outputLayer_weights[0].size(); j++){
            cur_delta_Etot_h2out += delta_E_Oout[j]*delta_Oout_Oin[j]*outputLayer_weights[i][j];
        }
        delta_Etot_h2out[i] = cur_delta_Etot_h2out;
    }

    // determine the new weights of the output layer
    for(int i=0; i<outputLayer_weights.size(); i++){
        for(int j=0; j<outputLayer_weights[i].size();j++){
            delta_outputLayer_weights[i][j]=delta_E_Oout[j]*delta_Oout_Oin[j]*hiddenLayerOut[i];
            outputLayer_weights[i][j] -= (learning_rate*delta_outputLayer_weights[i][j]);
        }
    }

    // determine the new bias of the output layer
    if(type == "ann" || type == "cnn"){
        for(int i=0; i<outputLayer_bias.size(); i++){
            delta_outputLayer_bias[i] = delta_E_Oout[i] * delta_Oout_Oin[i] * 1;
            outputLayer_bias[i]-=(learning_rate*delta_outputLayer_bias[i]);
        }
    }

    // returns the error derivation which is needed for the next layer
    return delta_Etot_h2out;
}

// for the second hidden layer
QVector<double> CNN::newWeights_hiddenLayer2(QVector<double> delta_Etot_h2out)
{
    // calculate the derivation between input und output of the second hidden layer neuron
    QVector<double> delta_H2out_H2in = reLu_derivation(weight_sum(hiddenLayer1_out, hiddenLayer2_weights, hiddenLayer2_bias));          // weight_sum(input, weights, bias) == H2input!

    // calculates the error derivation which is needed for the next Layer or Convolution
    QVector<double> delta_Etot_h1out(hiddenLayer2_weights.size());

    double cur_delta_Etot_h1out = 0;
    for(int i=0; i<hiddenLayer2_weights.size(); i++){
        cur_delta_Etot_h1out = 0;
        for(int j=0; j<delta_Etot_h2out.size(); j++){
            cur_delta_Etot_h1out += delta_Etot_h2out[j]*delta_H2out_H2in[j]*hiddenLayer2_weights[i][j];
        }
        delta_Etot_h1out[i] = cur_delta_Etot_h1out;
    }

    // determine the new weights of the second hidden layer
    for(int i=0; i<hiddenLayer2_weights.size(); i++){
        for(int j=0; j<hiddenLayer2_weights[0].size();j++){
            delta_hiddenLayer2_weights[i][j]=delta_Etot_h2out[j]*delta_H2out_H2in[j]*hiddenLayer1_out[i];
            hiddenLayer2_weights[i][j] -= (learning_rate*delta_hiddenLayer2_weights[i][j]);
        }
    }

    // determine the new bias of the second hidden layer
    if(type == "ann" || type == "cnn"){
        for(int i=0; i<hiddenLayer2_bias.size(); i++){
            delta_hiddenLayer2_bias[i] = delta_Etot_h2out[i] * delta_H2out_H2in[i] * 1;
            hiddenLayer2_bias[i]-=(learning_rate*delta_hiddenLayer2_bias[i]);
        }
    }

    // returns the error derivation which is needed for the next layer or convolution
    return delta_Etot_h1out;
}

// for the first hidden layer
QVector<double> CNN::newWeights_hiddenLayer1(QVector<double> delta_Etot_h1out)
{
    // calculate the derivation between input und output of the first hidden layer neuron
    QVector<double> delta_H1out_H1in = reLu_derivation(weight_sum(inputLayer_ann, hiddenLayer1_weights, hiddenLayer1_bias));          // weight_sum(input, weights, bias) == H1input!

    // calculates the error derivation which is needed for the next Layer or Convolution
    QVector<double> delta_E_convolution(hiddenLayer1_weights.size());

    double cur_delta_E_convolution = 0;
    for(int i=0; i<hiddenLayer1_weights.size(); i++){
        cur_delta_E_convolution = 0;
        for(int j=0; j<hiddenLayer1_weights[0].size(); j++){
            cur_delta_E_convolution += delta_Etot_h1out[j]*delta_H1out_H1in[j]*hiddenLayer1_weights[i][j];
        }
        delta_E_convolution[i] = cur_delta_E_convolution;
    }

    // determine the new weights of the first hidden layer
    for(int i=0; i<hiddenLayer1_weights.size(); i++){
        for(int j=0; j<hiddenLayer1_weights[i].size();j++){
            delta_hiddenLayer1_weights[i][j]=delta_Etot_h1out[j]*delta_H1out_H1in[j]*inputLayer_ann[i];
            hiddenLayer1_weights[i][j] -= (learning_rate*delta_hiddenLayer1_weights[i][j]);
        }
    }

    // determine the new bias of the first hidden layer
    if(type == "ann" || type == "cnn"){
        for(int i=0; i<hiddenLayer1_bias.size(); i++){
            delta_hiddenLayer1_bias[i] = delta_Etot_h1out[i] * delta_H1out_H1in[i] * 1;
            hiddenLayer1_bias[i]-=(learning_rate*delta_hiddenLayer1_bias[i]);
        }
    }

    // returns the error derivation which is needed for the next layer or convolution
    return delta_E_convolution;
}

// sum up the input of the neuron
QVector<double> CNN::weight_sum(QVector<double> input, QVector<QVector<double>> weights, QVector<double> bias)
{
    QVector<double> output(bias.size());
    for(int i=0; i<bias.size(); i++){
        double curWeight_sum = 0;
        for(int j=0; j<input.size(); j++){
            curWeight_sum += input[j]*weights[j][i];
        }
        output[i]= curWeight_sum + bias[i];
    }

    // returns the summed inputs
    return output;
}

// calculate the new filter of the CNN
QVector<QVector<double>> CNN::newFilter_convolutiona1(QVector<double> delta_Output, QVector<int> errorPosition, QVector<QVector<double>> curFilter){
    QVector<QVector<double>> curErrorData, curInputData, curErrorFilter=curFilter;


    // calculate the Vector<Vector>> size of the input
    int sqrtInput = (int)qSqrt(inputLayer_cnn.size());


    // Due to the max pooling, there is only one error for the maximum value so that the output has a significantly smaller dimension.
    // To solve the problem, the other values ​​of the error must be set to zero.
    // In the following, the output with the correct dimension is formed from the stored positions.
    QVector<double> filledDelta_output(errorPosition.size()*4);
    filledDelta_output.fill(0);

    int counterRow=0;
    int counterColumn=0;
    for(int i=0; i<errorPosition.size();i++){
        if(i!=0 && i%3==0){
            counterRow+=2;
            counterColumn=0;
        }
        switch(errorPosition[i]){
        case 0: filledDelta_output[(counterRow*6)+counterColumn+0]=delta_Output[i];
            break;
        case 1: filledDelta_output[(counterRow*6)+counterColumn+1]=delta_Output[i];
            break;
        case 2: filledDelta_output[(counterRow*6)+6+counterColumn+0]=delta_Output[i];
            break;
        default: filledDelta_output[(counterRow*6)+6+counterColumn+1]=delta_Output[i];
            break;
        }

        counterColumn+=2;
    }
/*
    std::cout << "delta_output: " << std::endl;
    for(int i=0; i<delta_Output.size(); i++){
        std::cout << delta_Output[i] << ", ";
    }
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "error position: " << std::endl;
    for(int i=0; i<errorPosition.size(); i++){
        std::cout << errorPosition[i] << ", ";
    }
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "filled delta output: " << std::endl;
    for(int i=0; i<filledDelta_output.size(); i++){
        std::cout << filledDelta_output[i] << ", ";
    }
    std::cout << std::endl;
    std::cout << std::endl;*/

   /* for(int i=0; i<curErrorFilter.size(); i++){
        for(int j=0; j<curErrorFilter[0].size();j++){
            std::cout << curErrorFilter[i][j] << ", ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
*/
    // delete elements to get the same size like the input data
    delta_Output.clear();
    for(int i=1; i<(filledDelta_output.size()-5); i++){
        if((i%6!=0) || i==0){
            delta_Output.append(filledDelta_output[i-1]);
        }
    }

   /* std::cout << "delta output new: " << std::endl;
    for(int i=0; i<delta_Output.size(); i++){
        std::cout << delta_Output[i] << ", ";
    }
    std::cout << std::endl;
    std::cout << std::endl;*/

    // calculate the Vector<Vector>> size of the output
    int sqrtError = (int)qSqrt(delta_Output.size());

    QVector<double> curErrorDataRow(sqrtError), curInputDataRow(sqrtInput);

    // create an Vector<Vector> for the Input data
    for(int i=0; i<sqrtInput; i++){
        for(int j=0; j<sqrtInput; j++){
            curInputDataRow[j]=inputLayer_cnn[(i*sqrtInput)+j];
        }
        curInputData.append(curInputDataRow);
    }/*
    std::cout << "cur input data: " << std::endl;
    for(int i=0; i<curInputData.size(); i++){
        for(int j=0; j<curInputData[0].size();j++){
            std::cout << curInputData[i][j] << ", ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;*/

    // create an Vector<Vector> for the Error data
    for(int i=0; i<sqrtError; i++){
        for(int j=0; j<sqrtError; j++){
            curErrorDataRow[j]=delta_Output[(i*sqrtError)+j];
        }
        curErrorData.append(curErrorDataRow);
    }

   /* std::cout << "cur error data: " << std::endl;
    for(int i=0; i<curErrorData.size(); i++){
        for(int j=0; j<curErrorData[0].size();j++){
            std::cout << curErrorData[i][j] << ", ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
*/
    // calculate the gradient of each filter
    // goes through each element of the filter
    for(int i_filter=0; i_filter<(sqrtInput-curFilter[0].size()+1); i_filter++){
        for(int j_filter=0; j_filter<(sqrtInput-curFilter.size()+1); j_filter++){

            // goes through the output elements und sum them up
            for(int i_out=0; i_out<(sqrtInput-curFilter[0].size()+1); i_out++){
                for(int j_out=0; j_out<(sqrtInput-curFilter.size()+1); j_out++){
                    curErrorFilter[i_filter][j_filter] += curErrorData[i_out][j_out]*curInputData[i_filter+i_out][j_filter+j_out];
                }
            }

        }
    }
/*    std::cout << std::endl;
    std::cout << "i_filter, i_out max: " << sqrtInput-curFilter[0].size() << std::endl;
    std::cout << "j_filter, j_out max: " << sqrtInput-curFilter.size() << std::endl;
    std::cout << std::endl;
*/
    /*for(int i=0; i<delta_Output.size(); i++){
        std::cout << delta_Output[i] << ", ";
    }
    std::cout << std::endl;
    std::cout << std::endl;
    */

/*    for(int i=0; i<curErrorFilter.size(); i++){
        for(int j=0; j<curErrorFilter[0].size();j++){
            std::cout << curErrorFilter[i][j] << ", ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
*/
/*
    for(int i=0; i<curFilter.size(); i++){
        for(int j=0; j<curFilter[0].size();j++){
            std::cout << curFilter[i][j] << ", ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
*/
    // update the filter using the leraning rate
    for(int i=0; i<curFilter.size(); i++){
        for(int j=0; j<curFilter[0].size(); j++){
            curFilter[i][j]=curFilter[i][j]-learning_rate*curErrorFilter[i][j];
        }
    }

    return curFilter;
    // https://medium.com/@pavisj/convolutions-and-backpropagations-46026a8f5d2c
    // https://www.jefkine.com/general/2016/09/05/backpropagation-in-convolutional-neural-networks/
}
//####################################################################################
//################################  learn ANN/CNN: END ###############################
//####################################################################################


//####################################################################################
//#####################  activation functions + derivation: START ####################
//####################################################################################

// sigmoid function
QVector<double> CNN::sigmoid(QVector<double> weight_sum)
{
    QVector<double> predicted(weight_sum.size());
    for(int i=0; i<weight_sum.size();i++){
        predicted[i]=1/(1+qExp(-weight_sum[i]));
    }
    return predicted;
}

QVector<double> CNN::sigmoid_derivation(QVector<double> input)
{
    QVector<double> derivation(input.size());
    for(int i=0; i<input.size(); i++){
        derivation[i] = (1/(1+qExp(-input[i])))*(1-(1/(1+qExp(-input[i]))));
    }
    return derivation;
}

// rectifier linear unit function
QVector<double> CNN::reLu(QVector<double> weight_sum)
{
    QVector<double> predicted(weight_sum.size());
    for(int i=0; i<weight_sum.size(); i++){
        if(weight_sum[i]<0){
            predicted[i]=0;
        }else{
            predicted[i]=weight_sum[i];
        }
    }
    return predicted;
}

QVector<double> CNN::reLu_derivation(QVector<double> input)
{
    QVector<double> derivation(input.size());
    for(int i=0; i<input.size();i++){
        if(input[i]<0){
            derivation[i] = 0;
        }else{
            derivation[i] = 1;
        }
    }
    return derivation;
}

// leaky rectifier linear unit function
QVector<double> CNN::leakyReLu(QVector<double> weight_sum)
{
    QVector<double> predicted(weight_sum.size());
    for(int i=0; i<weight_sum.size(); i++){
        if(weight_sum[i]<0){
            predicted[i]=0.5*weight_sum[i];
        }else{
            predicted[i]=weight_sum[i];
        }
    }
    return predicted;
}

QVector<double> CNN::leakyReLu_derivation(QVector<double> input)
{
    QVector<double> derivation(input.size());
    for(int i=0; i<input.size();i++){
        if(input[i]<0){
            derivation[i] = 0.5;
        }else{
            derivation[i] = 1;
        }
    }
    return derivation;
}

//####################################################################################
//######################  activation functions + derivation: END #####################
//####################################################################################


//####################################################################################
//########################  loss function + derivation: START ########################
//####################################################################################

// sparese categorical crossentropy (logarithm)
double CNN::sparse_categorical_crossentropy(QVector<double> y_true, QVector<double> y_predicted)
{
    double epsilon = 1e-15;
    double error_sum = 0;
    for(int i=0;i<y_true.size();i++){
        if (y_predicted[i]<epsilon){
            y_predicted[i]=epsilon;
        }else if(y_predicted[i]>(1-epsilon)){
            y_predicted[i]=1-epsilon;
        }
        error_sum -= (y_true[i]*qLn(y_predicted[i])+(1-y_true[i])*qLn(1-y_predicted[i]));

    }
    return error_sum;
}

QVector<double> CNN::sparse_categorical_crossentropy_derivation(QVector<double> y_true, QVector<double> y_predicted)
{
    double epsilon = 1e-15;
    QVector<double> error_derivation(y_true.size());
    for(int i=0;i<y_true.size();i++){
        if (y_predicted[i]<epsilon){
            y_predicted[i]=epsilon;
        }else if(y_predicted[i]>(1-epsilon)){
            y_predicted[i]=1-epsilon;
        }
        error_derivation[i] = -((y_true[i]*(1/(y_predicted[i])))+((1-y_true[i])*(1/(1-y_predicted[i]))));
    }
    return error_derivation;
}

// square error function
double CNN::square_error(QVector<double> y_true, QVector<double> y_predicted)
{
    double error_sum = 0;
    for(int i=0;i<y_true.size();i++){
        error_sum += 0.5*pow((y_true[i]-y_predicted[i]),2);
    }
    return error_sum;
}

QVector<double> CNN::square_error_derivation(QVector<double> y_true, QVector<double> y_predicted)
{
    QVector<double> error_derivation(y_true.size());
    for(int i=0;i<y_true.size();i++){
        error_derivation[i] = -(y_true[i]-y_predicted[i]);
    }
    return error_derivation;
}

//####################################################################################
//#########################  loss function + derivation: END #########################
//####################################################################################

// saves the new determined weights and bias in the file
void CNN::saveNeuralNetwork()
{
    // save weights and bias for ann with or without bias
    if(type == "ann" || type == "annwoBias"){
        saveWeightsBiasInFile("weights&bias/"+type+"/hiddenLayer1_weights_"+type+".dat", "weights&bias/"+type+"/hiddenLayer1_bias_"+type+".dat", hiddenLayer1_weights, hiddenLayer1_bias);
        saveWeightsBiasInFile("weights&bias/"+type+"/hiddenLayer2_weights_"+type+".dat", "weights&bias/"+type+"/hiddenLayer2_bias_"+type+".dat", hiddenLayer2_weights, hiddenLayer2_bias);
        saveWeightsBiasInFile("weights&bias/"+type+"/outputLayer_weights_"+type+".dat", "weights&bias/"+type+"/outputLayer_bias_"+type+".dat", outputLayer_weights, outputLayer_bias);
        std::cout << "new weights and bias for " << type.toStdString() << " are saved!" << std::endl;

    // save weights and bias for cnn with or without bias
    }else if(type == "cnn" || type == "cnnwoBias"){
        saveWeightsBiasInFile("weights&bias/"+type+"/hiddenLayer1_weights_"+type+".dat", "weights&bias/"+type+"/hiddenLayer1_bias_"+type+".dat", hiddenLayer1_weights, hiddenLayer1_bias);
        saveWeightsBiasInFile("weights&bias/"+type+"/outputLayer_weights_"+type+".dat", "weights&bias/"+type+"/outputLayer_bias_"+type+".dat", outputLayer_weights, outputLayer_bias);
        // save the new filters
        saveFilterInFile("filters/filter1.dat", filter1);
        saveFilterInFile("filters/filter2.dat", filter2);
        saveFilterInFile("filters/filter3.dat", filter3);
        saveFilterInFile("filters/filter4.dat", filter4);
        std::cout << "new filters, weights and bias for " << type.toStdString() << " are saved!" << std::endl;
    }
}

// reset the neural network
void CNN::resetNeuralNetwork()
{
    // warning messages
    QMessageBox answer;
    answer.setWindowTitle("Delete Neural Network");

    QMessageBox deleteNN;
    deleteNN.setWindowTitle("Delete Neural Network");
    deleteNN.setText("If you delete the weights and bias, Control will forget everything "
                     "and need to relearn how to recognize NV centers. Are you really sure "
                     "you want to delete everything?");
    deleteNN.setStandardButtons(QMessageBox::Yes);
    deleteNN.addButton(QMessageBox::No);
    deleteNN.setDefaultButton(QMessageBox::No);

    if(deleteNN.exec() == QMessageBox::Yes){
        answer.setText("Weights and Bias deleted! Please save to confirm.");
        answer.exec();

        // fill the weights and bias with random normal distributed number arround zero
        fillRandom_NormalDistribution();
        // reset the filters
        resetFilters();
    }else{
        answer.setText("Control survives! :)");
        answer.exec();
    }
}

// in order not to get extremely high values ​​as input, the weights sum output must be around zero. Therefore a distribution around zero is taken, in this case a normal distribution
void CNN::fillRandom_NormalDistribution()
{
    // get random normal distributed values around zero with a width of 0.1
    std::random_device rd{};
    std::mt19937 gen{rd()};

    std::normal_distribution<> d{0,0.1};

    // new values for the ann with and without bias
    if(type == "ann" || type == "annwoBias"){
        // clear the previous values
        hiddenLayer1_weights.clear();
        hiddenLayer2_weights.clear();
        outputLayer_weights.clear();
        delta_hiddenLayer1_weights.clear();
        delta_hiddenLayer2_weights.clear();
        delta_outputLayer_weights.clear();
        hiddenLayer1_bias.clear();
        hiddenLayer2_bias.clear();
        outputLayer_bias.clear();

        QVector<double> fill_weightsoutcome(5);
        QVector<double> fill_weightshidden1(17);
        QVector<double> fill_weightshidden2(10);

        // first hidden layer
        for(int i=0; i<25;i++){
            for(int j=0; j<17; j++){
                fill_weightshidden1[j]=d(gen);
            }
            hiddenLayer1_weights.append(fill_weightshidden1);
            delta_hiddenLayer1_weights.append(fill_weightshidden1);
            if(hiddenLayer1_bias.size()<17) hiddenLayer1_bias.append(fill_weightshidden1[0]);
            // at ann without bias the bias is set to zero
            if(type == "annwoBias") hiddenLayer1_bias[hiddenLayer1_bias.size()-1]=0;
        }

        // second hidden layer
        for(int i=0; i<17;i++){
            for(int j=0; j<10; j++){
                fill_weightshidden2[j]=d(gen);
            }
            hiddenLayer2_weights.append(fill_weightshidden2);
            delta_hiddenLayer2_weights.append(fill_weightshidden2);
            if(hiddenLayer2_bias.size()<10) hiddenLayer2_bias.append(fill_weightshidden2[0]);
            // at ann without bias the bias is set to zero
            if(type == "annwoBias") hiddenLayer2_bias[hiddenLayer2_bias.size()-1]=0;
        }

        // output layer
        for(int i=0; i<10;i++){
            for(int j=0; j<5; j++){
                fill_weightsoutcome[j]=d(gen);
            }
            outputLayer_weights.append(fill_weightsoutcome);
            delta_outputLayer_weights.append(fill_weightsoutcome);
            if(outputLayer_bias.size()<5) outputLayer_bias.append(fill_weightsoutcome[0]);
            // at ann without bias the bias is set to zero
            if(type == "annwoBias") outputLayer_bias[outputLayer_bias.size()-1]=0;
        }

    // new values for the cnn with and without bias
    }else if(type == "cnn" || type == "cnnwoBias"){

        // clear the previous values
        hiddenLayer1_weights.clear();
        outputLayer_weights.clear();
        delta_hiddenLayer1_weights.clear();
        delta_outputLayer_weights.clear();
        hiddenLayer1_bias.clear();
        outputLayer_bias.clear();

        QVector<double> fill_weightsoutcome(5);
        QVector<double> fill_weightshidden(20);

        // first hidden layer
        for(int i=0; i<36;i++){
            for(int j=0; j<20; j++){
                fill_weightshidden[j]=d(gen);
            }
            hiddenLayer1_weights.append(fill_weightshidden);
            delta_hiddenLayer1_weights.append(fill_weightshidden);
            if(hiddenLayer1_bias.size()<20) hiddenLayer1_bias.append(fill_weightshidden[0]);
            // at cnn without bias the bias is set to zero
            if(type == "cnnwoBias") hiddenLayer1_bias[hiddenLayer1_bias.size()-1]=0;
        }

        // output layer
        for(int i=0; i<20;i++){
            //#####for(int j=0; j<20; j++){
            for(int j=0; j<5; j++){
                fill_weightsoutcome[j]=d(gen);
            }
            outputLayer_weights.append(fill_weightsoutcome);
            delta_outputLayer_weights.append(fill_weightsoutcome);
            if(outputLayer_bias.size()<5) outputLayer_bias.append(fill_weightsoutcome[0]);
            // at cnn without bias the bias is set to zero
            if(type == "cnnwoBias") outputLayer_bias[outputLayer_bias.size()-1]=0;
        }
    }
}

// reset filters
void CNN::resetFilters(){
    //The filters are not reset by means of a normal distribution, but rather reset to given filters that search for quarter circles
    QVector<double> curFilter1 = {-1, -1, 1, -1, 1, 1, 1, 1, 1};
    QVector<double> curFilter2 = {1, -1, -1, 1, 1, -1, 1, 1, 1};
    QVector<double> curFilter3 = {1, 1, 1, -1, 1, 1, -1, -1, 1};
    QVector<double> curFilter4 = {1, 1, 1, 1, 1, -1, 1, -1, -1};

    for(int i=0; i<filter1.size(); i++){
        for(int j=0; j<filter1[0].size(); j++){
            filter1[i][j] = curFilter1[(i*filter1.size())+j];
            filter2[i][j] = curFilter2[(i*filter1.size())+j];
            filter3[i][j] = curFilter3[(i*filter1.size())+j];
            filter4[i][j] = curFilter4[(i*filter1.size())+j];
        }
    }

}


//####################################################################################
//###############################  Read and Write: START #############################
//####################################################################################

// read filter from the files
bool CNN::readFiltersFromFile(QString fileName, QVector<QVector<double>> cur_filter)
{
    QVector<double> row(3);
    bool no_error = true;
    QFile fileFilter(weight_path + fileName);

    if(fileFilter.open(QIODevice::ReadOnly)){
        QTextStream DataFromFile(&fileFilter);
        QString line;
        while(!DataFromFile.atEnd()){
            line = DataFromFile.readLine();
            QStringList splitLine = line.split("\t");
            row[0]=splitLine[0].toDouble();
            row[1]=splitLine[1].toDouble();
            row[2]=splitLine[2].toDouble();
            cur_filter.append(row);
        }
        fileFilter.close();
    }else{
        no_error = false;
    }
    if(fileName == "filters/filter1.dat"){
        filter1 = cur_filter;
    }else if(fileName == "filters/filter2.dat"){
        filter2 = cur_filter;
    }else if(fileName == "filters/filter3.dat"){
        filter3 = cur_filter;
    }else if(fileName == "filters/filter4.dat"){
        filter4 = cur_filter;
    }
    // returns true when an error has occurred
    return no_error;
}

// reads the data from the files for weights and bias
bool CNN::readWeightsBiasFromFile(QString fileNameWeights, QString fileNameBias, int nb_input, int nb_output, QVector<QVector<double>> weights, QVector<QVector<double>> delta_weights, QVector<double> bias)
{
    // returns an error if loading fails
    bool no_error = true;

    QVector<double> fill_weightsOutput(nb_output);

    // reads weights from file
    QFile fileW(weight_path + fileNameWeights);
    if(fileW.open(QIODevice::ReadOnly)){
        QTextStream DataFromFile(&fileW);
        QString line;
        while(!DataFromFile.atEnd()){
            for(int i=0; i<nb_input; i++){
                for(int j=0; j<nb_output; j++){
                    line = DataFromFile.readLine();
                    fill_weightsOutput[j] = line.toDouble();
                }
                weights.append(fill_weightsOutput);
                delta_weights.append(fill_weightsOutput);
            }
        }
        fileW.close();
    }else{
        no_error = false;
    }

    // reads bias from file
    QFile fileB(weight_path + fileNameBias);
    if(fileB.open(QIODevice::ReadOnly)){
        QTextStream DataFromFile(&fileB);
        QString line;
        while(!DataFromFile.atEnd()){
            for(int j=0; j<nb_output; j++){
                line = DataFromFile.readLine();
                bias.append(line.toDouble());
            }
        }
        fileB.close();
    }else{
        no_error = false;
    }

    // sets the weights and bias to the values ​​read from the file
    if(fileNameWeights == "weights&bias/"+type+"/hiddenLayer1_weights_"+type+".dat" && fileNameBias == "weights&bias/"+type+"/hiddenLayer1_bias_"+type+".dat"){
        hiddenLayer1_weights = weights;
        delta_hiddenLayer1_weights = delta_weights;
        hiddenLayer1_bias = bias;
        delta_hiddenLayer1_bias = bias;
    }else if(fileNameWeights == "weights&bias/"+type+"/hiddenLayer2_weights_"+type+".dat" && fileNameBias == "weights&bias/"+type+"/hiddenLayer2_bias_"+type+".dat"){
        hiddenLayer2_weights = weights;
        delta_hiddenLayer2_weights = delta_weights;
        hiddenLayer2_bias = bias;
        delta_hiddenLayer2_bias = bias;
    }else if(fileNameWeights == "weights&bias/"+type+"/outputLayer_weights_"+type+".dat" && fileNameBias == "weights&bias/"+type+"/outputLayer_bias_"+type+".dat"){
        outputLayer_weights = weights;
        delta_outputLayer_weights = delta_weights;
        outputLayer_bias = bias;
        delta_outputLayer_bias = bias;
    }

    // returns true when an error has occurred
    return no_error;
}

// saves the current weights and bias in the file
void CNN::saveWeightsBiasInFile(QString fileNameWeights, QString fileNameBias, QVector<QVector<double>> weights, QVector<double> bias)
{
    // save the weights
    QFile fileW(weight_path + fileNameWeights);
    fileW.open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream outH1w(&fileW);
        for(int i=0; i<weights.size(); i++){
            for(int j=0; j<weights[0].size(); j++){
                outH1w << weights[i][j] << "\n";
            }
        }
    fileW.close();

    // save the bias
    QFile fileB(weight_path + fileNameBias);
    fileB.open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream outH1b(&fileB);
        for(int i=0; i<bias.size(); i++){
            outH1b << bias[i] << "\n";
        }
    fileB.close();
}

// saves the current filters in the file
void CNN::saveFilterInFile(QString fileName, QVector<QVector<double>> filterData)
{
    QFile fileF(weight_path + fileName);
    fileF.open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream outF(&fileF);
        for(int i=0; i<filterData.size(); i++){
            for(int j=0; j<filterData[0].size(); j++){
                outF << filterData[i][j] << "\t";
            }
            outF << "\n";
        }
    fileF.close();
}

// reads the learn data
void CNN::readLearnDataFromFile()
{
    bool no_error = true;

    learnDataInput.clear();
    learnDataOutput.clear();

    // data of the colormap
    QVector<double> cur_learnDataInput(25);
    // output data like position and heigth and wide of the bounding box
    QVector<double> cur_learnDataOutput(5);

    // data where a person looked at it
    QFile file1(weight_path + "TrainingData/5x5trainData1.dat");
    QFileInfo getFilenName_var1(file1);
    if(file1.open(QIODevice::ReadOnly)){
        QTextStream DataFromFile(&file1);
        QString line;
        while(!DataFromFile.atEnd()){
            line = DataFromFile.readLine();
            QStringList splitLine = line.split("\t");
            for(int i=0; i<5; i++){
                cur_learnDataOutput[i]=splitLine[i].toDouble();
            }
            for(int j=0; j<25; j++){
                cur_learnDataInput[j]=splitLine[j+5].toDouble();
            }
            learnDataInput.append(cur_learnDataInput);
            learnDataOutput.append(cur_learnDataOutput);
        }
        file1.close();
    }else{
        no_error = false;
    }
/*
    // data where NO person looked at (Control has to decide this alone!!)
    QFile file2(weight_path + "TrainingData/5x5trainData2.dat");
    QFileInfo getFilenName_var2(file2);
    if(file2.open(QIODevice::ReadOnly)){
        QTextStream DataFromFile(&file2);
        QString line;
        while(!DataFromFile.atEnd()){
            line = DataFromFile.readLine();
            QStringList splitLine = line.split("\t");
            for(int i=0; i<5; i++){
                cur_learnDataOutput[i]=splitLine[i].toDouble();
            }
            for(int j=0; j<25; j++){
                cur_learnDataInput[j]=splitLine[j+5].toDouble();
            }
            learnDataInput.append(cur_learnDataInput);
            learnDataOutput.append(cur_learnDataOutput);
        }
        file2.close();
    }else{
        no_error = false;
    }*/

    std::cout << "number of training examples: " << learnDataInput.size() << std::endl;

    if(!no_error){
        std::cout << "error by loading data!" << std::endl;
    }
}

//####################################################################################
//################################  Read and Write: END ##############################
//####################################################################################
