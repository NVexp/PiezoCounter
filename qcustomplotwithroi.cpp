#include "qcustomplotwithroi.h"
#include <iostream>

void print_QPointF(QPointF p)
{
    std::cout << "x=" << p.x() << ", y=" << p.y() << '\n';
}

QCustomPlotWithROI::QCustomPlotWithROI(QWidget* parentWidget): QCustomPlot(parentWidget), lineH(nullptr), lineV(nullptr)
{
    colorMap = new QCPColorMap(xAxis, yAxis);
    setInteractions(QCP::iRangeZoom|QCP::iSelectItems|QCP::iRangeDrag|QCP::iSelectLegend);


    stagePosition = new QCPItemTracer(this);
    QPen crossPen;
    crossPen.setWidth(2);
    crossPen.setColor(Qt::green);
    stagePosition->setPen(crossPen);
    stagePosition->setVisible(false);
    stagePosition->setStyle(QCPItemTracer::TracerStyle::tsPlus);

    YouAreHere = new QCPItemText(this);
    YouAreHere->setText("You're here !");
    YouAreHere->setColor(Qt::green);
    YouAreHere->setVisible(false);
    YouAreHere->setSelectable(false);
    YouAreHere->position->setType(QCPItemPosition::ptAbsolute);

    //Drop down menu (right click)
    this->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(contextMenuRequest(QPoint)));

}

QCustomPlotWithROI::~QCustomPlotWithROI()
{
    while(ROIStore.size()>0)
    {
        removeROI();
    }
    removeItem(stagePosition);
    removeItem(YouAreHere);
}

// creates drop down menu if right click
void QCustomPlotWithROI::contextMenuRequest(QPoint pos){
    QMenu *menu = new QMenu(this);
    menu->setAttribute(Qt::WA_DeleteOnClose);

    // so that the ROI is created at the current mouse position, the position must be saved
    curMousePosX = this->xAxis->pixelToCoord(pos.x());
    curMousePosY = this->yAxis->pixelToCoord(pos.y());

    menu->addAction("add ROI", this, SLOT(addROIbyClick(bool)));
    menu->addAction("go to ROI", this, SLOT(goToSelectedROI()));
    menu->addAction("delete ROI", this, SLOT(removeSelectedROI()));
    menu->addAction("delete all ROI's", this, SLOT(removeAllROIs(bool)));

    menu->popup(this->mapToGlobal(pos));
}

//go to the position of the selected ROI
void QCustomPlotWithROI::goToSelectedROI(){
    QMessageBox check;
    if(!ROIStore.isEmpty()){
        int index=0;
        bool select = false;

        //check which ROI is selected and emits a singnal with the coordinates
        while(index<ROIStore.size()){
            if(ROIStore[index]->selected()){
                QPointF position = 0.5*(ROIStore[index]->topLeft->coords() + ROIStore[index]->bottomRight->coords());
                emit(posMovedTo(position.x(),position.y()));
                select = true;

            }
            index++;
        }
        //error message if no ROI is selected
        if(!select){
            check.setText("Please select the ROI you want to go");
            check.exec();
        }
    //error message if no ROI's exists
    }else{
        check.setText("There is no ROI");
        check.exec();
    }
}

//returns the length of one cell along X in the color map
double QCustomPlotWithROI::singleCellStepX()
{
    double Xcell1, Xcell0 = 0;
    colorMap->data()->cellToCoord(1, 0, &Xcell1, 0 );
    colorMap->data()->cellToCoord(0, 0, &Xcell0, 0);
    return (Xcell1 - Xcell0);
}

//returns the length of one cell along Y in the color map
double QCustomPlotWithROI::singleCellStepY()
{
    double Ycell1, Ycell0 = 0;
    colorMap->data()->cellToCoord(0, 1, 0, &Ycell1);
    colorMap->data()->cellToCoord(0, 0, 0, &Ycell0);
    return (Ycell1 - Ycell0);
}

//get the coordinate of the next middle of pixel in order to move keep items like
//ROIs or cursor lines centered on pixels
QPointF QCustomPlotWithROI::getNextPixelCoords(QPointF point)
{
    double px_X = singleCellStepX();
    double xMin = colorMap->data()->keyRange().lower;
    double intX = round((point.x() - xMin)/px_X);
    double px_Y = singleCellStepY();
    double yMin = colorMap->data()->valueRange().lower;
    double intY = round((point.y() - yMin)/px_Y);
    return QPointF{xMin + px_X*intX, px_Y*intY + yMin};
}

//add at the current mouse position a ROI
//activated by drop down menu
void QCustomPlotWithROI::addROIbyClick(bool state){
    //size equal to 300nm
    addROI({curMousePosX,curMousePosY}, 0.3, 0.3);
    enableROILegend(enableLegend);
    this->replot();
}

//setup and add a ROI to the graph (using coordinates)
void QCustomPlotWithROI::addROI(QPointF ROICenter, double sizeX, double sizeY)
{
    //dynamically construct a new ROI
    QCPRectROI* ROI = new QCPRectROI(this);
    ROI->setPen(QPen(Qt::red, 6));
    ROI->setSelectedPen(QPen(Qt::green,8));
    // QCPItemPosition should be ptPlotCoords by default but we want to make it really clear & explicit here !!
    ROI->topLeft->setType(QCPItemPosition::ptPlotCoords);
    ROI->bottomRight->setType(QCPItemPosition::ptPlotCoords);

    QPointF quarterROI{sizeX/2.,sizeY/2.};
    ROI->topLeft->setCoords(ROICenter - quarterROI);
    ROI->bottomRight->setCoords(ROICenter + quarterROI);

    //add the ROI* to the vector storing all of them
    ROIStore.push_back(ROI);
    ROI->ROIid = ROIStore.size();


    //add a doted vertical and horizontal line to the ROI that cross in the middle of it
    ROI->HLine->start->setParentAnchor(ROI->top);
    ROI->HLine->end->setParentAnchor(ROI->bottom);
    ROI->HLine->setSelectable(false);
    ROI->HLine->setPen(Qt::DotLine);
    ROI->VLine->start->setParentAnchor(ROI->left);
    ROI->VLine->end->setParentAnchor(ROI->right);
    ROI->VLine->setPen(Qt::DotLine);
    ROI->VLine->setSelectable(false);

    //round convert the sizes of the ROI to integers
    //then setup the size label for the ROI
    ROI->scaleFactor = 0.01;
    QPointF sizeOfROI = getROIsize(ROIStore.size());
    std::string sizestr = "ROI " + std::to_string(ROI->ROIid) + ": " + std::to_string(sizeOfROI.x()*1000) + "nm, " + std::to_string(sizeOfROI.y()*1000)+"nm";
    ROI->sizeLabel->setText(QString::fromStdString(sizestr));
    ROI->sizeLabel->setSelectable(false);
    ROI->sizeLabel->setColor(Qt::white);

    ROI->sizeLabel->position->setType(QCPItemPosition::ptAbsolute);
    QPointF sizepos = ROI->bottom->pixelPosition() - QCPRectROI::label_offset;
    ROI->sizeLabel->position->setCoords(sizepos);

    connect(ROI, SIGNAL(rectangleMoved()), this, SLOT(replot()));
    connect(ROI, SIGNAL(rectangleSizeChange()), this, SLOT(replot()));
}


//setup and add a ROI to the graph (using pixels)
void QCustomPlotWithROI::addROI(QPointF ROICenter, int sizeX, int sizeY)
{
    //dynamically construct a new ROI
    QCPRectROI* ROI = new QCPRectROI(this);
    ROI->setPen(QPen(Qt::red,8));
    ROI->setSelectedPen(QPen(Qt::green,10));
    // QCPItemPosition should be ptPlotCoords by default but we want to make it really clear & explicit here !!
    ROI->topLeft->setType(QCPItemPosition::ptPlotCoords);
    ROI->bottomRight->setType(QCPItemPosition::ptPlotCoords);

    int sizeXodd = sizeX + (1-sizeX%2);
    int sizeYodd = sizeY + (1-sizeY%2);

    //define a vector that spans one quarter of the area of the ROI (half x_size * half y_size)
    QPointF quarterROI{((double)sizeXodd/2.)*singleCellStepX(), ((double)sizeYodd/2.)*singleCellStepY()};

    ROI->topLeft->setCoords(ROICenter - quarterROI);
    ROI->bottomRight->setCoords(ROICenter + quarterROI);
    //QPointF quarterROI = {sizeX/2., sizeY/2.};

    //add the ROI* to the vector storing all of them
    ROIStore.push_back(ROI);

    ROI->ROIid = ROIStore.size();
    std::cout << "ROIid:" << ROI->ROIid << '\n';

    //add a doted vertical and horizontal line to the ROI that cross in the middle of it
    ROI->HLine->start->setParentAnchor(ROI->top);
    ROI->HLine->end->setParentAnchor(ROI->bottom);
    ROI->HLine->setSelectable(false);
    ROI->HLine->setPen(Qt::DotLine);
    ROI->VLine->start->setParentAnchor(ROI->left);
    ROI->VLine->end->setParentAnchor(ROI->right);
    ROI->VLine->setPen(Qt::DotLine);
    ROI->VLine->setSelectable(false);

    //round convert the sizes of the ROI to integers
    //then setup the size label for the ROI
    ROI->scaleFactor = 1;
    int sizeXint = roundToInt(ROI->sizeX()/singleCellStepX());
    int sizeYint = roundToInt(ROI->sizeY()/singleCellStepY());
    std::string sizestr = "ROI " + std::to_string(ROI->ROIid) + ": " + std::to_string(sizeXint) + "px, " + std::to_string(sizeYint)+"px";
    ROI->sizeLabel->setText(QString::fromStdString(sizestr));
    ROI->sizeLabel->setSelectable(false);
    ROI->sizeLabel->setColor(Qt::white);

    ROI->sizeLabel->position->setType(QCPItemPosition::ptAbsolute);
    QPointF sizepos = ROI->bottom->pixelPosition() - QCPRectROI::label_offset;
    ROI->sizeLabel->position->setCoords(sizepos);

    //connect the signals of the ROI to the parent plot for resizing and moving
    connect(ROI, SIGNAL(rectangleMoved()), this, SLOT(replot()));
    connect(ROI, SIGNAL(rectangleSizeChange()), this, SLOT(replot()));
}


//remove a ROI using the removeItem QCustomPlot function
//and not just a delete because the QCP owns the item
void QCustomPlotWithROI::removeROI()
{
    if (ROIStore.size() > 0)
    {
        bool success = removeItem(ROIStore[ROIStore.size()-1]);
        YouAreHere->setVisible(false);
        if(success)
            ROIStore.removeLast();
        else
            std::cerr << "Problem removing ROI!\n";
    }
}

//remove the seleced ROI
void QCustomPlotWithROI::removeSelectedROI(){
    QMessageBox check;
    if(!ROIStore.isEmpty()){
        int index=0;
        bool select = false;

        //check which ROI is selected and delete this ROI
        while(index<ROIStore.size()){
            if(ROIStore[index]->selected()){
                removeItem(ROIStore[index]);
                ROIStore.remove(index);
                select = true;

                //if ROI is removed all other ROI's after gets a new Id
                for(int i=0; i<ROIStore.size();i++){
                    ROIStore[i]->ROIid = i+1;
                    std::string sizestr;
                    if (ROIStore[i]->scaleFactor == 1){
                        int sizeXint = roundToInt(ROIStore[i]->sizeX()/singleCellStepX());
                        int sizeYint = roundToInt(ROIStore[i]->sizeY()/singleCellStepY());
                        std::string sizestr = "ROI " + std::to_string(ROIStore[i]->ROIid) + ": " + std::to_string(sizeXint) + "px, " + std::to_string(sizeYint)+"px";
                    }else{
                        QPointF sizeOfROI = getROIsize(ROIStore[i]->ROIid);
                        std::string sizestr = "ROI " + std::to_string(ROIStore[i]->ROIid) + ": " + std::to_string(sizeOfROI.x()*1000) + "nm, " + std::to_string(sizeOfROI.y()*1000)+"nm";
                    }
                    std::cout << sizestr << std::endl;
                    ROIStore[i]->sizeLabel->setText(QString::fromStdString(sizestr));
                }

                this->replot();
            }
            index++;
        }
        //error message if no ROI is selected
        if(!select){
            check.setText("Please select the ROI you want to delete");
            check.exec();
        }
    //error message if no ROI's exists
    }else{
        check.setText("There is no ROI");
        check.exec();
    }
}

//removes all ROI's
void QCustomPlotWithROI::removeAllROIs(bool state){
    QMessageBox check;
    if(ROIStore.size() > 0){
        while (!ROIStore.isEmpty()) {
            removeROI();
        }
        this->replot();
    }else{
        //error message if no ROI's exists
        if(!state){
            check.setText("There is no ROI");
            check.exec();
        }
    }
}

//removes the legend of every ROI (position, height, wide, id)
void QCustomPlotWithROI::enableROILegend(bool state){
    enableLegend = state;
    if(ROIStore.size() > 0){
        for(int i=0; i<ROIStore.size();i++){
            ROIStore[i]->sizeLabel->setVisible(state);
        }
    }
}


//void QCustomPlotWithROI::setNewPosition(int x, int y, int ROIid){
//    ROIStore[ROIid-1]->topLeft()
//}


void QCustomPlotWithROI::enableCrosshair()
{
    lineH = new QCPMovableStraightLine(this);
    lineV = new QCPMovableStraightLine(this);
    QPen linePen;
    linePen.setWidth(1);
    linePen.setStyle(Qt::DashLine);
    lineH->setPen(linePen);
    lineV->setPen(linePen);
    lineH->setSelectable(false);
    lineV->setSelectable(false);
    // QCPItemPosition should be ptPlotCoords by default but we want to make it really clear & explicit here !!
    lineH->point1->setType(QCPItemPosition::ptPlotCoords);
    lineH->point2->setType(QCPItemPosition::ptPlotCoords);
    lineV->point1->setType(QCPItemPosition::ptPlotCoords);
    lineV->point2->setType(QCPItemPosition::ptPlotCoords);

    //get middle point and range of the image
    double keyMiddle = 0.5*(colorMap->data()->keyRange().lower + colorMap->data()->keyRange().upper);
    double valueMiddle = 0.5*(colorMap->data()->valueRange().lower + colorMap->data()->valueRange().upper);
    double keyRange = colorMap->data()->keyRange().upper - colorMap->data()->keyRange().lower;
    double valueRange = colorMap->data()->valueRange().upper - colorMap->data()->valueRange().lower;
    QPointF P1H{keyMiddle - keyRange/4., valueMiddle};
    QPointF P2H{keyMiddle + keyRange/4., valueMiddle};
    QPointF P1V{keyMiddle, valueMiddle - valueRange/4.};
    QPointF P2V{keyMiddle, valueMiddle + valueRange/4.};
    //coordinates for horizontal line
    lineH->point1->setCoords(P1H);
    lineH->point2->setCoords(P2H);
    //coordinates for vertical line
    lineV->point1->setCoords(P1V);
    lineV->point2->setCoords(P2V);

    connect(lineV, SIGNAL(lineMoved()), this, SLOT(replot()));
    connect(lineH, SIGNAL(lineMoved()), this, SLOT(replot()));

    replot();
}



void QCustomPlotWithROI::disableCrosshair()
{
    removeItem(lineH);
    lineH = nullptr;
    removeItem(lineV);
    lineV = nullptr;
    replot();
}

//register effects of keys being pressed on the keyboard
void QCustomPlotWithROI::keyPressEvent(QKeyEvent* event)
{
    int keyPressed = event->key();
    switch(keyPressed)
    {
        case Qt::Key_Control:
            flag_VertSizeChange = true;
        break;
        case Qt::Key_Shift:
            flag_BigStep = true;
        break;
        //needed for Neural Network!
        case Qt::Key_A:
             aPressed = true;
        break;

    }
}

//register effects of keys being released on the keyboard
void QCustomPlotWithROI::keyReleaseEvent(QKeyEvent *event)
{
    int keyPressed = event->key();
    switch(keyPressed)
    {
        case Qt::Key_Control:
            flag_VertSizeChange = false;
        break;
        case Qt::Key_Shift:
            flag_BigStep = false;
        break;
            //needed for Neural Network
        case Qt::Key_A:
            aPressed = false;
        break;

    }
}

//void QCustomPlotWithROI::scanROIsAndSizeChange(QWheelEvent *event)
void QCustomPlotWithROI::wheelEvent(QWheelEvent* event)
{
    bool must_rescale = true;
    //if one ROI is selected, then change its size according to the scrolling
    for (auto ROI : ROIStore)
    {
        if (ROI->selected())
        {
            must_rescale = must_rescale && false;
            ROI->sizeChangeFromWheel(event, flag_VertSizeChange, flag_BigStep);
        }
    }

    //if one ROI was selected, bool 'must_rescale' is now false and the zoom in/out
    //will not take place
    if (must_rescale)
    {
        //I want to keep the functionality of the base class QCustomPlot so I can
        //just call the event handler from the base class ! it is that simple !!
        QCustomPlot::wheelEvent(event);
        //keep position of "you are here !" sign with respect to ROI
        if (YouAreHere->visible())
        {
            QPointF here = stagePosition->position->pixelPosition() - QCPRectROI::label_offset;
            YouAreHere->position->setCoords(here);
        }
        //keep position of size label with respect to the ROI
        for (auto ROI : ROIStore)
        {
            QPointF sizepos = ROI->bottom->pixelPosition() - QCPRectROI::label_offset;
            ROI->sizeLabel->position->setCoords(sizepos);
        }
        replot();
        return;
    }

    replot();
}


//mouse movement handler to keep the position of certain item that are not setup using plot coordinates
//using widget pixel coordinate
void QCustomPlotWithROI::mouseMoveEvent(QMouseEvent* event)
{
    //keep position of "you are here !" sign with respect to ROI
    if (YouAreHere->visible())
    {
        QPointF here = stagePosition->position->pixelPosition() - QCPRectROI::label_offset;
        YouAreHere->position->setCoords(here);
    }
    //keep position of size label with respect to the ROI
    for (auto ROI : ROIStore)
    {
        QPointF sizepos = ROI->bottom->pixelPosition() - QCPRectROI::label_offset;
        ROI->sizeLabel->position->setCoords(sizepos);
    }

    //eventually keep the functionality of the base class QCustomPlot
    QCustomPlot::mouseMoveEvent(event);

}

QPointF QCustomPlotWithROI::getROIposition(int ROIid)
{
    return 0.5*(ROIStore[ROIid-1]->topLeft->coords() + ROIStore[ROIid-1]->bottomRight->coords());
}

QPointF QCustomPlotWithROI::getROIsize(int ROIid){
    QPointF size{ROIStore[ROIid-1]->sizeX(),ROIStore[ROIid-1]->sizeY()};
    return size;
}


//sets the position of "You are here" tracer to the middle of the chosen ROI
void QCustomPlotWithROI::setYouAreHereLabel(int ROIid)
{
    if (!(stagePosition->visible()))
    {
        stagePosition->setVisible(true);
    }
    QPointF ROIpos = getROIposition(ROIid);
    stagePosition->position->setCoords(ROIpos);

    //set the position of the "You are here !" sign
    QPointF here = stagePosition->position->pixelPosition() - QCPRectROI::label_offset;
    YouAreHere->position->setCoords(here);
    YouAreHere->setVisible(true);

    replot();

    //emit signals to talk to the LCD number
    emit(posMovedToX(ROIpos.x()));
    emit(posMovedToY(ROIpos.y()));
}
