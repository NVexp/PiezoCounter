#include "mainwindow.h"
#include <iostream>
//this is blabla
#include <QApplication>


/*
**************************************************************************

rep_std_single.cpp                                       (c) Spectrum GmbH

**************************************************************************

Example for all SpcMDrv based analog and digital generator cards.

Information about the different products and their drivers can be found
online in the Knowledge Base:
https://www.spectrum-instrumentation.com/en/platform-driver-and-series-differences

Shows standard replay mode as single shot, continous or single restart

Feel free to use this source for own projects and modify it in any kind

Documentation for the API as well as a detailed description of the hardware
can be found in the manual for each device which can be found on our website:
https://www.spectrum-instrumentation.com/en/downloads

Further information can be found online in the Knowledge Base:
https://www.spectrum-instrumentation.com/en/knowledge-base-overview

**************************************************************************
*/


/*
**************************************************************************
vDoCardSetup
**************************************************************************
*/


// ----- include standard driver header from library -----
#include "c_header/dlltyp.h"
#include "c_header/regs.h"
#include "c_header/spcerr.h"
#include "c_header/spcm_drv.h"

// ----- include of common example librarys -----
#include "common/spcm_lib_card.h"
#include "common/spcm_lib_data.h"

// ----- operating system dependent functions for thread, event, keyboard and mutex handling -----
#include "common/ostools/spcm_oswrap.h"
#include "common/ostools/spcm_ostools.h"

// ----- standard c include files -----
#include <stdio.h>
#include <stdlib.h>

#include <QCoreApplication>
#include <iostream>

void vDoCardSetup (ST_SPCM_CARDINFO *pstCard, int32 lReplayMode, int64 llLoops = 0)
    {
    int i;
    int64 llChannelMask;


    // set mask for maximal channels
    if (pstCard->lMaxChannels >= 64)
        llChannelMask = -1; // -1 is all bits set to 1 = 0xffffffffffffffff
    else
        llChannelMask = ((int64) 1 << pstCard->lMaxChannels) - 1;


    // we try to set the samplerate to 1 MHz (M2i) or 50 MHz (M4i) on internal PLL, no clock output
    if (pstCard->bM4i)
        bSpcMSetupClockPLL (pstCard, MEGA(50), false);
    else
        bSpcMSetupClockPLL (pstCard, MEGA(100), false);
    printf ("Sampling rate set to %.1lf MHz\n", (double) pstCard->llSetSamplerate / 1000000);


    // setup the replay mode and the trigger
    switch (lReplayMode)
        {

        // with loops == 1: singleshot replay with software trigger
        // with loops == 0: endless continuous mode with software trigger
        case SPC_REP_STD_SINGLE:
            bSpcMSetupModeRepStdLoops  (pstCard, llChannelMask, KILO_B(1), llLoops);
            bSpcMSetupTrigSoftware (pstCard, true);

            // on M2i starting with build 1604 we can use the trigger output as a marker for each loop start
            // be sure to have the trigger output enabled for this
            if (pstCard->bM2i)
                {
                if ((pstCard->lLibVersion & 0xFFFF) >= 1604)
                    spcm_dwSetParam_i32 (pstCard->hDrv, SPC_CONTOUTMARK, 1);
                }
            else
                {
                // all newer replay cards support multi-purpose lines
                spcm_dwSetParam_i32 (pstCard->hDrv, SPCM_X0_MODE, SPCM_XMODE_CONTOUTMARK); // output continuous marker
                }
            break;

        // single restart (one signal on every trigger edge) with ext trigger positive edge
        case SPC_REP_STD_SINGLERESTART:
            bSpcMSetupModeRepStdSingleRestart (pstCard, llChannelMask, KILO_B(1), 0);
            bSpcMSetupTrigExternal (pstCard, SPC_TM_POS, false, 0);
            break;
        }



     // type dependent card setup
    switch (pstCard->eCardFunction)
        {

        // analog generator card setup
        case AnalogOut:

            // program all output channels to +/- 1 V with no offset
            for (i=0; i < pstCard->lMaxChannels; i++)
                bSpcMSetupAnalogOutputChannel (pstCard, i, 1000, 0, 0);
            break;

        // digital generator card setup
        case DigitalOut:
        case DigitalIO:
            for (i=0; i < pstCard->uCfg.stDIO.lGroups; i++)
                bSpcMSetupDigitalOutput (pstCard, i, SPCM_STOPLVL_LOW, 0, 3300);
            break;
        }
    }



/*
**************************************************************************
DoDataCalculation: calculates the output data
**************************************************************************
*/

bool bDoDataCalculation (ST_SPCM_CARDINFO *pstCard, void* pvBuffer)
{
void*   ppvChannelData[SPCM_MAX_AOCHANNEL];
int     i;

printf ("Calculation of output data\n");

switch (pstCard->eCardFunction)
    {

    // analog waveform generator card, each channel gets a different waveform
    case AnalogOut:
        {
        // allocate buffers for each channel
        for (i=0; i < pstCard->lMaxChannels; i++)
            {
            if (pstCard->lBytesPerSample == 1)
                ppvChannelData[i] = new int8[(unsigned) pstCard->llSetMemsize];
            else
                ppvChannelData[i] = new int16[(unsigned) pstCard->llSetMemsize];
            if (!ppvChannelData[i])
                return (nSpcMErrorMessageStdOut (pstCard, "Memory allocation error\n", false) == 0);
            }

        // calculate channel data
        for (i=0; i < pstCard->lMaxChannels; i++)
            {
            switch (i)
                {
                case 0: bSpcMCalcSignal (pstCard, ppvChannelData[0], (uint32) pstCard->llSetMemsize, 0, eSine);              break;
                case 1: bSpcMCalcSignal (pstCard, ppvChannelData[1], (uint32) pstCard->llSetMemsize, 0, eTriangle);          break;
                case 2: bSpcMCalcSignal (pstCard, ppvChannelData[2], (uint32) pstCard->llSetMemsize, 0, eSawtooth);          break;
                case 3: bSpcMCalcSignal (pstCard, ppvChannelData[3], (uint32) pstCard->llSetMemsize, 0, eRectangle);         break;
                case 4: bSpcMCalcSignal (pstCard, ppvChannelData[4], (uint32) pstCard->llSetMemsize, 0, eInvertedSine);      break;
                case 5: bSpcMCalcSignal (pstCard, ppvChannelData[5], (uint32) pstCard->llSetMemsize, 0, eInvertedTriangle);  break;
                case 6: bSpcMCalcSignal (pstCard, ppvChannelData[6], (uint32) pstCard->llSetMemsize, 0, eInvertedSawtooth);  break;
                case 7: bSpcMCalcSignal (pstCard, ppvChannelData[7], (uint32) pstCard->llSetMemsize, 0, eInvertedRectangle); break;
                }
            }

        // mux it into the output buffer
        bSpcMMuxData (pstCard, pvBuffer, (uint32) pstCard->llSetMemsize, ppvChannelData);

        // clean up channel buffers
        for (i=0; i < pstCard->lMaxChannels; i++)
            {
            if (pstCard->lBytesPerSample == 1)
                delete [] (int8*)ppvChannelData[i];
            else
                delete [] (int16*)ppvChannelData[i];
            }

        break;
        }

    // digital generator card: sine over all channels
    case DigitalOut:
    case DigitalIO:
        {
        // we need to tell the calc function the number of bytes for one complete word -> [channels/8]
        printf("SetMemSize parameter: %d\n", (uint32)pstCard->llSetMemsize);
        printf("SetChannels parameters: %d\n", pstCard->lSetChannels / 8);
        bSpcMCalcSignal (pstCard, pvBuffer, (uint32) pstCard->llSetMemsize, pstCard->lSetChannels / 8, eSine);
        break;
        }
    }

    return true;
}



int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();

}
