

#include "scanandcount.h"
#include <iostream>
#include <algorithm>
#include <fstream>
#include <chrono>
#include <ctime>
#include <iomanip>
#include <QMessageBox>
#include <QTimer>
#include <labjackt7.h>

#include "qcustomplotwithroi.h"
#include "loadfilecreatmosaic.h"



// LENGTH UNIT FOR ARGUMENTS OF METHODS FOR ScanAndCount CLASS, GLOBAL VARIABLES AND FOR GUI WIDGETS ARE nm
// PI_XXX and PI_qXXX FUNCTIONS TAKE AND RETURN VALUES IN µm
// TIME UNIT IS ps, because unit for TimeTagger20 is ps
static const double dMicrosec = 1.e6;
static const timestamp_t tMicrosec = 1000*1000;
static const double dMillisec = 1.e9;
static const timestamp_t tMillisec = 1000*1000*1000;
static const int iStepXmax = 200;
static const double dStepXmax = static_cast<double>(iStepXmax);
static const int iStepYmax = 200;
static const double dStepYmax = static_cast<double>(iStepYmax);
static const int iStepZmax = 100;
static const double dStepZmax = static_cast<double>(iStepZmax);
static const int segLength_WavGen = 27;
static const double bigStepMaxVelocityX = 1000.;
static const double bigStepMaxVelocityY = 1000.;
static const double bigStepMaxVelocityZ = 1000.;
static const double scanMaxVelocityX = 2000;
static const double scanMaxVelocityY = 2000;

static const int maxNbOfPtsInWaveTable = 512*512;// = 2^18

//static const long testValueWaveTableRate = (int)std::round(30.*dMillisec/servoCycleTime)

#define MAXPOS 300000
#define MINPOS 0


double dVecAverage(const std::vector<double>& V)
{
    unsigned int N = V.size();
    double sum = std::accumulate(V.begin(),V.end(), 0.);
    return sum/(double)N;
}

double dVecVariance(const std::vector<double>& V)
{
    double A = dVecAverage(V);
    unsigned int N = V.size();
    std::vector<double> squares(N, 0.);
    for(int i=0;i<N;++i)
    {
        squares[i] = V[i]*V[i];
    }

    double sqA = dVecAverage(squares);
    return sqA-A*A;
}

void dVecAverageAndVariance(const std::vector<double>& V, double& av, double& var)
{
    unsigned int N = V.size();
    double sum_id = 0.;
    double sum_sq = 0;
    for (int j = 0; j<N; ++j)
    {
        sum_id += V[j];
        sum_sq += V[j]*V[j];
    }

    av = sum_id/(double)N;
    double sq_av = sum_sq/(double)N;
    var = sq_av-av*av;
}

ScanAndCount::ScanAndCount(QCustomPlotWithROI* plot, bool E725_phys_conn, bool TT20_phys_conn)
    : QObject(),
    ID(-1),
    E725_phys_connected(E725_phys_conn),
    TT20_phys_connected(TT20_phys_conn),
    iError(0),
    TT(nullptr),
    currentScanParameters{0,0,0,0,"XY",0,0,{0,0},{0,0,0},0,0,0,0,0,0,0},
    scanningOn(false),
    scanningStoppedByCommand(false),
    image2DPlot(plot),
    currentImageDate_yyyymmddHHMMSS("")
{
    /*
    -----------------------------------------
    connect to the controller over TCP and
    initialize important parameters
    -----------------------------------------
    */

    if(E725_phys_connected)
    {
        ID = PI_ConnectTCPIP("141.51.197.29", 50000);
        if (ID<0)
        {
            iError = PI_GetError(ID);
            PI_TranslateError(iError, szErrorMessage, 1024);
            std::cout << "TCP Connection failure: ERROR" << iError << ": " << szErrorMessage << std::endl;
        }

        char bufferMsg[200];
        if(!PI_qIDN(ID, bufferMsg, 200))
        {
            iError = PI_GetError(ID);
            PI_TranslateError(iError, szErrorMessage, 1024);
            std::cout << "Failure querying device info: ERROR" << iError << ": " << szErrorMessage << std::endl;
        }

        std::cout << "\n\n" << bufferMsg << "\n\n" << std::endl;

        std::cout << "setting the WaveTable rate to " << initValueWaveTableRate << std::endl;
        /*
        int WavGenIds[2] {1,2};
        //int timePerPt = (long)(minTimePerPt/servoCycleTime);
        int WaveTableRates[2] {10, 10};
        int interp[2] {0,0};
        */
        int WavGenIds[1] {0};
        //int timePerPt = (long)(minTimePerPt/servoCycleTime);
        int WaveTableRates[1] {initValueWaveTableRate};
        int interp[1] {0};

        if(!PI_WTR(ID, WavGenIds, WaveTableRates, interp, 1))
        {
            iError = PI_GetError(ID);
            PI_TranslateError(iError, szErrorMessage, 1024);
            std::cout << "Failure setting wave table rate: ERROR" << iError << ": " << szErrorMessage << std::endl;
        }


        /*
        -----------------------------------------
        set the time per point for data recording
        -----------------------------------------

        if(!PI_RTR(ID, testValueWaveTableRate/10))
        {
            iError = PI_GetError(ID);
            PI_TranslateError(iError, szErrorMessage, 1024);
            std::cout << "Failure setting record table rate: ERROR" << iError << ": " << szErrorMessage << std::endl;
        }*/

    }
    else {
        std::cout << "##### PI E-725 piezo controller is registered as not physically connected or not switched on. ####" << std::endl;
    }


    /*
    -----------------------------------------
    connect to the TimeTagger (over USB).
    -----------------------------------------
    */
    if(TT20_phys_connected)
    {
        TT = createTimeTagger("1740000JFU");
        TT->setTriggerLevel(5,0.7);
        TT->setTriggerLevel(6,0.7);
    }
    else {
        std::cout << "##### Swabian Instruments TimeTagger20 is registered as not physically connected or not switched on. ####" << std::endl;
    }
    /*
    -----------------------------------------
    set the time per pixel
    -----------------------------------------
    */

    /*
    -----------------------------------------
    add a proper header to the table
    -----------------------------------------
    */
    QStringList header{"Axis", "Pixel size", "Pixel number"};
    //ui->defdWaveTables->setHorizontalHeaderLabels(header);

    /*
    -----------------------------------------
    set onTarget-Output for piezo controller for axis 1 and 2 to TriggerOutput 2 and 4
    set TriggerOutput 1 on axis 3 to provide crosstalk
    -----------------------------------------
    */

    std::vector<int> piTriggerOutputIds {1,1,2,2,4,4,5};
    std::vector<int> piTriggerParamter {2,3,2,3,2,3,3};
    std::vector<double> pdValue {3,2,1,2,2,2,4};


    /* New ones?????
    //  vectors need to have the same size
    //  piTriggerOutputIds defines the ID of the outputchanel (see p. 261 "I/O Socket" for details of socket pins)
    //  "on-target" needs two piTriggerParameters ("2,3") and pdValues ("<axis>,2")
    //  "line action trigger" needs one piTriggerParameter ("3") and one pdValues ("4")
    std::vector<int> piTriggerOutputIds {1,1,2,2,4,4,6,6,5};
    std::vector<int> piTriggerParamter {2,3,2,3,2,3,2,3,3};
    std::vector<double> pdValue {3,2,1,2,2,2,3,2,4};
    */


    if(!PI_CTO(ID, piTriggerOutputIds.data(), piTriggerParamter.data(), pdValue.data(), pdValue.size()))
    {
        iError = PI_GetError(ID);
        PI_TranslateError(iError, szErrorMessage, 1024);
        std::cout << "Failure to clean waveform: ERROR" << iError << ": " << szErrorMessage << std::endl;
    }

}

ScanAndCount::~ScanAndCount()
{
    if(E725_phys_connected)
    {
        //open control loop of all axes

        setServo(Axis::X, 0);
        setServo(Axis::Y, 0);
        setServo(Axis::Z, 0);


        PI_CloseConnection(ID);
    }

    if(TT20_phys_connected)
    {
        freeTimeTagger(TT);
    }
}

int ScanAndCount::getIStepXmax(){
    return iStepXmax;
}

int ScanAndCount::getIStepYmax(){
    return iStepYmax;
}


inline int sign(int x) { return (x > 0) - (x < 0);}

template<typename T>
std::vector<size_t> find_below_threshold(const std::vector<T>& V, T th)
{
    std::vector<size_t> foundIndices;

    auto it = V.begin();
    if (V[0]<th)
        foundIndices.push_back(0);

    while (it != V.end())
    {
        it = std::find_if(it+1, V.end(), [&th](T h) { return h<th; });
        if (it != V.end())
        {
            foundIndices.push_back(it - V.begin());
        }
    }

    return foundIndices;
}



bool ScanAndCount::setupWaveTableScan_ZigZag(int L1, int L2, int Steps1, int Steps2, int IDAxis1, int IDAxis2)
{
    if ((Steps1 >iStepXmax) || (Steps2 > iStepYmax))
        std::cout << "Step is too large: valid range is up to " << iStepXmax << " for X and " << iStepYmax << " for Y." <<std::endl;
    Steps1 = Min(Steps1, iStepXmax);
    Steps2 = Min(Steps2, iStepYmax);

    /*
    -----------------------------------------
    define Waveform in 2 Wave Tables
    -----------------------------------------
    */

    //first generate the whole data in 2 vectors
    std::vector<double> allXPts, allYPts;
    double XPos = 0;
    double YPos = 0;
    allXPts.reserve(L1*L2+1);
    allYPts.reserve(L1*L2+1);
    for (int i = 0; i<L1*L2; ++i)
    {
        allXPts.push_back(XPos/1000.);
        allYPts.push_back(YPos/1000.);
        XPos += ((i + 1) / L1 == i / L1)*std::pow(-1, i / L1)*Steps1;
        YPos += ((i + 1) / L1 != i / L1) ? Steps2 : 0;// we use the fact that the integer divisions (i+1)/Lx and i/Lx are
                                                     // only different when i reaches the end of a line
                                                     // (i+1) is a multiple of Lx or Ly
    }

    //add a pixel to the wave table in order to make sure the pixel
    //output (so-called Trigger Line Action) finishes the scan with a pixel output high
    //because it allows us to detect/assign data to pixel
    allXPts.push_back(allXPts[L1*L2-1]+Steps1/1000.);
    allYPts.push_back(allYPts[L1*L2-1]);

    //checking that all points stay within the range of the instruments
    double XCoordMax = *std::max_element(allXPts.begin(), allXPts.end());
    double YCoordMax = *std::max_element(allYPts.begin(), allYPts.end());
    double XCoordMin = *std::min_element(allXPts.begin(), allXPts.end());
    double YCoordMin = *std::min_element(allYPts.begin(), allYPts.end());

    if ((XCoordMax*1000. > (double)MAXPOS) || (YCoordMax *1000.> (double)MAXPOS) || (XCoordMin*1000. < (double)MINPOS) || (YCoordMin*1000. < (double)MINPOS))
    {
        std::cout << "The selected scan goes beyond the range of the instrument !! Please use another range." << std::endl;
        return false;
    }

    //then clean the waveforms
    int WaveTableIds[] {IDAxis1,IDAxis2};

    if(!PI_WCL(ID, WaveTableIds, 2))
    {
        iError = PI_GetError(ID);
        PI_TranslateError(iError, szErrorMessage, 1024);
        std::cout << "Failure to clean waveform: ERROR" << iError << ": " << szErrorMessage << std::endl;
        return false;
    }

    //safety check that the code above is not bad
    if((allXPts.size() != allYPts.size()) || allXPts.size()!=L1*L2+1)
    {
        std::cout << "Error on array size for wave table data" << std::endl;
        return false;
    }

    //transfer wave table data for X and then Y
    if (!PI_WAV_PNT(ID, IDAxis1, 1, L1*L2+1, 0, allXPts.data()))
    {
        iError = PI_GetError(ID);
        PI_TranslateError(iError, szErrorMessage, 1024);
        std::cout << "WaveForm upload failure: ERROR" << iError << ": " << szErrorMessage << std::endl;
        return false;
    }

    // then transfer the Y axis points with the right parameters
    if (!PI_WAV_PNT(ID, IDAxis2, 1, L1*L2+1, 0, allYPts.data()))
    {
        iError = PI_GetError(ID);
        PI_TranslateError(iError, szErrorMessage, 1024);
        std::cout << "WaveForm upload failure: ERROR" << iError << ": " << szErrorMessage << std::endl;
        return false;
    }

    int WavGenIds[] {IDAxis1,IDAxis2};

    /*
    -----------------------------------------
     connect Wave Generator to Wave Tables
    -----------------------------------------
    */
    //WaveTable Ids go up to 30
    //can connect several generators/axis to a single wave table
    // weird syntax here because it should at least not have an array
    //as argument for the Wave Table ID

    if (!PI_WSL(ID, WavGenIds, WaveTableIds, 2))
    {
        iError = PI_GetError(ID);
        PI_TranslateError(iError, szErrorMessage, 1024);
        std::cout << "Failure WaveTable connection to WaveGen: ERROR" << iError << ": " << szErrorMessage << std::endl;
        return false;
    }

    /*
    -----------------------------------------
    set the QTable defdWaveTable to indicate what has been set up
    -----------------------------------------
    */
    //WavGenTableLineFill(0, Lx, Xstep, Axis::X); --> moved to Mainwindow.on_TwoDScanSetup_clicked()
    //WavGenTableLineFill(1, Ly, Ystep, Axis::Y); --> moved to Mainwindow.on_TwoDScanSetup_clicked()

    /*
    -----------------------------------------
    set the Wave Generator to 1 cycle
    -----------------------------------------
    */
    int NbOfCycles[] {1,1};
    if (!PI_WGC(ID, WavGenIds, NbOfCycles, 2))
    {
        iError = PI_GetError(ID);
        PI_TranslateError(iError, szErrorMessage, 1024);
        std::cout << "Failure setting WaveGen cycles: ERROR" << iError << ": " << szErrorMessage << std::endl;
        return false;
    }

    /*
    ---------------------------------------------------------------------------
    clear and set the states of the trigger output for points in the waveform
    ---------------------------------------------------------------------------
    */


    if (!PI_TWC(ID))
    {
        iError = PI_GetError(ID);
        PI_TranslateError(iError, szErrorMessage, 1024);
        std::cout << "Failure clearing trigger line action: ERROR" << iError << ": " << szErrorMessage << std::endl;
        return false;
    }

    int block_index = 1;

    //##################################
    //WARNING : WE DETERMINED THAT PI_TWS HAS AN UPPER LIMIT OF LENGTH OF 10 FOR THE ARGUMENT ARRAYS
    //HENCE THE FOLLOWING WHERE THE TRIGGER DEFINITION IS CUT IN SMALL ARRAYS
    //##################################
    std::vector<int> outputIds = {5, 5, 5, 5, 5, 5, 5, 5, 5, 5};
    std::vector<int> waveformStates = {1, 0, 1, 0, 1, 0, 1, 0, 1, 0};
    std::vector<int> waveformPts (10, 0);
    int S;

    if(!AIuse){
        std::cout << "Setting line action trigger:" << std::endl;
    }

    while(block_index*10<=L1*L2+1)//full blocks of max length 10
    {
        S = (block_index-1)*10+1;
        for (int k = 0; k<10; ++k)
        {
            waveformPts[k] = S+k;
        }

        //std::cout << "Block nb " << block_index << std::endl;
        if (!PI_TWS(ID, outputIds.data(), waveformPts.data(), waveformStates.data(), waveformPts.size()))
        {
            iError = PI_GetError(ID);
            PI_TranslateError(iError, szErrorMessage, 1024);
            std::cout << "Failure setting trigger line action: ERROR" << iError << ": " << szErrorMessage << std::endl;
            return false;
        }
        block_index++;
    }

    if ((L1*L2+1)%10 != 0) //last block of length <10; happens only if Lx*Ly is not a multiple of 10
    {
        outputIds.resize((L1*L2+1)%10);
        waveformStates.resize((L1*L2+1)%10);
        waveformPts.resize((L1*L2+1)%10);
        S = (block_index-1)*10+1;
        for (int k = 0; k<(L1*L2+1)%10; ++k)
        {
            waveformPts[k] = S+k;
        }
        if(!AIuse){
            std::cout << "Block nb " << block_index << std::endl;
        }
        if (!PI_TWS(ID, outputIds.data(), waveformPts.data(), waveformStates.data(), waveformPts.size()))
        {
            iError = PI_GetError(ID);
            PI_TranslateError(iError, szErrorMessage, 1024);
            std::cout << "Failure setting trigger line action: ERROR" << iError << ": " << szErrorMessage << std::endl;
            return false;
        }
    }
    std::vector<int> outputIds_test = {5, 5, 5, 5, 5, 5, 5, 5, 5, 5};
    std::vector<int> waveformStates_test = {1, 0, 1, 0, 1, 0, 1, 0, 1, 0};
    std::vector<int> waveformPts_test = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    if (!PI_TWS(ID, outputIds_test.data(), waveformPts_test.data(), waveformStates_test.data(), waveformPts_test.size()))
    {
        iError = PI_GetError(ID);
        PI_TranslateError(iError, szErrorMessage, 1024);
        std::cout << "Failure setting trigger line action: ERROR" << iError << ": " << szErrorMessage << std::endl;
        return false;
    }

    if(!AIuse){
        std::cout << "Trigger line action defined"  << std::endl;
    }

    return true;
}




//calculates the wave generator offset needed for some scan parameters
Pos2D ScanAndCount::waveGenOffsetForZigZag(int Lx, int Ly, int Xstep, int Ystep, Pos2D center)
{
    Pos2D offset {0,0};
    offset.X = center.X-((Lx-1)*Xstep)/2;
    offset.Y = center.Y-((Ly-1)*Ystep)/2;
    if ((offset.X<0) || (offset.Y)<0)
    {
        std::cout << "offset point of scan has negative position: change scan position slightly (shift up and right)." << std::endl;
        QMessageBox msgBox;
        msgBox.setText("offset point of scan has negative position: change scan position slightly (shift up and right).");
        msgBox.exec();
        return offset;
    }
    return offset;
}

bool ScanAndCount::startWaveGenWithCounterScan_ZigZag(int L1, int L2, int Steps1, int Steps2, Pos2D center)
{
    //test if scan is in range of instrument
    if( (center.X-(L1+2)*Steps1/2 < MINPOS) || (center.X+(L1+2)*Steps2/2 > MAXPOS) || (center.Y-(L2+2)*Steps2/2 < MINPOS) || (center.Y+(L2+2)*Steps2/2 > MAXPOS) )
    {
        QMessageBox msgBox;
        msgBox.setText("Scan out of range");
        msgBox.exec();
        return false;
    }

    currentScanParameters.center = center;

    //first calculate and set the right offset for the scan
    Pos2D offsetPt = waveGenOffsetForZigZag(L1, L2, Steps1, Steps2, center);
    int WaveGenIds[2] {currentScanParameters.IDAxis1,currentScanParameters.IDAxis2};
    double offsetAr[2] {(double)(offsetPt.X)/1000., (double)(offsetPt.Y)/1000.};
    if(!PI_WOS(ID, WaveGenIds, offsetAr, 2))
    {
        iError = PI_GetError(ID);
        PI_TranslateError(iError, szErrorMessage, 1024);
        std::cout << "Failure setting WaveGen offset: ERROR" << iError << ": " << szErrorMessage << std::endl;
        return false;
    }
    if(!AIuse){
        std::cout << "offset defined" << std::endl;
    }

    //set the key/Xaxis and value/Yaxis for the colormap plotting
    //X and Y are in microns, go from 0 to Lx-1/Ly-1

    //############## ranges for plot coordinates that start at 0 ##############
    /*
    QCPRange XRange(0, (Lx-1)*Xstep/1000.);
    QCPRange YRange(0, (Ly-1)*Ystep/1000.);
    */

    //############## ranges for absolute plot coordinates, corresponding to the coordinates used by the piezo controller ##############
    QCPRange XRange(offsetPt.X/1000., (offsetPt.X+L1*Steps2)/1000.);
    QCPRange YRange(offsetPt.Y/1000., (offsetPt.Y+L2*Steps2)/1000.);


    image2DPlot->colorMap->data()->setRange(XRange, YRange);
    image2DPlot->colorMap->data()->setSize(L1, L2);

    //initialize the image vector to the correct X and Y sizes filled with zeros
    imageVec.clear();
    imageVec.resize(currentScanParameters.Npx_X*currentScanParameters.Npx_Y, 0.);

    //then move X and Y close to this start point
    bool startXOK;
    bool startYOK;
    if(currentScanParameters.Axis.toStdString()=="XY"){
        startXOK = moveAxisTo_VelLimit(Axis::X, offsetPt.X-Steps1);
        startYOK = moveAxisTo_VelLimit(Axis::Y, offsetPt.Y);
    }else if(currentScanParameters.Axis.toStdString()=="XZ"){
        startXOK = moveAxisTo_VelLimit(Axis::X, offsetPt.X-Steps1);
        startYOK = moveAxisTo_VelLimit(Axis::Z, offsetPt.Y);
    }else if(currentScanParameters.Axis.toStdString()=="YZ"){
        startXOK = moveAxisTo_VelLimit(Axis::Y, offsetPt.X-Steps1);
        startYOK = moveAxisTo_VelLimit(Axis::Z, offsetPt.Y);
    }

    if(!(startXOK && startYOK))
    {
        std::cout << "Error: was unable to move to start position! (might be out of range)";

        return false;
    }
    if(!AIuse){
        std::cout << "moved to start point" << std::endl;
    }

    //setup and start the measurement on the TimeTagger for the scan
    std::vector<channel_t> risingEdg;
    std::vector<channel_t> fallingEdg;

    if(currentScanParameters.Axis.toStdString()=="XY"){
        risingEdg = {3, 7};  // ----WARNING!!!!: THE EQUAL SIGN IS NECESSARY IN THIS CASE but it isn't if you write a list of string or doubles...
        fallingEdg = {-3, -7};
    }else if(currentScanParameters.Axis.toStdString()=="XZ"){
        risingEdg = {7, 5};  // ----WARNING!!!!: THE EQUAL SIGN IS NECESSARY IN THIS CASE but it isn't if you write a list of string or doubles...
        fallingEdg = {-7, -5};
    }else if(currentScanParameters.Axis.toStdString()=="YZ"){
        risingEdg = {3, 5};  // ----WARNING!!!!: THE EQUAL SIGN IS NECESSARY IN THIS CASE but it isn't if you write a list of string or doubles...
        fallingEdg = {-3, -5};
    }


    //First we create the 2 measurement instances to measure photon counts and signals
    //from the piezo controler E-725, which implicitely calls start on these measurements
    Combiner combinedRising(TT, risingEdg);
    Combiner combinedFalling(TT, fallingEdg);
    std::vector<channel_t> combined = {combinedFalling.getChannel(), combinedRising.getChannel()};
    CountBetweenMarkers scanPhotonCountWithMarkers (TT, 1, combinedRising.getChannel(), combinedFalling.getChannel(), L1*L2*8);

    std::vector<channel_t> WavGenChans = {-6, 6};
    Combiner WavGenPoints(TT, WavGenChans);
    CountBetweenMarkers scanPixelMarkers(TT, combinedRising.getChannel(), WavGenPoints.getChannel(), CHANNEL_UNUSED, L1*L2*2 );

    //following needed in case we want an absolute time 0 between the 2 measurement Pixelmarkers and PhotonCountWithMarkers
    //Combiner combinedAll(TT,{6, 3, 7});
    //CountBetweenMarkers scanFirstFlanks(TT, combinedRising->getChannel(), combinedAll->getChannel(), CHANNEL_UNUSED, 10 );
    TT->sync();//wait to make sure the measurements are fully initialized

    //then create an instance of SynchronizedMeasurements
    SynchronizedMeasurements synced_WaveGenAndPhotonCount(TT);
    synced_WaveGenAndPhotonCount.registerMeasurement(&scanPhotonCountWithMarkers);
    synced_WaveGenAndPhotonCount.registerMeasurement(&scanPixelMarkers);
    //synced_WaveGenAndCount.registerMeasurement(&scanFirstFlanks);

    TT->sync();//wait to make sure synchronisation is finshed
    synced_WaveGenAndPhotonCount.stop();
    synced_WaveGenAndPhotonCount.clear();
    TT->sync();

    //start measurement in a synchronized way
    synced_WaveGenAndPhotonCount.start();
    if(!AIuse){
        std::cout << "Synchronized measurement started" << std::endl;
    }

    //then just start the wave generator
    int startModesWavGen[2] {1,1};
    if(!PI_WGO(ID, WaveGenIds, startModesWavGen, 2))
    {
        iError = PI_GetError(ID);
        PI_TranslateError(iError, szErrorMessage, 1024);
        std::cout << "Failure starting WaveGenerator: ERROR" << iError << ": " << szErrorMessage << std::endl;
        return false;
    }


    //check every few hundreds ms if the generator is still on; if not leave the while loop !
    scanningOn = true;
    bool stillMoving = true;
    int WavGenIds[] {currentScanParameters.IDAxis1,currentScanParameters.IDAxis2};
    BOOL genOn[] {-1,-1};
    int Counter = 0;
    emit(scanProgress_pxCount(0)); //Emit signal to tell the progress bar to change state to 0%
    while(stillMoving){
        if(!PI_IsGeneratorRunning(ID, WavGenIds, genOn, 2))
        {
            iError = PI_GetError(ID);
            PI_TranslateError(iError, szErrorMessage, 1024);
            std::cout << "Failure checking for WaveGenerator: ERROR" << iError << ": " << szErrorMessage << std::endl;
            stillMoving=false;
        }

        //case when the WaveGenerator has stopped i.e. the scan has reached the end
        //delete measurement instances, reset the timer and flag GeneratorOn
        if(!(genOn[0]||genOn[1]))
        {
            stillMoving=false;
        }
        if (Counter%10 == 0)
        {
            fetchAndProcessData(scanPhotonCountWithMarkers, scanPixelMarkers, false, true, currentScanParameters.Axis.toStdString());
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(300));
        ++Counter;
    }
    if(!AIuse){
        std::cout << "Counter: "<< Counter << "*300 ms" << std::endl;
    }

    scanningOn=false;

    //stop measurement in a synchronized way
    synced_WaveGenAndPhotonCount.stop();
    if(!AIuse){
        std::cout << "Synchronized measurement stopped" << std::endl;
    }

    // move back to center
    if(currentScanParameters.Axis.toStdString()=="XY"){
        moveAxisTo_VelLimit(Axis::X, currentScanParameters.center.X);
        moveAxisTo_VelLimit(Axis::Y, currentScanParameters.center.Y);
        std::cout << "XY-Scan" << std::endl;
    }else if(currentScanParameters.Axis.toStdString()=="XZ"){
        moveAxisTo_VelLimit(Axis::X, currentScanParameters.center.X);
        moveAxisTo_VelLimit(Axis::Z, currentScanParameters.center.Y);
        std::cout << "XZ-Scan" << std::endl;
    }else if(currentScanParameters.Axis.toStdString()=="YZ"){
        moveAxisTo_VelLimit(Axis::Y, currentScanParameters.center.X);
        moveAxisTo_VelLimit(Axis::Z, currentScanParameters.center.Y);
        std::cout << "YZ-Scan" << std::endl;
    }

    fetchAndProcessData(scanPhotonCountWithMarkers, scanPixelMarkers, false, scanningStoppedByCommand, "normal-scan");

    emit(ScanPlot2D(currentScanParameters.Npx_X, currentScanParameters.Npx_X*currentScanParameters.Npx_Y));
    //plotData(currentScanParameters.Npx_X, currentScanParameters.Npx_X*currentScanParameters.Npx_Y);

    //then let the MainWindow know it can plot the data acquired and prepare the plotting along crosshair lines
    emit(readyFor2DPlot());

    return true;
}


void ScanAndCount::on_STOPButton_clicked_scanandcount()
{
    BOOL moving[] {1};

    stopMove();

    //the idea is that when the command is received, the stage might take a while to come a complete halt
    //hence checking for movement
    while(moving[0])
    {
        if(!PI_IsMoving(ID, nullptr, moving))
        {
            iError = PI_GetError(ID);
            PI_TranslateError(iError, szErrorMessage, 1024);
            std::cout << "Failure checking move: ERROR" << iError << ": " << szErrorMessage << std::endl;
        }
    }

    scanningStoppedByCommand = true;
    scanningOn=false;
}


void ScanAndCount::plotData(int Lx, int NumbersOfPixels){
    int pxIndex = 0;
    if(NumbersOfPixels<Lx){
        //std::cout << "Error: NumbersOfPixels<Lx" << std::endl;
        return;
    }else{
        //IMPORTANT : this is where the data is passed to the 2D plotting widget
        for(int Yint = 0; Yint<ceil(NumbersOfPixels/Lx); ++Yint)
        {
            for(int Xint = (Yint%2==0? 0: Lx-1); Xint>=0 && Xint<Lx;  Yint%2==0? ++Xint: --Xint)//zigzag scanning
            {
                image2DPlot->colorMap->data()->setCell(Xint, Yint, imageVec[pxIndex]);
                ++pxIndex;

            }
        }
        if(NumbersOfPixels==currentScanParameters.Npx_X*currentScanParameters.Npx_Y && AIuse == false){
            std::cout << "End of plotData" << std::endl;
        }
        image2DPlot->replot();
        return;
    }
}


//slot to check whether the WaveGenerator is running and if not, terminates the measurement processes with the TimeTagger
//this function assumes it is called after the wave generator was started by the slot on_TwoDScanstart_clicked
void ScanAndCount::fetchAndProcessData(CountBetweenMarkers& onTargetTimeWindowsWPhotonEvents, CountBetweenMarkers& pixelMarkers, bool printData, bool partialScan, std::string scanType)
{
    //stop the counter
    //just for testing (scan without touching the counter)
    //*******std::cout << "Fetch scan data and display:" <<std::endl;
    //get the data from Counter measurement from the TimeTagger
    //std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    TT->sync();

    //######### WARNING: all the vectors directly loaded from the TimeTagger contains a lot of #########
    //######### 0s at the end because we make the buffer for the measurement twice as large    #########
    //######### to accomodate the additional overshoots (or noise) time windows!!!             #########

    //fetch arrays for the onTarget bins and the corresponding counts
    std::vector<int> photonsCountsData;
    onTargetTimeWindowsWPhotonEvents.getData([&photonsCountsData](int size){photonsCountsData.resize(size); return photonsCountsData.data();});

    std::vector<timestamp_t> onTargetTimeWindows_lengths;
    onTargetTimeWindowsWPhotonEvents.getBinWidths([&onTargetTimeWindows_lengths](int size){onTargetTimeWindows_lengths.resize(size); return onTargetTimeWindows_lengths.data();});

    std::vector<timestamp_t> onTargetTimeWindows_startPts;
    onTargetTimeWindowsWPhotonEvents.getIndex([&onTargetTimeWindows_startPts](int size){onTargetTimeWindows_startPts.resize(size); return onTargetTimeWindows_startPts.data();});

    //fetch arrays for the pixels markers (Trigger Action Line from WaveGenerator) and number of on-target bins per pixel (ideally 1 but potentially more because of overshoots upon position stabilisation)
    std::vector<timestamp_t> pixelLengths;
    pixelMarkers.getBinWidths([&pixelLengths](int size){pixelLengths.resize(size); return pixelLengths.data();});

    std::vector<int> countsOnTargetWindows;
    pixelMarkers.getData([&countsOnTargetWindows](int size){countsOnTargetWindows.resize(size); return countsOnTargetWindows.data();});

    //get the time difference between first OnTarget pulse and first pixel marker
    //std::vector<timestamp_t> FirstTags;
    //scanFirstFlanks.getBinWidths([&FirstTags](int size){FirstTags.resize(size); return FirstTags.data();});

    //-------------------------------------------------------------------
    // get the right number of pixels, depending on the fact that the scan
    // reached the end or was stopped before the end or evaluated midway
    //-------------------------------------------------------------------
    int PxNb;
    if(!partialScan)//the scan reached the end --> we know we want (pixel nb in X)*(pixel nb in Y) pixels
    {
        PxNb = currentScanParameters.Npx_X*currentScanParameters.Npx_Y;
    }
    else //the scan was stopped before reaching the end
         //the number of pixels is the number of non-zero entries in the bin width vector from pixel marker measurement
    {
        int p = 0;
        timestamp_t Tpx = pixelLengths[0];
        while(Tpx)
        {
            ++p;
            Tpx=pixelLengths[p];
        }
        if(p>1)
            PxNb = p-1;
        else
        {
            std::cout << "No data to fetch !" << std::endl;
            return;
        }
        scanningStoppedByCommand = false;

        //Emit signal to tell the progress bar to change state
        //std::cout << "Value for Signal:" << lround(PxNb*100/(currentScanParameters.Npx_X*currentScanParameters.Npx_Y)) << std::endl;
        emit(scanProgress_pxCount(lround(PxNb*100/(currentScanParameters.Npx_X*currentScanParameters.Npx_Y))));
    }


    //------------------------------------------------
    // now we reconstruct the image with one integration
    // time and one count per pixel of the image
    //------------------------------------------------
    //std::cout << "Pixel number is " << PxNb  << std::endl;
    int index_firstonTargetWindowInPixel = 0;
    std::vector<double> Img_times_microsec;
    Img_times_microsec.resize(PxNb, -1LL);
    std::vector<int> Img_counts;
    Img_counts.resize(PxNb, -1);
    int onTargetWindows_nb;
    int currentCounts;
    timestamp_t currentTime = 0LL;

    //int overShootCounts = 0;
    for(int K = 0; K<PxNb; ++K)//index traversing the vector containing the number of on-target time windows for each pixel so the length of this loop is the final amount of pixels
    {
        onTargetWindows_nb = countsOnTargetWindows[K];
        currentCounts = 0;
        currentTime = 0LL;
        for(int L =0; L<onTargetWindows_nb; ++L)
        {
            currentCounts += photonsCountsData[index_firstonTargetWindowInPixel+L];
            currentTime += onTargetTimeWindows_lengths[index_firstonTargetWindowInPixel+L];
            //overShootCounts++;
        }
        index_firstonTargetWindowInPixel += onTargetWindows_nb;
        Img_counts[K] = currentCounts;
        Img_times_microsec[K] = ((double)currentTime)/1.e6;
    }

    //std::cout << "Counted overshoots: " << overShootCounts-PxNb << std::endl;

    //------------------------------------------------
    // compute the standard deviation, average, minimum
    // and maximum the final time data and print
    //------------------------------------------------

    double average_t, variance_t;
    dVecAverageAndVariance(Img_times_microsec, average_t, variance_t);
    double max_time = *std::max_element(Img_times_microsec.begin(),Img_times_microsec.end());
    double min_time = *std::min_element(Img_times_microsec.begin(),Img_times_microsec.end());
    //std::cout << "Counting time per pixel (unit=microsec): Average = " << average_t << ", Standard deviation = " << sqrt(variance_t) << std::endl;
   // std::cout << "Max integration time (us): " << max_time << ", Min integration time (us): " << min_time << std::endl;

    //------------------------------------------------
    //print the data to cout in case it was asked
    //to do so via the bool parameter 'printData'
    //------------------------------------------------
    if(printData)
    {
        std::cout << "Measured data (all times in ms):\n";
        std::vector<timestamp_t> pixelMarkersStartPts;
        pixelMarkers.getIndex([&pixelMarkersStartPts](int size){pixelMarkersStartPts.resize(size); return pixelMarkersStartPts.data();});

        auto print_ms = [](long long A) {std::cout << (double)A / 1.e9 << "  ";};
        auto print_ints = [](int I) {std::cout << I << "  ";};
        //std::cout << "Photon counts:\n";
        //for_each(photonsCountsData.begin(), photonsCountsData.end(), print_ms}); // WARNING: Counts are ints not longlongs like timestamps
        //std::cout << std::endl;

        std::cout << "Length of on-target time windows:\n";
        for_each(onTargetTimeWindows_lengths.begin(), onTargetTimeWindows_lengths.end(), print_ms);
        std::cout << std::endl;

        std::cout << "Start point of on-target time windows (ms):\n";
        for_each(onTargetTimeWindows_startPts.begin(), onTargetTimeWindows_startPts.end(), print_ms);
        std::cout << std::endl;

        std::cout << "Start point of pixel markers:\n";
        for_each(pixelMarkersStartPts.begin(), pixelMarkersStartPts.end(), print_ms);
        std::cout << std::endl;

        std::cout << "Length of pixels (ms): should be WaveTableRate*0.05ms\n";
        for_each(pixelLengths.begin(), pixelLengths.end(), print_ms);
        std::cout << std::endl;

        std::cout << '\n' << "Number of on-target time windows:\n";
        for_each(countsOnTargetWindows.begin(), countsOnTargetWindows.end(), print_ints);
        std::cout << std::endl;

        std::cout << '\n' << "Length of time windows for photon counting:" << std::endl;
        for_each(Img_times_microsec.begin(), Img_times_microsec.end(), [](double D) {std::cout << D/1.e3 << "  ";});
        std::cout << std::endl;
    }

    //build the image as vector (in order zigzag upwards, starting left to right) in Counts/ms = kCounts/s
    //the size of the image imageVec is already set at the beginning of the scan
    for(int px = 0; px<PxNb; ++px)
    {
        imageVec[px] = ((double)Img_counts[px])/Img_times_microsec[px]*1.e3;
    }


    //save image vector to .dat text file
    if(!partialScan && AIuse==false)//the scan reached the end --> we know we want (pixel nb in X)*(pixel nb in Y) pixels
    {
        //save the current date+time in member variable of MainWindow to identify this image
        std::time_t rawtime;
        std::tm* timeinfo;
        char yyyymmddHHMMSS [80];
        std::time(&rawtime);
        timeinfo = std::localtime(&rawtime);
        std::strftime(yyyymmddHHMMSS,80,"%Y%m%d%H%M%S",timeinfo);
        currentImageDate_yyyymmddHHMMSS = scanType + "_" + yyyymmddHHMMSS + "_" + currentScanParameters.Axis.toStdString();
        //std::cout << currentImageDate_yyyymmddHHMMSS << std::endl;

        writeDataToFile(Img_counts, Img_times_microsec, scanType, currentImageDate_yyyymmddHHMMSS);
        emit(scanProgress_pxCount(100));
    }else{
        //plotData(currentScanParameters.Npx_X, PxNb);
        emit(ScanPlot2D(currentScanParameters.Npx_X, PxNb));
    }


    return;

}

bool ScanAndCount::setOpenLoopValue(Axis A, double value)
{
    //define the right string for a certain axis
    char ax[] = "0";
    if (A == Axis::X)
    {
        *ax = '1';
    }
    else if (A == Axis::Y)
    {
        *ax = '2';
    }
    else if (A == Axis::Z)
    {
        *ax = '3';
    }

    double val[1] {value/1000.};

    if(!PI_SVA(ID, ax, val))
    {
        iError = PI_GetError(ID);
        PI_TranslateError(iError, szErrorMessage, 1024);
        std::cout << "Failure setting open-loop value: ERROR" << iError << ": " << szErrorMessage << std::endl;
        return false;
    }

}

bool ScanAndCount::setWaveTableRate(int WTRval)
{
    //with 0 as wave generator ids, all wave generator (all axis) are set with the same parameter
    int WavGenIds[1] {0};
    int WaveTableRates[1] {WTRval};
    int interp[1] {0};

    if(!PI_WTR(ID, WavGenIds, WaveTableRates, interp, 1))
    {
        iError = PI_GetError(ID);
        PI_TranslateError(iError, szErrorMessage, 1024);
        std::cout << "Failure setting wave table rate: ERROR" << iError << ": " << szErrorMessage << std::endl;
        return false;
    }

    return true;
}


bool ScanAndCount::readAllPiezoVoltages()
{
    return true;
}

bool ScanAndCount::stopMove()
{
    if(!scanningOn)
    {
        std::cout << "No scanning to be stopped !" << std::endl;
    }
    else
    {
        std::cout << "Movement was stopped by command." << std::endl;
    }

    //stop everything !
    if(!PI_STP(ID))
    {
        iError = PI_GetError(ID);
        PI_TranslateError(iError, szErrorMessage, 1024);
        std::cout << "Failure stopping: ERROR" << iError << ": " << szErrorMessage << std::endl;
        return false;
    }

    return true;
}

// move functions will move in steps for which the piezo dynamic response has been optimized
// we do not use MOV directly because that can cause the stage to move very large steps for which
// it has no been optimized and as a result could cause oscillations (because this happens in open loop)
// another probably better solution would be to work with MOV command and a limited velocity but that requires a bit of measurement
// (of what reasonable value can the velocity be limited to)
// here a template could be used to define the function only once and cover all three axes, maybe...


//############### NOT YET USED --> BEWARE OF "iStepXmax" in nbOfSteps and stepWSign !!! #################
bool ScanAndCount::moveAxisTo_SmallSteps(Axis A, int target)
{
    //first limit input to acceptable range
    if ((target>MAXPOS) || (target < MINPOS))
    {
        std::cout << "Invalid axis position !!" << std::endl;
        return false;
    }

    double maxStep;
    //1. define the right axis string
    char ax[] = "0";
    if (A == Axis::X)
    {
        *ax = '1';
        maxStep = dStepXmax;
    }
    else if (A == Axis::Y)
    {
        *ax = '2';
        maxStep = dStepYmax;
    }
    else if (A == Axis::Z)
    {
        *ax = '3';
        maxStep = dStepZmax;
    }

    // 2. then, see how many optimized steps are needed
    double pos[1];
    if(!PI_qPOS(ID, ax, pos))//get current position
    {
        iError = PI_GetError(ID);
        PI_TranslateError(iError, szErrorMessage, 1024);
        std::cout << "Position query failure: ERROR" << iError << ": " << szErrorMessage << std::endl;
        return false;
    }

    //calculate parameters of movement
    int distance = target-(int)pos; //int distance between current position and target
    int Sign = sign(distance);
    int nbOfSteps = std::abs(distance/iStepXmax);
    double stepWSign = (double)(Sign*iStepXmax)/1000.;//number of full steps
    //int lastStep = distance-nbOfSteps*Sign*iStepX; // last step, by construction smaller or equal to an optimized step to reach the final target

    BOOL moving[] {1};
    //3. then move the right amount of maximum steps
    for(int k = 0; k < nbOfSteps; ++k)
    {
        //resetBoolArr(moving, 1);
        *moving = 1;

        if(!PI_MVR(ID, ax, &stepWSign))//start movement
        {
            iError = PI_GetError(ID);
            PI_TranslateError(iError, szErrorMessage, 1024);
            std::cout << "Failure to move(rel): ERROR" << iError << ": " << szErrorMessage << std::endl;
            return false;
        }

        while(moving[0])//check if stage is moving
        {
            if(!PI_IsMoving(ID, nullptr, moving))
            {
                iError = PI_GetError(ID);
                PI_TranslateError(iError, szErrorMessage, 1024);
                std::cout << "Failure checking move: ERROR" << iError << ": " << szErrorMessage << std::endl;
                return false;
            }
        }
    }
    // 4.last, the stage should now be close enough so we move with MOV and it should stop on target
    double dTarget = (double)target;
    *moving = 1;
    if(!PI_MOV(ID, ax, &dTarget))
    {
        iError = PI_GetError(ID);
        PI_TranslateError(iError, szErrorMessage, 1024);
        std::cout << "Failure to move(abs): ERROR" << iError << ": " << szErrorMessage << std::endl;
        return false;
    }

    while(moving[0])//check if stage is moving
    {
        if(!PI_IsMoving(ID, nullptr, moving))
        {
            iError = PI_GetError(ID);
            PI_TranslateError(iError, szErrorMessage, 1024);
            std::cout << "Failure checking move: ERROR" << iError << ": " << szErrorMessage << std::endl;
            return false;
        }
    }

    return true;
}


bool ScanAndCount::moveAxisTo_VelLimit(Axis A, int target)
{
    //0. first limit input to acceptable range
    if ((target>MAXPOS) || (target < MINPOS))
    {
        std::cout << "Invalid axis position !!" << std::endl;
        return false;
    }

    double maxVelBigStep[1];
    double maxVelOther[1];
    //1. define the right axis string and array arguments for PI_VEL
    char ax[] = "0";
    if (A == Axis::X)
    {
        *ax = '1';
        *maxVelBigStep = bigStepMaxVelocityX;
        *maxVelOther = scanMaxVelocityX;
    }
    else if (A == Axis::Y)
    {
        *ax = '2';
        *maxVelBigStep = bigStepMaxVelocityY;
        *maxVelOther = scanMaxVelocityY;
    }
    else if (A == Axis::Z)
    {
        *ax = '3';
        *maxVelBigStep = bigStepMaxVelocityZ;
        *maxVelOther = bigStepMaxVelocityZ;
    }


    //2. limit velocity
    if(!PI_VEL(ID, ax, maxVelBigStep))
    {
        iError = PI_GetError(ID);
        PI_TranslateError(iError, szErrorMessage, 1024);
        std::cout << "Failure to set velocity: ERROR" << iError << ": " << szErrorMessage << std::endl;
        return false;
    }

    BOOL moving[] {1};
    double dTarget = (double)target;
    double arrTarget[1] {dTarget/1000.};
    //3. move but with limited speed/slew rate
    if(!PI_MOV(ID, ax, arrTarget))//start movement
    {
        iError = PI_GetError(ID);
        PI_TranslateError(iError, szErrorMessage, 1024);
        std::cout << "Failure to move: ERROR" << iError << ": " << szErrorMessage << std::endl;
        return false;
    }

    *moving = 1;
    while(moving[0])//check if stage is moving
    {
        if(!PI_IsMoving(ID, nullptr, moving))
        {
            iError = PI_GetError(ID);
            PI_TranslateError(iError, szErrorMessage, 1024);
            std::cout << "Failure checking move: ERROR" << iError << ": " << szErrorMessage << std::endl;
            return false;
        }
    }

    //4. set velocity back to its higher value
    if(!PI_VEL(ID, ax, maxVelOther))
    {
        iError = PI_GetError(ID);
        PI_TranslateError(iError, szErrorMessage, 1024);
        std::cout << "Failure to set velocity: ERROR" << iError << ": " << szErrorMessage << std::endl;
        return false;
    }

    //5. display new positions
    //on_readValues_clicked(); --> should be done in mainwindow

    return true;
}


//moves by a certain step (in nm) relative to current position
bool ScanAndCount::moveAxisStep_VelLimit(Axis A, int step)
{

    double maxVelBigStep[1];
    double maxVelOther[1];
    //0. define the right axis string and array arguments for PI_VEL
    char ax[] = "0";
    if (A == Axis::X)
    {
        *ax = '1';
        *maxVelBigStep = bigStepMaxVelocityX;
        *maxVelOther = scanMaxVelocityX;
    }
    else if (A == Axis::Y)
    {
        *ax = '2';
        *maxVelBigStep = bigStepMaxVelocityY;
        *maxVelOther = scanMaxVelocityY;
    }
    else if (A == Axis::Z)
    {
        *ax = '3';
        *maxVelBigStep = bigStepMaxVelocityZ;
        *maxVelOther = bigStepMaxVelocityZ;
    }

    //1. get current position and limit target to acceptable range

    double pos[1];
    if(!PI_qPOS(ID, ax, pos))
    {
        iError = PI_GetError(ID);
        PI_TranslateError(iError, szErrorMessage, 1024);
        std::cout << "Position query failure: ERROR" << iError << ": " << szErrorMessage << std::endl;
        return false;
    }
    int target = ((int)pos)*1000 + step; //int distance between current position and target

    if ((target>MAXPOS) || (target < MINPOS))
    {
        std::cout << "Invalid axis position !!" << std::endl;
        return false;
    }


    //2. limit velocity
    if(!PI_VEL(ID, ax, maxVelBigStep))
    {
        iError = PI_GetError(ID);
        PI_TranslateError(iError, szErrorMessage, 1024);
        std::cout << "Failure to set velocity: ERROR" << iError << ": " << szErrorMessage << std::endl;
        return false;
    }

    BOOL moving[1] {1};
    double dStep[1] {(double)step/1000.};
    //3. move but limit speed/slew rate
    if(!PI_MVR(ID, ax, dStep))//start movement
    {
        iError = PI_GetError(ID);
        PI_TranslateError(iError, szErrorMessage, 1024);
        std::cout << "Failure to move: ERROR" << iError << ": " << szErrorMessage << std::endl;
        return false;
    }

    *moving = 1;
    while(moving[0])//check if stage is moving
    {
        if(!PI_IsMoving(ID, nullptr, moving))
        {
            iError = PI_GetError(ID);
            PI_TranslateError(iError, szErrorMessage, 1024);
            std::cout << "Failure checking move: ERROR" << iError << ": " << szErrorMessage << std::endl;
            return false;
        }
    }

    //4. set velocity back to its higher value
    if(!PI_VEL(ID, ax, maxVelOther))
    {
        iError = PI_GetError(ID);
        PI_TranslateError(iError, szErrorMessage, 1024);
        std::cout << "Failure to set velocity: ERROR" << iError << ": " << szErrorMessage << std::endl;
        return false;
    }

    //5. display new positions
    //on_readValues_clicked(); --> should be done in  mainwindow

}

std::vector<double> ScanAndCount::getImageVec()
{
    return imageVec;
}


std::vector<double> ScanAndCount::readAllPositions_Closed()
{
    char allAxes[] = "1 2 3";
    double result[3];
    std::vector<double> res {-1.,-1.,-1.};

    if(!PI_qPOS(ID, allAxes, result))
    {
        iError = PI_GetError(ID);
        PI_TranslateError(iError, szErrorMessage, 1024);
        std::cout << "Failure on position query: ERROR" << iError << ": " << szErrorMessage << std::endl;
        return res;
    }

    res = {result[0]*1000.,result[1]*1000., result[2]*1000.};
    return res;
}


std::vector<double> ScanAndCount::readAllPositions_Open()
{
    char allAxes[] = "1 2 3";
    double result[3];
    std::vector<double> res {-1.,-1.,-1.};

    if(!PI_qSVA(ID, allAxes, result))
    {
        iError = PI_GetError(ID);
        PI_TranslateError(iError, szErrorMessage, 1024);
        std::cout << "Failure on open loop position query: ERROR" << iError << ": " << szErrorMessage << std::endl;
        return res;
    }

    res = {result[0]*1000., result[1]*1000., result[2]*1000.};
    return res;
}


//set the servo state (closed or open loop) to a certain value (b=0->Open, b=1->Closed)
bool ScanAndCount::setServo(Axis A, BOOL b)
{
    //define the right string for a certain axis
    char ax[] = "0";
    if (A == Axis::X)
    {
        *ax = '1';
    }
    else if (A == Axis::Y)
    {
        *ax = '2';
    }
    else if (A == Axis::Z)
    {
        *ax = '3';
    }

    BOOL state[1] = {b};

    if(!PI_SVO(ID, ax, state))
    {
        iError = PI_GetError(ID);
        PI_TranslateError(iError, szErrorMessage, 1024);
        std::cout << "Failure setting close/open loop: ERROR" << iError << ": " << szErrorMessage << std::endl;
        return false;
    }

    return true;
}

void ScanAndCount::writeDataToFile(std::vector<int> counts, std::vector<double> bins_microsec, std::string scanType, std::string date)
{
    /*
    if((counts.size()!=currentScanParameters.Npx_X*currentScanParameters.Npx_Y) || (bins.size()!=currentScanParameters.Npx_X*currentScanParameters.Npx_Y)){
        std::cout << "wrong nb. of pixels and counts!" << std::endl;
        return;
    }*/

    std::filesystem::current_path(globalPath);
    currentImagePath = globalPath.string();
    std::ofstream outfile (date+".dat");

    outfile << "Stepsize X/Stepsize Y/X Px number/Y Px number/Pos X/Pos Y/Pos Z" << std::endl;
    outfile << currentScanParameters.Xstep << "\t" << currentScanParameters.Ystep << "\t";
    outfile << currentScanParameters.Npx_X << "\t" << currentScanParameters.Npx_Y << "\t";
    outfile << currentScanParameters.positions[0] << "\t" << currentScanParameters.positions[1] << "\t" << currentScanParameters.positions[2] << std::endl;

    outfile << "X\t Y\t Counts/second\t counts\t time[micro s]" << "\n" << std::endl;

    for(int i=0; i<currentScanParameters.Npx_Y; ++i)
    {
        for(int j=0; j<currentScanParameters.Npx_X; ++j)
        {
            //x-position
            if(i%2==0)
            {
                outfile << currentScanParameters.center.X-(int)(((currentScanParameters.Npx_X-1)*currentScanParameters.Xstep)/2.0)+(j*currentScanParameters.Xstep) << "\t";
            }else{
                outfile << currentScanParameters.center.X-(int)(((currentScanParameters.Npx_X-1)*currentScanParameters.Xstep)/2.0)+((currentScanParameters.Npx_X-(j+1))*currentScanParameters.Xstep) << "\t";
            }
            //y-position
            outfile << currentScanParameters.center.Y-(int)(((currentScanParameters.Npx_Y-1)*currentScanParameters.Ystep)/2.0)+(i*currentScanParameters.Ystep) << "\t";
            //kCounts/sec
            outfile << (int)((counts[i*currentScanParameters.Npx_X+j]*1.e6/bins_microsec[i*currentScanParameters.Npx_X+j])) << "\t" ;
            //counts
            outfile << counts[i*currentScanParameters.Npx_X+j] << "\t";
            //time value
            outfile << bins_microsec[i*currentScanParameters.Npx_X+j] << std::endl;
        }
    }

    outfile.close();
}

void ScanAndCount::on_TwoDScanStart_clicked_scanandcount()
{
    Pos2D middle;
    auto positions = readAllPositions_Closed();
    currentScanParameters.positions[0] = (int)(positions[0]);
    currentScanParameters.positions[1] = (int)(positions[1]);
    currentScanParameters.positions[2] = (int)(positions[2]);
    if(currentScanParameters.Axis=="XY"){
        middle.X = (int)positions[0];
        middle.Y = (int)positions[1];
        currentScanParameters.Z = (int)(positions[2]);
        if(!AIuse){
            std::cout << "Z position: " << currentScanParameters.Z << std::endl;
        }
    }else if(currentScanParameters.Axis=="XZ"){
        middle.X = (int)positions[0];
        middle.Y = (int)positions[2];
        currentScanParameters.Z = (int)(positions[1]);
        if(!AIuse){
            std::cout << "Y position: " << currentScanParameters.Z << std::endl;
        }
    }else if(currentScanParameters.Axis=="YZ"){
        middle.X = (int)positions[1];
        middle.Y = (int)positions[2];
        currentScanParameters.Z = (int)(positions[0]);
        if(!AIuse){
            std::cout << "X position: " << currentScanParameters.Z << std::endl;
        }
    }

    startWaveGenWithCounterScan_ZigZag(currentScanParameters.Npx_X, currentScanParameters.Npx_Y, currentScanParameters.Xstep, currentScanParameters.Ystep, middle);

    // emits an signal if the scan is done
    emit finished_2DScan(true);
}


void ScanAndCount::on_testButton_clicked_scanandcount()
{
    Pos2D middle;
    auto positions = readAllPositions_Closed();
    middle.X = (int)(positions[0]);
    middle.Y = (int)(positions[1]);

    currentScanParameters.Z = (int)(positions[2]);

    startWaveGenWithCounterScan_ZigZag(currentScanParameters.Npx_X, currentScanParameters.Npx_Y, currentScanParameters.Xstep, currentScanParameters.Ystep, middle);
}

void ScanAndCount::on_TwoDScanSetup_clicked_scanandcount(int L1, int L2, int Steps1, int Steps2, int IDAxis1, int IDAxis2, QString Axis)
{
    //check for WaveTable size
    if (L1*L2 >= maxNbOfPtsInWaveTable)
    {
        QMessageBox msgBox;
        msgBox.setText("Wave tables are too large! Maximum size is 2^18=262144=512*512.");
        msgBox.exec();
        return;
    }
    currentScanParameters.Npx_X = L1;
    currentScanParameters.Npx_Y = L2;
    currentScanParameters.Xstep = Steps1;
    currentScanParameters.Ystep = Steps2;
    currentScanParameters.Axis = Axis;
    currentScanParameters.IDAxis1 = IDAxis1;
    currentScanParameters.IDAxis2 = IDAxis2;

    if(!AIuse){
        std::cout << "L1=" << currentScanParameters.Npx_X << ", L2=" << currentScanParameters.Npx_Y << ", Steps1=" << currentScanParameters.Xstep << ", Step2=" << currentScanParameters.Ystep << std::endl;
    }

    setupWaveTableScan_ZigZag(L1, L2, Steps1, Steps2, IDAxis1, IDAxis2);
}



void ScanAndCount::on_loopAXIS_stateChanged(int state, Axis AX)
{
    setServo(AX, state);
    BOOL moving[] {1};
    //checking for movement on all axes
    while(moving[0])
    {
        if(!PI_IsMoving(ID, nullptr, moving))
        {
            iError = PI_GetError(ID);
            PI_TranslateError(iError, szErrorMessage, 1024);
            std::cout << "Failure checking move: ERROR" << iError << ": " << szErrorMessage << std::endl;
        }
    }
}

void ScanAndCount::on_ThreeDScanStart_clicked_scanandcount()
{
    if(!currentScanParameters.Npx_X){
        std::cout << "no wavetable defined!" << std::endl;
        return;
    }

    Pos2D middle;
    auto positions = readAllPositions_Closed();
    middle.X = (int)(positions[0]);
    middle.Y = (int)(positions[1]);
    currentScanParameters.positions[0] = (int)(positions[0]);
    currentScanParameters.positions[1] = (int)(positions[1]);
    currentScanParameters.positions[2] = (int)(positions[2]);




    std::vector<int> zPositions;
    for(int i=0; i<currentScanParameters.Npx_Z; ++i){
        zPositions.push_back((int)(positions[2]+(1-(int)(currentScanParameters.Npx_Z))*currentScanParameters.Zstep/2+currentScanParameters.Zstep*i));
    }

    //save globalPath for reset after Z-Scan
    std::filesystem::path globalPathSave = globalPath;

    bool existsZScanFolder = false;
    int numbersZScanFolders = 1;
    globalPath /= "ZScan"+std::to_string(numbersZScanFolders);
    do{
        if (!std::filesystem::exists(globalPath))
        {
            std::cout << "Path " << globalPath << " does not exist!" << std::endl;
            if(std::filesystem::create_directory(globalPath)) {
                    std::cout << "Path created" << std::endl;
                    existsZScanFolder = true;
            }else{
                std::cout << "unable to create Path" << std::endl;
            }
        }else{
            ++numbersZScanFolders;
            globalPath = std::filesystem::path(globalPath).replace_filename("ZScan"+std::to_string(numbersZScanFolders));
            std::cout << globalPath << ", " << std::filesystem::path(globalPath).remove_filename() << ", " << numbersZScanFolders << std::endl;
            if(numbersZScanFolders>100){
                std::cout << "BREAK!" << std::endl;
                break;
            }
        }
    }while(!existsZScanFolder);

    LoadFileCreatMosaic* partialVariable = new LoadFileCreatMosaic();
    std::string folderNameForPlot;
    folderNameForPlot = globalPath.string();
    std::replace(folderNameForPlot.begin(), folderNameForPlot.end(), '\\', '/');
    for(int i=0; i<currentScanParameters.Npx_Z; i++){
        currentScanParameters.Z = zPositions[i];
        moveAxisTo_VelLimit(Axis::Z, zPositions[i]);
        startWaveGenWithCounterScan_ZigZag(currentScanParameters.Npx_X, currentScanParameters.Npx_Y, currentScanParameters.Xstep, currentScanParameters.Ystep, middle);
        //partialVariable->loadDataFromZScan(folderNameForPlot);
    }
    moveAxisTo_VelLimit(Axis::Z, (int)(positions[2]));

    globalPath = globalPathSave;

    delete partialVariable;
}

void ScanAndCount::set3DScanParameters(int Lz, int Zstep)
{
    currentScanParameters.Zstep = Zstep;
    currentScanParameters.Npx_Z = Lz;
}

void ScanAndCount::setGlobalPath(std::filesystem::path path)
{
    globalPath = path;
}

void ScanAndCount::bleaching(int xSteps, int ySteps, int time){
    auto positions = readAllPositions_Closed();
    double xStart;
    double yStart;
    if(xSteps%2==0){
        xStart=(double)(xSteps)/2.-0.5;
    }else{
        xStart=(double)(xSteps)/2.;
    }
    if(ySteps%2==0){
        yStart=(double)(ySteps)/2.-0.5;
    }else{
        yStart=(double)(ySteps)/2.;
    }

    for(int i=0; i<xSteps; i++){
        moveAxisTo_VelLimit(Axis::X, (int)(positions[0]+300*((double)i-xStart)));
        for(int j=0; j<ySteps; j++){
            moveAxisTo_VelLimit(Axis::Y, (int)(positions[1]+300*((double)j-yStart)));
            std::this_thread::sleep_for(std::chrono::milliseconds(time));
        }
    }
    moveAxisTo_VelLimit(Axis::X, positions[0]);
    moveAxisTo_VelLimit(Axis::Y, positions[1]);
}

void ScanAndCount::setMosaicParameters(int LeftPicturesMosaic, int RightPicturesMosaic, int UpPicturesMosaic, int DownPicturesMosaic)
{
    currentScanParameters.MosaicLeft = LeftPicturesMosaic;
    currentScanParameters.MosaicRight = RightPicturesMosaic;
    currentScanParameters.MosaicUp = UpPicturesMosaic;
    currentScanParameters.MosaicDown = DownPicturesMosaic;
}

void ScanAndCount::StartMosaicScan()
{
    if(!currentScanParameters.Npx_X){
        std::cout << "no wavetable defined!" << std::endl;
        return;
    }

    Pos2D middle;
    auto positions = readAllPositions_Closed();
    middle.X = (int)(positions[0]);
    middle.Y = (int)(positions[1]);

    std::vector<std::vector<int>> MosaicPositionsX;
    std::vector<std::vector<int>> MosaicPositionsY;
    for(int i=0; i<(currentScanParameters.MosaicLeft+currentScanParameters.MosaicRight+1); ++i){
        for(int j=0; j<(currentScanParameters.MosaicUp+currentScanParameters.MosaicDown+1); j++){
            MosaicPositionsX[i][j]=middle.X;
            MosaicPositionsY[i][j]=middle.Y;
            //zPositions.push_back((int)(positions[2]+(1-(int)(currentScanParameters.Npx_Z))*currentScanParameters.Zstep/2+currentScanParameters.Zstep*i));
        }
    }

//    //save globalPath for reset after Z-Scan
//    std::filesystem::path globalPathSave = globalPath;

//    bool existsZScanFolder = false;
//    int numbersZScanFolders = 1;
//    globalPath /= "ZScan"+std::to_string(numbersZScanFolders);
//    do{
//        if (!std::filesystem::exists(globalPath))
//        {
//            std::cout << "Path " << globalPath << " does not exist!" << std::endl;
//            if(std::filesystem::create_directory(globalPath)) {
//                    std::cout << "Path created" << std::endl;
//                    existsZScanFolder = true;
//            }else{
//                std::cout << "unable to create Path" << std::endl;
//            }
//        }else{
//            ++numbersZScanFolders;
//            globalPath = std::filesystem::path(globalPath).replace_filename("ZScan"+std::to_string(numbersZScanFolders));
//            std::cout << globalPath << ", " << std::filesystem::path(globalPath).remove_filename() << ", " << numbersZScanFolders << std::endl;
//            if(numbersZScanFolders>100){
//                std::cout << "BREAK!" << std::endl;
//                break;
//            }
//        }
//    }while(!existsZScanFolder);

//    LoadFileCreatMosaic* partialVariable = new LoadFileCreatMosaic();
//    std::string folderNameForPlot;
//    folderNameForPlot = globalPath.string();
//    std::replace(folderNameForPlot.begin(), folderNameForPlot.end(), '\\', '/');
//    for(int i=0; i<currentScanParameters.Npx_Z; i++){
//        currentScanParameters.Z = zPositions[i];
//        moveAxisTo_VelLimit(Axis::Z, zPositions[i]);
//        startWaveGenWithCounterScan_ZigZag(currentScanParameters.Npx_X, currentScanParameters.Npx_Y, currentScanParameters.Xstep, currentScanParameters.Ystep, middle);
//        //partialVariable->loadDataFromZScan(folderNameForPlot);
//    }
//    moveAxisTo_VelLimit(Axis::Z, (int)(positions[2]));

//    globalPath = globalPathSave;

//    delete partialVariable;
}

double ScanAndCount::getCurrentCountsFromTT()
{
    countRate = new Countrate(TT, {1});
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    std::vector<double> value;
    countRate->getData([&value](int size) {value.resize(size); return value.data(); });
    delete countRate;
    return value[0];
}

// set the usage to AI or user
void ScanAndCount::setAIuse(bool state)
{
    AIuse = state;
}


// start 2D thread scan
void ScanAndCount::start2DScanThread(){
    std::thread thread_start_2dScan(&ScanAndCount::on_TwoDScanStart_clicked_scanandcount, this);
    thread_start_2dScan.detach();
}


// Control functions
void ScanAndCount::setAIParameters(int centerPositionROIX, int centerPositionROIY, int centerPositionROIZ){
    // set height
    currentScanParameters.Z = centerPositionROIZ;

    // save current parameters
    int Xstep_s = currentScanParameters.Xstep;
    int Ystep_s = currentScanParameters.Ystep;
    int curZ_s = currentScanParameters.Z;
    int nbPixelX_s = currentScanParameters.Npx_X;
    int nbPixelY_s = currentScanParameters.Npx_Y;
    /*std::cout << "Xstep_s: " << Xstep_s << std::endl;
    std::cout << "Ystep_s: " << Ystep_s << std::endl;
    std::cout << "nbPixelX_s: " << nbPixelX_s << std::endl;
    std::cout << "nbPixelY_s: " << nbPixelY_s << std::endl;
*/
    Pos2D middle;
    middle.X = centerPositionROIX;
    middle.Y = centerPositionROIY;

    currentScanParameters.Z = centerPositionROIZ;
    currentScanParameters.Xstep = 40;
    currentScanParameters.Ystep = 40;
    currentScanParameters.Npx_X = 20;
    currentScanParameters.Npx_Y = 20;

    //setupWaveTableScan_ZigZag(20,20,40,40);
    //std::cout << "pos X: " << centerPositionROIX << std::endl;
    //std::cout << "pos Y: " << centerPositionROIY << std::endl;
    //std::cout << "pos Z: " << centerPositionROIZ << std::endl;


    std::thread AI2DMeasurement(&ScanAndCount::StartAIScan2D, this, middle);
    AI2DMeasurement.detach();


/*
    setupWaveTableScan_ZigZag(nbPixelX_s, nbPixelY_s,Xstep_s,Ystep_s);
    currentScanParameters.Xstep = Xstep_s;
    currentScanParameters.Ystep = Ystep_s;
    currentScanParameters.Z = curZ_s;
    currentScanParameters.Npx_X = nbPixelX_s;
    currentScanParameters.Npx_Y = nbPixelY_s;*/

}

void ScanAndCount::StartAIScan2D(Pos2D middle)
{
    // FIRST create the Control folder with each NV center
    //save globalPath for reset after Z-Scan
    std::filesystem::path globalPathSave = globalPath;

    bool existsControlFolder = false;
    int numberROI = 1;

    globalPath /= "Control";

    // creates 'Control' folder if there isn't an folder named 'Control'
    if(!std::filesystem::exists(globalPath)){
        std::filesystem::create_directory(globalPath);
    }

    // create an folder for the current NV center wher the Scan is saved
    globalPath /= "NV"+std::to_string(numberROI);
    do{
        if (!std::filesystem::exists(globalPath))
        {
            std::filesystem::create_directory(globalPath);
            existsControlFolder = true;

        }else{
            numberROI++;
            globalPath = std::filesystem::path(globalPath).replace_filename("NV"+std::to_string(numberROI));
        }
    }while(!existsControlFolder);


    // SECOND: find the z parameter with the hightest number of counts
    QVector<double> countsPerSec(10);
    // set the hight of the first checkpoint
    currentScanParameters.Z-=400;
    double maxCount = 0, maxCountHight;

    // check ten points
    for(int i=0; i<10; i++){
        currentScanParameters.Z+=(i*50);
        Sleep(150);
        countsPerSec[i]=getCurrentCountsFromTT();
        // the point with the highest countrate will be the choosen to scan
        if(maxCount < countsPerSec[i]){
            maxCount = countsPerSec[i];
            maxCountHight = currentScanParameters.Z;
        }
        //std::cout << "height: " << currentScanParameters.Z << ", counts: " << countsPerSec[i] << std::endl;
    }

    // set the hight with the higthest countrate
    currentScanParameters.Z=maxCountHight;


    // THIRD: start measurement
    startWaveGenWithCounterScan_ZigZag(currentScanParameters.Npx_X, currentScanParameters.Npx_Y, currentScanParameters.Xstep, currentScanParameters.Ystep, middle);

    // set parameters to the previous vales/ path
    globalPath = globalPathSave;

    // emit a signal if the measurement is finished
    emit StateAIMeasurement(false);
}

