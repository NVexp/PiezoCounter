
#ifndef LABJACKT7_H
#define LABJACKT7_H

#include <QObject>

class LabJackT7 : public QObject
{
    Q_OBJECT
public:
    
    int T7Handle;
    int LJError;
    
    LabJackT7();
    ~LabJackT7();

    void pulseSetupAndEnable(int pulseNb, int pulseLen);
    void pulseDisable();
    
    double analogReadOut(const char* channel);
    
signals:
    void valueRead(double d);
    
};

#endif // LABJACKT7_H
