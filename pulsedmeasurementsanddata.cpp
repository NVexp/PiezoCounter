#include "pulsedMeasurementsanddata.h"
#include "include/TimeTagger.h"
#include <iostream>
#include <QFile>
#include <QDir>
#include <QTextStream>





static const double dMicrosec = 1.e6;
static const timestamp_t tMicrosec = 1000*1000;
static const double dMillisec = 1.e9;
static const timestamp_t tMillisec = 1000*1000*1000;


PulsedMeasurementsAndData::PulsedMeasurementsAndData():
    TTPulsed(nullptr)
{
    TTPulsed = createTimeTagger("1740000JFU");
    TTPulsed->setTriggerLevel(5,0.7);
    TTPulsed->setTriggerLevel(6,0.7);
    Dip1FitParameters = new FitParameters;
    Dip2FitParameters = new FitParameters;

}

PulsedMeasurementsAndData::~PulsedMeasurementsAndData()
{
    //delete LJ_T7;
    freeTimeTagger(TTPulsed);
}


void PulsedMeasurementsAndData::setGlobalPath(std::filesystem::path path)
{
    globalPath = path;
}


void PulsedMeasurementsAndData::prepareCounterForODMR()
{
    ODMRCountsWithMarker= new CountBetweenMarkers(TTPulsed, 1, 4, -4, currentODMRParams.loopNb*currentODMRParams.stepNb);
    TTPulsed->sync();
}

void PulsedMeasurementsAndData::fetchAndProcessDataForODMR(){

    TTPulsed->sync();
    std::cout << "Before loading data" << std::endl;
    std::vector<int> photonsCountsData;
    ODMRCountsWithMarker->getData([&photonsCountsData](int size){photonsCountsData.resize(size); return photonsCountsData.data();});

    std::vector<timestamp_t> CountBinsTimeLength;
    ODMRCountsWithMarker->getBinWidths([&CountBinsTimeLength](int size){CountBinsTimeLength.resize(size); return CountBinsTimeLength.data();});

    std::cout << "Counts.size(): " << photonsCountsData.size() << ", photonsCountsData.size()/currentODMRParams.stepN: " << 1.0*photonsCountsData.size()/currentODMRParams.stepNb << std::endl;

    photonCountsDataProcessed.clear();
    CountBinsTimeLengthProcessed.clear();
    emit(ODMRValuesClear());
    int countsTemp;
    double timeTemp;
    for(int i=0; i<currentODMRParams.stepNb; i++){
        countsTemp=0;
        timeTemp=0;
        for(int j=0; j<currentODMRParams.loopNb; j++){
            countsTemp=countsTemp+photonsCountsData[i+j*currentODMRParams.stepNb];
            timeTemp=timeTemp+CountBinsTimeLength[i+j*currentODMRParams.stepNb]*1e-6;
        }
        photonCountsDataProcessed.push_back(countsTemp);
        CountBinsTimeLengthProcessed.push_back(timeTemp);
        //td::cout << "Values to append. Freqency:" << currentODMRParams.FreqStart+i*(currentODMRParams.FreqEnd-currentODMRParams.FreqStart)/(currentODMRParams.stepNb-1) << " and Counts:" << (double)(countsTemp/timeTemp*1.e6) << std::endl;
        emit(ODMRValues((double)(currentODMRParams.FreqStart+i*(currentODMRParams.FreqEnd-currentODMRParams.FreqStart)/(currentODMRParams.stepNb-1)),(double)(countsTemp)));
    }
    emit(PlotODMR());
    std::cout << "After processing data" << std::endl;
    writeDataToFile("ODMR-Scan");

    delete ODMRCountsWithMarker;
}

void PulsedMeasurementsAndData::prepareCounterForRabi()
{
    RabiCountsWithMarker = new CountBetweenMarkers(TTPulsed, 1, 4, -4, currentRabiParams.Steps*currentRabiParams.LoopNb);
    TTPulsed->sync();
}

void PulsedMeasurementsAndData::fetchAndProcessDataForRabi(){

    TTPulsed->sync();
    std::cout << "Before loading data" << std::endl;
    std::vector<int> photonsCountsData;
    RabiCountsWithMarker->getData([&photonsCountsData](int size){photonsCountsData.resize(size); return photonsCountsData.data();});

    std::vector<timestamp_t> CountBinsTimeLength;
    RabiCountsWithMarker->getBinWidths([&CountBinsTimeLength](int size){CountBinsTimeLength.resize(size); return CountBinsTimeLength.data();});

    std::cout << "Counts.size(): " << photonsCountsData.size() << ", photonsCountsData.size()/currentRabiParams.Steps(=" << currentRabiParams.Steps << "): " << 1.0*photonsCountsData.size()/currentRabiParams.Steps << std::endl;

    photonCountsDataProcessed.clear();
    CountBinsTimeLengthProcessed.clear();

    emit(ODMRValuesClear());
    std::vector<int> countsTemp;
    std::vector<double> timeTemp;
    for(int i=0; i<currentRabiParams.LoopNb; i++){
        for(int j=0; j<currentRabiParams.Steps; j++){
            if(i==0){
                countsTemp.push_back(photonsCountsData[j]);
                timeTemp.push_back(CountBinsTimeLength[j]);
            }else{
                countsTemp[j]=countsTemp[j]+photonsCountsData[i*currentRabiParams.Steps+j];
                timeTemp[j]=timeTemp[j]+CountBinsTimeLength[i*currentRabiParams.Steps+j];
            }
        }
    }
    for(int i=0; i<countsTemp.size(); ++i){
        emit(ODMRValues((double)(currentRabiParams.Stepsize*i),(double)(countsTemp[i])));
    }

    for(int i=0; i<countsTemp.size(); ++i){
        photonCountsDataProcessed.push_back(countsTemp[i]);
        CountBinsTimeLengthProcessed.push_back(timeTemp[i]);
    }

    emit(PlotODMR());
    std::cout << "After processing data" << std::endl;
    writeDataToFile("Rabi-Scan");

    delete RabiCountsWithMarker;
}

void PulsedMeasurementsAndData::prepareCounterForPulsedODMR()
{
    PulsedODMRCountsWithMarker = new CountBetweenMarkers(TTPulsed, 1, 4, -4, currentPulsedODMRParams.LoopNb*currentPulsedODMRParams.stepNb);
    TTPulsed->sync();
}

void PulsedMeasurementsAndData::fetchAndProcessDataForPulsedODMR(){

    TTPulsed->sync();
    std::cout << "Before loading data" << std::endl;
    std::vector<int> photonsCountsData;
    PulsedODMRCountsWithMarker->getData([&photonsCountsData](int size){photonsCountsData.resize(size); return photonsCountsData.data();});

    std::vector<timestamp_t> CountBinsTimeLength;
    PulsedODMRCountsWithMarker->getBinWidths([&CountBinsTimeLength](int size){CountBinsTimeLength.resize(size); return CountBinsTimeLength.data();});

    std::cout << "Counts.size(): " << photonsCountsData.size() << ", photonsCountsData.size()/currentPulsedODMRParams.stepNb(=" << currentPulsedODMRParams.stepNb << "): " << 1.0*photonsCountsData.size()/currentPulsedODMRParams.stepNb << std::endl;

    photonCountsDataProcessed.clear();
    CountBinsTimeLengthProcessed.clear();

    emit(ODMRValuesClear());
    std::vector<int> countsTemp;
    std::vector<double> timeTemp;
    for(int i=0; i<currentPulsedODMRParams.LoopNb; i++){
        for(int j=0; j<currentPulsedODMRParams.stepNb; j++){
            if(i==0){
                countsTemp.push_back(photonsCountsData[j]);
                timeTemp.push_back(CountBinsTimeLength[j]);
            }else{
                countsTemp[j]=countsTemp[j]+photonsCountsData[i*currentPulsedODMRParams.stepNb+j];
                timeTemp[j]=timeTemp[j]+CountBinsTimeLength[i*currentPulsedODMRParams.stepNb+j];
            }
        }
    }
    for(int i=0; i<countsTemp.size(); ++i){
        emit(ODMRValues((double)currentPulsedODMRParams.FreqStart+i*(currentPulsedODMRParams.FreqEnd-currentPulsedODMRParams.FreqStart)/(currentPulsedODMRParams.stepNb-1),(double)(countsTemp[i])));
        photonCountsDataProcessed.push_back(countsTemp[i]);
        CountBinsTimeLengthProcessed.push_back(timeTemp[i]);
    }

    emit(PlotODMR());
    std::cout << "After processing data" << std::endl;
    writeDataToFile("Pulsed-ODMR-Scan");

    delete PulsedODMRCountsWithMarker;
}

void PulsedMeasurementsAndData::prepareCounterForT1()
{
    T1CountsWithMarker= new CountBetweenMarkers(TTPulsed, 1, 4, -4, currentT1Params.Steps*currentT1Params.LoopNb*2);
    TTPulsed->sync();
}

void PulsedMeasurementsAndData::fetchAndProcessDataForT1(){

    TTPulsed->sync();
    std::cout << "Before loading data" << std::endl;
    std::vector<int> photonsCountsData;
    T1CountsWithMarker->getData([&photonsCountsData](int size){photonsCountsData.resize(size); return photonsCountsData.data();});

    std::vector<timestamp_t> CountBinsTimeLength;
    T1CountsWithMarker->getBinWidths([&CountBinsTimeLength](int size){CountBinsTimeLength.resize(size); return CountBinsTimeLength.data();});

    std::cout << "Counts.size(): " << photonsCountsData.size() << ", photonsCountsData.size()/currentT1Params.Steps(=" << currentT1Params.Steps << "): " << 1.0*photonsCountsData.size()/currentT1Params.Steps << std::endl;

    photonCountsDataProcessed.clear();
    photonCountsPIDataProcessed.clear();
    CountBinsTimeLengthProcessed.clear();

    emit(ODMRValuesClear());
    std::vector<int> countsTemp;
    std::vector<double> timeTemp;
    std::vector<int> countsPiTemp;
    for(int i=0; i<currentT1Params.LoopNb; i++){
        for(int j=0; j<currentT1Params.Steps; j++){
            if(i==0){
                countsTemp.push_back(photonsCountsData[j*2+1]);
                timeTemp.push_back(CountBinsTimeLength[j*2]);
                countsPiTemp.push_back(photonsCountsData[j*2]);
            }else{
                countsTemp[j]=countsTemp[j]+photonsCountsData[(i*currentT1Params.Steps+j)*2+1];
                timeTemp[j]=timeTemp[j]+CountBinsTimeLength[(i*currentT1Params.Steps+j)*2];
                countsPiTemp[j]=countsPiTemp[j]+photonsCountsData[(i*currentT1Params.Steps+j)*2];
            }
        }
    }
    for(int i=0; i<countsTemp.size(); ++i){
        emit(ODMRValues((double)(currentT1Params.Stepsize*i),(double)(countsTemp[i])));
        emit(T1Value((double)(countsPiTemp[i])));
    }

    for(int i=0; i<countsTemp.size(); ++i){
        photonCountsDataProcessed.push_back(countsTemp[i]);
        photonCountsPIDataProcessed.push_back(countsPiTemp[i]);
        CountBinsTimeLengthProcessed.push_back(timeTemp[i]);
    }

    if((withRefocus)&&(withRefocusXValues.empty())){
        //std::cout << "First time with refocus!" << std::endl;
        for(int i=0; i<countsTemp.size(); ++i){
            withRefocusXValues.push_back((double)(currentCompPulsDetunParams.Freq+currentCompPulsDetunParams.Stepsize*(i-currentCompPulsDetunParams.Steps/2)));
            withRefocusYValues.push_back(countsTemp[i]);
            withRefocusY2Values.push_back(countsPiTemp[i]);
        }
    }else if(withRefocus){
        //std::cout << "Not first time with refocus!" << std::endl;
        for(int i=0; i<countsTemp.size(); ++i){
            withRefocusYValues[i]+=countsTemp[i];
            withRefocusY2Values[i]+=countsTemp[i];
        }
    }

    emit(PlotODMR());
    emit(PlotT1());
    std::cout << "After processing data" << std::endl;
    writeDataToFile("T1-Scan");

    delete T1CountsWithMarker;
}

void PulsedMeasurementsAndData::prepareCounterForRamsey(){
    RamseyCountsWithMarker = new CountBetweenMarkers(TTPulsed, 1, 4, -4, currentRamseyParams.Steps*currentRamseyParams.LoopNb);
    TTPulsed->sync();
}

void PulsedMeasurementsAndData::fetchAndProcessDataForRamsey(){
    TTPulsed->sync();
    std::cout << "Before loading data" << std::endl;
    std::vector<int> photonsCountsData;
    RamseyCountsWithMarker->getData([&photonsCountsData](int size){photonsCountsData.resize(size); return photonsCountsData.data();});

    std::vector<timestamp_t> CountBinsTimeLength;
    RamseyCountsWithMarker->getBinWidths([&CountBinsTimeLength](int size){CountBinsTimeLength.resize(size); return CountBinsTimeLength.data();});

    std::cout << "Counts.size(): " << photonsCountsData.size() << ", photonsCountsData.size()/currentRamseyParams.Steps(=" << currentRamseyParams.Steps << "): " << 1.0*photonsCountsData.size()/currentRamseyParams.Steps << std::endl;

    photonCountsDataProcessed.clear();
    CountBinsTimeLengthProcessed.clear();

    emit(ODMRValuesClear());
    std::vector<int> countsTemp;
    std::vector<double> timeTemp;
    for(int i=0; i<currentRamseyParams.LoopNb; i++){
        for(int j=0; j<currentRamseyParams.Steps; j++){
            if(i==0){
                countsTemp.push_back(photonsCountsData[j]);
                timeTemp.push_back(CountBinsTimeLength[j]);
            }else{
                countsTemp[j]=countsTemp[j]+photonsCountsData[i*currentRamseyParams.Steps+j];
                timeTemp[j]=timeTemp[j]+CountBinsTimeLength[i*currentRamseyParams.Steps+j];
            }
        }
    }
    for(int i=0; i<countsTemp.size(); ++i){
        emit(ODMRValues((double)(currentRamseyParams.Stepsize*i),(double)(countsTemp[i])));
    }

    for(int i=0; i<countsTemp.size(); ++i){
        photonCountsDataProcessed.push_back(countsTemp[i]);
        CountBinsTimeLengthProcessed.push_back(timeTemp[i]);
    }

    emit(PlotODMR());
    std::cout << "After processing data" << std::endl;
    writeDataToFile("Ramsey-Scan");

    delete RamseyCountsWithMarker;
}

void PulsedMeasurementsAndData::prepareCounterForSpinEcho(){
    SpinEchoCountsWithMarker = new CountBetweenMarkers(TTPulsed, 1, 4, -4, currentSpinEchoParams.Steps*currentSpinEchoParams.LoopNb);
    TTPulsed->sync();
}

void PulsedMeasurementsAndData::fetchAndProcessDataForSpinEcho(){
    TTPulsed->sync();
    std::cout << "Before loading data SpinEcho" << std::endl;
    std::vector<int> photonsCountsData;
    SpinEchoCountsWithMarker->getData([&photonsCountsData](int size){photonsCountsData.resize(size); return photonsCountsData.data();});

    std::vector<timestamp_t> CountBinsTimeLength;
    SpinEchoCountsWithMarker->getBinWidths([&CountBinsTimeLength](int size){CountBinsTimeLength.resize(size); return CountBinsTimeLength.data();});

    std::cout << "Counts.size(): " << photonsCountsData.size() << ", photonsCountsData.size()/currentSpinEchoParams.Steps(=" << currentSpinEchoParams.Steps << "): " << 1.0*photonsCountsData.size()/currentSpinEchoParams.Steps << std::endl;

    photonCountsDataProcessed.clear();
    CountBinsTimeLengthProcessed.clear();

    emit(ODMRValuesClear());
    std::vector<int> countsTemp;
    std::vector<double> timeTemp;
    for(int i=0; i<currentSpinEchoParams.LoopNb; i++){
        for(int j=0; j<currentSpinEchoParams.Steps; j++){
            if(i==0){
                countsTemp.push_back(photonsCountsData[j]);
                timeTemp.push_back(CountBinsTimeLength[j]);
            }else{
                countsTemp[j]=countsTemp[j]+photonsCountsData[i*currentSpinEchoParams.Steps+j];
                timeTemp[j]=timeTemp[j]+CountBinsTimeLength[i*currentSpinEchoParams.Steps+j];
            }
        }
    }
    for(int i=0; i<countsTemp.size(); ++i){
        emit(ODMRValues((double)(currentSpinEchoParams.Stepsize*i),(double)(countsTemp[i])));
    }

    for(int i=0; i<countsTemp.size(); ++i){
        photonCountsDataProcessed.push_back(countsTemp[i]);
        CountBinsTimeLengthProcessed.push_back(timeTemp[i]);
    }

    emit(PlotODMR());
    std::cout << "After processing data" << std::endl;
    writeDataToFile("SpinEcho-Scan");

    delete SpinEchoCountsWithMarker;
}




void PulsedMeasurementsAndData::prepareCounterForTestReadout(){
    TestReadoutCountsWithMarker = new CountBetweenMarkers(TTPulsed, 1, 4, -4, 2*currentTestReadoutParams.LoopNb);
    TTPulsed->sync();
}

void PulsedMeasurementsAndData::fetchAndProcessDataForTestReadout(){
    TTPulsed->sync();
    std::cout << "Before loading data SpinEcho" << std::endl;
    std::vector<int> photonsCountsData;
    TestReadoutCountsWithMarker->getData([&photonsCountsData](int size){photonsCountsData.resize(size); return photonsCountsData.data();});

    std::vector<timestamp_t> CountBinsTimeLength;
    TestReadoutCountsWithMarker->getBinWidths([&CountBinsTimeLength](int size){CountBinsTimeLength.resize(size); return CountBinsTimeLength.data();});

    photonCountsDataProcessed.clear();
    CountBinsTimeLengthProcessed.clear();

    int DarkCounts = 0;
    int BrightCounts = 0;

    for(int i=0; i<photonsCountsData.size(); i++){
        photonCountsDataProcessed.push_back(photonsCountsData[i]);
        CountBinsTimeLengthProcessed.push_back(CountBinsTimeLength[i]);
        if(i%2){
            DarkCounts = DarkCounts + photonsCountsData[i];
        }else{
            BrightCounts = BrightCounts + photonsCountsData[i];
        }
    }

    std::cout << "Sum DarkCounts: " << DarkCounts << std::endl;
    std::cout << "Sum BrightCounts: " << BrightCounts << std::endl;
//    for(int i=0; i<countsTemp.size(); ++i){
//        emit(ODMRValues((double)(currentSpinEchoParams.Stepsize*i),(double)(countsTemp[i])));
//    }

    emit(PlotODMR());
    std::cout << "After processing data" << std::endl;
    writeDataToFile("TestRead-Scan");

    delete TestReadoutCountsWithMarker;
}




void PulsedMeasurementsAndData::prepareCounterForCompPulsDetun()
{
    CompPulsDetunCountsWithMarker = new CountBetweenMarkers(TTPulsed, 1, 4, -4, currentCompPulsDetunParams.Steps*currentCompPulsDetunParams.LoopNb);
    TTPulsed->sync();
}

void PulsedMeasurementsAndData::fetchAndProcessDataForCompPulsDetun(){

    TTPulsed->sync();
    std::cout << "Before loading data" << std::endl;
    std::vector<int> photonsCountsData;
    CompPulsDetunCountsWithMarker->getData([&photonsCountsData](int size){photonsCountsData.resize(size); return photonsCountsData.data();});

    std::vector<timestamp_t> CountBinsTimeLength;
    CompPulsDetunCountsWithMarker->getBinWidths([&CountBinsTimeLength](int size){CountBinsTimeLength.resize(size); return CountBinsTimeLength.data();});

    std::cout << "Counts.size(): " << photonsCountsData.size() << ", photonsCountsData.size(): " << 1.0*photonsCountsData.size()/currentCompPulsDetunParams.Steps << std::endl;

    photonCountsDataProcessed.clear();
    CountBinsTimeLengthProcessed.clear();

    emit(ODMRValuesClear());
    std::vector<int> countsTemp;
    std::vector<double> timeTemp;
    for(int i=0; i<currentCompPulsDetunParams.LoopNb; i++){
        for(int j=0; j<currentCompPulsDetunParams.Steps; j++){
            if(i==0){
                countsTemp.push_back(photonsCountsData[j]);
                timeTemp.push_back(CountBinsTimeLength[j]);
            }else{
                countsTemp[j]=countsTemp[j]+photonsCountsData[i*currentCompPulsDetunParams.Steps+j];
                timeTemp[j]=timeTemp[j]+CountBinsTimeLength[i*currentCompPulsDetunParams.Steps+j];
            }
        }
    }
    for(int i=0; i<countsTemp.size(); ++i){
        emit(ODMRValues((double)(currentCompPulsDetunParams.Freq+currentCompPulsDetunParams.Stepsize*(i-currentCompPulsDetunParams.Steps/2)),(double)(countsTemp[i])));
        //std::cout << "Freq: " << currentCompPulsDetunParams.Freq+currentCompPulsDetunParams.Stepsize*(i-currentCompPulsDetunParams.Steps/2) << std::endl;
    }

    for(int i=0; i<countsTemp.size(); ++i){
        photonCountsDataProcessed.push_back(countsTemp[i]);
        CountBinsTimeLengthProcessed.push_back(timeTemp[i]);
    }
    if((withRefocus)&&(withRefocusXValues.empty())){
        //std::cout << "First time with refocus!" << std::endl;
        for(int i=0; i<countsTemp.size(); ++i){
            withRefocusXValues.push_back((double)(currentCompPulsDetunParams.Freq+currentCompPulsDetunParams.Stepsize*(i-currentCompPulsDetunParams.Steps/2)));
            withRefocusYValues.push_back(countsTemp[i]);
        }
    }else if(withRefocus){
        //std::cout << "Not first time with refocus!" << std::endl;
        for(int i=0; i<countsTemp.size(); ++i){
            withRefocusYValues[i]+=countsTemp[i];
        }
    }

    emit(PlotODMR());
    std::cout << "After processing data" << std::endl;
    writeDataToFile("CompPulsDetun-Scan");

    delete CompPulsDetunCountsWithMarker;
}

void PulsedMeasurementsAndData::prepareCounterForCompPulsPI2()
{
    CompPulsPI2CountsWithMarker = new CountBetweenMarkers(TTPulsed, 1, 4, -4, currentCompPulsDetunParams.Steps*currentCompPulsDetunParams.LoopNb);
    TTPulsed->sync();
}

void PulsedMeasurementsAndData::fetchAndProcessDataForCompPulsPI2(){

    TTPulsed->sync();
    std::cout << "Before loading data" << std::endl;
    std::vector<int> photonsCountsData;
    CompPulsPI2CountsWithMarker->getData([&photonsCountsData](int size){photonsCountsData.resize(size); return photonsCountsData.data();});

    std::vector<timestamp_t> CountBinsTimeLength;
    CompPulsPI2CountsWithMarker->getBinWidths([&CountBinsTimeLength](int size){CountBinsTimeLength.resize(size); return CountBinsTimeLength.data();});

    std::cout << "Counts.size(): " << photonsCountsData.size() << ", photonsCountsData.size()/currentT1Params.Steps(=" << currentCompPulsDetunParams.Steps << "): " << 1.0*photonsCountsData.size()/currentCompPulsDetunParams.Steps << std::endl;

    photonCountsDataProcessed.clear();
    CountBinsTimeLengthProcessed.clear();

    emit(ODMRValuesClear());
    std::vector<int> countsTemp;
    std::vector<double> timeTemp;
    for(int i=0; i<currentCompPulsDetunParams.LoopNb; i++){
        for(int j=0; j<currentCompPulsDetunParams.Steps; j++){
            if(i==0){
                countsTemp.push_back(photonsCountsData[j]);
                timeTemp.push_back(CountBinsTimeLength[j]);
            }else{
                countsTemp[j]=countsTemp[j]+photonsCountsData[i*currentCompPulsDetunParams.Steps+j];
                timeTemp[j]=timeTemp[j]+CountBinsTimeLength[i*currentCompPulsDetunParams.Steps+j];
            }
        }
    }
    for(int i=0; i<countsTemp.size(); ++i){
        emit(ODMRValues((double)(currentCompPulsDetunParams.PiTime*i/currentCompPulsDetunParams.Steps),(double)(countsTemp[i])));
    }

    for(int i=0; i<countsTemp.size(); ++i){
        photonCountsDataProcessed.push_back(countsTemp[i]);
        CountBinsTimeLengthProcessed.push_back(timeTemp[i]);
    }

    emit(PlotODMR());
    std::cout << "After processing data" << std::endl;
    writeDataToFile("CompPulsPi2-Scan");

    delete CompPulsPI2CountsWithMarker;
}

void PulsedMeasurementsAndData::prepareCounterForSech()
{
    SechCountsWithMarker = new CountBetweenMarkers(TTPulsed, 1, 4, -4, currentSechParams.Steps*currentSechParams.LoopNb);
    TTPulsed->sync();
}

void PulsedMeasurementsAndData::fetchAndProcessDataForSech(){

    TTPulsed->sync();
    std::cout << "Before loading data" << std::endl;
    std::vector<int> photonsCountsData;
    SechCountsWithMarker->getData([&photonsCountsData](int size){photonsCountsData.resize(size); return photonsCountsData.data();});

    std::vector<timestamp_t> CountBinsTimeLength;
    SechCountsWithMarker->getBinWidths([&CountBinsTimeLength](int size){CountBinsTimeLength.resize(size); return CountBinsTimeLength.data();});

    std::cout << "Counts.size(): " << photonsCountsData.size() << ", photonsCountsData.size()/currentSechParams.Steps(=" << currentSechParams.Steps << "): " << 1.0*photonsCountsData.size()/currentSechParams.Steps << std::endl;

    photonCountsDataProcessed.clear();
    CountBinsTimeLengthProcessed.clear();

    emit(ODMRValuesClear());
    std::vector<int> countsTemp;
    std::vector<double> timeTemp;
    for(int i=0; i<currentSechParams.LoopNb; i++){
        for(int j=0; j<currentSechParams.Steps; j++){
            if(i==0){
                countsTemp.push_back(photonsCountsData[j]);
                timeTemp.push_back(CountBinsTimeLength[j]);
            }else{
                countsTemp[j]=countsTemp[j]+photonsCountsData[i*currentSechParams.Steps+j];
                timeTemp[j]=timeTemp[j]+CountBinsTimeLength[i*currentSechParams.Steps+j];
            }
        }
    }
    for(int i=0; i<countsTemp.size(); ++i){
        emit(ODMRValues((double)(currentSechParams.Stepsize*i),(double)(countsTemp[i])));
    }

    for(int i=0; i<countsTemp.size(); ++i){
        photonCountsDataProcessed.push_back(countsTemp[i]);
        CountBinsTimeLengthProcessed.push_back(timeTemp[i]);
    }

    emit(PlotODMR());
    std::cout << "After processing data" << std::endl;
    writeDataToFile("Sech-Scan");

    delete SechCountsWithMarker;
}

void PulsedMeasurementsAndData::writeDataToFile(std::string pathName) {
    std::time_t rawtime;
    std::tm* timeinfo;
    char yyyymmddHHMMSS [80];
    std::time(&rawtime);
    timeinfo = std::localtime(&rawtime);
    std::strftime(yyyymmddHHMMSS,80,"%Y%m%d%H%M%S",timeinfo);
    std::string currentImageDate_yyyymmddHHMMSS = yyyymmddHHMMSS;

    std::filesystem::path globalPathSave = globalPath;

    if(!withRefocus){
        globalPath /= pathName;
        if((!std::filesystem::exists(globalPath))) {
            std::cout << "Path " << globalPath << " does not exist!" << std::endl;
            if(std::filesystem::create_directory(globalPath)) {
                    std::cout << "Path created" << std::endl;
            }else{
                std::cout << "unable to create Path" << std::endl;
            }
        }else{
            std::cout << "Path exists" << std::endl;
        }
    }else {
        std::cout << "Refocus measurement" << std::endl;
    }

    currentDataFromFile.PathLoaded = globalPath.string();
    currentDataFromFile.fileNameLoaded = pathName+currentImageDate_yyyymmddHHMMSS;

    std::cout << "Befor creating file" << std::endl;
    std::filesystem::current_path(globalPath);
    std::ofstream outfile (pathName+currentImageDate_yyyymmddHHMMSS+".dat");

    if(pathName=="Rabi-Scan"){
        outfile << "xPos [nm]/yPos [nm]/zPos [nm]/Frequency [MHz]/Rabi Stepssize [ns]/Number Rabi Steps/Loop Number/Excitation Time/Laser Time/MW Amplitude/Rabi0-Time" << std::endl;
        outfile << CurrentPosition.xPos << "\t" << CurrentPosition.yPos << "\t" << CurrentPosition.zPos << "\t";
        outfile << currentRabiParams.Freq << "\t" << currentRabiParams.Stepsize << "\t";
        outfile << currentRabiParams.Steps << "\t" << currentRabiParams.LoopNb << "\t" ;
        outfile << currentRabiParams.TimeCounter << "\t" << currentRabiParams.TimeLaser << "\t";
        outfile << currentRabiParams.Amp << "\t" << currentRabiParams.Rabi0Time << std::endl;

        outfile << "tau [ns]\t Counts\t time[ps]" << "\n" << std::endl;

        for(int i=0; i<currentRabiParams.Steps; ++i)
        {
            //time [ns]
            outfile << i*currentRabiParams.Stepsize << "\t";
            //Counts
            outfile << (int)(photonCountsDataProcessed[i]) << "\t" ;
            //time value
            outfile << CountBinsTimeLengthProcessed[i] << std::endl;
        }
    }
    if(pathName=="ODMR-Scan"){
        outfile << "xPos [nm]/yPos [nm]/zPos [nm]/Start Frequency [MHz]/End Frequency [MHz]/Number of Frequency Steps/Loop Number/Excitation Time" << std::endl;
        outfile << CurrentPosition.xPos << "\t" << CurrentPosition.yPos << "\t" << CurrentPosition.zPos << "\t";
        outfile << currentODMRParams.FreqStart << "\t" << currentODMRParams.FreqEnd << "\t";
        outfile << currentODMRParams.stepNb << "\t" << currentODMRParams.loopNb << "\t" << currentODMRParams.excitTime << std::endl;

        outfile << "f [MHz]\t Counts\t time[micro s]" << "\n" << std::endl;

        for(int i=0; i<currentODMRParams.stepNb; ++i)
        {
            //f [MHz]
            outfile << currentODMRParams.FreqStart+i*(currentODMRParams.FreqEnd-currentODMRParams.FreqStart)/(currentODMRParams.stepNb-1) << "\t";
            //kCounts/sec
            outfile << (int)((photonCountsDataProcessed[i]*1.e6/CountBinsTimeLengthProcessed[i])) << "\t" ;
            //time value
            outfile << CountBinsTimeLengthProcessed[i] << std::endl;
        }
    }
    if(pathName=="Pulsed-ODMR-Scan"){
        outfile << "xPos [nm]/yPos [nm]/zPos [nm]/Start Frequency [MHz]/End Frequency [MHz]/Number of Frequency Steps/Loop Number/ pi-pulse [ns]" << std::endl;
        outfile << CurrentPosition.xPos << "\t" << CurrentPosition.yPos << "\t" << CurrentPosition.zPos << "\t";
        outfile << currentPulsedODMRParams.FreqStart << "\t" << currentPulsedODMRParams.FreqEnd << "\t";
        outfile << currentPulsedODMRParams.stepNb << "\t" << currentPulsedODMRParams.LoopNb << "\t" << currentPulsedODMRParams.pi_Pulse << std::endl;

        outfile << "f [MHz]\t Counts\t time[ps]" << "\n" << std::endl;

        for(int i=0; i<currentPulsedODMRParams.stepNb; ++i)
        {
            //f [MHz]
            outfile << currentPulsedODMRParams.FreqStart+i*(currentPulsedODMRParams.FreqEnd-currentPulsedODMRParams.FreqStart)/(currentPulsedODMRParams.stepNb-1) << "\t";
            //kCounts/sec
            outfile << (int)(photonCountsDataProcessed[i]) << "\t" ;
            //time value
            outfile << CountBinsTimeLengthProcessed[i] << std::endl;
        }
    }
    if(QString::fromStdString(pathName).contains("T1-Scan")){
        outfile << "xPos [nm]/yPos [nm]/zPos [nm]/Stepssize [µs]/Number Steps/Loop Number/Excitation Time/0-Time/frequency [MHz]" << std::endl;
        outfile << CurrentPosition.xPos << "\t" << CurrentPosition.yPos << "\t" << CurrentPosition.zPos << "\t";

        /*
        outfile << currentT1Params.Stepsize/1000 << "\t" << currentT1Params.Steps << "\t" << currentT1Params.LoopNb << "\t";
        outfile << currentT1Params.TimeCounter << "\t" << currentT1Params.PiTime << "\t" << currentT1Params.Freq <<  std::endl;

        outfile << "tau [ns]\t Counts w pi\t Counts w/o pi\t time[µs]" << "\n" << std::endl;

        for(int i=0; i<currentT1Params.Steps; ++i)
        {
            //time [ns]
            outfile << i*currentT1Params.Stepsize << "\t";
            //Counts w pi
            outfile << (int)(photonCountsPIDataProcessed[i]) << "\t" ;
            //Counts w/o pi
            outfile << (int)(photonCountsDataProcessed[i]) << "\t" ;
            //time value
            outfile << CountBinsTimeLengthProcessed[i] << std::endl;
        }
        */

        if(QString::fromStdString(pathName).contains("combined")){
            outfile << currentT1Params.Stepsize/1000 << "\t" << currentT1Params.Steps << "\t" << currentT1Params.LoopNb << "\t";
            outfile << currentT1Params.TimeCounter << "\t" << currentT1Params.PiTime << "\t" << currentT1Params.Freq <<  std::endl;

            outfile << "tau [ns]\t Counts w pi\t Counts w/o pi\t time[ps]" << "\n" << std::endl;

            for(int i=0; i<currentT1Params.Steps; ++i)
            {
                //time [ns]
                outfile << (int)(withRefocusXValues[i]) << "\t";
                //Counts w pi
                outfile << (int)(withRefocusY2Values[i]) << "\t" ;
                //Counts w/o pi
                outfile << (int)(withRefocusYValues[i]) << "\t" ;
                //time value
                outfile << CountBinsTimeLengthProcessed[i] << std::endl;
            }
        }else{
            outfile << currentT1Params.Stepsize/1000 << "\t" << currentT1Params.Steps << "\t" << currentT1Params.LoopNb*NbOfRefocus << "\t";
            outfile << currentT1Params.TimeCounter << "\t" << currentT1Params.PiTime << "\t" << currentT1Params.Freq <<  std::endl;

            outfile << "tau [ns]\t Counts w pi\t Counts w/o pi\t time[µs]" << "\n" << std::endl;

            for(int i=0; i<currentT1Params.Steps; ++i)
            {
                //time [ns]
                outfile << i*currentT1Params.Stepsize << "\t";
                //Counts w pi
                outfile << (int)(photonCountsPIDataProcessed[i]) << "\t" ;
                //Counts w/o pi
                outfile << (int)(photonCountsDataProcessed[i]) << "\t" ;
                //time value
                outfile << CountBinsTimeLengthProcessed[i]*NbOfRefocus << std::endl;
            }
        }
    }
    if(pathName=="Ramsey-Scan"){
        outfile << "xPos [nm]/yPos [nm]/zPos [nm]/Frequency [MHz]/Ramsey Stepssize [ns]/Number of Steps/PiOver2Time [ns]/Loop Number/Excitation Time" << std::endl;
        outfile << CurrentPosition.xPos << "\t" << CurrentPosition.yPos << "\t" << CurrentPosition.zPos << "\t";
        outfile << currentRamseyParams.Freq << "\t" << currentRamseyParams.Stepsize << "\t";
        outfile << currentRamseyParams.Steps << "\t" << currentRamseyParams.PiOver2time << "\t" << currentRamseyParams.LoopNb << "\t" ;
        outfile << currentRamseyParams.TimeCounter << std::endl;

        outfile << "tau [ns]\t Counts\t time[micro s]" << "\n" << std::endl;

        for(int i=0; i<currentRamseyParams.Steps; ++i)
        {
            //time [ns]
            outfile << i*currentRamseyParams.Stepsize << "\t";
            //Counts
            outfile << (int)(photonCountsDataProcessed[i]) << "\t" ;
            //time value
            outfile << CountBinsTimeLengthProcessed[i] << std::endl;
        }
    }
    if(pathName=="SpinEcho-Scan"){
        outfile << "xPos [nm]/yPos [nm]/zPos [nm]/Frequency [MHz]/Stepssize [ns]/Number of Steps/Loop Number/PiOver2Time [ns]/PiTime/Excitation Time" << std::endl;
        outfile << CurrentPosition.xPos << "\t" << CurrentPosition.yPos << "\t" << CurrentPosition.zPos << "\t";
        outfile << currentSpinEchoParams.Freq << "\t" << currentSpinEchoParams.Stepsize << "\t";
        outfile << currentSpinEchoParams.Steps << "\t" << currentSpinEchoParams.LoopNb << "\t" ;
        outfile << currentSpinEchoParams.PiOver2time << "\t" << currentSpinEchoParams.PiTime << "\t";
        outfile << currentSpinEchoParams.TimeCounter << std::endl;

        outfile << "tau [ns]\t Counts\t time[micro s]" << "\n" << std::endl;

        for(int i=0; i<currentSpinEchoParams.Steps; ++i)
        {
            //time [ns]
            outfile << i*currentSpinEchoParams.Stepsize << "\t";
            //Counts
            outfile << (int)(photonCountsDataProcessed[i]) << "\t" ;
            //time value
            outfile << CountBinsTimeLengthProcessed[i] << std::endl;
        }
    }
    if(pathName=="TestRead-Scan"){
        outfile << "xPos [nm]/yPos [nm]/zPos [nm]/Frequency [MHz]/Loop Number/PiTime/Excitation Time" << std::endl;
        outfile << CurrentPosition.xPos << "\t" << CurrentPosition.yPos << "\t" << CurrentPosition.zPos << "\t";
        outfile << currentTestReadoutParams.Freq << "\t" << currentTestReadoutParams.LoopNb << "\t" ;
        outfile << currentTestReadoutParams.PiTime << "\t" << currentTestReadoutParams.TimeCounter << std::endl;

        outfile << "BrightCounts \t DarkCounts " << "\n" << std::endl;

        for(int i=0; i<(currentTestReadoutParams.LoopNb*2); i=i+2)
        {
            //Counts Bright
            outfile << (int)(photonCountsDataProcessed[i]) << "\t" ;
            //Counts Dark
            outfile << (int)(photonCountsDataProcessed[i+1]) << std::endl;
        }
    }
    if(QString::fromStdString(pathName).contains("CompPulsDetun-Scan")){
        outfile << "xPos [nm]/yPos [nm]/zPos [nm]/Number of Phases/Frequency [MHz]/Stepsize [1/ampl]/Number Steps/Loop Number/pi-pulse [ns]" << std::endl;
        outfile << CurrentPosition.xPos << "\t" << CurrentPosition.yPos << "\t" << CurrentPosition.zPos << "\t";
        outfile << currentCompPulsDetunParams.PhaseNb << "\t" << currentCompPulsDetunParams.Freq << "\t" << currentCompPulsDetunParams.Stepsize << "\t";
        if(QString::fromStdString(pathName).contains("combined")){
            outfile << currentCompPulsDetunParams.Steps << "\t" << currentCompPulsDetunParams.LoopNb*(NbOfRefocus) << "\t" << currentCompPulsDetunParams.PiTime << std::endl;

            outfile << "f [MHz]\t Counts\t time[micro s]" << "\n" << std::endl;

            for(int i=0; i<currentCompPulsDetunParams.Steps; ++i)
            {
                //f [MHz] Freq+Stepsize*(i-stepNb/2)
                outfile << currentCompPulsDetunParams.Freq+currentCompPulsDetunParams.Stepsize*(i-currentCompPulsDetunParams.Steps/2) << "\t";
                //kCounts/sec
                outfile << (int)(withRefocusYValues[i]) << "\t" ;
                //time value
                outfile << CountBinsTimeLengthProcessed[i]*NbOfRefocus << std::endl;
            }
        }else{
            outfile << currentCompPulsDetunParams.Steps << "\t" << currentCompPulsDetunParams.LoopNb << "\t" << currentCompPulsDetunParams.PiTime << std::endl;

            outfile << "f [MHz]\t Counts\t time[micro s]" << "\n" << std::endl;

            for(int i=0; i<currentCompPulsDetunParams.Steps; ++i)
            {
                //f [MHz] Freq+Stepsize*(i-stepNb/2)
                outfile << currentCompPulsDetunParams.Freq+currentCompPulsDetunParams.Stepsize*(i-currentCompPulsDetunParams.Steps/2) << "\t";
                //kCounts/sec
                outfile << (int)(photonCountsDataProcessed[i]) << "\t" ;
                //time value
                outfile << CountBinsTimeLengthProcessed[i] << std::endl;
            }
        }

    }
    if(pathName=="CompPulsPi2-Scan"){
        outfile << "xPos [nm]/yPos [nm]/zPos [nm]/Number of Phases/Frequency [MHz]/Stepsize [1/ampl]/Number Steps/Loop Number/pi-pulse [ns]" << std::endl;
        outfile << CurrentPosition.xPos << "\t" << CurrentPosition.yPos << "\t" << CurrentPosition.zPos << "\t";
        outfile << currentCompPulsDetunParams.PhaseNb << "\t" << currentCompPulsDetunParams.Freq << "\t" << currentCompPulsDetunParams.Stepsize << "\t";
        outfile << currentCompPulsDetunParams.Steps << "\t" << currentCompPulsDetunParams.LoopNb << "\t" << currentCompPulsDetunParams.PiTime << std::endl;

        outfile << "Time [ns]\t Counts\t time[micro s]" << "\n" << std::endl;

        for(int i=0; i<currentCompPulsDetunParams.Steps; ++i)
        {
            //t [ns]
            outfile << currentCompPulsDetunParams.PiTime*i/currentCompPulsDetunParams.Steps << "\t";
            //kCounts/sec
            outfile << (int)(photonCountsDataProcessed[i]) << "\t" ;
            //time value
            outfile << CountBinsTimeLengthProcessed[i] << std::endl;
        }
    }
    if(pathName=="Sech-Scan"){
        outfile << "xPos [nm]/yPos [nm]/zPos [nm]/Frequency [MHz]/Sech Stepssize [ns]/Number Sech Steps/Loop Number/Excitation Time/Laser Time/Sech0-Time" << std::endl;
        outfile << CurrentPosition.xPos << "\t" << CurrentPosition.yPos << "\t" << CurrentPosition.zPos << "\t";
        outfile << currentSechParams.Freq << "\t" << currentSechParams.Stepsize << "\t";
        outfile << currentSechParams.Steps << "\t" << currentSechParams.LoopNb << "\t" ;
        outfile << currentSechParams.TimeCounter << "\t" << currentSechParams.TimeLaser << "\t";
        outfile << currentSechParams.segment0Time << std::endl;

        outfile << "tau [ns]\t Counts\t time[micro s]" << "\n" << std::endl;

        for(int i=0; i<currentSechParams.Steps; ++i)
        {
            //time [ns]
            outfile << (i+1)*currentSechParams.Stepsize << "\t";
            //Counts
            outfile << (int)(photonCountsDataProcessed[i]) << "\t" ;
            //time value
            outfile << CountBinsTimeLengthProcessed[i] << std::endl;
        }
    }
    outfile.close();

    if (!withRefocus) {
        globalPath = globalPathSave;
    }

    std::cout << "Writing to file finished" << std::endl;
}



void PulsedMeasurementsAndData::readDataFromDatFile(QString fileName)
{

    QFile file(fileName);
    QFileInfo getFilenName_var(fileName);

    //set filname and path for saving the loaded file with the belonging name
    currentDataFromFile.fileNameLoaded = getFilenName_var.baseName().toStdString();
    currentDataFromFile.PathLoaded = getFilenName_var.absolutePath().toStdString();

    // set the type of the measurement to name the belonging axis
    if(fileName.contains("Rabi")){
        typeOfMeasurement = "Rabi";
    }else if(fileName.contains("ODMR")){
        typeOfMeasurement = "ODMR";
    }else if(fileName.contains("Ramsey")){
        typeOfMeasurement = "Ramsey";
    }else if(fileName.contains("SpinEcho")){
        typeOfMeasurement = "SpinEcho";
    }

    emit(ODMRValuesClear());

    if(file.open(QIODevice::ReadOnly)){
        QTextStream DataFromFile(&file);
        int lineCounter = 1;
        QString line;
        std::size_t foundT1 = currentDataFromFile.fileNameLoaded.find("T1");
        while(!DataFromFile.atEnd()){
            line = DataFromFile.readLine();
            if(lineCounter>4){
                QStringList splitLine = line.split("\t");
                emit(ODMRValues(splitLine[0].toDouble(),splitLine[1].toDouble()));
                if (foundT1!=std::string::npos){
                    emit(T1Value((double)(splitLine[2].toDouble())));
                }
            }

            ++lineCounter;
        }
        file.close();
        emit(PlotODMR());
        if (foundT1!=std::string::npos){
            emit(PlotT1());
        }

    }
}

/*
void PulsedMeasurementsAndData::setAIcwODMRParameters(){
    currentODMRParams.loopNb = 100;
    currentODMRParams.excitTime = 1000.;
    currentODMRParams.FreqStart = 2400.;
    currentODMRParams.FreqEnd = 3400.;
    currentODMRParams.stepNb = 501;
}

void PulsedMeasurementsAndData::startAIODMRScan(){
    if(checkClickedButton(0, "prep")){
        return;
    }

    //double excite = ui->excTime->value();
    ViConstString chan = "1";

    //double Fstart = ui->scanStartFreq->value();
    //double Fend = ui->scanStopFreq->value();
    //int stepNb = ui->scanStepNb->value();

    auto frequencies = linearFreqArray(currentODMRParams.FreqStart*MHz_M8195A, currentODMRParams.FreqEnd*MHz_M8195A, currentODMRParams.stepNb-1);

    std::vector<double> times (frequencies.size(), currentODMRParams.excitTime*microsec_M8195A);
    std::vector<double> phases (frequencies.size(), 0);
    std::vector<double> amplitudes (frequencies.size(), 1);

    ViStatus status = M8195A.loadSineArrayWaveformAndSeqTable(chan, frequencies, maxSampleRate, times, currentODMRParams.loopNb, phases, amplitudes);
//    ViStatus status = M8195A.loadSineArrayWaveformAndSeqTable(chan, frequencies, maxSampleRate, times, loops);

    //PulsedMeasurementsAndData_var->currentODMRParams.stepNb=stepNb+1;
    //PulsedMeasurementsAndData_var->currentODMRParams.FreqEnd=Fend;
    //PulsedMeasurementsAndData_var->currentODMRParams.FreqStart=Fstart;
    //PulsedMeasurementsAndData_var->currentODMRParams.excitTime=excite;

//    ui->StartODMR->setEnabled(true);
}*/

