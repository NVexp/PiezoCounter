#ifndef QCPMOVABLESTRAIGHTLINE_H
#define QCPMOVABLESTRAIGHTLINE_H
#include "qcustomplot.h"

class QCustomPlotWithROI;


class QCPMovableStraightLine : public QCPItemStraightLine
{
    Q_OBJECT

public:
    QCPMovableStraightLine(QCustomPlotWithROI* parentPlot);
    //virtual ~QCPMovableStraightLine();

    void mouseMoveEvent(QMouseEvent* event, const QPointF& startPos);
    void mousePressEvent(QMouseEvent* event, const QVariant& details);
    void mouseReleaseEvent(QMouseEvent *event, const QPointF &startPos);

    QCustomPlotWithROI* parentPlot() const {return parentPlotwROI;}

    QCustomPlotWithROI* parentPlotwROI;
    QPointF point1_startOfMove;
    bool flagMoving;

signals:
    void lineMoved();

private:

};

#endif // QCPMOVABLESTRAIGHTLINE_H
