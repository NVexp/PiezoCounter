#ifndef QCPMOVABLETRACER_H
#define QCPMOVABLETRACER_H
#include "qcustomplot.h"
#include "qcustomplotwithroi.h"

class QCPMovableTracer : public QCPItemTracer
{
public:
    QCPMovableTracer(QCustomPlotWithROI* parentPlot);

    bool flagMoving;
    QPointF position_startOfMove;
    QCustomPlotWithROI* parentPlotwROI;

    QCustomPlotWithROI* parentPlot() const {return parentPlotwROI;}

    void mouseMoveEvent(QMouseEvent* event, const QPointF& startPos);
    void mousePressEvent(QMouseEvent* event, const QVariant& details);
    void mouseReleaseEvent(QMouseEvent* event, const QPointF &startPos);
};

#endif // QCPMOVABLETRACER_H
