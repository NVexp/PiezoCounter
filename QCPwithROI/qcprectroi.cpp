#include "qcprectroi.h"
#include "qcustomplotwithroi.h"
#include <iostream>

static const double max_position_piezo = 300000.;
static const double min_position_piezo = 0.;

//need to declare AGAIN and define the static member here
QPointF QCPRectROI::label_offset = QPointF{0,10};

QCPRectROI::QCPRectROI(QCustomPlotWithROI* parentPlot):
    QCPItemRect(parentPlot),
    mParentPlotwROI(parentPlot),
    ROIid(0)
{
    topLeftPos_startOfMove.setX(0);
    topLeftPos_startOfMove.setY(0);
    sizeLabel = new QCPItemText(parentPlot);
    VLine = new QCPItemLine(parentPlot);
    HLine = new QCPItemLine(parentPlot);

}


QCPRectROI::~QCPRectROI()
{
    parentPlot()->removeItem(sizeLabel);
    parentPlot()->removeItem(VLine);
    parentPlot()->removeItem(HLine);
    //std::cout << "ROI destroyed !\n";
}

//event handler for moving around a ROI in a QCustomPlot
void QCPRectROI::mouseMoveEvent(QMouseEvent* event, const QPointF& startPos)
{
    if(selected() && leftButton==true)
    {
        // calculate the position of topLeft anchor with respect to the start point of the moveEvent
        // we could just as well use the bottom right; it is an arbitrary choice

        // the invariant during a move is the vector distance between the mouse and the
        // top left point of the ROI
        // posDifference = posTopLeft@endMove - posMouse@endMove = posTopLeft@startMove - posMouse@startMove
        // hence posTopLeft@endMove = posMouse@endMove + posDifference

        // startPos stays the same for a whole succession of mouse movements as long as the
        // left button is not released ! it does not change at each new mouseMoveEvent

        QPointF topLeft_to_bottomRight = bottomRight->coords() - topLeft->coords();

        //transform startPos as well as event->localPos() into plot coordinates using the link to
        //the parent QCPwithROI, which is the entity that knows about the relationship between widget pixels
        //and axis coordinates
        double eventKey, eventValue = 0;
        mParentPlotwROI->colorMap->pixelsToCoords(event->localPos(), eventKey, eventValue);
        QPointF eventLocalPos(eventKey, eventValue);

        double startPosKey, startPosValue = 0;
        mParentPlotwROI->colorMap->pixelsToCoords(startPos, startPosKey, startPosValue);
        QPointF startPosCoords(startPosKey, startPosValue);

        //calculate new position of top left anchor
        QPointF new_topLeft_pos = eventLocalPos + topLeftPos_startOfMove - startPosCoords;

        //limit the position to physically reachable region
        if(new_topLeft_pos.x() > max_position_piezo)
            new_topLeft_pos.setX(max_position_piezo);
        if(new_topLeft_pos.y() > max_position_piezo)
            new_topLeft_pos.setY(max_position_piezo);
        if(new_topLeft_pos.x() < min_position_piezo)
            new_topLeft_pos.setX(min_position_piezo);
        if(new_topLeft_pos.y() < min_position_piezo)
            new_topLeft_pos.setY(min_position_piezo);

        topLeft->setCoords(new_topLeft_pos);
        bottomRight->setCoords(topLeft->coords() + topLeft_to_bottomRight);

        //also refresh position of the size label
        QPointF sizePos = bottom->pixelPosition() - label_offset;
        sizeLabel->position->setCoords(sizePos);

        emit(rectangleMoved());
    }
    //std::cout << "top left position -> X:" << topLeft->coords().x() << ", Y:" << topLeft->coords().y() << std::endl;
}


// when the left button is pressed, this event handler registers where the
// top left anchor is a the start of the subsequent move
void QCPRectROI::mousePressEvent(QMouseEvent* event, const QVariant& details)
{
    if(selected() && event->button()==Qt::LeftButton)
    {
        leftButton=true;
        topLeftPos_startOfMove.setX(topLeft->coords().x());
        topLeftPos_startOfMove.setY(topLeft->coords().y());
    }
    if(selected() && event->button() == Qt::RightButton){
        leftButton=false;
    }
}

//not very pretty with all the if's but it works quite nicely
//probably possible using bit masks...
void QCPRectROI::sizeChangeFromWheel(QWheelEvent* event, bool vertical, bool bigStep)
{
    int d = event->angleDelta().y();
    QPointF newPtl;
    QPointF newPbr;
    double deltaX = mParentPlotwROI->singleCellStepX();
    double deltaY = mParentPlotwROI->singleCellStepY();

    //double scaleFactor = 0.01;

    if (vertical) // chose vertical size by pressing Ctrl
    {
        if(d>0) //scrolling up
        {
            if(bigStep) //making steps of 20 px, by pressing shift
            {
                QPointF Ybigstep{0,scaleFactor*10.*deltaY};
                newPtl = topLeft->coords() - Ybigstep;
                newPbr = bottomRight->coords() + Ybigstep;
                topLeft->setCoords(newPtl);
                bottomRight->setCoords(newPbr);
            }
            else //making steps of 2 px
            {
                QPointF Ystep{0,scaleFactor*deltaY};
                newPtl = topLeft->coords() - Ystep;
                newPbr = bottomRight->coords() + Ystep;
                topLeft->setCoords(newPtl);
                bottomRight->setCoords(newPbr);
            }
        }
        else if(d<0) //scrolling down
        {
            if(bigStep)
            {
                if(sizeY()>scaleFactor*24.*deltaY) //size has to be large enough to decrease it
                {
                QPointF Ybigstep{0,scaleFactor*10.*deltaY};
                newPtl = topLeft->coords() + Ybigstep;
                newPbr = bottomRight->coords() - Ybigstep;
                topLeft->setCoords(newPtl);
                bottomRight->setCoords(newPbr);
                }
            }
            else
            {
                if(sizeY()>scaleFactor*4.*deltaY) //size has to be large enough to decrease it
                {
                    QPointF Ystep{0,scaleFactor*deltaY};
                    newPtl = topLeft->coords() + Ystep;
                    newPbr = bottomRight->coords() - Ystep;
                    topLeft->setCoords(newPtl);
                    bottomRight->setCoords(newPbr);
                }
            }
        }
    }
    else //horizontal case, same as above
    {
        if(d>0)
        {
            if(bigStep)
            {
                QPointF Xbigstep{scaleFactor*10.*deltaX,0};
                newPtl = topLeft->coords() - Xbigstep;
                newPbr = bottomRight->coords() + Xbigstep;
                topLeft->setCoords(newPtl);
                bottomRight->setCoords(newPbr);
            }
            else
            {
                QPointF Xstep{scaleFactor*deltaX,0};
                newPtl = topLeft->coords() - Xstep;
                newPbr = bottomRight->coords() + Xstep;
                topLeft->setCoords(newPtl);
                bottomRight->setCoords(newPbr);
            }
        }
        else if(d<0)
        {
            if(bigStep)
            {
                if(sizeX()>scaleFactor*24.*deltaX)
                {
                QPointF Xbigstep{scaleFactor*10.*deltaX,0};
                newPtl = topLeft->coords() + Xbigstep;
                newPbr = bottomRight->coords() - Xbigstep;
                topLeft->setCoords(newPtl);
                bottomRight->setCoords(newPbr);
                }
            }
            else
            {
                if(sizeX()>scaleFactor*4.*deltaX)
                {
                    QPointF Xstep{scaleFactor*deltaX,0};
                    newPtl = topLeft->coords() + Xstep;
                    newPbr = bottomRight->coords() - Xstep;
                    topLeft->setCoords(newPtl);
                    bottomRight->setCoords(newPbr);
                }
            }
        }
    }

    //then update the size label above the ROI depending on the scale factor
    std::string sizestr;
    if (scaleFactor == 1){
        int sizeXint = roundToInt(sizeX()/mParentPlotwROI->singleCellStepX());
        int sizeYint = roundToInt(sizeY()/mParentPlotwROI->singleCellStepY());
        std::string sizestr = "ROI " + std::to_string(ROIid) + ": " + std::to_string(sizeXint) + "px, " + std::to_string(sizeYint)+"px";
    }else{
        double sizeXdouble = bottomRight->coords().x() - topLeft->coords().x();
        double sizeYdouble = bottomRight->coords().y() - topLeft->coords().y();
        std::string sizestr = "ROI " + std::to_string(ROIid) + ": " + std::to_string(sizeXdouble*1000) + "nm, " + std::to_string(sizeYdouble*1000)+"nm";
    }
    sizeLabel->setText(QString::fromStdString(sizestr));
    QPointF sizepos = bottom->pixelPosition() - label_offset;
    sizeLabel->position->setCoords(sizepos);
}

