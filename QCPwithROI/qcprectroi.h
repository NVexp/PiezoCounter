#ifndef QCPRECTROI_H
#define QCPRECTROI_H
#include "qcustomplot.h"

//class forward declaration
class QCustomPlotWithROI;

class QCPRectROI : public QCPItemRect
{
    Q_OBJECT

public:
    static QPointF label_offset;

    QCustomPlotWithROI* mParentPlotwROI;
    int ROIid;
    QCPItemText* sizeLabel;
    QCPItemLine* VLine;
    QCPItemLine* HLine;
    bool leftButton = false; //to make a diffrents between Left and Right button clicked for mousemove event

    QCPRectROI(QCustomPlotWithROI* parentPlot);
    virtual ~QCPRectROI();
    QCustomPlotWithROI* parentPlot() const {return mParentPlotwROI;}

    QPointF topLeftPos_startOfMove;
    inline double sizeX(){return (bottomRight->coords().x() - topLeft->coords().x());}
    inline double sizeY(){return (bottomRight->coords().y() - topLeft->coords().y());}

    void mousePressEvent(QMouseEvent* event, const QVariant& details);
    void mouseMoveEvent(QMouseEvent* event, const QPointF& startPos);

    // factor that scales the wheel zoom effect
    double scaleFactor = 0.01;

signals:
    void rectangleMoved();
    void rectangleSizeChange();

public slots:
    void sizeChangeFromWheel(QWheelEvent* event, bool vertical, bool bigStep);


};

#endif // QCPRECTROI_H
