#include "qcpmovablestraightline.h"
#include <iostream>
#include "qcustomplotwithroi.h"


QCPMovableStraightLine::QCPMovableStraightLine(QCustomPlotWithROI* parentPlot):
    QCPItemStraightLine(parentPlot),
    parentPlotwROI(parentPlot),
    flagMoving(false)
{
    point1_startOfMove.setX(0);
    point1_startOfMove.setY(0);

}

//QCPMovableStraightLine::~QCPMovableStraightLine()
//{
//}


//same principle as the corresponding event h
void QCPMovableStraightLine::mouseMoveEvent(QMouseEvent* event, const QPointF& startPos)
{
    if (flagMoving)
    {
        QPointF diff_P1ToP2_startOfMove = point2->coords() - point1->coords();

        //transform startPos as well as event->localPos() into plot coordinates using the link to
        //the parent QCPwithROI, which is the entity that knows about the relationship between widget pixels
        //and axis coordinates
        // startPos stays the same for a whole succession of mouse movements as long as the
        // left button is not released ! it does not change at each new mouseMoveEvent
        double eventKey, eventValue = 0;
        parentPlotwROI->colorMap->pixelsToCoords(event->localPos(), eventKey, eventValue);
        QPointF eventLocalPos(eventKey, eventValue);
        double startPosKey, startPosValue = 0;
        parentPlotwROI->colorMap->pixelsToCoords(startPos, startPosKey, startPosValue);
        QPointF startPosCoords(startPosKey, startPosValue);


        // then calculate the position of point1 anchor with respect to the start point of the moveEvent
        // we could just as well use point2, it is an arbitrary choice

        // the invariant during a move is, depending on the orientation of axis, the X or Y distance between
        // the mouse and the point1 anchor of the QCPStraightLine
        // here we keep the vector distance constant such that we do not have
        // can use the same code for both horizontal and vertical lines
        // posDifference = posPoint1@endMove - posMouse@endMove = posPoint1@startMove - posMouse@startMove
        // hence posPoint1@endMove = posMouse@endMove + posDifference

        QPointF coordsDifference_moveStartToPoint1Begin = point1_startOfMove - startPosCoords;
        QPointF point1_new = parentPlot()->getNextPixelCoords(eventLocalPos + coordsDifference_moveStartToPoint1Begin);

        //we use singleCellStep to move the lines 1 whole pixel at a time and keep the lines in the middle of pixels
        double Xstep = parentPlot()->singleCellStepX();
        double Ystep = parentPlot()->singleCellStepY();
        //only update if the new point is within the range of the image
        if (point1_new.x()<parentPlot()->colorMap->data()->keyRange().upper+Xstep && point1_new.x()>parentPlot()->colorMap->data()->keyRange().lower-Xstep
                && point1_new.y()<parentPlot()->colorMap->data()->valueRange().upper+Ystep && point1_new.y()>parentPlot()->colorMap->data()->valueRange().lower-Ystep)
        {
            point1->setCoords(point1_new);
        }

        //set point2 at the same distance relative to point1 as at the start
        QPointF point2_new = parentPlot()->getNextPixelCoords(point1->coords() + diff_P1ToP2_startOfMove);
        point2->setCoords(point2_new);
    }
    emit(lineMoved());
}

// when the left button is pressed, this event handler registers where the
// top point1 is at the start of the subsequent move
void QCPMovableStraightLine::mousePressEvent(QMouseEvent* event, const QVariant& details)
{
    if(event->button()==Qt::LeftButton)
    {
        /*
        std::cout << "mouse press for movable line";
        bool verticalPressed = (point1->coords().x() == point2->coords().x());
        bool horizontalPressed = (point1->coords().y() == point2->coords().y());

        if (verticalPressed)
            std::cout << " vertical" << std::endl;
        else if (horizontalPressed)
            std::cout << " horizontal" << std::endl;
        */

        point1_startOfMove.setX(point1->coords().x());
        point1_startOfMove.setY(point1->coords().y());
        //set the flag indicating that the line is moving or ready to move
        flagMoving = true;

    }

}

void QCPMovableStraightLine::mouseReleaseEvent(QMouseEvent *event, const QPointF &startPos)
{
    if(event->button()==Qt::LeftButton)
        flagMoving = false;
}
