#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "include/PI_GCS2_DLL.h"
#include "include/Iterators.h"
#include "include/TimeTagger.h"
#include <filesystem>
#include "qcustomplot.h"
#include "m8195adev.h"
#include "m2icard.h"
#include "pulsedmeasurementsanddata.h"
#include "arrayPreparationsForPulsedMeasurements.h"
#include "magnetstage/magnetcontrolclass.h"
#include "magnetstage/visualization3dplot.h"
//#include "alglib/fittingroutines.h"
#include "Sora/cnn.h"
#include "Sora/control.h"
#include "Sora/sora.h"

// ----- include standard driver header from library -----
#include "c_header/dlltyp.h"
#include "c_header/regs.h"
#include "c_header/spcerr.h"
#include "c_header/spcm_drv.h"

// ----- include of common example librarys -----
#include "common/spcm_lib_card.h"
#include "common/spcm_lib_data.h"

// ----- operating system dependent functions for thread, event, keyboard and mutex handling -----
#include "common/ostools/spcm_oswrap.h"
#include "common/ostools/spcm_ostools.h"

// ----- standard c include files -----
#include <stdio.h>
#include <stdlib.h>
#include <direct.h>

#include "QCPwithROI/qcpsequencedata.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE


//examines what edges there are in successive 64bit chunks, makes a corresponding
//64bit word per channel and parallelize them into the output buffer

class ScanAndCount;
class LoadFileCreatMosaic;
class QSeqEditor;
class M2iCard;


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


    std::vector<QSeqEditor*> seqEditors_in_GUI;
    std::vector<QCheckBox*> constStates_in_GUI;

    //by default the ui pointer is made private but we don't care about that. Private property is stupid ! Hail Karl Marx !
    Ui::MainWindow* ui;

    ScanAndCount* ScanAndCount_var;
    M2iCard* M2i7005_var;
    M8195Adev* M8195A_var;

    QCPColorScale* colorScale3Dplot;
    QCPColorScale* colorScale2Dplot;
    LoadFileCreatMosaic* LoadFileCreatMosaic_var;
    PulsedMeasurementsAndData* PulsedMeasurementsAndData_var;
    arrayPreparationsForPulsedMeasurements* ArrayPrepForPulsedMeasurements_var;

    //fittingroutines* fittingroutines_var;

    CNN *cnnwoBias_var;
    Control *control_var;
    Sora *sora_var;


    //for error message handling from E-725 piezo controller
    char szErrorMessage[1024];
    int	iError;
    char bufferDataRec[600];

    bool GeneratorOn;
    std::filesystem::path globalPath;
    std::filesystem::path globalPathSave; //to save the global path (main folder) if using subfolders as global path

    //maybe we don't need these 2 QVectors because this data is already contained in the colorMap one way or another
    //but it is simpler to just put it back there
    QVector<double> keyVec;
    QVector<double> valueVec;

    //Scan visualization function
    void fillKeyVec();
    void fillValueVec();
    void enableCrosshair();
    void disableCrosshair();
    QVector<double> getHData();
    QVector<double> getVData();

    //Frequency Tab
    QVector<double> ODMRXValues;
    QVector<double> ODMRYValues;
    QVector<double> normedODMRYValues, fitODMRYValues;
    QVector<double> ODMRFFTXValues, ODMRFFTYValues;
    QVector<double> T1YValues;
    QVector<double> GraphXValues;
    QVector<double> GraphYValues;

    /*Vectors for kind of prepared and started measurments:
     * 0: ODMR
     * 1: Rabi
     * 2: Pulsed ODMR
     * 3: Ramsey
     * 4: Spin-Echo
     * 5: CompPulsDetun
     * 6: piOver2Pulses
     */
    std::vector<int> MeasurementVectorInt={0, 0, 0, 0, 0, 0, 0, 0};
    std::vector<QString> MeasurementVectorString={"ODMR", "Rabi", "pulsed ODMR", "Ramsey", "spin-echo", "CompPulsDetun", "piOver2Pulses", "Sech-Pulse-Rabi"};
    bool checkClickedButton(int index, std::string MeasurementType);

    QCustomPlot* PlotForODMR;
    int RabiTime0;
    //void CreateCW_ODMRSequence(std::vector< std::vector<int>>& risingEdges,  std::vector< std::vector<int>>& fallingEdges);
    int CreateRabiSequence(std::vector< std::vector<int>>& risingEdges,  std::vector< std::vector<int>>& fallingEdges);
    void CreateRabiFrequencies(double Freq, int stepNb, int Stepsize, int loops);


    //void* generateSeqData(std::vector<void*> edges);
    void translateTimestampsToParallelAndPutInBuffer(int seqLength, int chanNb, std::vector< std::vector<int> >& timestamps_rise, std::vector< std::vector<int> >& timestamps_fall, int16_t* pBuffer);
    bool writeSeqToFile(int nbActChan, int length, double tStep_in_ns, const std::vector< std::vector<edgeProp> >& risingE_Vec, const std::vector< std::vector<edgeProp> >& fallingE_Vec, const std::string& path, const std::string& name);
    bool loadSeqFromFile(std::filesystem::path filePath, std::string filename, std::vector< std::vector<edgeProp> >& risingE_outVec, std::vector< std::vector<edgeProp> >& fallingE_outVec);

signals:
    void frequencyMeasurementFinished(QString typeOfMeasurement);
    // signals for Sora
    void stateAIMagnet(bool state);
    void StateAIODMRUpload(bool state);
    void StateAIcwODMR(bool state);

public slots:
    bool closedLoopCheck();
    void checkForLaser();
    //std::vector<double> getRecordedData(int length);
    void WavGenTableLineFill(int lineNbInTable, int pxNumber, int stepSize, QString st);
    void goToPosition(int xPos, int yPos, int zPos);

    // drop down menu for the ODMR plot
    void ODMRContextMenuRequest(QPoint pos);
    void dropDownMeasureValues_ODMR();
    void dropDownNormedMeasureValues_ODMR();
    void dropDownFitValues_ODMR();
    void dropDownFFTMeasureValues_ODMR();

private slots:
    void on_readValues_clicked();
    void on_STOPButton_clicked();
    void on_loopX_stateChanged(int st);
    void on_loopY_stateChanged(int st);
    void on_loopZ_stateChanged(int st);
    void on_setValX_editingFinished();
    void on_stepX_valueChanged(int step);
    void on_setValY_editingFinished();
    void on_stepY_valueChanged(int step);
    void on_setValZ_editingFinished();
    void on_stepZ_valueChanged(int step);
    void on_TwoDScanSetup_clicked();
    void TwoDScanSetup(int Lx, int Ly, int Xstep, int Ystep, int IDAxis1, int IDAxis2, QString ScanAxis);
    void on_TwoDScanStart_clicked();
    void TwoDScanStart();

    // Check/Create folder at the start of the program
    void doesPathExist_ifNotCreate();
    void on_ThreeDScanStart_clicked();
    void on_MosaicScanStart_clicked();
    void on_Bleaching_clicked();

    //Scan visulaization functions
    void moveToPosition(double xPos, double yPos);
    void on_setROILegend_clicked(bool checked);
    void on_addROI_clicked();
    void on_removeROI_clicked();
    void replotAlongHLine();
    void replotAlongVLine();
    void plotAfterDataAcq();
    void on_crossHairBox_stateChanged(int newState);
    void on_goToRoiButton_clicked();
    void on_resetAxes_clicked();
    void on_setMaxPlot_clicked();
    void on_setMaxPlotAuto_clicked();
    void on_PDFsave_clicked();
    void on_PNGsave_clicked();
    void on_LoadDATFile_clicked();
    void on_WaveTableRateButton_clicked();
    void on_LoadDatFile3DScan_clicked();
    void on_setMaxPlot_3D_clicked();
    void on_setMaxPlot_3D_auto_clicked();
    void on_PNGsave3D_clicked();
    void on_PDFsave3D_clicked();
    void on_SetPath_clicked();
    void plot2DData(int Lx, int NumbersOfPixels);

    // Frequency Tab
    void on_STOPbutton_clicked();
    void on_RUNbutton_clicked();
    void startFrequencySequence(QString typeOfMeasurement);
    void frequencyUploadDone(QString typeOfMeasurement);
    void frequencyMeasurementDone(QString typeOfMeasurement);
    void on_loadWaveformAndSeqTable_clicked();
    void loadWaveformAndSeqTable(int loopNb, double excitTime, double freqStart, double freqEnd, int stepNb);
    void on_triggerMode_currentIndexChanged(const QString &arg1);
    //void prepareWavegenForODMR();
    void on_SingleLorentzianFit_clicked();
    void on_DoubleLorentzianFit_clicked();
    void on_DampedSinusFit_clicked();
    void on_saveFitParametersODMRPlot_clicked();

    void on_PrepareRabi_clicked();
    void PrepareRabi(double freq, int MWsteps, double MWstepsize, double timeCounter, double timeLaser, int loops, double rabi0, double amp, int withRefocus);
    void on_StartODMR_clicked();
    void on_ReadDataODMR_clicked();
    void on_StartRabi_clicked();
    void on_ReadDataRabi_clicked();

    void on_pulsedODMRPrepare_clicked();
    void pulsedODMRPrepare(double freqStart, double freqEnd, int nbSteps, double piPulse, int timeLaser, int timeCounter, int loops, int withRefocus);
    void on_pulsedODMRStart_clicked();
    void on_pulsedODMRReadData_clicked();
    int CreatePulsedODMRSequence(std::vector< std::vector<int>>& risingEdges,  std::vector< std::vector<int>>& fallingEdges);
    void CreatePulsedODMRFrequencies();

    void on_ramseyButtonPrepare_clicked();
    void on_ramseyButtonStart_clicked();
    void on_ramseyButtonRead_clicked();
    int CreateRamseySequence(std::vector< std::vector<int>>& risingEdges,  std::vector< std::vector<int>>& fallingEdges);
    void CreateRamseyFrequencies(double Freq, int stepNb, int loops, double PiOver2time);
    int CreateRamseySequenceSmall(std::vector< std::vector<int>>& risingEdges,  std::vector< std::vector<int>>& fallingEdges);
    void CreateRamseyFrequenciesSmall(double Freq, int stepNb, int loops, double PiOver2time, double stepSize);

    void on_spinEchoButtonPrepare_clicked();
    void on_spinEchoButtonStart_clicked();
    void on_spinEchoButtonRead_clicked();
    int CreateSpinEchoSequence(std::vector< std::vector<int>>& risingEdges,  std::vector< std::vector<int>>& fallingEdges);
    void CreateSpinEchoFrequencies(double Freq, int stepNb, int loops, double PiOver2time, double PiTime);
    int CreateSpinEchoSequenceSmall(std::vector< std::vector<int>>& risingEdges,  std::vector< std::vector<int>>& fallingEdges);
    void CreateSpinEchoFrequenciesSmall(double Freq, int stepNb, int loops, double PiOver2time, double PiTime, double stepSize);

    void on_PrepareT1wPI_clicked();
    void T1Prepare( double Freq, int nbSteps, int stepSize, double piPulse, int timeLaser, int timeCounter, int loops);
    void on_StartT1wPI_clicked();
    void on_ReadDataT1wPI_clicked();
    void CreateT1wPIFrequencies(double Freq, int stepNb, int Stepsize, int loops);
    int CreateT1Sequence(std::vector< std::vector<int>>& risingEdges,  std::vector< std::vector<int>>& fallingEdges);

    /*
    void on_PrepareT1_clicked();
    void on_StartT1_clicked();
    void on_ReadDataT1_clicked();
    */

    void on_LoadWagenData_clicked();
    void on_WavegenPNGsave_clicked();
    void on_WavegenPDFsave_clicked();
    
    void PostionForPulsedMeasurments();

    // Compusite Pulses Tab
    void CreateCompTest(double Freq, int stepNb, int loops);
    void compositePulsesReadAndFill(QString fileName);
    void on_LoadCompPulses_clicked();

    int CreateCompusitePulseSequence(std::vector< std::vector<int>>& risingEdges,  std::vector< std::vector<int>>& fallingEdges);
    void CreateCompPulsDetunFrequencies(double Freq, int stepNb, double Stepsize, int loops);
    bool CreateCompusitePulsePi2Sequence(std::vector< std::vector<int>>& risingEdges,  std::vector< std::vector<int>>& fallingEdges);
    void CreateCompPulsPi2SequenceFrequencies(double Freq, int stepNb, double Stepsize, int loops);

    void pulsedMeasurementWithRefocus(std::string pathName);

    // Slots for polting signals from PulseMeasrumentsAndData and Fittingroutines
    void clearODMRValues();
    void writeODMRValues(double xValue, double yValue);
    void writeT1Value(double yValue);
    void plotODMRData();
    void plotT1Data();
    void clearGraphData();
    void writeGraphData(double xValue, double yValue);
    void PlotGraphData();

    // Pulsed Tab
    void on_initM2i_clicked();
    void on_confM2i_clicked();
    void on_loadData_clicked();
    void on_outputM2i_clicked();
    void on_closeM2i_clicked();
    void on_setPermWord_clicked();
    void on_writeFileTestButton_clicked();
    void on_parseButton0_clicked();
    void on_parseButton1_clicked();
    void on_parseButton2_clicked();
    void on_parseButton3_clicked();
    void on_parseButton4_clicked();
    void on_parseButton5_clicked();
    void on_parseButton6_clicked();
    void on_parseButton7_clicked();
    void on_loadFileTestButton_clicked();
    void on_changeSeqParams_clicked();
    void on_getCountsPerSec_clicked();

    // Magnetstage Tab
    void EnableDisableButtonsMagnet(bool state);
    void on_MoveButtonMagnet_clicked();

    void moveMagnetStage(double distance_mm, double angleSmall_deg, double angleBig_deg);
    void updateMagnetTab();
    void on_ConnectMagnetstage_clicked();
    void on_DisconnectMagnetstage_clicked();
    void on_magFieldStrength_editingFinished();
    void on_distanceDiamondMagnet_editingFinished();
    void on_StopButtonMagnet_clicked();
    void on_settingsMagnetstage_clicked();
    void on_simultanMovementMagnetstage_clicked(bool checked);
    void on_instantanMovementMagnetstage_clicked(bool checked);
    void on_angleSmallMotor_editingFinished();
    void on_angleBigMotor_editingFinished();
    void on_visualization2DPlot_clicked();
    void on_ResetMagnetstage_clicked();
    void on_bordersMagnetestage_clicked();

    // Sora
    void refocus();
    void on_openSoraButton_clicked();
    void AI_moveMagnetStage(double smallMotor, double bigMotor, double distanceMotor);

    void on_predictNVCenter_clicked();
    void setAIODMRParameters(int loopNb, double excitTime, double freqStart, double freqEnd, int stepNb);
    void uploadAIODMRParameters(int loopNb, double excitTime, double freqStart, double freqEnd, int stepNb);
    void startAIcwODMR(int loop_nb);
    void startCwODMR(int loop_nb);


    void on_pushButton_clicked();

    void on_LoadCompPulsData_clicked();

    void on_compPulsPrep_clicked();

    void on_compPulsStart_clicked();

    void on_compPulsRead_clicked();

    void on_pi2SequencePrepare_clicked();

    void on_pi2SequenceStart_clicked();

    void on_pi2SequenceRead_clicked();




    void on_sechPrep_clicked();
    void on_sechStart_clicked();
    void on_sechRead_clicked();
    void CreateSechFrequencies(double Freq, int stepNb, double Stepsize, int loops);
    bool CreateSechSequence(std::vector< std::vector<int>>& risingEdges,  std::vector< std::vector<int>>& fallingEdges);

    void on_CompPulsPNGsave_clicked();

    void on_CompPulsPDFsave_clicked();


    //Test for Redout
    void on_pushButton_3_clicked();
    void on_pushButton_2_clicked();
    void on_pushButton_5_clicked();
    bool MainWindow::CreateTestReadoutSequence(std::vector< std::vector<int>>& risingEdges,  std::vector< std::vector<int>>& fallingEdges);
    void MainWindow::CreateTestReadoutFrequencies(double Freq, int loops, double PiTime);

private:
    // Magnetstage Tab
    magnetcontrolclass *magnet;

    // Timer: update the UI as long the movement isn't done
    //QTimer *updateMagneticUIMovement;
    QTimer *updateMagnetStageUI;

    //pointer is either ODMRPlot or compPlot (object oriented)
    QCustomPlot *curDataWidget;


    QVector<QVector<double>> compPulsesForTable;

    // Sora
    QVector<double> predictNVCenter(QVector<double> inputData, int nb_pixelX, int nb_pixelY, int resolution, int posX, int posY);

};
#endif // MAINWINDOW_H
