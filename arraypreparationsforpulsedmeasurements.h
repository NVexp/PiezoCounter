#ifndef ARRAYPREPARATIONSFORPULSEDMEASUREMENTS_H
#define ARRAYPREPARATIONSFORPULSEDMEASUREMENTS_H

#include <QObject>

#include "m8195adev.h"
#include "m2icard.h"

class arrayPreparationsForPulsedMeasurements : public QObject
{
    Q_OBJECT
public:
    arrayPreparationsForPulsedMeasurements();

    //M8195Adev *M8195A_var;
    //M2iCard *M2i7005_var;

    // cw ODMR
    void prepareCwODMR(int loopNb, double excitTime, double freqStart, double freqEnd, int stepNb, M2iCard* M2i7005_var, M8195Adev* M8195A_var);
    void createCwODMRSequence(std::vector<std::vector<int>> &risingEdges,  std::vector<std::vector<int>> &fallingEdges, int stepNb, double excitTime);
    void createCwODMRFrequencies(int loopNb, double excitTime, double freqStart, double freqEnd, int stepNb, M8195Adev* M8195A_var);

    // Rabi
    int prepareRabi(double freq, int MWsteps, double MWstepsize, double timeCounter, double timeLaser, int loobs, double rabi0, double amp, M2iCard* M2i7005_var, M8195Adev* M8195A_var);
    int createRabiSequence(std::vector<std::vector<int>>& risingEdges, std::vector<std::vector<int>>& fallingEdges, int MWsteps, double MWstepsize, double timeCounter, double timeLaser, double rabi0);
    void createRabiFrequencies(double freq, int MWstep, int MWstepsize, int loops, double amp, M8195Adev* M8195A_var);

    // pulsed ODMR
    int preparePulsedODMR(double freqStart, double freqEnd, int nbSteps, double piPulse, int timeLaser, int timeCounter, int loops, M2iCard* M2i7005_var, M8195Adev* M8195A_var);
    int createPulsedODMRSequence(std::vector<std::vector<int>>& risingEdges, std::vector<std::vector<int>>& fallingEdges, int timeLaser, int nbSteps, int timeCounter, int pulsedODMRTime, double piPulse);
    void createPulsedODMRFrequencies(double freqStart, double freqEnd, int nbSteps, double piPulse, int loops, M8195Adev* M8195A_var);

    // Ramsey


    // Spin Echo


    // T1
    int prepareT1(double Freq, int nbSteps, double stepSize, double piPulse, double timeCounter, double timeLaser, int loops, M2iCard* M2i7005_var);
    int createT1Sequence(std::vector< std::vector<int>>& risingEdges,  std::vector< std::vector<int>>& fallingEdges, int timeLaser, int nbSteps, int stepSize, int timeCounter, double piPulse);
    void createT1Frequencies(double Freq, int nbSteps, double piPulse, int loops, M8195Adev* M8195A_var);

signals:
    void uploadIsDone(QString measureType);
};

#endif // ARRAYPREPARATIONSFORPULSEDMEASUREMENTS_H
