#ifndef VISUALIZATION2DPLOT_H
#define VISUALIZATION2DPLOT_H

#include <QWidget>

#include "qcustomplot.h"

namespace Ui {
class visualization2dplot;
}

class visualization2dplot : public QWidget
{
    Q_OBJECT

public:
    explicit visualization2dplot(QWidget *parent = nullptr);
    ~visualization2dplot();

    void smallRotStagePlot(double posSmallRotStage, double posTransStage);
    void bigRotStagePlot(double posBigRotStage, double posTransStage);

private slots:


    void on_autoscaleSmallRotStage_clicked(bool checked);

    void on_autoscaleBigRotStage_clicked(bool checked);

private:
    Ui::visualization2dplot *ui;

    bool autoscaleSmallRotStage = false;
    bool autoscaleBigRotStage = false;

    double positionTransStage;
};

#endif // VISUALIZATION2DPLOT_H
