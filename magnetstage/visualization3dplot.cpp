#include "visualization3dplot.h"
#include "ui_visualization3dplot.h"

#include <iostream>
#include <QtDataVisualization/Q3DSurface>
#include <QtDataVisualization/QCustom3DItem>
#include <QtMath>

#include <QTimer>

visualization3DPlot::visualization3DPlot(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::visualization3DPlot)
{
    ui->setupUi(this);

    // - 3D Plot:
    plot3DGraph = new QtDataVisualization::Q3DSurface();
    magneticSphere = new QtDataVisualization::QCustom3DItem();
    magneticFieldArrow = new QtDataVisualization::QCustom3DItem();
    coordinateSystemX = new QtDataVisualization::QCustom3DItem();
    coordinateSystemY = new QtDataVisualization::QCustom3DItem();
    coordinateSystemZ = new QtDataVisualization::QCustom3DItem();

    //NV-axis
    NVAxis1 = new QtDataVisualization::QCustom3DItem();
    NVAxis2 = new QtDataVisualization::QCustom3DItem();
    NVAxis3 = new QtDataVisualization::QCustom3DItem();
    NVAxis4 = new QtDataVisualization::QCustom3DItem();


    //creates a widget in the layout assign it to the 3D plot
    QWidget *plot = QWidget::createWindowContainer(plot3DGraph);
    ui->Visualization3D->addWidget(plot);
    plot3DGraph->setFlags(plot3DGraph->flags()^Qt::FramelessWindowHint);

    //Create the three axis (range/ name /visibility)
    plot3DGraph->setAxisX(new QtDataVisualization::QValue3DAxis);
    plot3DGraph->setAxisY(new QtDataVisualization::QValue3DAxis);
    plot3DGraph->setAxisZ(new QtDataVisualization::QValue3DAxis);

    plot3DGraph->axisX()->setRange(-60,60);
    plot3DGraph->axisY()->setRange(-20,60);
    plot3DGraph->axisZ()->setRange(-60,60);

    plot3DGraph->axisX()->setTitle("x-Achse");
    plot3DGraph->axisX()->setTitleVisible(true);
    plot3DGraph->axisY()->setTitle("y-Achse");
    plot3DGraph->axisY()->setTitleVisible(true);
    plot3DGraph->axisZ()->setTitle("z-Achse");
    plot3DGraph->axisZ()->setTitleVisible(true);

    //coordinate system arrow (position of the diamond)
    coordinateSystemX->setPosition(QVector3D(0.0f,0.0f,7.2f));
    coordinateSystemX->setScaling(QVector3D(0.12f, 0.25f, 0.12f));
    QQuaternion x_axisRotation = QQuaternion::fromAxisAndAngle(1.0f,0.0f,0.0f,270);
    coordinateSystemX->setRotation(x_axisRotation);
    coordinateSystemX->setMeshFile(QStringLiteral("C:/Users/DiamondExp/Documents/Qt projects/PiezoScanPlusCounter/magnetstage/models/narrowarrow_koord.obj"));
    QImage x_axisColor = QImage(2, 2, QImage::Format_RGB32);
    x_axisColor.fill(Qt::red);
    coordinateSystemX->setTextureImage(x_axisColor);

    coordinateSystemY->setPosition(QVector3D(0.0f,9.0f,0.0f));
    coordinateSystemY->setScaling(QVector3D(0.12f, 0.25f, 0.12f));
    QQuaternion y_axisRotation = QQuaternion::fromAxisAndAngle(0.0f,1.0f,0.0f,270);
    coordinateSystemY->setRotation(y_axisRotation);
    coordinateSystemY->setMeshFile(QStringLiteral("C:/Users/DiamondExp/Documents/Qt projects/PiezoScanPlusCounter/magnetstage/models/narrowarrow_koord.obj"));
    QImage y_axisColor = QImage(2, 2, QImage::Format_RGB32);
    y_axisColor.fill(Qt::green);
    coordinateSystemY->setTextureImage(y_axisColor);

    coordinateSystemZ->setPosition(QVector3D(7.2f,0.0f,0.0f));
    coordinateSystemZ->setScaling(QVector3D(0.12f, 0.25f, 0.12f));
    QQuaternion z_axisRotation = QQuaternion::fromAxisAndAngle(0.0f,0.0f,1.0f,270);
    coordinateSystemZ->setRotation(z_axisRotation);
    coordinateSystemZ->setMeshFile(QStringLiteral("C:/Users/DiamondExp/Documents/Qt projects/PiezoScanPlusCounter/magnetstage/models/narrowarrow_koord.obj"));
    QImage z_axisColor = QImage(2, 2, QImage::Format_RGB32);
    z_axisColor.fill(Qt::blue);
    coordinateSystemZ->setTextureImage(z_axisColor);


    plot3DGraph->addCustomItem(coordinateSystemX);
    plot3DGraph->addCustomItem(coordinateSystemY);
    plot3DGraph->addCustomItem(coordinateSystemZ);


    //NV-axis
    NVAxis1->setPosition(QVector3D(0,0,0));
    NVAxis1->setScaling(QVector3D(0.15f, 1.2f, 0.15f));
    QQuaternion NV1rotZ = QQuaternion::fromAxisAndAngle(0.0f,0.0f,1.0f,55);
    NVAxis1->setRotation(NV1rotZ);
    NVAxis1->setMeshFile(QStringLiteral("C:/Users/DiamondExp/Documents/Qt projects/PiezoScanPlusCounter/magnetstage/models/narrowarrow_koord.obj"));
    QImage NVAxis1Color = QImage(2, 2, QImage::Format_RGB32);
    NVAxis1Color.fill(Qt::darkBlue);
    NVAxis1->setTextureImage(NVAxis1Color);

    NVAxis2->setPosition(QVector3D(0,0,0));
    NVAxis2->setScaling(QVector3D(0.15f, 1.2f, 0.15f));
    QQuaternion NV2rotZ = QQuaternion::fromAxisAndAngle(0.0f,0.0f,-1.0f,55);
    NVAxis2->setRotation(NV2rotZ);
    NVAxis2->setMeshFile(QStringLiteral("C:/Users/DiamondExp/Documents/Qt projects/PiezoScanPlusCounter/magnetstage/models/narrowarrow_koord.obj"));
    QImage NVAxis2Color = QImage(2, 2, QImage::Format_RGB32);
    NVAxis2Color.fill(Qt::darkGreen);
    NVAxis2->setTextureImage(NVAxis2Color);

    NVAxis3->setPosition(QVector3D(0,0,0));
    NVAxis3->setScaling(QVector3D(0.15f, 1.2f, 0.15f));
    QQuaternion NV3rotX = QQuaternion::fromAxisAndAngle(1.0f,0.0f,0.0f,125);
    NVAxis3->setRotation(NV3rotX);
    NVAxis3->setMeshFile(QStringLiteral("C:/Users/DiamondExp/Documents/Qt projects/PiezoScanPlusCounter/magnetstage/models/narrowarrow_koord.obj"));
    QImage NVAxis3Color = QImage(2, 2, QImage::Format_RGB32);
    NVAxis3Color.fill(Qt::darkRed);
    NVAxis3->setTextureImage(NVAxis3Color);

    NVAxis4->setPosition(QVector3D(0,0,0));
    NVAxis4->setScaling(QVector3D(0.15f, 1.2f, 0.15f));
    QQuaternion NV4rotX = QQuaternion::fromAxisAndAngle(-1.0f,0.0f,0.0f,125);
    NVAxis4->setRotation(NV4rotX);
    NVAxis4->setMeshFile(QStringLiteral("C:/Users/DiamondExp/Documents/Qt projects/PiezoScanPlusCounter/magnetstage/models/narrowarrow_koord.obj"));
    QImage NVAxis4Color = QImage(2, 2, QImage::Format_RGB32);
    NVAxis4Color.fill(Qt::magenta);
    NVAxis4->setTextureImage(NVAxis4Color);

    plot3DGraph->addCustomItem(NVAxis1);
    plot3DGraph->addCustomItem(NVAxis2);
    plot3DGraph->addCustomItem(NVAxis3);
    plot3DGraph->addCustomItem(NVAxis4);

    NVAxis1->setVisible(!NVAxis1->isVisible());
    NVAxis2->setVisible(!NVAxis2->isVisible());
    NVAxis3->setVisible(!NVAxis3->isVisible());
    NVAxis4->setVisible(!NVAxis4->isVisible());
}

visualization3DPlot::~visualization3DPlot()
{
    delete ui;
}

void visualization3DPlot::Magnetic3DPlot(double move2posTransStage_mm, double move2posRotStageSmall_deg, double move2posRotStageBig_deg)
{
    //calculation of the current position of the sphere
    //posX = (move2posTransStage_mm + 5) * sin(qDegreesToRadians(move2posRotStageSmall_deg+0.));
    //posY = -(move2posTransStage_mm + 5) * cos(qDegreesToRadians(move2posRotStageSmall_deg+0.)) * cos(qDegreesToRadians(0.+move2posRotStageBig_deg));
    //posZ = (move2posTransStage_mm + 5) * cos(qDegreesToRadians(move2posRotStageSmall_deg+0.)) * sin(qDegreesToRadians(0.+move2posRotStageBig_deg));
    //posX = (move2posTransStage_mm) * sin(qDegreesToRadians(move2posRotStageSmall_deg)) * sin(qDegreesToRadians(0.+move2posRotStageBig_deg));
    //posY = (move2posTransStage_mm) * cos(qDegreesToRadians(move2posRotStageSmall_deg));
    //posZ = (move2posTransStage_mm) * cos(qDegreesToRadians(move2posRotStageSmall_deg)) * sin(qDegreesToRadians(0.+move2posRotStageBig_deg));

    posX = (move2posTransStage_mm) * sin(qDegreesToRadians(move2posRotStageSmall_deg+0.));
    posY = (move2posTransStage_mm) * cos(qDegreesToRadians(move2posRotStageSmall_deg+0.)) * cos(qDegreesToRadians(0.+move2posRotStageBig_deg))+5;
    posZ = (move2posTransStage_mm) * cos(qDegreesToRadians(move2posRotStageSmall_deg+0.)) * sin(qDegreesToRadians(0.+move2posRotStageBig_deg));

    //Create the sphere and the arrow
    //This Code includs the magnetic sphere
    //magneticSphere->setPosition(QVector3D(10.0f,20.f,15.0f));
    magneticSphere->setPosition(QVector3D(posX,posY,posZ));
    magneticSphere->setScaling(QVector3D(0.04f, 0.04f, 0.04f));
    magneticSphere->setMeshFile(QStringLiteral("C:/Users/DiamondExp/Documents/Qt projects/PiezoScanPlusCounter/magnetstage/models/largesphere.obj"));
    QImage sunColor = QImage(2, 2, QImage::Format_RGB32);
    sunColor.fill(Qt::blue);
    magneticSphere->setTextureImage(sunColor);


    //This Code includs the magnetic field arrow
    magneticFieldArrow->setPosition(QVector3D(posX,posY,posZ));
    magneticFieldArrow->setScaling(QVector3D(0.15f, 0.7f, 0.15f));

    //get the current position of the polar and azimuthal angle and positioned the arrow
    QQuaternion polarRotation = QQuaternion::fromAxisAndAngle(0.0f ,0.0f ,-1.0f , move2posRotStageSmall_deg);
    QQuaternion azimuthalRotation = QQuaternion::fromAxisAndAngle(-1.0f,0.0f,0.0f,move2posRotStageBig_deg);
    QQuaternion totalRotation = azimuthalRotation * polarRotation;
    magneticFieldArrow->setRotation(totalRotation);

    magneticFieldArrow->setMeshFile(QStringLiteral("C:/Users/DiamondExp/Documents/Qt projects/PiezoScanPlusCounter/magnetstage/models/narrowarrow.obj"));
    QImage arrowColor = QImage(2, 2, QImage::Format_RGB32);
    arrowColor.fill(Qt::black);
    magneticFieldArrow->setTextureImage(arrowColor);


    //show the sphere and the arrow
    plot3DGraph->addCustomItem(magneticSphere);
    plot3DGraph->addCustomItem(magneticFieldArrow);
}

void visualization3DPlot::on_magneticSphere_clicked()
{
    magneticSphere->setVisible(!magneticSphere->isVisible());
}

void visualization3DPlot::on_magneticFieldArrow_clicked()
{
    magneticFieldArrow->setVisible(!magneticFieldArrow->isVisible());
}

void visualization3DPlot::on_magneticCoordinatSystem_clicked()
{
    coordinateSystemX->setVisible(!coordinateSystemX->isVisible());
    coordinateSystemY->setVisible(!coordinateSystemY->isVisible());
    coordinateSystemZ->setVisible(!coordinateSystemZ->isVisible());
}


void visualization3DPlot::on_NVaxis_clicked(bool checked)
{
    NVAxis1->setVisible(checked);
    NVAxis2->setVisible(checked);
    NVAxis3->setVisible(checked);
    NVAxis4->setVisible(checked);

    ui->NVaxis1->setChecked(checked);
    ui->NVaxis2->setChecked(checked);
    ui->NVaxis3->setChecked(checked);
    ui->NVaxis4->setChecked(checked);
}

void visualization3DPlot::on_NVaxis1_clicked(bool checked)
{
    NVAxis1->setVisible(checked);
    if(ui->NVaxis2->isChecked() && ui->NVaxis3->isChecked() && ui->NVaxis4->isChecked()) ui->NVaxis->setChecked(checked);
}

void visualization3DPlot::on_NVaxis2_clicked(bool checked)
{
    NVAxis2->setVisible(checked);
    if(ui->NVaxis1->isChecked() && ui->NVaxis3->isChecked() && ui->NVaxis4->isChecked()) ui->NVaxis->setChecked(checked);
}

void visualization3DPlot::on_NVaxis3_clicked(bool checked)
{
    NVAxis3->setVisible(checked);
    if(ui->NVaxis1->isChecked() && ui->NVaxis2->isChecked() && ui->NVaxis4->isChecked()) ui->NVaxis->setChecked(checked);
}

void visualization3DPlot::on_NVaxis4_clicked(bool checked)
{
    NVAxis4->setVisible(checked);
    if(ui->NVaxis1->isChecked() && ui->NVaxis2->isChecked() && ui->NVaxis3->isChecked()) ui->NVaxis->setChecked(checked);
}
