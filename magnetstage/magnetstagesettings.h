#ifndef MAGNETSTAGESETTINGS_H
#define MAGNETSTAGESETTINGS_H

#include <QWidget>

namespace Ui {
class magnetstagesettings;
}

class magnetstagesettings : public QWidget
{
    Q_OBJECT

public:
    explicit magnetstagesettings(QWidget *parent = nullptr);
    ~magnetstagesettings();

    void updateUiStageInformationPosition(double posSmallRotStage, double posBigRotStage, double posTransStage);
    void updateUiStageInformationTemperature(double tempSmallRotStage, double tempBigRotStage, double tempTransStage);
    void updateUiStageInformationVoltage(double voltSmallRotStage, double voltBigRotStage, double voltTransStage);
    void updateUiStageInformationCurrent(double curSmallRotStage, double curBigRotStage, double curTransStage);

    bool getMusicState();
    int getZeroPointState();
    void setZeroPointStateBack();

private slots:
    void on_changeZeroPoints_clicked();

    void on_musicCheckBox_clicked(bool checked);

private:
    Ui::magnetstagesettings *ui;

    bool musicOnOff = false;
    //Note 0: nothing changed, 1: big motor, 2: small motor, 3: both motors
    int zeroPoint = 0;


};

#endif // MAGNETSTAGESETTINGS_H
