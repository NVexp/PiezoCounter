#include "visualization2dplot.h"
#include "ui_visualization2dplot.h"

#include <iostream>
#include <QtMath>
#include "qcustomplot.h"

visualization2dplot::visualization2dplot(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::visualization2dplot)
{
    ui->setupUi(this);

    QWidget::setWindowTitle("2D plot");

    // named the axis
    ui->positionSmallRotStage->xAxis->setLabel("x in [mm]");
    ui->positionSmallRotStage->yAxis->setLabel("y in [mm]");
    // named the axis
    ui->positionBigRotStage->xAxis->setLabel("x in [mm]");
    ui->positionBigRotStage->yAxis->setLabel("y in [mm]");
}

visualization2dplot::~visualization2dplot()
{
    delete ui;
}

void visualization2dplot::smallRotStagePlot(double posSmallRotStage, double posTransStage)
{
    //delete the previous data and replace it with new data
    ui->positionSmallRotStage->clearGraphs();
    positionTransStage = posTransStage;

    double pos_x = posTransStage * cos(qDegreesToRadians(posSmallRotStage));
    double pos_y = posTransStage * sin(qDegreesToRadians(posSmallRotStage));

    // autoscale
    if (autoscaleSmallRotStage){
        ui->positionSmallRotStage->xAxis->setRange(-55,55);
        ui->positionSmallRotStage->yAxis->setRange(-15,60);
    }else{
        ui->positionSmallRotStage->xAxis->setRange(-posTransStage-(0.1*posTransStage),posTransStage+(0.1*posTransStage));
        ui->positionSmallRotStage->yAxis->setRange(0.25*(-posTransStage-(0.2*posTransStage)),(posTransStage+(0.2*posTransStage)));
    }

    //generate the data (180/3=60)
    QVector<double> x(61), y(61),posx(61),posy(61),posz(61);

    for (int i=0;i<61;i++){
        //movement cyricle
        x[i] = posTransStage * cos(qDegreesToRadians(i*3.));
        y[i] = posTransStage * sin(qDegreesToRadians(i*3.));

        //position point
        posx[i] = pos_x + 0.04*(posTransStage * cos(qDegreesToRadians(i*3.)));
        posy[i] = pos_y + 0.04*(posTransStage * sin(qDegreesToRadians(i*3.)));
        posz[i] = pos_y - 0.04*(posTransStage * sin(qDegreesToRadians(i*3.)));
    }
    //graphs
    QCPGraph *movementSmallMotor = ui->positionSmallRotStage->addGraph();
    QCPGraph *movementSmallMotorPosUp = ui->positionSmallRotStage->addGraph();
    QCPGraph *movementSmallMotorPosDown = ui->positionSmallRotStage->addGraph();
    movementSmallMotor->setData(x,y);
    movementSmallMotorPosUp->setData(posx,posy);
    movementSmallMotorPosDown->setData(posx,posz);

    //color between 'movementSmallMotorPosDown' and 'movementSmallMotorPosUp' black
    movementSmallMotorPosDown->setBrush(Qt::black);
    movementSmallMotorPosDown->setChannelFillGraph(movementSmallMotorPosUp);

    ui->positionSmallRotStage->replot();
}

void visualization2dplot::bigRotStagePlot(double posBigRotStage, double posTransStage)
{
    //delete the previous data and replace it with new data
    ui->positionBigRotStage->clearGraphs();

    double pos_x = posTransStage * cos(qDegreesToRadians(posBigRotStage));
    double pos_y = posTransStage * sin(qDegreesToRadians(posBigRotStage));

    // autoscale
    if (autoscaleSmallRotStage){
        ui->positionBigRotStage->xAxis->setRange(-55,55);
        ui->positionBigRotStage->yAxis->setRange(-15,60);
    }else{
        ui->positionBigRotStage->xAxis->setRange(-posTransStage-(0.1*posTransStage),posTransStage+(0.1*posTransStage));
        ui->positionBigRotStage->yAxis->setRange(0.25*(-posTransStage-(0.2*posTransStage)),(posTransStage+(0.2*posTransStage)));
    }

    //generate the data (180/3=60)
    QVector<double> x(61), y(61),posx(61),posy(61),posz(61);

    for (int i=0;i<61;i++){
        //movement cyricle
        x[i] = posTransStage * cos(qDegreesToRadians(i*3.));
        y[i] = posTransStage * sin(qDegreesToRadians(i*3.));

        //position point
        posx[i] = pos_x + 0.04*(posTransStage * cos(qDegreesToRadians(i*3.)));
        posy[i] = pos_y + 0.04*(posTransStage * sin(qDegreesToRadians(i*3.)));
        posz[i] = pos_y - 0.04*(posTransStage * sin(qDegreesToRadians(i*3.)));
    }
    //graphs
    QCPGraph *movementBigMotor = ui->positionBigRotStage->addGraph();
    QCPGraph *movementBigMotorPosUp = ui->positionBigRotStage->addGraph();
    QCPGraph *movementBigMotorPosDown = ui->positionBigRotStage->addGraph();
    movementBigMotor->setData(x,y);
    movementBigMotorPosUp->setData(posx,posy);
    movementBigMotorPosDown->setData(posx,posz);

    //color between 'movementSmallMotorPosDown' and 'movementSmallMotorPosUp' black
    movementBigMotorPosDown->setBrush(Qt::black);
    movementBigMotorPosDown->setChannelFillGraph(movementBigMotorPosUp);

    ui->positionBigRotStage->replot();
}

void visualization2dplot::on_autoscaleSmallRotStage_clicked(bool autoscaleState)
{
    autoscaleSmallRotStage = autoscaleState;

    // autoscale
    if (autoscaleSmallRotStage){
        ui->positionSmallRotStage->xAxis->setRange(-55,55);
        ui->positionSmallRotStage->yAxis->setRange(-15,60);
    }else{
        ui->positionSmallRotStage->xAxis->setRange(-positionTransStage-(0.1*positionTransStage),positionTransStage+(0.1*positionTransStage));
        ui->positionSmallRotStage->yAxis->setRange(0.25*(-positionTransStage-(0.2*positionTransStage)),(positionTransStage+(0.2*positionTransStage)));
    }
    ui->positionSmallRotStage->replot();
}

void visualization2dplot::on_autoscaleBigRotStage_clicked(bool autoscaleState)
{
    autoscaleBigRotStage = autoscaleState;

    // autoscale
    if (autoscaleBigRotStage){
        ui->positionBigRotStage->xAxis->setRange(-55,55);
        ui->positionBigRotStage->yAxis->setRange(-15,60);
    }else{
        ui->positionBigRotStage->xAxis->setRange(-positionTransStage-(0.1*positionTransStage),positionTransStage+(0.1*positionTransStage));
        ui->positionBigRotStage->yAxis->setRange(0.25*(-positionTransStage-(0.2*positionTransStage)),(positionTransStage+(0.2*positionTransStage)));
    }
    ui->positionBigRotStage->replot();
}
