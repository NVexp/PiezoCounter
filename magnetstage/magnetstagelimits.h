#ifndef MAGNETSTAGELIMITS_H
#define MAGNETSTAGELIMITS_H

#include <QWidget>
#include <QtDataVisualization/Q3DSurface>

namespace Ui {
class magnetstagelimits;
}

class magnetstagelimits : public QWidget
{
    Q_OBJECT

public:
    explicit magnetstagelimits(QWidget *parent = nullptr);
    ~magnetstagelimits();


private slots:
    void on_checkPossibleDistance_clicked();

private:
    Ui::magnetstagelimits *ui;

    QtDataVisualization::Q3DSurface *magnetstageLimitsPlot;

    //QVector<double> smallRotStage, transStage, bigRotStage;

    // fitted function:
    // f(x,z) = a*(x-b)^2+c*(z-d)^2+e = y
    double a = 0.0160961;
    double b = -0.00063412;
    double c = 0.00887396;
    double d = -14.2215;
    double e = -17.0023;

};

#endif // MAGNETSTAGELIMITS_H
