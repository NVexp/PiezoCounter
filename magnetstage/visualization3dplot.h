#ifndef VISUALIZATION3DPLOT_H
#define VISUALIZATION3DPLOT_H

#include <QWidget>

#include <QtDataVisualization/Q3DSurface>
#include <QtDataVisualization/QCustom3DItem>

#include <QTimer>

namespace Ui {
class visualization3DPlot;
}

class visualization3DPlot : public QWidget
{
    Q_OBJECT

public:
    explicit visualization3DPlot(QWidget *parent = nullptr);
    ~visualization3DPlot();

    Ui::visualization3DPlot *ui;

public slots:
    void Magnetic3DPlot(double move2posTransStage_mm, double move2posRotStageSmall_deg, double move2posRotStageBig_deg);


private slots:
    void on_magneticCoordinatSystem_clicked();
    void on_magneticSphere_clicked();
    void on_magneticFieldArrow_clicked();

    void on_NVaxis_clicked(bool checked);
    void on_NVaxis1_clicked(bool checked);
    void on_NVaxis2_clicked(bool checked);
    void on_NVaxis3_clicked(bool checked);
    void on_NVaxis4_clicked(bool checked);

private:

    //3D plot variables
    QtDataVisualization::Q3DSurface *plot3DGraph;
    QtDataVisualization::QCustom3DItem *magneticSphere;
    QtDataVisualization::QCustom3DItem *magneticFieldArrow;
    QtDataVisualization::QCustom3DItem *coordinateSystemX;
    QtDataVisualization::QCustom3DItem *coordinateSystemY;
    QtDataVisualization::QCustom3DItem *coordinateSystemZ;

    //NV-axis variables
    // NV1 and NV2 form an plane with a normal vector parallel to the diamond surface
    QtDataVisualization::QCustom3DItem *NVAxis1; // 111
    QtDataVisualization::QCustom3DItem *NVAxis2; // 010
    // NV1 and NV2 form an plane with a normal vector parallel to the diamond surface
    QtDataVisualization::QCustom3DItem *NVAxis3; // 001
    QtDataVisualization::QCustom3DItem *NVAxis4; // 100

    double posX;
    double posY;
    double posZ;

    //3D border plot variables
    QtDataVisualization::QSurface3DSeries *borderSeries;

    // fitted function for the borders:
    // f(x,z) = a*(x-b)^2+c*(z-d)^2+e = y
    double a = 0.0160961;
    double b = -0.00063412;
    double c = 0.00887396;
    double d = -14.2215;
    double e = -17.0023;

};

#endif // VISUALIZATION3DPLOT_H
