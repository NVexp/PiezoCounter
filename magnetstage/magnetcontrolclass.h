#ifndef MAGNETCONTROLCLASS_H
#define MAGNETCONTROLCLASS_H

#include "ximc.h"
#include <QObject>
#include <QTimer>
#include <QMediaPlayer>

#include "magnetstage/magnetstagesettings.h"
#include "magnetstage/visualization2dplot.h"
#include "magnetstage/magnetstagelimits.h"


class magnetcontrolclass : public QObject
{
    Q_OBJECT
public:
    magnetcontrolclass(QObject *parent = nullptr);
    ~magnetcontrolclass();
    void connect2motors();
    void disconnect2motors();
    bool getStateMotors();

    //get the current positions of the motors
    double getPositionRotStageBig();
    double getPositionRotStageSmall();
    double getPositionTransStage();

    //move functions
    void setSimultanMovement(bool simultanMovement);
    void stopMotors();

    bool checkMagnetstageLimits(double move2posTransStage_mm, double move2posRotStageSmall_deg, double move2posRotStageBig_deg);

    // additional windows
    void openSettings();
    void open2DVisualization();
    void update2DVisualization();
    void openBorders();

    // move state: true --> motor moves, false --> motor don't move
    bool moveState = false;

    // - 2D Plot
    visualization2dplot *visualization2dim;

signals:
    void reachPosition(bool state);

public slots:
    void moveMagnetStage(double move2posTransStage_mm, double move2posRotStageSmall_deg, double move2posRotStageBig_deg);

private slots:
    void updateSettingsWindow();


private:
    result_t result;
    device_enumeration_t devenum;

    device_t rotStageBig;
    device_t rotStageSmall;
    device_t transStage;

    //need for current voltage and current
    engine_settings_t engine_settings_rotStageBig;
    engine_settings_t engine_settings_rotStageSmall;
    engine_settings_t engine_settings_transStage;

    //need for current position and temperature
    status_t status_rotStageBig;
    status_t status_rotStageSmall;
    status_t status_transStage;

    //names of the motors
    char name_rotStageBig[256];
    char name_rotStageSmall[256];
    char name_transStage[256];

    //saves the current state of the motors
    bool openMotors = false;

    //move parameters
    double move2posRotStageBig_deg;
    double move2posRotStageSmall_deg;
    double move2posTransStage_mm;
    bool small2big = true;
    bool simultanMovement = false;

    // window pointers + parameters
    // - settings
    magnetstagesettings *settings;
    QTimer *settingWindowTimer;

    // - border window
    magnetstagelimits *limitsStage;

    //music player
    QMediaPlayer *ElevatorMusic;

};

#endif // MAGNETCONTROLCLASS_H
