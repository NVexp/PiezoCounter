
#include "loadfilecreatmosaic.h"
#include <iostream>
#include <algorithm>
#include <fstream>
#include <chrono>
#include <ctime>
#include <iomanip>
#include <QMessageBox>
#include <QTimer>
#include <utility>
#include <filesystem>
#include <direct.h>
#include <QFile>
#include <QDir>

#include "qcustomplotwithroi.h"


LoadFileCreatMosaic::LoadFileCreatMosaic(QCustomPlotWithROI* plot, QCustomPlotWithROI* image3D):
    image2DPlot(plot),
    image3DPlot(image3D)
{

}

LoadFileCreatMosaic::LoadFileCreatMosaic()
{

}

void LoadFileCreatMosaic::readDataFromDatFile(QString fileName)
{
    currentDataFromFile.kcountsPerSec.clear();

    QFile file(fileName);
    QFileInfo getFilenName_var(fileName);

    //set filname and path for saving the loaded file with the belonging name
    currentDataFromFile.fileNameLoaded2D = getFilenName_var.baseName().toStdString();
    currentDataFromFile.PathLoaded2D = getFilenName_var.absolutePath().toStdString();

//    currentDataFromFile.PathLoadedFile = ((getFilenName_var.fileName()).section(".",0,0)).toStdString();

    if(file.open(QIODevice::ReadOnly)){
        QTextStream DataFromFile(&file);
        int lineCounter = 1;
        QString line;
        while(!DataFromFile.atEnd()){
            line = DataFromFile.readLine();
            if(lineCounter==2)
            {
                QStringList splitLine = line.split("\t");
                currentDataFromFile.Xstep = splitLine[0].toInt();
                currentDataFromFile.Ystep = splitLine[1].toInt();
                currentDataFromFile.Npx_X = splitLine[2].toInt();
                currentDataFromFile.Npx_Y = splitLine[3].toInt();
                currentDataFromFile.XPos = splitLine[4].toInt();
                currentDataFromFile.YPos = splitLine[5].toInt();
                currentDataFromFile.ZPos = splitLine[6].toInt();
            }
            if(lineCounter>4){
                QStringList splitLine = line.split("\t");
                currentDataFromFile.kcountsPerSec.push_back(splitLine[2].toDouble()/1000.0);
            }

            ++lineCounter;
        }
        file.close();
    }
}

std::string LoadFileCreatMosaic::getFileNameOfLoaded2D(){
    return currentDataFromFile.fileNameLoaded2D;
}

std::string LoadFileCreatMosaic::getPathOfLoaded2D(){
    return currentDataFromFile.PathLoaded2D;
}

std::string LoadFileCreatMosaic::getFileNameOfLoaded3D(){
    return currentDataFromFile.fileNameLoaded3D;
}

std::string LoadFileCreatMosaic::getPathOfLoaded3D(){
    return currentDataFromFile.PathLoaded3D;
}


void LoadFileCreatMosaic::loadDataFromSingleScan(std::string fileName){
    readDataFromDatFile(QString::fromStdString(fileName));
    //QCPRange XRange(0, (currentDataFromFile.Npx_X-1)*currentDataFromFile.Xstep/1000.);
    //QCPRange YRange(0, (currentDataFromFile.Npx_Y-1)*currentDataFromFile.Ystep/1000.);
    QCPRange XRange((currentDataFromFile.XPos-((currentDataFromFile.Npx_X/2.)*currentDataFromFile.Xstep))/1000., (currentDataFromFile.XPos+((currentDataFromFile.Npx_X/2.)*currentDataFromFile.Xstep))/1000.);
    QCPRange YRange((currentDataFromFile.YPos-((currentDataFromFile.Npx_Y/2.)*currentDataFromFile.Ystep))/1000., (currentDataFromFile.YPos+((currentDataFromFile.Npx_Y/2.)*currentDataFromFile.Ystep))/1000.);
    //parentWindow->QCPimageWROI->colorMap->data()->setRange(XRange, YRange);
    image2DPlot->colorMap->data()->setRange(XRange, YRange);
    image2DPlot->colorMap->data()->setSize(currentDataFromFile.Npx_X, currentDataFromFile.Npx_Y);

    int pxIndex = 0;
    for(int Yind = 0; Yind<currentDataFromFile.Npx_Y; ++Yind)
    {
        for(int Xind = (Yind%2==0? 0: currentDataFromFile.Npx_X-1); Xind>=0 && Xind<currentDataFromFile.Npx_X;  Yind%2==0? ++Xind: --Xind)
        {
            image2DPlot->colorMap->data()->setCell(Xind, Yind, currentDataFromFile.kcountsPerSec[pxIndex]);
            ++pxIndex;
        }
    }
    image2DPlot->colorMap->rescaleDataRange();
    image2DPlot->replot();
}


void LoadFileCreatMosaic::loadDataFromZScan(std::string folderName){

    QDir ZScan(QString::fromStdString(folderName));

    //set filname and path for saving the loaded files
    currentDataFromFile.fileNameLoaded3D = ZScan.dirName().toStdString();
    currentDataFromFile.PathLoaded3D = folderName;

    //set filter so that just files of the type .dat are loaded
    QStringList filter;
    filter << "*.dat";
    ZScan.setNameFilters(filter);

    //counts the number of files with the ending .dat
    int numberPictures = ZScan.count();

    //calculate number of pictures per row and line
    int numPicRow = ceil(sqrt(numberPictures));

    //create a list with this elements for calling them later
    QStringList ZScanList = ZScan.entryList(filter);


    for(int picture_numb = 0; picture_numb < numberPictures; picture_numb++){
        QString currentFilename = QString::fromStdString(folderName)+"/"+ZScanList.at(picture_numb);
        readDataFromDatFile3D(currentFilename);
    }

    QCPRange XRange(0, (numPicRow*currentDataFromFile.Npx_X+((numPicRow-1)*5)-1)*currentDataFromFile.Xstep/1000.);
    QCPRange YRange(0, (numPicRow*currentDataFromFile.Npx_Y+((numPicRow-1)*5)-1)*currentDataFromFile.Ystep/1000.);
    //parentWindow->QCPimageWROI->colorMap->data()->setRange(XRange, YRange);
    image2DPlot->colorMap->data()->setRange(XRange, YRange);
    image3DPlot->colorMap->data()->setSize((numPicRow*currentDataFromFile.Npx_X+((numPicRow-1)*5))+10, ((numPicRow*currentDataFromFile.Npx_Y+((numPicRow-1)*5)))+10);

    //create an array thats fits all zScan pictures and fill it with zeros
    std::vector< std::vector<double> > dataToPlot(currentDataFromFile.Npx_X*numPicRow+(numPicRow-1)*5+10,std::vector<double>(numPicRow*currentDataFromFile.Npx_Y+(numPicRow-1)*5+10,0));

    //calculate position of all pictures and fill the data in the above created array. 5 pixels between the picutres and 5 additonal pixels at the edges
    int startPointX = -(currentDataFromFile.Npx_Y+5)+5;
    int startPointY = (numPicRow-1)*currentDataFromFile.Npx_Y+(numPicRow-1)*5+5;
    int XPoint=0;
    int YPoint=0;
    for(int i=0; i<currentPlotValues.size(); ++i){
        if((i%numPicRow)==0 && (i!=0)){
            startPointY = startPointY-(currentDataFromFile.Npx_Y+5);
            startPointX = 5;
        }else{
            startPointX = startPointX+currentDataFromFile.Npx_X+5;
        }
        XPoint=startPointX;
        YPoint=startPointY-1;
        for(int j=0; j<currentPlotValues[i].size(); ++j){
            if((j%currentDataFromFile.Npx_X)==0){
                XPoint=startPointX;
                ++YPoint;
            }else{
                ++XPoint;
            }
            dataToPlot[XPoint][YPoint]=currentPlotValues[i][j];
        }
    }

    //plot data
    for(int Yind = 0; Yind<(int)dataToPlot.size(); ++Yind)
    {
        for (int Xind = 0; Xind<(int)dataToPlot[Yind].size(); Xind++){
            image3DPlot->colorMap->data()->setCell(Xind, Yind, dataToPlot[Xind][Yind]);
        }
    }

    image3DPlot->colorMap->rescaleDataRange();
    image3DPlot->replot();
    currentPlotValues.clear();
}



//functions for loading data from dat files
void LoadFileCreatMosaic::readDataFromDatFile3D(QString filename){//, QString fileName2, QString fileName3){

    //Vectors in wich the readed data is saved und formed from Zig Zack to left->right
    QVector<double> currentDat;
    QVector<double> currentDatFormed;

    QFile file(filename);

//open .dat file and save the conts in currentData
    if(file.open(QIODevice::ReadOnly)){
        QTextStream DataFromFile(&file);
        int lineCounter = 1;
        QString line;
        while(!DataFromFile.atEnd()){
            line = DataFromFile.readLine();
            if(lineCounter==2){
                QStringList splitLine = line.split("\t");
                currentDataFromFile.Xstep = splitLine[0].toInt();
                currentDataFromFile.Ystep = splitLine[1].toInt();
                currentDataFromFile.Npx_X = splitLine[2].toInt();
                currentDataFromFile.Npx_Y = splitLine[3].toInt();
                currentDataFromFile.XPos = splitLine[4].toInt();
                currentDataFromFile.YPos = splitLine[5].toInt();
                currentDataFromFile.ZPos = splitLine[6].toInt();
            }
            if(lineCounter>4){
                QStringList splitLine = line.split("\t");
                currentDat.push_back(splitLine[2].toDouble()/1000.0);
            }
            ++lineCounter;
        }
        file.close();
    }

    //the readed Data has a zigzag form here they are formed to a left->right form
    for(int Yind = 0; Yind<currentDataFromFile.Npx_Y; ++Yind)
    {
        for(int Xind = (Yind%2==0? 0: currentDataFromFile.Npx_X-1); Xind>=0 && Xind<currentDataFromFile.Npx_X;  Yind%2==0? ++Xind: --Xind){
            currentDatFormed.push_back(currentDat[(Yind*currentDataFromFile.Npx_X)+Xind]);
        }
    }

    //the vector is added as a new column in currentPlotValues
    currentPlotValues.push_back(currentDatFormed);
    currentDatFormed.clear();
    currentDat.clear();
}


