#include <LJM_Utilities.h>
#include <LabJackM.h>

#include "labjackt7.h"


LabJackT7::LabJackT7()
{
    //Open T7 device (arg1 = 7 <-> T7 device), by TCP (arg2 = 2 <-> TCP protocol)
    LJError = LJM_Open(7, 2, "141.51.198.19", &T7Handle);
    ErrorCheck(LJError, "Error on LJM_Open");
}

LabJackT7::~LabJackT7()
{
    //Close LabJack T7 connection
    LJError = LJM_Close(T7Handle);
    ErrorCheck(LJError, "LJM_Close");
}

double LabJackT7::analogReadOut(const char* channel)
{
    //read the current value from the analog channel
    double read_value = 0;
    LJError = LJM_eReadName(T7Handle, channel, &read_value);
    ErrorCheck(LJError, "Error on ReadValue");

    //tell the LCD display widget that a new value is ready
    emit(valueRead(read_value));
    
    return read_value;
}

//pulseLen in microseconds
void LabJackT7::pulseSetupAndEnable(int pulseNb, int pulseLen)
{
    //make sure DIO port is low and clock is diable
    const char* nameDIO = "FIO0";
    LJError = LJM_eWriteName(T7Handle, nameDIO, 0);
    const char* nameCL = "DIO_EF_CLOCK0_ENABLE";
    LJError = LJM_eWriteName(T7Handle, nameCL, 0);
    ErrorCheck(LJError, "Error on CLOCK0_ENABLE");
    //configure clock and DIO_EF (=Extended Features)
    const char* arNamesConfig[9] =
    {
        "DIO0_EF_INDEX",
        "DIO0_EF_OPTIONS",
        "DIO0_EF_CONFIG_A",
        "DIO0_EF_CONFIG_B",
        "DIO0_EF_CONFIG_C",
        "DIO_EF_CLOCK0_DIVISOR",
        "DIO_EF_CLOCK0_ROLL_VALUE",
        "DIO_EF_CLOCK0_ENABLE",
        "DIO0_EF_ENABLE"
    };

    double arValuesConfig[9] =
    {
        2,   //INDEX
        0,   //OPTIONS --> Clock Id
        pulseLen*10., //CONFIG_A -> Hi to Lo point
        0,   //CONFIG_B -> Lo to Hi point
        (double)pulseNb,   //CONFIG_C -> Number of pulses
        8,   //DIVISOR
        pulseLen*20.,//ROLL_VALUE -> nb of clock cycles during which 1 pulse is output
        1,   //CLOCK#_ENABLE
        1    //DIO#_ENABLE
    };

    int err_addr = 0;
    LJError = LJM_eWriteNames(T7Handle, 9, arNamesConfig, arValuesConfig, &err_addr);
    ErrorCheck(LJError, "Error on eWriteNames for PulseOut");
    ErrorCheckWithAddress(LJError, err_addr, "LJM_eWriteNames - arNamesConfig");
    //assuming 80MHz core clock, clock is f=10MHz<->T=100ns so pulse should be 500*T=100µs long
}

void LabJackT7::pulseDisable()
{
    int err_addr = 0;
    const char* arNamesDisable[2] = { "DIO_EF_CLOCK0_ENABLE", "DIO0_EF_ENABLE"};
    double arValuesDisable[2] = {0,0};
    LJError = LJM_eWriteNames(T7Handle, 2, arNamesDisable, arValuesDisable, &err_addr);
    ErrorCheckWithAddress(LJError, err_addr, "LJM_eWriteNames - arNamesDisable");
}
