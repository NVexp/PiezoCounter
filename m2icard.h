#ifndef M2ICARD_H
#define M2ICARD_H

#include <vector>
#include <filesystem>
#include <iostream>
#include <string>

#include "QCPwithROI/qcpsequencedata.h"


// ----- include standard driver header from library -----
#include "c_header/dlltyp.h"
#include "c_header/regs.h"
#include "c_header/spcerr.h"
#include "c_header/spcm_drv.h"

// ----- include of common example librarys -----
#include "common/spcm_lib_card.h"
#include "common/spcm_lib_data.h"

// ----- operating system dependent functions for thread, event, keyboard and mutex handling -----
#include "common/ostools/spcm_oswrap.h"
#include "common/ostools/spcm_ostools.h"


#define PERMLVL_ALL_ZEROS 0
#define PERMLVL_ALL_ONES 1
#define PERMLVL_INDIV 2
#define PERMLVL_HIGH_Z 3

//returns the next (integer) power of 2 after the parameter i.e. log2(x)+1 if x!=2^n, otherwise n
int next_power_of_2(int A);

typedef std::vector< std::vector<int> > multichanEdgeData;

struct uint_triplet{
    unsigned int el1;
    unsigned int el2;
    unsigned int el3;
};
typedef uint_triplet uint_triplet;

class M2iCard
{
public:

    char                szBuffer[1024];     // a character buffer for any messages
    ST_SPCM_CARDINFO    stCard;             // info structure of my card
    uint64              qwMemInBytes;
    void*               pvBuffer;

    M2iCard();
    ~M2iCard();

    //Pulsed Tap
    void M2iInit();
    bool M2iSetup_StdReplay(int64 chanMask, int64 clockSpeed, bool clockOut, bool triggerOut, long long nbLoops,  unsigned int memSize);
    bool M2iSetup_SeqReplay(uint64 chanMask, int64 clockSpeed, uint32 maxTime, uint32 paramNb, bool clockOut, bool triggerOut);

    //void* generateSeqData(std::vector<void*> edges);
    bool M2iLoadIntoMemoryFromGui(int chanNumber);
    bool M2iLoadIntoMemoryFromVec(int chanNumber, std::vector< std::vector<int> > rising, std::vector< std::vector<int> > falling);
    bool M2iOutputData();
    bool M2iSetConstLvlMode(int perm_mode);
    bool M2iSetConstLvl(int actChanNb, uint16_t lvls);
    void M2iClose();
    void translateTimestampsToParallelAndPutInBuffer(int seqLength, int chanNb, std::vector< std::vector<int> >& timestamps_rise, std::vector< std::vector<int> >& timestamps_fall, int16_t* pBuffer);
    bool writeSeqToFile(int nbActChan, int length, double tStep_in_ns, const std::vector< std::vector<edgeProp> >& risingE_Vec, const std::vector< std::vector<edgeProp> >& fallingE_Vec, const std::string& path, const std::string& name);
    bool loadSeqFromFile(std::filesystem::path filePath, std::string filename, std::vector< std::vector<edgeProp> >& risingE_outVec, std::vector< std::vector<edgeProp> >& fallingE_outVec);
    bool M2iExpSequenceReplay( int chanMask,
                               int64 clockSpeed,
                               const std::vector<uint32>& runLengths,
                               int loops,
                               std::vector< multichanEdgeData >& risEdg,
                               std::vector< multichanEdgeData >& fallEdg,
                               bool clockOut,
                               bool triggerOut
                               );


    //void M2iSequencerTestDataPrepare(int index, multichanEdgeData& risingE, multichanEdgeData& fallingE);
    //void M2iSequencerTestFunc(int N);


};

#endif // M2ICARD_H
